-----
SETUP
-----

It's required that you get PHPRtfLite to be able to use the download essay feature. You can get the api from here https://github.com/phprtflite/PHPRtfLite.
Once you download PHPRtfLite copy the 'lib' folder into your libraries directory.

Enable all modules that are required as well as the Drafting Board module and give all roles the proper permissions to the modules.
Versions of modules used:
    - Services version 6.x-2.4
    - Organic groups version 6.x-2.1

On the service settings page set the 'Authentication Module' to 'Key authentication'.

Setup a service key with domain set to 'am' and give access to:
    icam.get_issue_data
    icam.rebuttal_data
    icam.init
    icam.issue_analyzer_save
    icam.claim_creator_save
    icam.introduction_introducer_save
    icam.paragraph_constructor_save
    icam.critic_crusher_save
    icam.conclusion_crafter_save
    icam.set_quick_save
    icam.get_quick_save
    icam.get_class_votes
    icam.set_student_vote
    system.connect

Create a 'Drafting Board' node.  For the 'Zip file upload' use a zipped file with the drafting board content.
Example of what is needed in the zip file:
	- zip file name must be 'arg_mod_upload'
    - folder named 'images' to hold the images referenced in the csv files
	- folder named 'audio' to hold the audio files referenced in the csv files
	- folder named 'extra' to hold the Glossary.txt file
    - csv file named 'Issue Analyzer.csv'
    - csv file named 'Evidence.csv'
    - csv file named 'Story Chunks.csv'
    - csv file named 'Claims.csv'
    - csv file named 'Hooks & Bridges.csv'
    - csv file named 'Clinchers.csv'
    - csv file named 'Paragraph Constructor.csv'
    - csv file named 'Critic Crusher 1.csv'
	- csv file named 'Critic Crusher 2.csv'
	- csv file named 'Conclusion Starters.csv'
	- csv file named 'Conclusion Reasons.csv'
View files in the examples folder to get the templates.

Format the Glossary.txt with three lines per glossary entry, first line is comma-separated list of words 
that are being defined defined, (election, elections, ...) second line is the heading of the glossary entry 
and the third line is the body of the glossary entry. The third line can have HTML in it, so long as 
there's no linebreaks.  Drafting board uses the entries in the order that they show up in the glossary.


---------------
EXTRA LRMI DATA
---------------

To get extra lrmi data tracked you will need to have cck module enabled.  Add these any of these fields to drafting board content type:
    resource_image
    lrmi_keywords
    lrmi_author
    lrmi_publisher
    lrmi_educational_use
    lrmi_interactivity_type
    lrmi_learning_resource
    lrmi_time_required
    lrmi_typical_age_range


-------
PLAYING
-------

Make sure the role you want to be able to use the drafting board tool has the proper permissions.

Navigate to the drafting board node with an account that has access to it to start playing.


---------
REPORTING
---------

With og groups the admin of the group can view all members progress through a drafting board topic by visiting 'node/[node id]/report' using the drafting board's node id.