Drupal.behaviors.draftingboardReport = function(context) {
    $('.progress-bar', context).each(function() {
        if ($(this).hasClass('in-progress')) {
            var active_item_position = $(this).children('.active').position();
            var progress_bar_position = 990 - Math.round(active_item_position.left);
            $(this).css('background-position', '-' + progress_bar_position + 'px top');
        }
    });
    
    $('#class-selector', context).change(function (event) {
      if (!window.location.origin)
        window.location.origin = window.location.protocol+"//"+window.location.host;
      
      //reload page with new class set in url
      var baseUrl = window.location.origin;
      var origUrl = window.location.href;
      var pathname = origUrl.replace(baseUrl, '');
      var pathSplit = pathname.split('/');
      
      for (var i = 0; i < pathSplit.length; i++) {
        if (pathSplit[i] === 'report') {
          pathSplit[i + 1] = $(this).val();
        }
      }
      
      var newClassUrl = baseUrl + pathSplit.join('/');
      window.location.replace(newClassUrl);
    });
};