package org.icivics.arg.ui.claimCreator.client;

import org.icivics.arg.data.model.client.ReasonInstance;
import org.icivics.arg.ui.common.client.AudioControl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceLinkerListItem extends Composite {
	private static EvidenceLinkerListItemUiBinder uiBinder = GWT.create(EvidenceLinkerListItemUiBinder.class);
	interface EvidenceLinkerListItemUiBinder extends UiBinder<Widget, EvidenceLinkerListItem> {}
	
	@UiField
	protected ParagraphElement reasonText;
	@UiField
	protected AudioControl audioControl;
	
	@UiField
	protected EvidenceLinkItem item1;
	@UiField
	protected EvidenceLinkItem item2;
	
	protected EvidenceLinkItem[] items = new EvidenceLinkItem[2];

	public EvidenceLinkerListItem() {
		initWidget(uiBinder.createAndBindUi(this));
		items[0] = item1;
		items[1] = item2;
	}
	
	public void init(ReasonInstance reason, String audioURL) {
		reasonText.setInnerText(reason.reason.text);
		audioControl.loadSound(audioURL,audioURL);
	}
}
