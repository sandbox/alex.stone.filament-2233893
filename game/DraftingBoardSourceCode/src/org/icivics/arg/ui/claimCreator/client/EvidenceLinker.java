package org.icivics.arg.ui.claimCreator.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.ConfirmPopup;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceListPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceLinker extends Subpanel<ClaimCreator> {
	private static EvidenceLinkerUiBinder uiBinder = GWT.create(EvidenceLinkerUiBinder.class);
	interface EvidenceLinkerUiBinder extends UiBinder<Widget, EvidenceLinker> {}
	
	@UiField
	ParagraphElement titleText;
	@UiField
	DivElement reasonListHolder;
	
	SimplePanel popupHolder;
	
	@UiField
	LabelButton submitButton, backButton;
	
	@UiField
	ParagraphElement claimHolder;
	
	@UiField
	AudioConcatenatedControl claimAudio;
	
	
	//read-only
	public boolean isCounterClaim;
	
	
	
	private EvidenceLinkerListItem[] items = new EvidenceLinkerListItem[3];
	private ArrayList<Item> mUsedItems = new ArrayList<Item>();
	private boolean[] mCorrect = {true, true, true, true, true, true};
	
	private EvidenceListPanel mEvidencePanel;
	private EvidenceDetailPanel mItemPanel;
	protected TextPopup mPopup;
		
	public EvidenceLinker(ClaimCreator parent, boolean isCounterClaim) {
		super(parent);

		this.isCounterClaim = isCounterClaim;
		initWidget(uiBinder.createAndBindUi(this));
		mEvidencePanel = new EvidenceListPanel(isCounterClaim ? DataController.getCurrentIssue().counterClaim.claim : DataController.getCurrentIssue().claim.claim);
		
		for (int i = 0; i < 3; ++i) {
			items[i] = new EvidenceLinkerListItem();
			getRoot().add(items[i], reasonListHolder);
		}
		
		//claimAudio.loadSound("./audio/youclaim.mp3", "youclaim");
		claimAudio.loadSound(DataController.getCurrentIssue().claim.claim.audioSrc, "claim");
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		getParentPanel().hud.setEvidenceHandler(new EvidenceSidebar.SelectItemHandler() {
			public void onSelectItem(Item item) {
				GWT.log("Item selected");
				useEvidenceItem(item);
			}
		});
		
		popupHolder = getParentPanel().mOverlay;
		
		ClaimInstance claim = DataController.getClaimInstance();
		
		for (int i = 0; i < 3; ++i) {
			items[i].init(claim.reasons[i], claim.reasons[i].reason.audioSrc);
		}
		
		claimHolder.setInnerText(claim.getClaimText());
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Back button clicked");
				backButton_onClick();
			}
		};
		
		getParentPanel().hud.init(DataController.getCurrentClaim());
		
	}
	
	private int countCorrect() {
		ClaimInstance claim = isCounterClaim ? DataController.getCurrentIssue().counterClaim : DataController.getCurrentIssue().claim;
		
		int numCorrect = 0;
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 2; ++j)
				if (checkCorrect(items[i].items[j].getItem().id, i, claim))
					numCorrect++;
		
		/*
		for (int i = 0; i < mCorrect.length; ++i)
			if (mCorrect[i])
				++numCorrect;
		*/
		return numCorrect;
	}
	
	private boolean checkCorrect(String itemID, int reasonIndex, ClaimInstance claim) {
		Item[] items = claim.reasons[reasonIndex].reason.items;
		for (int i = 0; i < items.length; ++i)
			if (itemID.equals(items[i].id))
				return true;
		return false;
	}
	
	private boolean checkCorrect(String itemID, int reasonIndex) {
		return checkCorrect(itemID, reasonIndex, isCounterClaim ? DataController.getCurrentIssue().counterClaim : DataController.getCurrentIssue().claim);
	}
	
	private boolean checkCorrect() {
		ClaimInstance claim = isCounterClaim ? DataController.getCurrentIssue().counterClaim : DataController.getCurrentIssue().claim;
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 2; ++j)
				if (!checkCorrect(items[i].items[j].getItem().id, i, claim))
					return false;
		return true;
	}
	
	private boolean checkComplete() {
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 2; ++j) {
				if (items[i].items[j].getItem() == null) {
					submitButton.setEnabled(false);
					return false;
				}
			}
		}
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick(event);
			}
		};
	
		submitButton.setEnabled(true);
		return true;
	}
	
	protected void feedbackPopup_onClose(TextPopup popup) {
		// Clear incorrect evidence
		ClaimInstance claim = isCounterClaim ? DataController.getCurrentIssue().counterClaim : DataController.getCurrentIssue().claim;
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 2; ++j)
				if (!checkCorrect(items[i].items[j].getItem().id, i, claim)){
					items[i].items[j].markIncorrect(claim.reasons[i].reason.mismatchText);
				}
		
		popup.clearHandler();
		if (mPopup == popup)
			mPopup = null;
	}
	
	protected void confirmEvidencePopup_onClose(ConfirmPopup popup){
		
	}
	
	protected void onItemPanelClose() {
		if (mItemPanel == null)
			return;
		
		popupHolder.remove(mItemPanel);
		mItemPanel.destroy();
		mItemPanel = null;
	}
	
	private void backButton_onClick(){
		GWT.log("Back button clicked, and called the right function");
		getParentPanel().close(this,true);
	}
	
	private void submitButton_onClick(ClickEvent e) {
		if (mEvidencePanel.isAttached() || mPopup != null || mItemPanel != null)
			return;
		
		if(getParentPanel().numCorrectLinks == -1)
			getParentPanel().numCorrectLinks = countCorrect();
		
		if (checkCorrect()) {
			submitButton.clickHandler = null;
			exit();
		} else {
			getParentPanel().hud.onItemPanelClose();
			
			getParentPanel().numLinkFailures += 6 - countCorrect();
			
			GWT.log("Submit incorrect");
			mPopup = new TextPopup();
			mPopup.setTitle("Incorrect");
			mPopup.setBody("One or more of the items you selected is incorrectly assigned to a reason.");
			mPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			popupHolder.add(mPopup);
		}
		
		GWT.log("Number of evidence items matched the first time: " + getParentPanel().numCorrectLinks + 
				" Number of failures so far: " + getParentPanel().numLinkFailures);
	}
	
	private void exit() {
		
		getParentPanel().close(this, false);
	}
	
	protected void markEmpties(Boolean mark){
		markEmpties(mark, null);
	}
	
	protected void markEmpties(Boolean mark, Item item){
		Boolean itemUsed = false;
		if(item != null)
			for(int i=0; i<3; i++)
				for(int j=0; j<2; j++)
					if(item == items[i].items[j].getItem())
						itemUsed = true;
		
		// Find the ones that need marking/unmarking
		for(int i=0; i<3; i++)
			for(int j=0; j<2; j++)
				if(mark && !itemUsed){
					if(items[i].items[j].getItem() == null && item != null)
						markAnEmpty(items[i].items[j],true);
				} else {
					markAnEmpty(items[i].items[j],false);
				}
	}
	
	private void markAnEmpty(EvidenceLinkItem itemSlot, Boolean mark){
		if(mark){
			itemSlot.addStyleName("add");
		}else{
			itemSlot.removeStyleName("add");
		}
	}
	
	private void useEvidenceItem(final Item item){
		
		if(item != null){
			
			markEmpties(true, item);
			for (int i = 0; i < 3; ++i) {
				final int reasonIndex = i;
				for (int j = 0; j < 2; ++j) {
					final int itemIndex = j;
					items[i].items[j].setHandler(new EvidenceLinkItem.Handler() {
						public void onView() {
							getParentPanel().hud.showItem(items[reasonIndex].items[itemIndex].getItem());
						}
						public void onAdd(){
						
							if(!mUsedItems.contains(item)){
								
								ConfirmPopup confirmPopup = new ConfirmPopup("This makes sense","This does not make sense");
								
								confirmPopup.setTitle("Does your evidence make sense?  ");
								
								confirmPopup.setBodyHTML("<p class=\"reason\"><strong>Your Reason: </strong>" + 
											DataController.getClaimInstance().reasons[reasonIndex].reason.text + "</p>"+
										    "<p class=\"evidence\"><strong>Your Evidence: </strong>" + item.summary + "</p>");
								
								confirmPopup.styleForEvidence();
								ArrayList<String> urls = new ArrayList<String>();
								urls.add(DataController.getIssueURL() + "yourreason.mp3");
								urls.add(DataController.getClaimInstance().reasons[reasonIndex].reason.audioSrc);
								urls.add(DataController.getIssueURL() + "yourevidence.mp3");
								
								urls.add(item.summaryAudioSrc);
								GWT.log("SUMMARY URL: " + item.summaryAudioSrc);
								confirmPopup.setAudio(urls,"reason" + reasonIndex + "item" + item.id + "part");
								confirmPopup.setHandler(new ConfirmPopup.Handler(){
									@Override
									public void onConfirm(ConfirmPopup popup) {
										items[reasonIndex].items[itemIndex].setItem(item);
										mUsedItems.add(item);
										checkComplete();
										GWT.log("Adding evidence " + item.id);
										markEmpties(false);
										getParentPanel().hud.onItemPanelClose();
										//TODO: mark evidence as used
										getParentPanel().hud.markItem(items[reasonIndex].items[itemIndex].getItem(), true);
									}
									@Override
									public void onCancel(ConfirmPopup popup) {
										GWT.log("Cancelled adding evidence");
									}
								});
								popupHolder.add(confirmPopup);
							} else {
								GWT.log("Evidence already used");
							}
						}
						public void onRemove(){
							getParentPanel().hud.onItemPanelClose();
							if(mUsedItems.remove(items[reasonIndex].items[itemIndex].getItem())){
								checkComplete();
								getParentPanel().hud.markItem(items[reasonIndex].items[itemIndex].getItem(), false);
								useEvidenceItem(null);
								items[reasonIndex].items[itemIndex].clearItem();
								GWT.log("Removed evidence");	
							}
							else
								GWT.log("Trying to remove evidence which was not used");
						}
					});
				}
			}
		}else{
			markEmpties(false);
			for (int i = 0; i < 3; ++i) {
				for (int j = 0; j < 2; ++j) {
					if(items[i].items[j].getItem() == null){
						items[i].items[j].setHandler(null);
					}
				}
			}
		}
		
	}
}
