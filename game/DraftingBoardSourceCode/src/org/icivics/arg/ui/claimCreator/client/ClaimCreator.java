package org.icivics.arg.ui.claimCreator.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.json.client.ClaimCreatorProgress;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.SimplePanel;


public class ClaimCreator extends ModuleUI<ClaimCreator> {
	protected int numCorrectLinks = -1;
	protected int numLinkFailures = 0;
	protected ClaimCreatorProgress userData;
	
	protected EvidenceSidebar hud;
	protected SimplePanel mOverlay;
	
	public ClaimCreator(EvidenceSidebar hud, SimplePanel overlay) {
		this.hud = hud;
		mOverlay = overlay;
		//hud.init(DataController.getCurrentClaim());
		hud.init();
	}
	
	protected void commitScore() {
		//6 = total number of items attached to used reasons
		double linkScore = 6.0 / (6.0 + numLinkFailures);
		DataController.getCurrentIssue().scoreEvidenceLinker = linkScore;
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
				
		userData = DataController.getModuleProgress().forClaimCreator();
		openUI(getSavedUI());
	}
	
	private Subpanel<ClaimCreator> getSavedUI() {
		int stepIndex = userData.getStepIndex();
		
		// Seems that it's safer to start at the beginning, to make sure the claim is set correctly
		// Otherwise you can end up in the justification screen without reasons chosen for the claim, for example
		//stepIndex = 0;
		
		DataController.moduleStepIndex = stepIndex;
		
		if (stepIndex > 2) {
			numCorrectLinks = userData.getCorrectLinks();
			numLinkFailures = userData.getLinkFailures();
		}
		
		switch (stepIndex) {
		/*
			case 0: 
				hud.init();
				return new VoteResults(this);
			*/
			case 0: 
				hud.init();
				return new PinningReasons(this, false);
			case 1: 
				hud.init();
				return new Justification(this);
			case 2: 
				hud.init(DataController.getCurrentClaim());
				return new EvidenceLinker(this, false);
			case 3: return null;
			default:
				DataController.moduleStepIndex = 0;
				return new PinningReasons(this, false);
		}
		
		//return getClass().getMethod("RunStep" + stepIndex, param1, param2, param3);
	}	
	
	void close(PinningReasons ui) {
		// Not needed in Critic Crusher
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		String[] reasonIDs = new String[claim.reasons.length];
		for (int i = 0; i < claim.reasons.length; ++i) {
			reasonIDs[i] = claim.reasons[i].reason.id;
		}
		userData.setClaimReasonIDs(reasonIDs);
		
		DataController.saveModuleProgress(1);
		hud.init();
		transitionTo(new Justification(this));
	}
	
	void close(Justification ui, boolean goingBack) {
		
		if(!goingBack){
			hud.onItemPanelClose();
			userData.setReasonExplanation(DataController.getCurrentIssue().reasonSelectExplanation);
			
			DataController.saveModuleProgress(2);
			
			hud.init(DataController.getCurrentClaim());
			transitionTo(new EvidenceLinker(this, false));
		} else {
			hud.init();
			DataController.saveModuleProgress(0);
			transitionTo(new PinningReasons(this, false));
		}
	}
	
	void close(EvidenceLinker ui, boolean goingBack) {
		userData.setCorrectLinks(numCorrectLinks);
		DataController.getCurrentIssue().claimCreatorFirstPass = numCorrectLinks;
		userData.setLinkFailures(numLinkFailures);
		DataController.getCurrentIssue().claimCreatorTotalFailures = numLinkFailures;
		
		GWT.log(goingBack ? "Backing up, beep beep beep" : "going forward, vrooom");
		
		if(!goingBack){
			hud.init();
			DataController.saveModuleProgress(3);
			finish();
		} else {
			DataController.saveModuleProgress(1);
			hud.init();
			transitionTo(new Justification(this));
		}
	}
}
