package org.icivics.arg.ui.claimCreator.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.data.model.client.ReasonInstance;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.BasicButton;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class PinningReasons extends Subpanel<ClaimCreator> {
	private static PinningReasonsUiBinder uiBinder = GWT.create(PinningReasonsUiBinder.class);
	interface PinningReasonsUiBinder extends UiBinder<Widget, PinningReasons> {}
	
	@UiField
	ParagraphElement titleText;
	@UiField
	ParagraphElement claimHolder;
	@UiField
	AudioControl claimAudio;
	
	@UiField
	DivElement background, directionsHeading;
	
	@UiField
	SimplePanel popupHolder;
	
	@UiField
	LabelButton submitButton;
	

	private HTMLPanel[] reasonItems;
	
	private BasicButton[] likeButtons;
	private BasicButton[] loveButtons;
	
	private Boolean[] reasonsLiked;
	private Boolean[] reasonsLoved;

	private TextPopup mPopup;
	
	private ClaimInstance getClaim() {
		return isCounterClaim ? DataController.getCurrentIssue().counterClaim : DataController.getCurrentIssue().claim;
	}
	
	//read-only
	public boolean isCounterClaim;

	public PinningReasons(ClaimCreator parent, boolean isCounterClaim) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		this.isCounterClaim = isCounterClaim;
		
		ClaimInstance claim = getClaim();
		int count = claim.claim.reasons.length;
		reasonItems = new HTMLPanel[count];
		loveButtons = new BasicButton[count];
		likeButtons = new BasicButton[count];
		reasonsLiked = new Boolean[count];
		reasonsLoved = new Boolean[count];
		for (int i = 0; i < count; i++) {
			reasonsLiked[i] = false;
			reasonsLoved[i] = false;
			
			Reason reason = claim.claim.reasons[i];
			
			reasonItems[i] = new HTMLPanel("");
			
			HTMLPanel reasonPanel = new HTMLPanel("");
			reasonPanel.addStyleName("reason");
			
			HTMLPanel buttonPanel = new HTMLPanel("");
			buttonPanel.addStyleName("buttons");
			
			reasonPanel.add(buttonPanel);
			
			loveButtons[i] = new BasicButton();
			loveButtons[i].addStyleName("favorite disabled");
			loveButtons[i].getElement().setInnerText("FAV");
			
			likeButtons[i] = new BasicButton();
			likeButtons[i].addStyleName("like");
			likeButtons[i].getElement().setInnerText("LIKE");
			
			buttonPanel.add(likeButtons[i]);
			buttonPanel.add(loveButtons[i]);
			
			AudioControl reasonAudio = new AudioControl();
			//TODO: Load the right file
			reasonAudio.loadSound(reason.audioSrc,"claim" + claim.claim.id + "reason" + (i+1));
			
			HTMLPanel textPanel = new HTMLPanel("p",reason.text);
			
			textPanel.addStyleName("text");
			reasonPanel.add(textPanel);
			reasonPanel.add(reasonAudio);
			
			getRoot().add(reasonPanel, background);
		}
		
		titleText.setInnerHTML("READ the list of possible reasons. LIKE the 3 reasons you want to use in your essay. FAVORITE the reason you like best.");
		//directionsHeading.setInnerHTML("SUPPORT YOUR CLAIM");
		claimHolder.setInnerHTML(claim.getClaimText());
		claimAudio.loadSound(claim.claim.audioSrc ,"claim" + claim.claim.id );
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick(event);
			}
		};
		for(int i=0; i < likeButtons.length; i++){
			final int reasonIndex = i;
			likeButtons[i].clickHandler = new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					thumb_onClick(reasonIndex);
				}
			};
			loveButtons[i].clickHandler = new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					star_onClick(reasonIndex);
				}
			};
			
		}
		
		ElementUtil.setTextScaling();
	}
	
	@Override
	protected void onDetach() {
		
		for(int i=0; i < likeButtons.length; i++){
			likeButtons[i].clickHandler = null;
			loveButtons[i].clickHandler = null;
		}
		
		submitButton.clickHandler = null;

		super.onDetach();
	}
	
	protected boolean checkCorrect(int reasonIndex) {
		return getClaim().claim.reasons[reasonIndex].isCorrect;
	}
	
	protected boolean checkCorrect() {
		ClaimInstance claim = getClaim();
		/*
		for (int i = 0; i < tackAssignments.length; ++i)
			if (!claim.claim.reasons[tackAssignments[i]].isCorrect)
				return false;
		*/
		return true;
	}
	
	protected void feedbackPopup_onClose(TextPopup popup) {
		popup.clearHandler();
		if (mPopup == popup)
			mPopup = null;
	}
	
	protected void submitButton_onClick(ClickEvent e) {
		if (mPopup != null)
			return;
		
		if (checkCorrect()) {
			submitButton.clickHandler = null;
			exit();
		} else {
			GWT.log("Submit incorrect");
			mPopup = new TextPopup();
			mPopup.setTitle("Incorrect");
			mPopup.setBody("One or more of the reasons you selected does not effectively support your side. Try again.");
			mPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			popupHolder.add(mPopup);
		}
	}
	
	protected void exit() {
		setReasons();
		getParentPanel().close(this);
	}
	
	protected void thumb_onClick(int reasonIndex){
		GWT.log("reasonIndex: " + reasonIndex);
		// If the reason is already liked, unlike it and return
		if(reasonsLiked[reasonIndex]){
			reasonsLiked[reasonIndex] = false;
			reasonsLoved[reasonIndex] = false;
			likeButtons[reasonIndex].removeStyleName("selected");

			//Find and enable the unselected button(s)
			for(int i=0; i < reasonsLiked.length; i++){
				if(!reasonsLiked[i]){
					likeButtons[i].setEnabled(true);
					likeButtons[i].removeStyleName("disabled");
				}
			}
		
			loveButtons[reasonIndex].removeStyleName("selected");
			loveButtons[reasonIndex].setEnabled(false);
			loveButtons[reasonIndex].addStyleName("disabled");
		} else {
			// Check to make sure there aren't too many reasons liked already
			int numLiked = 0;
			for(int i = 0; i < reasonsLiked.length; i++){
				if(reasonsLiked[i])
					numLiked++;
			}
			
			// If there are less than three reasons liked, add this reason to the list of liked reasons
			if(numLiked < 3){
				reasonsLiked[reasonIndex] = true;
				numLiked++;
				likeButtons[reasonIndex].addStyleName("selected");
				
				loveButtons[reasonIndex].removeStyleName("disabled");
				loveButtons[reasonIndex].setEnabled(true);
				
				if(numLiked >= 3){
					//Find and disable the unselected button(s)
					for(int i=0; i < reasonsLiked.length; i++){
						if(!reasonsLiked[i]){
							likeButtons[i].setEnabled(false);
							likeButtons[i].addStyleName("disabled");
						}
					}
				}
				
			}
		}
		updateSubmitButton();
	}
	
	protected void star_onClick(int reasonIndex){
		if(reasonsLoved[reasonIndex]){
			// Unlove the reason
			reasonsLoved[reasonIndex] = false;
			loveButtons[reasonIndex].removeStyleName("selected");
			for(int i=0; i < loveButtons.length; i++){
				if(reasonsLiked[i]){
					loveButtons[i].setEnabled(true);
					loveButtons[i].removeStyleName("disabled");
				}
				else{
					loveButtons[i].setEnabled(false);
					loveButtons[i].addStyleName("disabled");
				}
			}
		} else if(reasonsLiked[reasonIndex]){
			for(int i = 0; i < reasonsLoved.length; i++){
				if(reasonsLoved[i]){
					// A reason is already loved, so clear it before going on
					reasonsLoved[i] = false;
					loveButtons[i].removeStyleName("selected");
				}
			}
			// I can still love a thing, so let's do so
			if(reasonsLiked[reasonIndex]){
				reasonsLoved[reasonIndex] = true;
				loveButtons[reasonIndex].addStyleName("selected");
			}
		}
		updateSubmitButton();
	}
	
	protected void updateSubmitButton(){
		int numliked = 0;
		int numloved = 0;
		for(int i=0; i<reasonsLiked.length; i++){
			if(reasonsLiked[i])
				numliked++;
			if(reasonsLoved[i])
				numloved++;
		}
		
		if(numliked == 3 && numloved == 1)
			submitButton.setEnabled(true);
		else
			submitButton.setEnabled(false);
	}
	
	protected void setReasons(){
		int reasonIndex = 0;
		ClaimInstance claim = getClaim();
		// Set the favorite reason
		for(int i=0; i<reasonsLoved.length; i++){
			if(reasonsLoved[i]){
				reasonsLiked[i] = false;
				claim.reasons[reasonIndex] = new ReasonInstance(claim.claim.reasons[i]);
				reasonIndex++;
			}
		}
		
		// Set the other reasons, if any are left
		for(int i=0; i<reasonsLiked.length; i++){
			if(reasonsLiked[i]){
				claim.reasons[reasonIndex] = new ReasonInstance(claim.claim.reasons[i]);
				reasonIndex++;
			}
		}
	}
}
