package org.icivics.arg.ui.claimCreator.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Glossary;
import org.icivics.arg.data.model.client.Item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceLinkItem extends Composite {
	private static EvidenceLinkItemUiBinder uiBinder = GWT.create(EvidenceLinkItemUiBinder.class);
	interface EvidenceLinkItemUiBinder extends UiBinder<Widget, EvidenceLinkItem> {}
	
	private Item mItem = null;
	private Handler mHandler;
	
	public static interface Handler {
		void onAdd();
		void onView();
		void onRemove();
	}
	
	@UiField
	protected DivElement icon;
	
	@UiField
	protected DivElement text;
	
	@UiField
	protected Button removeButton;
	
	protected HandlerRegistration mClickReg;
	protected HandlerRegistration mRemoveButtonClickReg;

	private PopupPanel mismatchPopup = new PopupPanel(true);
	private Timer popupTimer;
	private HandlerRegistration[] mismatchHandlers = new HandlerRegistration[2];
	
	
	public EvidenceLinkItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		setItem(mItem);
		mismatchPopup.getElement().getStyle().setZIndex(2);
	}
	
	public Item getItem() {
		return mItem;
	}
	
	public void setItem(Item item) {
		mItem = item;
		clearState();
		if (isAttached()) {
			if (item == null) {
				icon.setClassName("icon");
				icon.setInnerHTML("");
				text.getElementsByTagName("p").getItem(0).setInnerText("");
				text.getElementsByTagName("p").getItem(1).setInnerText("");
				mClickReg = getWidget().addDomHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						addButton_onClick();
					}
				}, ClickEvent.getType());
				removeButton.setEnabled(false);
				removeButton.addStyleName("disabled");
			} else {
				getWidget().removeStyleName("empty");
				getWidget().addStyleName("complete");
				icon.setClassName("icon type" + item.type);
				icon.setInnerHTML("<img src=\"" +DataController.getIconPath(item.type) +"\"/>");
				text.getElementsByTagName("p").getItem(0).setInnerText(item.name);
				text.getElementsByTagName("p").getItem(1).setInnerText(item.description);
				mClickReg = getWidget().addDomHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						viewButton_onClick();
					}
				}, ClickEvent.getType());
				removeButton.addClickHandler(new ClickHandler(){
					public void onClick(ClickEvent event) {
						removeButton_onClick();
						event.stopPropagation();
					}
				});
				removeButton.setEnabled(true);
				removeButton.removeStyleName("disabled");
			}
		}
	}
	
	
	public void clearItem() {
		setItem(null);
		
	}
	
	public void setHandler(Handler handler) {
		mHandler = handler;
	}
	
	public void clearHandler() {
		mHandler = null;
	}
	
	protected void clearState() {
		getWidget().removeStyleName("complete");
		getWidget().removeStyleName("empty");
		getWidget().removeStyleName("incorrect");
		getWidget().addStyleName("empty");
		icon.setClassName("");
		if (mClickReg != null) {
			mClickReg.removeHandler();
			mClickReg = null;
		}
		for(int i = 0, count = mismatchHandlers.length; i<count; i++){
			if(mismatchHandlers[i] != null)
				mismatchHandlers[i].removeHandler();
		}
		if(popupTimer != null){
			popupTimer.cancel();
			popupTimer = null;
			hideMismatchPopup();
		}
	}
	
	public void markIncorrect(final String mismatchText){
		getWidget().removeStyleName("complete");
		getWidget().removeStyleName("empty");
		getWidget().removeStyleName("incorrect");
		getWidget().addStyleName("incorrect");
		
		//TODO: Activate the mismatch popup listeners
		mismatchHandlers[0] = this.addDomHandler(new MouseOverHandler(){

			@Override
			public void onMouseOver(MouseOverEvent event) {
				popupTimer = new Timer(){
					@Override
					public void run() {
						showMismatchPopup(mismatchText);
						
					}};
				popupTimer.schedule(500);
				
			}},MouseOverEvent.getType());
		mismatchHandlers[1] = this.addDomHandler(new MouseOutHandler(){

			@Override
			public void onMouseOut(MouseOutEvent event) {
				if(popupTimer != null){
					popupTimer.cancel();
					popupTimer = null;
					hideMismatchPopup();
				}
				
			}}, MouseOutEvent.getType());
	}
	
	public void destroy() {
		mItem = null;
	}
	
	protected void addButton_onClick() {
		if (mHandler != null)
			mHandler.onAdd();
	}
	
	protected void viewButton_onClick() {
		if (mHandler != null)
			mHandler.onView();
	}
	
	protected void removeButton_onClick(){
		if(mHandler != null)
			mHandler.onRemove();
	}
	
	private void showMismatchPopup(String mismatchText){
		HTMLPanel content = new HTMLPanel(mismatchText);
		content.addStyleName("evidence-mismatch");
		
		mismatchPopup.setWidget(content);
		
		final int pos_x = this.getAbsoluteLeft();
		final int pos_y = this.getAbsoluteTop();
		mismatchPopup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
			
			@Override
			public void setPosition(int offsetWidth, int offsetHeight) {
				mismatchPopup.setPopupPosition(pos_x,pos_y - offsetHeight);
			}
		});
	}
	
	private void hideMismatchPopup(){
		if(mismatchPopup != null)
			mismatchPopup.hide();
	}
}
