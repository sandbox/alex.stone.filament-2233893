package org.icivics.arg.ui.claimCreator.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

public class Justification extends Subpanel<ClaimCreator> {
	private static JustificationUiBinder uiBinder = GWT.create(JustificationUiBinder.class);
	interface JustificationUiBinder extends UiBinder<Widget, Justification> {}
	
	@UiField
	ParagraphElement reasonText;
	@UiField
	ParagraphElement claimHolder;
	@UiField
	HTMLPanel responseField;
	@UiField
	LabelButton submitButton, backButton;
	@UiField
	HTMLPanel paragraphWrap;
	@UiField
	AudioConcatenatedControl claimAudio;
	
	
	@UiField
	SimplePanel popupHolder;

	private HandlerRegistration responseChangeReg;
	private TextPopup mPopup;

	
	public Justification(ClaimCreator parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		
		reasonText.setInnerText(DataController.getCurrentIssue().claim.reasons[0].reason.text);
		claimHolder.setInnerText(DataController.getCurrentIssue().claim.getClaimText());
		
		claimAudio.loadSound(claim.claim.audioSrc ,"claim" + claim.claim.id );
		claimAudio.loadSound("/audio/because.mp3", "because");
		claimAudio.loadSound(claim.reasons[0].reason.audioSrc, "reason0");
	}
	
	@Override
	public void finishShowing() {
		super.finishShowing();
		
		Element label = DOM.getElementById("staticLabel");
		// Floating this left should fix an IE8 rendering weirdness
		label.getStyle().setFloat( com.google.gwt.dom.client.Style.Float.LEFT);
		
		responseField.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
		responseField.getElement().getStyle().setProperty("minWidth","6em");
		responseField.getElement().getStyle().setProperty("minHeight","1em");
		responseField.getElement().setAttribute("contentEditable", "true");
		
		if(DataController.getCurrentIssue().reasonSelectExplanation != null){
			responseField.getElement().setInnerText(DataController.getCurrentIssue().reasonSelectExplanation);
		}
		
		responseChangeReg = responseField.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				submitButton.setEnabled(checkCorrect());

				GWT.log("keyup" + event.getNativeKeyCode());
				updateScrollbars();
			}},KeyUpEvent.getType());
		
		submitButton.setEnabled(checkCorrect());
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		
		//responseField.getElement().setId("editableField");
		responseField.getElement().getStyle().setProperty("MozUserSelect", "text");
		responseField.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
		responseField.getElement().getStyle().setProperty("WebkitUserSelect", "text");
		responseField.getElement().getStyle().setProperty("userSelect", "text");
		
		addScrollbars();
		updateScrollbars();
		
		/*
		paragraphWrap.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Clicked the div, should set focus");
				grabFocus(child2.getElement().getId());
			}}, ClickEvent.getType());
		
		grabFocus(child2.getElement().getId());
		*/
		
	}


	@Override
	protected void onDetach() {
		submitButton.clickHandler = null;
		responseChangeReg.removeHandler();
		super.onDetach();
	}
	
	protected boolean checkCorrect() {
		return responseField.getElement().getInnerText().replaceAll("\\W", "").length() > 0;
		//FIXME: FIX ME or figure out why steve's GWT crashes here
		//GWT.log(responseField.getValue());
		//return responseField.getValue().length() > 1;
	}
	
	protected void feedbackPopup_onClose(TextPopup popup) {
		popup.clearHandler();
		if (mPopup == popup)
			mPopup = null;
		
		//responseField.setFocus(true);
	}
	
	protected void submitButton_onClick() {
		if (mPopup != null)
			return;
		
		if (checkCorrect()) {
			exit();
		} else {
			GWT.log("Submit incorrect");
			mPopup = new TextPopup();
			mPopup.setTitle("Invalid");
			mPopup.setBody("Enter an explanation to continue.");
			mPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			popupHolder.add(mPopup);
		}
	}
	
	protected void backButton_onClick(){
		getParentPanel().close(this, true);
	}
	
	protected void exit() {
		//submitButton.clickHandler = null;
		//backButton.clickHandler = null;
		DataController.getCurrentIssue().reasonSelectExplanation = responseField.getElement().getInnerText();
		getParentPanel().close(this, false);
	}
	
	private native void addScrollbars() /*-{
		$wnd.$('.claim-creator .paragraph-editor .text-wrap').perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true,
				useKeyboard:false
			});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.claim-creator .paragraph-editor .text-wrap').perfectScrollbar('update');
	}-*/;
}
