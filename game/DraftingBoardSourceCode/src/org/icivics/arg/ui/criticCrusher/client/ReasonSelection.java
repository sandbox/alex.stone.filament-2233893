package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ReasonSelection extends Subpanel<CriticCrusher> {

	private static ReasonSelectionUiBinder uiBinder = GWT.create(ReasonSelectionUiBinder.class);
	interface ReasonSelectionUiBinder extends UiBinder<Widget, ReasonSelection> {}

	@UiField
	HTMLPanel reasonA, reasonB;
	
	@UiField
	ParagraphElement reasonAField, reasonBField, claimHolder;
	
	@UiField LabelButton submitButton, backButton;
	
	@UiField
	SimplePanel popupHolder;
	
	@UiField
	AudioControl reasonBAudio, reasonAAudio, claimAudio;
	
	public String selectedReasonID;
	private Reason[] mCounterReasons;
	
	public ReasonSelection(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		mCounterReasons = DataController.findCounterReasons();
		
		claimHolder.setInnerText(DataController.getCurrentIssue().getOpposingClaim().text);
		claimAudio.loadSound(DataController.getCurrentIssue().getOpposingClaim().audioSrc);
		
		reasonAField.setInnerText(mCounterReasons[0].text);
		reasonAAudio.loadSound(mCounterReasons[0].audioSrc);
		reasonBField.setInnerText(mCounterReasons[1].text);
		reasonBAudio.loadSound(mCounterReasons[1].audioSrc);
		
		reasonA.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				selectReason(0);
			}}, ClickEvent.getType());
		reasonB.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				selectReason(1);
			}}, ClickEvent.getType());
		
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}};
			
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onBack();
			}
		};
		
		submitButton.setEnabled(false);
	}

	
	private void selectReason(int reason){
		if(submitButton.clickHandler != null){
			switch(reason){
				case 0: 
					reasonA.addStyleName("selected");
					reasonB.removeStyleName("selected");
					selectedReasonID = mCounterReasons[0].id;
					break;
				case 1:
					reasonB.addStyleName("selected");
					reasonA.removeStyleName("selected");
					selectedReasonID = mCounterReasons[1].id;
					break;
				default:
					GWT.log("Unknown reason selected");
			}
			
			submitButton.setEnabled(true);
		}
	}
	
	private void onSubmit(){
		submitButton.clickHandler = null;
		backButton.clickHandler = null;
		submitButton.setEnabled(false);
		getParentPanel().close(this,false);
	}
	
	private void onBack(){
		submitButton.clickHandler = null;
		backButton.clickHandler = null;
		getParentPanel().close(this, true);
	}
}
