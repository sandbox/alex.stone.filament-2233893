package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.CriticCrusherData;
import org.icivics.arg.data.model.client.CriticCrusherUserData;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.EvidenceIconButton;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class RebuttalEvidence extends FragmentSelectionUI {
	private static RebuttalEvidenceUiBinder uiBinder = GWT.create(RebuttalEvidenceUiBinder.class);
	interface RebuttalEvidenceUiBinder extends UiBinder<Widget, RebuttalEvidence> {}
	
	@UiField
	DivElement optionsHolder;
	
	@UiField
	SpanElement selectedSentence;
	
	@UiField
	SpanElement paragraphText;
	
	@UiField
	ParagraphElement claimText, counterClaimHolder, reasonText, theirReason;
	
	@UiField
	AudioConcatenatedControl yourClaimAudio, theirClaimAudio;
	
	private OptionToggle[] mOptions;
	private int[] mIndices;
	private int mSelectedIndex = -1;
	private EvidenceDetailPanel mItemPanel;
	
	public int paragraphIndex;
	
	public RebuttalEvidence(CriticCrusher parent, int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		initWidget(uiBinder.createAndBindUi(this));
		CriticCrusherData data = DataController.getCriticCrusherData(paragraphIndex);
		
		mOptions = new OptionToggle[data.rebuttalSentenceOptions.length];
		mIndices = new int[data.rebuttalSentenceOptions.length];
		initOptions(mOptions, mIndices, data.rebuttalSentenceOptions, data.rebuttalSentenceAudioURLs, optionsHolder);
		
		claimText.setInnerText(DataController.getCurrentIssue().counterClaim.getClaimText());
		reasonText.setInnerText(DataController.getCurrentIssue().counterReason.text);
		
		//itemButton.setType(data.item.type);
		
		Claim claim = DataController.getCurrentIssue().getSelectedClaim();
		Claim counterClaim = DataController.getCurrentIssue().getOpposingClaim();
		
		Reason myReason = claim.reasons[0];
		for(int i = 0; i < claim.reasons.length; i++)
			for(int j =0; j < claim.reasons[i].items.length; j++)
				if(claim.reasons[i].items[j].id.equals(data.rebuttalItem.id))
					myReason = claim.reasons[i];
		
		claimText.setInnerText(claim.text);
		reasonText.setInnerText(myReason.text);
		
		counterClaimHolder.setInnerText(counterClaim.text);
		theirReason.setInnerText(DataController.getCurrentIssue().counterReason.text);
		
		yourClaimAudio.loadSound(claim.audioSrc ,"claim" + claim.id );
		yourClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		yourClaimAudio.loadSound(myReason.audioSrc, "reason"+myReason.id);
		theirClaimAudio.loadSound(counterClaim.audioSrc, "claim" + counterClaim.id);
		theirClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		theirClaimAudio.loadSound(DataController.getCurrentIssue().counterReason.audioSrc, "reason"+DataController.getCurrentIssue().counterReason.id);
		
		CriticCrusherUserData userData = DataController.getCriticCrusherUserData(paragraphIndex);
		paragraphText.setInnerText(userData.getSentenceText(0) + " " + userData.getSentenceText(1));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		for (int i = 0; i < mOptions.length; ++i) {
			final int optionIndex = i;
			mOptions[i].clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					startOption_onSelect(optionIndex);
				}
			};
		}
		/*
		itemButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				itemButton_onClick();
			}
		};
		*/
	}

	@Override
	protected void onDetach() {
		for (int i = 0; i < mOptions.length; ++i)
			mOptions[i].destroy();
		super.onDetach();
	}
	
	protected boolean checkCorrect() {
		return true;
	}
	
	protected boolean checkParagraphMatch() {
		return mIndices[mSelectedIndex] == DataController.getCriticCrusherUserData(paragraphIndex).openingSelection;
	}
	
	protected boolean checkComplete() {
		return mSelectedIndex != -1;
	}
	
	protected void showItem(Item item) {
		mItemPanel = new EvidenceDetailPanel(item, false);
		mItemPanel.handler = new EvidenceDetailPanel.Handler() {
			@Override
			public void onClose() {
				onItemPanelClose();
			}
			public void onConfirm() {}
		};
		popupHolder.add(mItemPanel);
	}
	
	protected void closeItemPanel() {
		if (mItemPanel == null)
			return;
		
		popupHolder.remove(mItemPanel);
		mItemPanel.destroy();
		mItemPanel = null;
	}
	
	protected void onItemPanelClose() {
		closeItemPanel();
	}
	
	protected void itemButton_onClick() {
		if (mPopup != null || mItemPanel != null)
			return;
		
		showItem(DataController.getCriticCrusherData(paragraphIndex).rebuttalItem);
	}
	
	private void startOption_onSelect(int optionIndex) {
		if (mPopup != null)
			return;
		
		if (mSelectedIndex != -1)
			mOptions[mSelectedIndex].setSelected(false);
		mSelectedIndex = optionIndex;
		mOptions[mSelectedIndex].setSelected(true);
		
		selectedSentence.setInnerText(mOptions[mSelectedIndex].getText());
		
		if (checkComplete())
			submitButton.setEnabled(true);
	}
	
	protected String getIncorrectFeedbackBodyText() {
		return "???";
	}
	
	protected String getMismatchedFeedbackTitleText() {
		return "TRY AGAIN";
	}
	protected String getMismatchedFeedbackBodyText() {
		return "The sentence you chose is not the best way to support your side with evidence.";
	}
	
	protected void exit() {
		DataController.getCriticCrusherUserData(paragraphIndex).rebuttalSelection = mIndices[mSelectedIndex];
		getParentPanel().close(this);
	}
}
