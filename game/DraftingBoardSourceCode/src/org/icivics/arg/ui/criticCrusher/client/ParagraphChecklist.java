package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ParagraphChecklist extends Subpanel<CriticCrusher> {
	private static ParagraphChecklistUiBinder uiBinder = GWT.create(ParagraphChecklistUiBinder.class);
	interface ParagraphChecklistUiBinder extends UiBinder<Widget, ParagraphChecklist> {}
	/*
	@UiField
	HTMLPanel toggle1;
	@UiField
	HTMLPanel toggle2;
	@UiField
	HTMLPanel toggle3;
	@UiField
	HTMLPanel toggle4;
	@UiField
	HTMLPanel toggle5;
	*/
	
	@UiField
	ParagraphElement claimHolder, counterClaimHolder, yourReason, theirReason;
	
	@UiField
	DistinguishableTextBox paragraphField;
	
	@UiField
	AudioConcatenatedControl yourClaimAudio, theirClaimAudio;
	
	@UiField
	LabelButton submitButton;
	
	//private HTMLPanel[] toggles = new HTMLPanel[5];
	//private boolean[] toggleStates = {false, false, false, false, false};
	//private HandlerRegistration[] toggleRegs;
	
	public ParagraphChecklist(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		/*
		toggles[0] = toggle1;
		toggles[1] = toggle2;
		toggles[2] = toggle3;
		toggles[3] = toggle4;
		toggles[4] = toggle5;
		*/
		
		Claim claim = DataController.getCurrentIssue().getSelectedClaim();
		Claim counterClaim = DataController.getCurrentIssue().getOpposingClaim();
		
		Reason myReason = claim.reasons[0];
		for(int i = 0; i < claim.reasons.length; i++)
			for(int j =0; j < claim.reasons[i].items.length; j++)
				if(claim.reasons[i].items[j].id.equals(DataController.getCurrentIssue().counterReason.id))
					myReason = claim.reasons[i];
		
		claimHolder.setInnerText(claim.text);
		yourReason.setInnerText(myReason.text);
		
		counterClaimHolder.setInnerText(counterClaim.text);
		theirReason.setInnerText(DataController.getCurrentIssue().counterReason.text);	
		
		yourClaimAudio.loadSound(claim.audioSrc ,"claim" + claim.id );
		yourClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		yourClaimAudio.loadSound(myReason.audioSrc, "reason"+myReason.id);
		theirClaimAudio.loadSound(counterClaim.audioSrc, "claim" + counterClaim.id);
		theirClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		theirClaimAudio.loadSound(DataController.getCurrentIssue().counterReason.audioSrc, "reason"+DataController.getCurrentIssue().counterReason.id);
		
		paragraphField.setInnerText(DataController.getCurrentIssue().claim.rebuttalParagraphOriginal);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		/*
		toggleRegs = new HandlerRegistration[toggles.length];
		for (int i = 0; i < toggles.length; ++i) {
			final int buttonIndex = i;
			toggleRegs[i] = toggles[i].addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toggle_onClick(buttonIndex);
				}
			}, ClickEvent.getType());
		}
		*/
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		clearListeners();
		submitButton.destroy();
		super.onDetach();
	}
	
	private void clearListeners() {
		/*
		if (toggleRegs != null) {
			for (int i = 0; i < toggleRegs.length; ++i)
				toggleRegs[i].removeHandler();
			toggleRegs = null;
		}
		*/
		submitButton.clickHandler = null;
	}

	private boolean checkComplete() {
		/*
		for (int i = 0; i < toggleStates.length; ++i)
			if (!toggleStates[i])
				return false;
		*/
		return true;
	}

	private void toggle_onClick(int buttonIndex) {
		/*
		if (toggleStates[buttonIndex] = !toggleStates[buttonIndex]) {
			toggles[buttonIndex].addStyleName("checked");
			submitButton.setEnabled(checkComplete());
		} else {
			toggles[buttonIndex].removeStyleName("checked");
			submitButton.setEnabled(false);
		}
		*/
	}
	
	private void submitButton_onClick() {
		GWT.log("Submit clicked");
		clearListeners();
		
		DataController.getCurrentIssue().claim.rebuttalParagraph = paragraphField.getValue();
		getParentPanel().close(this);
	}
}
