package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.CriticCrusherData;
import org.icivics.arg.data.model.client.CriticCrusherUserData;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.EvidenceIconButton;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class RebuttalStrengthener extends FragmentSelectionUI {
	private static RebuttalStrengthenerUiBinder uiBinder = GWT.create(RebuttalStrengthenerUiBinder.class);
	interface RebuttalStrengthenerUiBinder extends UiBinder<Widget, RebuttalStrengthener> {}
	
	@UiField
	DivElement startOptionsHolder;
	@UiField
	DivElement endOptionsHolder;
	
	@UiField
	ParagraphElement claimText, counterClaimHolder, reasonText, theirReason;
	/*
	@UiField
	EvidenceIconButton itemButton1;
	@UiField
	EvidenceIconButton itemButton2;
	*/
	@UiField
	AudioConcatenatedControl yourClaimAudio, theirClaimAudio;
	
	private OptionToggle[] mStartOptions, mEndOptions;
	private int[] mStartIndices, mEndIndices;
	private int mSelectedStartIndex = -1, mSelectedEndIndex = -1;
	private EvidenceDetailPanel mItemPanel;
	
	public int paragraphIndex = 0;
	
	public RebuttalStrengthener(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		CriticCrusherData data = DataController.getCriticCrusherData(paragraphIndex);
		
		mStartOptions = new OptionToggle[data.strengthenerStartOptions.length];
		mStartIndices = new int[data.strengthenerStartOptions.length];
		initOptions(mStartOptions, mStartIndices, data.strengthenerStartOptions, data.strengthenerStartAudioURLs, startOptionsHolder);
		
		mEndOptions = new OptionToggle[data.strengthenerEndOptions.length];
		mEndIndices = new int[data.strengthenerEndOptions.length];
		initOptions(mEndOptions, mEndIndices, data.strengthenerEndOptions, data.strengthenerEndAudioURLs, endOptionsHolder);
		
		Claim claim = DataController.getCurrentIssue().getSelectedClaim();
		Claim counterClaim = DataController.getCurrentIssue().getOpposingClaim();
		
		Reason myReason = claim.reasons[0];
		for(int i = 0; i < claim.reasons.length; i++)
			for(int j =0; j < claim.reasons[i].items.length; j++)
				if(claim.reasons[i].items[j].id.equals(data.rebuttalItem.id))
					myReason = claim.reasons[i];
		
		claimText.setInnerText(claim.text);
		reasonText.setInnerText(myReason.text);
		
		counterClaimHolder.setInnerText(counterClaim.text);
		theirReason.setInnerText(DataController.getCurrentIssue().counterReason.text);	
		
		yourClaimAudio.loadSound(claim.audioSrc ,"claim" + claim.id );
		yourClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		yourClaimAudio.loadSound(myReason.audioSrc, "reason"+myReason.id);
		theirClaimAudio.loadSound(counterClaim.audioSrc, "claim" + counterClaim.id);
		theirClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		theirClaimAudio.loadSound(DataController.getCurrentIssue().counterReason.audioSrc, "reason"+DataController.getCurrentIssue().counterReason.id);
		
		CriticCrusherUserData userData = DataController.getCriticCrusherUserData(paragraphIndex);
		paragraphText.setInnerText(userData.getSentenceText(0));
		
		//itemButton1.setType(data.item.type);
	//	itemButton2.setType(data.rebuttalItem.type);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		final CriticCrusherData data = DataController.getCriticCrusherData(paragraphIndex);
		for (int i = 0; i < mStartOptions.length; ++i) {
			final int optionIndex = i;
			mStartOptions[i].clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					startOption_onSelect(optionIndex);
				}
			};
		}
		for (int i = 0; i < mEndOptions.length; ++i) {
			final int optionIndex = i;
			mEndOptions[i].clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					endOption_onSelect(optionIndex);
				}
			};
		}
		/*
		itemButton1.clickHandler = new ClickHandler() {
			final Item item = data.item;
			@Override
			public void onClick(ClickEvent event) {
				itemButton_onClick(item);
			}
		};
		itemButton2.clickHandler = new ClickHandler() {
			final Item item = data.rebuttalItem;
			@Override
			public void onClick(ClickEvent event) {
				itemButton_onClick(item);
			}
		};
		*/
	}

	@Override
	protected void onDetach() {
		for (int i = 0; i < mStartOptions.length; ++i)
			mStartOptions[i].destroy();
		for (int i = 0; i < mEndOptions.length; ++i)
			mEndOptions[i].destroy();
		/*
		itemButton1.destroy();
		itemButton2.destroy();
		*/
		super.onDetach();
	}
	
	protected boolean checkCorrect() {
		GWT.log("Testing two indices: " + mStartIndices[mSelectedStartIndex] + ", " + mEndIndices[mSelectedEndIndex]);
		return mStartIndices[mSelectedStartIndex] == mEndIndices[mSelectedEndIndex];
	}
	
	protected boolean checkParagraphMatch() {
		return mStartIndices[mSelectedStartIndex] == DataController.getCriticCrusherUserData(paragraphIndex).openingSelection;
	}
	
	protected boolean checkComplete() {
		return mSelectedStartIndex != -1 && mSelectedEndIndex != -1;
	}
	
	protected void showItem(Item item) {
		mItemPanel = new EvidenceDetailPanel(item, false);
		mItemPanel.handler = new EvidenceDetailPanel.Handler() {
			@Override
			public void onClose() {
				onItemPanelClose();
			}
			public void onConfirm() {}
		};
		popupHolder.add(mItemPanel);
	}
	
	protected void closeItemPanel() {
		if (mItemPanel == null)
			return;
		
		popupHolder.remove(mItemPanel);
		mItemPanel.destroy();
		mItemPanel = null;
	}
	
	protected void onItemPanelClose() {
		closeItemPanel();
	}
	
	protected void itemButton_onClick(Item item) {
		if (mPopup != null || mItemPanel != null)
			return;
		
		showItem(item);
	}
	
	private void startOption_onSelect(int optionIndex) {
		if (mPopup != null || mItemPanel != null)
			return;
		
		if (mSelectedStartIndex != -1)
			mStartOptions[mSelectedStartIndex].setSelected(false);
		mSelectedStartIndex = optionIndex;
		mStartOptions[mSelectedStartIndex].setSelected(true);
		
		if (checkComplete())
			submitButton.setEnabled(true);
	}
	
	private void endOption_onSelect(int optionIndex) {
		if (mPopup != null || mItemPanel != null)
			return;
		
		if (mSelectedEndIndex != -1)
			mEndOptions[mSelectedEndIndex].setSelected(false);
		mSelectedEndIndex = optionIndex;
		mEndOptions[mSelectedEndIndex].setSelected(true);
		
		if (checkComplete())
			submitButton.setEnabled(true);
	}

	
	protected String getIncorrectFeedbackBodyText() {
		return "The beginning of your sentence doesn't match with the end of your sentence. Try again.";
	}
	
	protected String getMismatchedFeedbackTitleText() {
		return "TRY AGAIN";
	}
	protected String getMismatchedFeedbackBodyText() {
		return "The sentence you matched does not form the best strengthening statement.";
	}
	
	protected void exit() {
		DataController.getCriticCrusherUserData(paragraphIndex).strengthenerSelection = mStartIndices[mSelectedStartIndex];
		getParentPanel().close(this);
	}
}
