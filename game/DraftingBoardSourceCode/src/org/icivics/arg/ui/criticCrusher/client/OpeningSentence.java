package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.CriticCrusherData;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class OpeningSentence extends FragmentSelectionUI {
	private static OpeningSentenceUiBinder uiBinder = GWT.create(OpeningSentenceUiBinder.class);
	interface OpeningSentenceUiBinder extends UiBinder<Widget, OpeningSentence> {}
	
	@UiField
	DivElement startOptionsHolder;
	@UiField
	DivElement endOptionsHolder;
	
	@UiField
	ParagraphElement claimText, counterClaimHolder, reasonText, theirReason;
	
	@UiField
	AudioConcatenatedControl yourClaimAudio, theirClaimAudio;
	
	private OptionToggle[] mStartOptions, mEndOptions;
	private int[] mStartIndices, mEndIndices;
	private int mSelectedStartIndex = -1, mSelectedEndIndex = -1;
	
	public int paragraphIndex = 0;
	
	public OpeningSentence(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		CriticCrusherData data = DataController.getCriticCrusherData(paragraphIndex);
		
		mStartOptions = new OptionToggle[data.openingStartOptions.length];
		mStartIndices = new int[data.openingStartOptions.length];
		initOptions(mStartOptions, mStartIndices, data.openingStartOptions, data.openingStartAudioURLs,startOptionsHolder);
		
		mEndOptions = new OptionToggle[data.openingEndOptions.length];
		mEndIndices = new int[data.openingEndOptions.length];
		initOptions(mEndOptions, mEndIndices, data.openingEndOptions, data.openingEndAudioURLs,	endOptionsHolder);
		
		
		Claim claim = DataController.getCurrentIssue().getSelectedClaim();
		Claim counterClaim = DataController.getCurrentIssue().getOpposingClaim();

		Reason myReason = claim.reasons[0];
		for(int i = 0; i < claim.reasons.length; i++)
			for(int j =0; j < claim.reasons[i].items.length; j++)
				if(claim.reasons[i].items[j].id.equals(data.rebuttalItem.id))
					myReason = claim.reasons[i];
		
		claimText.setInnerText(claim.text);
		reasonText.setInnerText(myReason.text);
		
		yourClaimAudio.loadSound(claim.audioSrc ,"claim" + claim.id );
		yourClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		yourClaimAudio.loadSound(myReason.audioSrc, "reason"+myReason.id);
		theirClaimAudio.loadSound(counterClaim.audioSrc, "claim" + counterClaim.id);
		theirClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		theirClaimAudio.loadSound(DataController.getCurrentIssue().counterReason.audioSrc, "reason"+DataController.getCurrentIssue().counterReason.id);
		
		
		//claimText.setInnerText(claim.text);
		//reasonText.setInnerText(claim.reasons[0].text);
		
		counterClaimHolder.setInnerText(counterClaim.text);
		theirReason.setInnerText(DataController.getCurrentIssue().counterReason.text);	
		
		paragraphText.setInnerText("");
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		for (int i = 0; i < mStartOptions.length; ++i) {
			final int optionIndex = i;
			mStartOptions[i].clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					startOption_onSelect(optionIndex);
				}
			};
		}
		for (int i = 0; i < mEndOptions.length; ++i) {
			final int optionIndex = i;
			mEndOptions[i].clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					endOption_onSelect(optionIndex);
				}
			};
		}
	}

	@Override
	protected void onDetach() {
		for (int i = 0; i < mStartOptions.length; ++i)
			mStartOptions[i].destroy();
		for (int i = 0; i < mEndOptions.length; ++i)
			mEndOptions[i].destroy();
		super.onDetach();
	}
	
	protected boolean checkCorrect() {
		GWT.log("The index of the selected sentence combo is " + mStartIndices[mSelectedStartIndex]);
		
		return mStartIndices[mSelectedStartIndex] == mEndIndices[mSelectedEndIndex];
	}
	
	protected boolean checkParagraphMatch() {
		return mStartIndices[mSelectedStartIndex] == 0;
	}
	
	protected boolean checkComplete() {
		return mSelectedStartIndex != -1 && mSelectedEndIndex != -1;
	}
	
	private void startOption_onSelect(int optionIndex) {
		if (mPopup != null)
			return;
		
		if (mSelectedStartIndex != -1)
			mStartOptions[mSelectedStartIndex].setSelected(false);
		mSelectedStartIndex = optionIndex;
		mStartOptions[mSelectedStartIndex].setSelected(true);
		
		if (checkComplete())
			submitButton.setEnabled(true);
	}
	
	private void endOption_onSelect(int optionIndex) {
		if (mPopup != null)
			return;
		
		if (mSelectedEndIndex != -1)
			mEndOptions[mSelectedEndIndex].setSelected(false);
		mSelectedEndIndex = optionIndex;
		mEndOptions[mSelectedEndIndex].setSelected(true);
		
		if (checkComplete())
			submitButton.setEnabled(true);
	}
	protected String getIncorrectFeedbackBodyText() {
		return "The beginning of your sentence doesn't match with the end of your sentence. Try again.";
	}
	
	protected String getMismatchedFeedbackTitleText() {
		return "TRY AGAIN";
	}
	protected String getMismatchedFeedbackBodyText() {
		return "The sentence you matched does not form the best opening statement.";
	}
	
	protected void exit() {
		DataController.getCriticCrusherUserData(paragraphIndex).openingSelection = mStartIndices[mSelectedStartIndex];
		getParentPanel().close(this);
	}
}
