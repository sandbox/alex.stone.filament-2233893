package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewParagraph extends Subpanel<CriticCrusher> {
	private static ViewParagraphUiBinder uiBinder = GWT.create(ViewParagraphUiBinder.class);
	interface ViewParagraphUiBinder extends UiBinder<Widget, ViewParagraph> {}
	
	@UiField
	HTMLPanel displayText;
	@UiField
	LabelButton submitButton;
	
	@UiField
	AudioConcatenatedControl paragraphAudio;
	
	
	@UiField
	SimplePanel popupHolder;
	
	
	public int paragraphIndex;
	
	public ViewParagraph(CriticCrusher parent, int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		initWidget(uiBinder.createAndBindUi(this));
		
		displayText.getElement().setInnerHTML(DataController.getCriticCrusherUserData(paragraphIndex).buildParagraphText());
		
		String[] audio = DataController.getCriticCrusherUserData(paragraphIndex).getParagraphAudio();
		for(int i = 0; i < audio.length; i++)
			paragraphAudio.loadSound(audio[i],"crusherSentence"+i);
		
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		super.onDetach();
	}
	
	private void submitButton_onClick() {
		submitButton.clickHandler = null;
		getParentPanel().close(this);
	}
}
