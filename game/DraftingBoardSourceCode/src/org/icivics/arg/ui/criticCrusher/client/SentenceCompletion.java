package org.icivics.arg.ui.criticCrusher.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.CriticCrusherData;
import org.icivics.arg.data.model.client.CriticCrusherUserData;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceCompletion extends Subpanel<CriticCrusher> {

	private static SentenceCompletionUiBinder uiBinder = GWT.create(SentenceCompletionUiBinder.class);
	interface SentenceCompletionUiBinder extends UiBinder<Widget, SentenceCompletion> {}

	private static String[] DIRECTIONS_AUDIO_NAMES = {"Directions_Crusher2_smackdown.mp3", "Directions_Crusher2_counter.mp3", "Directions_Crusher2_pointfact.mp3", "Directions_Crusher2_deliver.mp3"};
	
	private static String[] DIRECTIONS_HEADERS = {"SMACK DOWN THEIR REASON","COUNTER THEIR EVIDENCE","POINT TO A FACT IN YOUR EVIDENCE","DELIVER THE CRUSHER"};
	
	private static String[] DIRECTIONS = {"COMPLETE THE SENTENCE to identify the other side's reason and show why it is mistaken. (You are crushing the same reason as before, so this opening sentence will sound similar to the opening sentence of your practice paragraph.)",
										  "COMPLETE THE SENTENCE. In the FIRST HALF, admit that the other side's evidence may be true. In the SECOND HALF, use your evidence to point out how their reason is weak. (Don't forget to open the evidence and read it!)",
										  "COMPLETE THE SENTENCE using a fact from your own evidence. Your goal is to support your argument that your reason is stronger than theirs.", 
										  "Your \"crusher\" sentence ends the paragraph with a strong statement for your side. COMPLETE the sentence so it makes sense. You may need your OWN IDEAS instead of words from the evidence. "};
	
	@UiField
	ParagraphElement claimHolder, counterClaimHolder, yourReason, theirReason;
	
	@UiField
	AudioControl directionsAudio;
	
	@UiField
	DivElement directionsHeader;
	
	@UiField
	ParagraphElement directionsText;
	
	@UiField
	LabelButton submitButton, backButton;
	
	@UiField
	AudioConcatenatedControl yourClaimAudio, theirClaimAudio;
	
	@UiField
	HTMLPanel paragraphArea;
	
	HTMLPanel[] mSentences;
	int mCurrentSentence;
	
	Item[] mItems = new Item[2];
	
	public SentenceCompletion(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		
		
		mSentences = new HTMLPanel[4];
		
		CriticCrusherData data = DataController.getCriticCrusherData(1);
		CriticCrusherUserData userData = DataController.getCriticCrusherUserData(1);
		
		Claim claim = DataController.getCurrentIssue().getSelectedClaim();
		Claim counterClaim = DataController.getCurrentIssue().getOpposingClaim();
		
		mItems[0] = data.item;
		mItems[1] = data.rebuttalItem;

		Reason myReason = claim.reasons[0];
		for(int i = 0; i < claim.reasons.length; i++)
			for(int j =0; j < claim.reasons[i].items.length; j++)
				if(claim.reasons[i].items[j].id.equals(data.rebuttalItem.id))
					myReason = claim.reasons[i];
		
		claimHolder.setInnerText(claim.text);
		yourReason.setInnerText(myReason.text);
		
		counterClaimHolder.setInnerText(counterClaim.text);
		theirReason.setInnerText(DataController.getCurrentIssue().counterReason.text);	
		
		getParentPanel().sidebar.init(mItems);
		
		paragraphArea.clear();

		String[] defaults = new String[4];
		defaults[0] = data.clozeOpening;
		defaults[1] = data.clozeStrengthener;
		defaults[2] = data.clozeEvidenceSentence;
		defaults[3] = data.clozeClosing;
		
		for(int i = 0; i < 4; i++){
			mSentences[i] = buildSentence(defaults[i]);
		}
		
		yourClaimAudio.loadSound(claim.audioSrc ,"claim" + claim.id );
		yourClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		yourClaimAudio.loadSound(myReason.audioSrc, "reason"+myReason.id);
		theirClaimAudio.loadSound(counterClaim.audioSrc, "claim" + counterClaim.id);
		theirClaimAudio.loadSound(DataController.getIssueURL() + "/because.mp3", "because");
		theirClaimAudio.loadSound(DataController.getCurrentIssue().counterReason.audioSrc, "reason"+DataController.getCurrentIssue().counterReason.id);
		
		paragraphArea.add(mSentences[0]);
		mCurrentSentence = 1;
		
		directionsHeader.setInnerText(DIRECTIONS_HEADERS[0]);
		directionsText.setInnerText(DIRECTIONS[0]);
		directionsAudio.setLocation(DIRECTIONS_AUDIO_NAMES[0]);
		
		paragraphArea.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				submitButton.setEnabled(checkComplete());
			}}, KeyUpEvent.getType());
		
		submitButton.setEnabled(false);
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}};
			
		backButton.setEnabled(true);
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onBack();
			}
		};
	}
	
	private HTMLPanel buildSentence(String text){
		HTMLPanel sentence = new HTMLPanel("span", "");
		
		String[] textArray = text.split(ItemChunk.REPLACEMENT_TOKEN_REGEX);
		text = "";
		for(int i=0, count=textArray.length; i<count; i++){
			HTMLPanel dbContent = new HTMLPanel("span",textArray[i]);
			dbContent.addStyleName(DistinguishableTextBox.DRAFTINGBOARDCLASSNAME);
			sentence.add(dbContent);
			if(i+1 != count){
				HTMLPanel blank = new HTMLPanel("span", "&nbsp;");
				blank.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
				blank.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
				blank.getElement().getStyle().setProperty("minWidth","6em");
				blank.getElement().getStyle().setProperty("minHeight","1em");
				blank.addStyleName("user-written");
				blank.getElement().setAttribute("contenteditable","true");
				blank.getElement().setAttribute("contentEditable", "true");
				blank.getElement().getStyle().setProperty("MozUserSelect", "text");
				blank.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
				blank.getElement().getStyle().setProperty("WebkitUserSelect", "text");
				blank.getElement().getStyle().setProperty("userSelect", "text");
				sentence.add(blank);
			}
		}
		
		sentence.add(new HTMLPanel("span"," "));
		
		return sentence;
	}
	
	private boolean checkComplete() {
		boolean slotsFilled = true;
		for(int i=0 ; i < mCurrentSentence ; i++){
			Iterator<Widget> children = mSentences[i].iterator();
			//items[i-1].customSentenceText = "";
			while(children.hasNext()){
				Widget element = children.next();
				if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains("user-written") &&
						element.getElement().getInnerText().replaceAll("\\W", "").length() == 0){
					slotsFilled = false;
				}
			}
		}
		return slotsFilled;
	}
	
	private void advanceToNextSentence(){
		Iterator<Widget> children = mSentences[mCurrentSentence-1].iterator();
		while(children.hasNext()){
			Widget element = children.next();
			if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains("user-written")){
				element.removeStyleName("user-written");
				element.getElement().setAttribute("contenteditable","inherit");
				element.getElement().setAttribute("contentEditable", "inherit");
				element.getElement().getStyle().clearProperty("minHeight");
				element.getElement().getStyle().clearProperty("minWidth");
				element.getElement().getStyle().clearProperty("MozUserSelect");
				element.getElement().getStyle().clearProperty("KhtmlUserSelect");
				element.getElement().getStyle().clearProperty("WebkitUserSelect");
				element.getElement().getStyle().clearProperty("userSelect");
			}
		}
		
		getParentPanel().sidebar.init(mItems);
		
		if(mCurrentSentence < 4){
			paragraphArea.add(mSentences[mCurrentSentence]);
			
			directionsHeader.setInnerText(DIRECTIONS_HEADERS[mCurrentSentence]);
			directionsText.setInnerText(DIRECTIONS[mCurrentSentence]);
			directionsAudio.setLocation(DIRECTIONS_AUDIO_NAMES[mCurrentSentence]);
			
			mCurrentSentence++;
			submitButton.setEnabled(false);
		} else {
			close();
		}
		submitButton.setEnabled(checkComplete());
	}
	
	
	private void revertToPreviousSentence(){
		GWT.log("reverting to previous sentence, " + mCurrentSentence);
		//Clear and add the last few sentence back in
		paragraphArea.clear();
		for(int i = 0; i < mCurrentSentence-1; i++)
			paragraphArea.add(mSentences[i]);
		
		
		mCurrentSentence--;
		directionsHeader.setInnerText(DIRECTIONS_HEADERS[mCurrentSentence-1]);
		directionsText.setInnerText(DIRECTIONS[mCurrentSentence-1]);
		
		//Make the previous sentence editable again
		GWT.log("Trying to make this sentence editable:" + mSentences[mCurrentSentence-1].getElement().getInnerHTML());
		Iterator<Widget> children = mSentences[mCurrentSentence-1].iterator();
		while(children.hasNext()){
			Widget element = children.next();
			if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains(DistinguishableTextBox.STUDENTCLASSNAME)){
				element.addStyleName("user-written");
				element.getElement().setAttribute("contenteditable","true");
				element.getElement().setAttribute("contentEditable", "true");
				element.getElement().getStyle().setProperty("minHeight","1em");
				element.getElement().getStyle().setProperty("minWidth","6em");
				element.getElement().getStyle().setProperty("MozUserSelect", "text");
				element.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
				element.getElement().getStyle().setProperty("WebkitUserSelect", "text");
				element.getElement().getStyle().setProperty("userSelect", "text"); 
			}
		}
		
		
		submitButton.setEnabled(checkComplete());
	}
	
	public String getParagraph(){
		String text = "";
		for(int i = 0; i < mSentences.length; i++)
			text += mSentences[i].getElement().getInnerHTML();
		return text;
	}
	
	protected void onSubmit(){
		advanceToNextSentence();
	}
	
	protected void onBack(){
		if(mCurrentSentence == 1){
			getParentPanel().close(this,true);
			return;
		}
		
		revertToPreviousSentence();
	}
	
	private void close(){
		getParentPanel().close(this, false);
	}
	

}
