package org.icivics.arg.ui.criticCrusher.client;

import java.util.ArrayList;
import java.util.Random;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;

public abstract class FragmentSelectionUI extends Subpanel<CriticCrusher> {

	
	@UiField
	ParagraphElement claimText;
	@UiField
	ParagraphElement reasonText;
	
	@UiField
	ParagraphElement paragraphText;
	
	@UiField
	SimplePanel popupHolder;
	
	@UiField
	LabelButton submitButton;
	
	protected TextPopup mPopup;
	protected boolean mCorrect = true;
	
	public FragmentSelectionUI(CriticCrusher parent) {
		super(parent);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();

		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		if (mPopup != null) {
			mPopup.clearHandler();
			mPopup.removeFromParent();
			mPopup = null;
		}
		submitButton.destroy();
		super.onDetach();
	}
	
	protected void initOptions(OptionToggle[] buttonArray, int[] indexArray, String[] dataArray, String[] audioArray, Element listHolder) {
		ArrayList<Integer> availableIndices = new ArrayList<Integer>(dataArray.length);
		while (availableIndices.size() < dataArray.length)
			availableIndices.add(new Integer(availableIndices.size()));
		Random rnd = new Random();
		for (int i = 0; i < dataArray.length; ++i) {
			indexArray[i] = availableIndices.remove(rnd.nextInt(availableIndices.size())).intValue();
			buttonArray[i] = new OptionToggle();
			buttonArray[i].setText(dataArray[indexArray[i]]);
			buttonArray[i].setAudio(audioArray[indexArray[i]]);
			getRoot().add(buttonArray[i], listHolder);
		}
	}
	
	protected boolean checkCorrect() {
		return true;
	}
	
	protected boolean checkComplete() {
		return true;
	}
	
	protected boolean checkParagraphMatch() {
		return true;
	}
	
	protected void feedbackPopup_onClose(TextPopup popup) {
		popup.clearHandler();
		if (mPopup == popup)
			mPopup = null;
	}
	
	protected void submitButton_onClick() {
		if (mPopup != null)
			return;
		
		if (checkCorrect()) {
			if (checkParagraphMatch()) {
				++getParentPanel().numCorrectSelections;
				submitButton.clickHandler = null;
				exit();
			} else {
				++getParentPanel().numSelectionFailures;
				mCorrect = false;
				
				GWT.log("Submit mismatched");
				mPopup = new TextPopup();
				mPopup.setTitle(getMismatchedFeedbackTitleText());
				mPopup.setBody(getMismatchedFeedbackBodyText());
				mPopup.setHandler(new TextPopup.PopupHandler() {
					@Override
					public void onClose(TextPopup popup) {
						feedbackPopup_onClose(popup);
					}
				});
				popupHolder.add(mPopup);
			}
		} else {
			++getParentPanel().numSelectionFailures;
			mCorrect = false;
			
			GWT.log("Submit incorrect");
			mPopup = new TextPopup();
			mPopup.setTitle(getIncorrectFeedbackTitleText());
			mPopup.setBody(getIncorrectFeedbackBodyText());
			mPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			popupHolder.add(mPopup);
		}
	}
	
	protected String getMismatchedFeedbackTitleText() {
		return "Mismatch";
	}
	protected String getMismatchedFeedbackBodyText() {
		return "Although your sentence makes sense, it's not the strongest choice for your paragraph. Please select again.";
	}
	
	protected String getIncorrectFeedbackTitleText() {
		return "Incorrect";
	}
	protected abstract String getIncorrectFeedbackBodyText();
	
	protected abstract void exit();
}
