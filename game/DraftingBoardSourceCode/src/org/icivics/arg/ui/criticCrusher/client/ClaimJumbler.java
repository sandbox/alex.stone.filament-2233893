package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ClaimJumbler extends Subpanel<CriticCrusher> {
	private static ClaimJumblerUiBinder uiBinder = GWT.create(ClaimJumblerUiBinder.class);
	interface ClaimJumblerUiBinder extends UiBinder<Widget, ClaimJumbler> {}

	@UiField
	HTMLPanel previousButton,
			  nextButton,
			  claimHolder,
			  questionText;
	@UiField
	SimplePanel popupHolder;
	
	@UiField
	LabelButton submitButton;

	@UiField
	AudioControl jumblePlayer;
	//private CriticCrusher parent;
	
	private String[] mClaimOptions;
	private String[] mClaimOptionAudio;
	private int mCorrectOption;
	private int mCurrentOption;
	
	public ClaimJumbler(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		questionText.getElement().setInnerText(DataController.getCurrentIssue().issue.shortText);
		
		Claim claim = DataController.getCurrentIssue().getOpposingClaim();
		
		mClaimOptions = new String[claim.debateJumbles.size() + 1];
		mClaimOptionAudio = new String[claim.debateJumbles.size() + 1];
		
		int i = 0;
		for(; i < claim.debateJumbles.size(); i++){
			mClaimOptions[i] = claim.debateJumbles.get(i);
			mClaimOptionAudio[i] = claim.debateJumbleAudio.get(i);
		}
		mClaimOptions[i] = claim.text;
		mClaimOptionAudio[i] = claim.audioSrc;
		
		for(int k = 0; k < mClaimOptions.length; k++){
			String temp = mClaimOptions[k];
			int j = (int) Math.floor(mClaimOptions.length * Math.random());
			mClaimOptions[k] = mClaimOptions[j];
			mClaimOptions[j] = temp;
		}
			
		for(int k = 0; k < mClaimOptions.length; k++){
			if(mClaimOptions[k].equals(claim.text))
				mCorrectOption = k;
		}
		
		for(int k = 0; k < mClaimOptions.length; k++)
			GWT.log("Jumble option "+k+":" + mClaimOptions[k]);
		GWT.log("Correct jumble option: " + mCorrectOption);
		
		mCurrentOption = 0;
		
		displayClaimText(mCurrentOption);
		
		//this.parent = parent;
	}
	
	@Override
	public void onAttach(){
		super.onAttach();
		
		previousButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onBackBtnClicked();
			}}, ClickEvent.getType());
		
		nextButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onNextBtnClicked();
			}}, ClickEvent.getType());
		
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}
		};
		
		ElementUtil.setTextScaling();
	}

	private void onNextBtnClicked(){
		mCurrentOption ++;
		mCurrentOption %= mClaimOptions.length;
		displayClaimText(mCurrentOption);
	}
	
	private void onBackBtnClicked(){
		mCurrentOption --;
		if(mCurrentOption < 0)
			mCurrentOption = mClaimOptions.length - 1;
		mCurrentOption %= mClaimOptions.length;
		displayClaimText(mCurrentOption);
	}
	
	private void displayClaimText(int index){
		claimHolder.getElement().setInnerText(mClaimOptions[index]);
		jumblePlayer.loadSound(mClaimOptionAudio[index]);
	}
	
	private void onSubmit(){
		if(mCurrentOption == mCorrectOption){
			GWT.log("Correct option selected, moving on");
			
			submitButton.clickHandler = null;
			close();
		} else {
			GWT.log("Incorrect option selected, a popup that tells you so appears");
			
			TextPopup incorrectPopup = new TextPopup();
			incorrectPopup.setTitle("Incorrect Claim Selected");
			incorrectPopup.setBody("The selected claim does not correctly describe the other side's position");
			incorrectPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			popupHolder.add(incorrectPopup);
			
		}
	}
	
	protected void feedbackPopup_onClose(TextPopup popup){
		if(popup != null)
			popup.clearHandler();
		popup = null;
	}
	
	private void close() {
		getParentPanel().close(this);
	}
}
