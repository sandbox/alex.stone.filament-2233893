package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.BasicButton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class OptionToggle extends BasicButton {
	private static OptionToggleUiBinder uiBinder = GWT.create(OptionToggleUiBinder.class);
	interface OptionToggleUiBinder extends UiBinder<Widget, OptionToggle> {}
	
	@UiField
	ParagraphElement text;
	
	@UiField
	AudioControl audioPlayer;
	
	private boolean mSelected = false;
	
	public OptionToggle() {
		super(uiBinder);
		initWidget(uiBinder.createAndBindUi(this));
		initState();
	}
	
	public void setAudio(String url){
		audioPlayer.loadSound(url);
	}
	
	public void setText(String v) {
		text.setInnerText(v);
	}
	
	public String getText(){
		return text.getInnerText();
	}
	
	public void setSelected(boolean v) {
		if (mSelected == v)
			return;
		mSelected = v;
		
		if (v) {
			getWidget().addStyleName("selected");
		} else {
			getWidget().removeStyleName("selected");
		}
	}
	
	protected void widget_onClick(ClickEvent event) {
		if (!mSelected)
			super.widget_onClick(event);
	}
}
