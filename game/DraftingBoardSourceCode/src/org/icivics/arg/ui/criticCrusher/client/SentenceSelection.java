package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.CriticCrusherData;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceSelection extends Subpanel<CriticCrusher> {

	private static SentenceSelectionUiBinder uiBinder = GWT.create(SentenceSelectionUiBinder.class);
	interface SentenceSelectionUiBinder extends UiBinder<Widget, SentenceSelection> {}

	@UiField
	HTMLPanel optionA, optionB, optionC;
	
	@UiField
	ParagraphElement sentenceA, sentenceB, sentenceC;
	
	HTMLPanel[] mOptionButtons;
	ParagraphElement[] mOptionTextHolders;
	String[] mOptions;
	
	@UiField
	LabelButton submitButton, backButton;
	
	@UiField
	SpanElement paragraphSoFar, paragraphEnd;
	
	public SentenceSelection(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		mOptionButtons = new HTMLPanel[3];
		mOptionButtons[0] = optionA;
		mOptionButtons[1] = optionB;
		mOptionButtons[2] = optionC;
		
		mOptionTextHolders = new ParagraphElement[3];
		mOptionTextHolders[0] = sentenceA;
		mOptionTextHolders[1] = sentenceB;
		mOptionTextHolders[2] = sentenceC;
		
		CriticCrusherData data = DataController.getCriticCrusherData(1);
		
		mOptions = new String[data.closingSentenceOptions.length];
		
		paragraphSoFar.setInnerHTML(DataController.getCriticCrusherUserData(1).customParagraph);
		
		for(int i=0; i<3; i++){
			final int index = i;
			mOptions[i] = data.closingSentenceOptions[i];
			
			mOptionTextHolders[i].setInnerText(mOptions[i]);
			
			mOptionButtons[i].addDomHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					optionClicked(index);
				}}, ClickEvent.getType());
		}
		
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}
		};
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onBack();
			}
		};
	}

	public String getParagraph(){
		String text = "";
		text = DataController.getCriticCrusherUserData(1).customParagraph;
		text += "<span class=\"" + DistinguishableTextBox.STUDENTSELECTEDCLASSNAME +"\">" + paragraphEnd.getInnerText() + "</span>";
		return text;
	}
	
	private void optionClicked(int index){
		for(int i = 0; i < 3; i++)
			mOptionButtons[i].removeStyleName("selected");
		mOptionButtons[index].addStyleName("selected");
		paragraphEnd.setInnerText(mOptions[index]);
		
		GWT.log("Selected sentence: " + mOptions[index]);
		
		//TODO: Set the user selected text thing
	}
	
	private void onSubmit(){
		getParentPanel().close(this, false);
	}
	
	private void onBack(){
		getParentPanel().close(this, true);
	}
}
