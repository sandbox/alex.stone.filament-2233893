package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceStarter extends Subpanel<CriticCrusher> {

	private static SentenceStarterUiBinder uiBinder = GWT.create(SentenceStarterUiBinder.class);
	interface SentenceStarterUiBinder extends UiBinder<Widget, SentenceStarter> {}

	@UiField
	ParagraphElement claimHolder;
	
	@UiField
	AudioControl theirClaimPlayer;
	
	@UiField
	DivElement paragraphWrap;
	
	@UiField
	LabelButton submitButton, backButton;
	
	@UiField
	HTMLPanel responseField;
	
	@UiField
	SpanElement staticLabel;
	
	public SentenceStarter(CriticCrusher parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		claimHolder.setInnerText(DataController.getCurrentIssue().getOpposingClaim().text);
		theirClaimPlayer.loadSound(DataController.getCurrentIssue().getOpposingClaim().audioSrc);
		staticLabel.getStyle().setFloat( com.google.gwt.dom.client.Style.Float.LEFT);
		
		submitButton.setEnabled(false);
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		
		responseField.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				submitButton.setEnabled(responseField.getElement().getInnerText().length() > 0);
				GWT.log("keyup" + event.getNativeKeyCode());
				ElementUtil.updateScrollbars(paragraphWrap);
			}},KeyUpEvent.getType());
		
		responseField.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
		responseField.getElement().getStyle().setProperty("minWidth","6em");
		responseField.getElement().getStyle().setProperty("minHeight","1em");
		responseField.getElement().setAttribute("contentEditable", "true");
		
		responseField.getElement().getStyle().setProperty("MozUserSelect", "text");
		responseField.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
		responseField.getElement().getStyle().setProperty("WebkitUserSelect", "text");
		responseField.getElement().getStyle().setProperty("userSelect", "text");
		
		ElementUtil.addScrollbars(paragraphWrap);
		ElementUtil.updateScrollbars(paragraphWrap);
	}

	protected void submitButton_onClick() {
		// TODO Auto-generated method stub
		backButton.clickHandler = null;
		submitButton.clickHandler = null;
		close();
		GWT.log("Submit button has been clicked");
		
	}
	
	protected void backButton_onClick() {
		backButton.clickHandler = null;
		submitButton.clickHandler = null;
		getParentPanel().close(this, true);
	}
	
	private void close(){
		DataController.getCurrentIssue().counterClaim.initialExplanation = responseField.getElement().getInnerText();
		getParentPanel().close(this, false);
	}

}
