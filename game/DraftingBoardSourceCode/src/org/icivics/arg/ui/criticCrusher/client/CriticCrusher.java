package org.icivics.arg.ui.criticCrusher.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.client.DataController.ResultHandler;
import org.icivics.arg.data.json.client.CriticCrusherProgress;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.CriticCrusherData;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.ui.claimCreator.client.Justification;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.shared.GWT;

public class CriticCrusher extends ModuleUI<CriticCrusher> {
	protected int numCorrectSelections = 0;
	protected int numSelectionFailures = 0;
	protected CriticCrusherProgress userData;
	
	protected EvidenceSidebar sidebar;
	
	public CriticCrusher(EvidenceSidebar hud) {
		this.sidebar = hud;
	}
	
	private void saveFragmentData() {
		userData.setCorrectFragmentSelections(numCorrectSelections);
		userData.setFragmentSelectionFailures(numSelectionFailures);
	}
	
	@Override
	protected void commitScore() {
		DataController.getCurrentIssue().scoreRebuttal = 8.0 / (8.0 + numSelectionFailures);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		userData = DataController.getModuleProgress().forCriticCrusher();
		openUI(getSavedUI());
		
		
	}
	
	private void showItems(){
		CriticCrusherData data = DataController.getCriticCrusherData(0);

		Item[] displayItems = new Item[2];
		
		displayItems[0] = data.item;
		displayItems[1] = data.rebuttalItem;
		
		sidebar.init(displayItems);
	}
	
	private Subpanel<CriticCrusher> getSavedUI() {
		int stepIndex = userData.getStepIndex();
		DataController.moduleStepIndex = stepIndex;
		
		if (stepIndex > 0) {
			numCorrectSelections = userData.getCorrectFragmentSelections();
			numSelectionFailures = userData.getFragmentSelectionFailures();
		}
		
		if(stepIndex >= 3){
			// Load the selected reason
			DataController.getCurrentIssue().counterReason = DataController.getCurrentIssue().getOpposingClaim().getReason(userData.getCriticCrushReasonID());
			
			DataController.initCriticCrusher(new ResultHandler() {
				@Override
				public void onResult(boolean success) {
					if (success) {
						DataController.getModuleProgress().forCriticCrusher().applyProgress();
						showItems();
					} else {
						LogUtil.log("Failed to load Critic Crusher data.  App terminated.");
						//TODO: throw up an error of some sort
					}
				}
			});
			
		}
		
		switch (stepIndex){
			case 0: return new ClaimJumbler(this);
			case 1: return new SentenceStarter(this);
			case 2: return new ReasonSelection(this);

			case 3: return new OpeningSentence(this);
			case 4: return new RebuttalStrengthener(this);
			case 5: return new RebuttalEvidence(this, 0);
			case 6: return new ClosingAssertion(this, 0);
			case 7: return new ViewParagraph(this, 0);
			
			case 8: return new SentenceCompletion(this);
			case 9: return new SentenceCompletion(this);
			
			case 10: return new ParagraphChecklist(this);

			case 11: return null;
			default:
				DataController.moduleStepIndex = 0;
				return new OpeningSentence(this);
		}
	}
	
	
	void close(ClaimJumbler ui){
		DataController.saveModuleProgress(1);
		transitionTo(new SentenceStarter(this));
	}
	
	void close(SentenceStarter ui, boolean backwards){
		if(!backwards){
			DataController.saveModuleProgress(2);
			// Save the supporting sentence?
			transitionTo(new ReasonSelection(this));	
		} else {
			DataController.saveModuleProgress(0);
			transitionTo(new ClaimJumbler(this));	
		}
	}
	
	void close(ReasonSelection ui, boolean backwards){
		
		if(!backwards){
			userData.setCriticCrushReasonID(ui.selectedReasonID);
			DataController.getCurrentIssue().counterReason = DataController.getCurrentIssue().getOpposingClaim().getReason(ui.selectedReasonID);
			DataController.saveModuleProgress(3);
			
			GWT.log("Selected reason ID: " + ui.selectedReasonID);
			
			TextPopup popup = new TextPopup();
			popup.setTitle("Practice Paragraph");
			popup.setBody("<p>Each sentence in the \"Critic Crusher\" paragraph has a very special function!</p><br><ul><li>WORK THROUGH the next few screens to construct your practice paragraph.</li><li>PAY ATTENTION to the purpose of each sentence.</li><li>BE PREPARED to write another Critic Crusher paragraph with less help!</li></ul>");
			
			
			final CriticCrusher ccr = this;
			
			popup.setHandler(new TextPopup.PopupHandler(){
				@Override
				public void onClose(TextPopup popup) {
					
					DataController.initCriticCrusher(new ResultHandler() {
						@Override
						public void onResult(boolean success) {
							if (success) {
								DataController.getModuleProgress().forCriticCrusher().applyProgress();
								
								showItems();
								transitionTo(new OpeningSentence(ccr));
							} else {
								LogUtil.log("Failed to load Critic Crusher data.  App terminated.");
								//TODO: throw up an error of some sort
							}
						}
					});
				}
			});
			
			ui.popupHolder.add(popup);
		} else {
			DataController.saveModuleProgress(1);
			transitionTo(new SentenceStarter(this));
		}
	}
	
	
	void close(OpeningSentence ui) {
		userData.setOpeningIndex(ui.paragraphIndex, DataController.getCriticCrusherUserData(ui.paragraphIndex).openingSelection);
		saveFragmentData();

		DataController.saveModuleProgress(4);
		transitionTo(new RebuttalStrengthener(this));
	}
	
	void close(RebuttalStrengthener ui) {
		userData.setStrengthenerIndex(ui.paragraphIndex, DataController.getCriticCrusherUserData(ui.paragraphIndex).strengthenerSelection);
		saveFragmentData();
		
		DataController.saveModuleProgress(5);
		transitionTo(new RebuttalEvidence(this, 0));
	}
	
	void close(RebuttalEvidence ui) {
		userData.setRebuttalIndex(ui.paragraphIndex, DataController.getCriticCrusherUserData(ui.paragraphIndex).rebuttalSelection);
		saveFragmentData();
		
		DataController.saveModuleProgress(6);
		transitionTo(new ClosingAssertion(this, ui.paragraphIndex));
	}
	
	void close(ClosingAssertion ui) {
		userData.setClosingIndex(ui.paragraphIndex, DataController.getCriticCrusherUserData(ui.paragraphIndex).closingSelection);
		saveFragmentData();
		
		if (ui.paragraphIndex == 0) {
			DataController.saveModuleProgress(7);
			DataController.getCriticCrusherUserData(0).customParagraph = DataController.getCriticCrusherUserData(0).buildParagraphText();
			transitionTo(new ViewParagraph(this, 0));
		} 
	}
	
	void close(ViewParagraph ui) {
		
		TextPopup popup = new TextPopup();
		popup.setTitle("Essay Paragraph");
		popup.setBody("Now that you have had a little practice, you'll be able to create your own paragraph using the same reason, but different evidence.");
		
		final CriticCrusher ccr = this;
		
		popup.setHandler(new TextPopup.PopupHandler(){
			@Override
			public void onClose(TextPopup popup) {
				transitionTo(new SentenceCompletion(ccr));
			}
		});
		
		ui.popupHolder.add(popup);
	}
	
	void close(SentenceCompletion ui, boolean backwards){
		
		if(!backwards){
			//TODO: Save progress!.
			DataController.getCriticCrusherUserData(1).customParagraph = ui.getParagraph();
	
			userData.setSelectedParagraphIndex(1);
			DataController.getCurrentIssue().claim.rebuttalParagraphOriginal = DataController.getCriticCrusherUserData(1).buildParagraphText();
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.rebuttalParagraphOriginal);
			
			GWT.log("Paragraph original: " + DataController.getCurrentIssue().claim.rebuttalParagraphOriginal);
			GWT.log("Paragraph final: " + DataController.getCurrentIssue().claim.rebuttalParagraph);
			
			transitionTo(new ParagraphChecklist(this));
		} else {
			transitionTo(new ViewParagraph(this, 0));
		}
	}
	
	void close(SentenceSelection ui, boolean backwards){
		//TODO: Save Progress!
		if(!backwards){
			DataController.getCriticCrusherUserData(1).customParagraph = ui.getParagraph();
			
			userData.setSelectedParagraphIndex(1);
			DataController.getCurrentIssue().claim.rebuttalParagraphOriginal = DataController.getCriticCrusherUserData(1).buildParagraphText();
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.rebuttalParagraphOriginal);
			
			GWT.log("Paragraph original: " + DataController.getCurrentIssue().claim.rebuttalParagraphOriginal);
			GWT.log("Paragraph final: " + DataController.getCurrentIssue().claim.rebuttalParagraph);
			
			transitionTo(new ParagraphChecklist(this));
		} else {
			//TODO: Save progress!
			transitionTo(new SentenceCompletion(this));
		}
	}
	
	
	void close(ParagraphChecklist ui) {
		userData.setFinalParagraph(DataController.getCurrentIssue().claim.rebuttalParagraph);
		
		DataController.saveModuleProgress(11);
		finish();
	}
	
	// OLDOLDOLD
}
