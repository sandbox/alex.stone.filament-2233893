package org.icivics.arg.ui.paragraphBuilder.client;

import org.icivics.arg.util.client.ElementUtil;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

public class ChecklistQuestion extends Composite {

	private static ChecklistQuestionUiBinder uiBinder = GWT.create(ChecklistQuestionUiBinder.class);
	interface ChecklistQuestionUiBinder extends UiBinder<Widget, ChecklistQuestion> {}

	public interface ChangeHandler{void onChange();}
	private ChangeHandler changeHandler;
	
	private HTMLPanel options[] = new HTMLPanel[3];
	private HandlerRegistration mOptionRegs[] = new HandlerRegistration[3];
	
	private int mSelectedOption = -1;
	
	private static String[][]   QUESTIONS={
		{"Did I explain with detail?","Did I use my own words?","Does it make sense?"},
		{"Did I explain with detail?","Did I use my own words?","Does it make sense?"}};
	private static String[][][] OPTIONS = {{
		{"I used as many details as I could, even when I didn't have to.","I used some details, but in some places I tried to write as little as possible.","I completed the paragraph with as few words as possible."},
		{"I put the ideas from the evidence into my own words unless I was quoting someone.","I put some ideas in my own words, but I copied some phrases exactly.","I copied almost everything from the evidence and did not use my own words."},
		{"When read aloud, my completed paragraph flows together and makes sense.","When read aloud, my paragraph is confusing in a few places where I added text.","When read aloud, my paragraph doesn't make very much sense."}
	},{
		{"I used several details to carefully explain how each piece of evidence supports my reason.","I used a few details from the evidence, but I didn't explain as much as I could have.","I used as little detail and explanation as I could get away with."},
		{"I put the ideas from the evidence into my own words unless I was quoting someone.","I put some ideas in my own words, but I copied some phrases exactly.","I copied almost everything from the evidence and did not use my own words."},
		{"When read aloud, the ideas in my paragraph flow together and make sense.","When read aloud, there are one or two confusing spots in my paragraph.","When read aloud, my paragraph doesn't sound right and is confusing."}
	}};

	@UiField
	HTMLPanel wrapper;
	@UiField
	ParagraphElement questionText;
	
	public ChecklistQuestion(int level, int question) {
		initWidget(uiBinder.createAndBindUi(this));
		
		LogUtil.log("Making a rubric question");
		
		if(level < 2 && question < 3)
			questionText.setInnerText(QUESTIONS[level][question]);
		
		
		for(int i=0; i<3; i++){
			final int index = i;
			if(level < 2 && question < 3)
				options[i] = createOption(OPTIONS[level][question][i], 3-i);
			else
				options[i] = createOption(OPTIONS[0][0][0], 3-i);
			mOptionRegs[i] = options[i].addDomHandler(new ClickHandler(){
				
				@Override
				public void onClick(ClickEvent event) {
					onOptionClicked(index);
				}}, ClickEvent.getType());
			wrapper.add(options[i]);
		}
	}

	protected void onOptionClicked(int index){
		for(int i=0; i<options.length; i++){
			options[i].removeStyleName("checked");
		}
		options[index].addStyleName("checked");
		mSelectedOption = index;
		if(changeHandler != null)
			changeHandler.onChange();
	}	
	
	@Override
	protected void onAttach(){
		super.onAttach();
		ElementUtil.setTextScaling();
	}
	
	@Override
	protected void onDetach(){
		super.onDetach();
		for(int i=0; i < mOptionRegs.length; i++){
			mOptionRegs[i].removeHandler();
		}
	}
	
	public int getSelectedOption(){
		return mSelectedOption;
	}
	
	public void addChangeHandler(ChangeHandler handler){
		changeHandler = handler;
	}
	
	private HTMLPanel createOption(String text, int score){
		HTMLPanel option = new HTMLPanel(" ");
		option.addStyleName("option");
		
		HTMLPanel checkmark = new HTMLPanel(" ");
		checkmark.addStyleName("checkmark");
		option.add(checkmark);
		
		HTMLPanel textWrap = new HTMLPanel("p", text);
		textWrap.addStyleName("text");
		option.add(textWrap);
		
		HTMLPanel scorePanel = new HTMLPanel("p" , Integer.toString(score));
		scorePanel.addStyleName("score");
		option.add(scorePanel);
		
		return option;
	}
	
}
