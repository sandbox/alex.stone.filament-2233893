package org.icivics.arg.ui.paragraphBuilder.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.EvidenceReferencePanel;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ParagraphChecklist extends Subpanel<ParagraphBuilder> {
	private static ParagraphChecklistUiBinder uiBinder = GWT.create(ParagraphChecklistUiBinder.class);
	interface ParagraphChecklistUiBinder extends UiBinder<Widget, ParagraphChecklist> {}
	
	@UiField
	SimplePanel panelHolder;
	
	@UiField
	LabelButton submitButton;
	@UiField
	LabelButton backButton;
	
	@UiField
	HTMLPanel paragraphText;
	
	@UiField
	HTMLPanel sideColumn;
	
	private HTMLPanel[] toggles = new HTMLPanel[4];
	private boolean[] toggleStates = {false, false, false, false};
	private HandlerRegistration[] toggleRegs;
	private HandlerRegistration[] itemRegs;
	private Reason reason;
	
	private EvidenceDetailPanel mPopup;
	
	public int paragraphIndex = 2;
	private int scaffoldLevel;
	
	private ChecklistQuestion mQuestions[] = new ChecklistQuestion[3];

	
	public ParagraphChecklist(ParagraphBuilder parent, int paragraphIndex) {
		super(parent);
		
		LogUtil.log("Creating a paragraph rubric");
		
		this.paragraphIndex = paragraphIndex;
		
		this.scaffoldLevel = getParentPanel().scaffoldLevel;
		
		this.reason = DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason;
		
		LogUtil.log("Reason text inserted");
		
		initWidget(uiBinder.createAndBindUi(this));
		
		LogUtil.log("uiBinder bound");
		
		//paragraphText.getElement().setAttribute("contenteditable","true");
		paragraphText.getElement().setInnerHTML(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
		
		LogUtil.log("Paragraph loaded");
		
		for(int i=0; i <3; i++){
			if(scaffoldLevel > 0)
			sideColumn.add(mQuestions[i] = new ChecklistQuestion(scaffoldLevel-1, i));
		}
	}

	@Override
	protected void onAttach() {
		super.onAttach();
		
		for(int i=0; i<3; i++)
			mQuestions[i].addChangeHandler(new ChecklistQuestion.ChangeHandler(){
				@Override
				public void onChange(){
					submitButton.setEnabled(checkComplete());
				}
			});
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		getParentPanel().hud.init(claim.reasons[paragraphIndex].reason.items);
		
		addScrollbars();
	}
	
	@Override
	protected void onDetach() {
		clearListeners();
		submitButton.destroy();
		this.reason = null;
		super.onDetach();
	}
	
	private void clearListeners() {
		if (toggleRegs != null)
			for (int i = 0; i < toggleRegs.length; ++i)
				toggleRegs[i].removeHandler();
		toggleRegs = null;
		
		if (itemRegs != null)
			for (int i = 0; i < itemRegs.length; ++i)
				itemRegs[i].removeHandler();
		itemRegs = null;
		
		submitButton.clickHandler = null;
	}

	private boolean checkComplete() {
		for (int i = 0; i < mQuestions.length; ++i)
			if (mQuestions[i].getSelectedOption() == -1)
				return false;
		return true;
	}
	
	private void evidence_onClick(int index) {
		getParentPanel().hud.showItem(reason.items[index]);
	}
	
	protected void itemPopup_onClose() {
		if (mPopup == null)
			return;
		
		mPopup.removeFromParent();
		mPopup.destroy();
		mPopup = null;
	}

	private void toggle_onClick(int buttonIndex) {
		if (mPopup != null)
			return;
		
		if (toggleStates[buttonIndex] = !toggleStates[buttonIndex]) {
			toggles[buttonIndex].addStyleName("checked");
			submitButton.setEnabled(checkComplete());
		} else {
			toggles[buttonIndex].removeStyleName("checked");
			submitButton.setEnabled(false);
		}
	}

	private void submitButton_onClick() {
		GWT.log("Submit clicked");
		clearListeners();
		
		// I believe this is pointless?
		DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] = paragraphText.getElement().getInnerHTML();
		getParentPanel().close(this, true);
	}
	
	private void backButton_onClick(){
		GWT.log("Back button clicked");
		clearListeners();
		
		//DataController.moduleStepIndex -= 2;
		getParentPanel().close(this,false);
	}
	
	private native void addScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true
			});
	}-*/;
}
