package org.icivics.arg.ui.paragraphBuilder.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceOrderingListItem extends Composite {
	private static SentenceOrderingListItemUiBinder uiBinder = GWT.create(SentenceOrderingListItemUiBinder.class);
	interface SentenceOrderingListItemUiBinder extends UiBinder<Widget, SentenceOrderingListItem> {}
	
	@UiField
	ParagraphElement label;
	@UiField
	HTMLPanel upButton;
	@UiField
	HTMLPanel downButton;
	
	public SentenceOrderingListItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setLabel(String v) {
		label.setInnerText(v);
	}
	
	public void disableUpButton() {
		upButton.removeStyleName("upArrow");
		upButton.removeStyleName("upArrowDisabled");
		upButton.addStyleName("upArrowDisabled");
	}
	
	public void disableDownButton() {
		downButton.removeStyleName("downArrow");
		downButton.removeStyleName("downArrowDisabled");
		downButton.addStyleName("downArrowDisabled");
	}
}
