package org.icivics.arg.ui.paragraphBuilder.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.RewritePopup;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.WidgetCollection;

public class AddEvidence extends Subpanel<ParagraphBuilder> {
	private static AddEvidenceUiBinder uiBinder = GWT.create(AddEvidenceUiBinder.class);
	interface AddEvidenceUiBinder extends UiBinder<Widget, AddEvidence> {}
	
	@UiField
	SimplePanel popupHolder;
	AddEvidenceConfirm popup;
	
	@UiField
	HTMLPanel sideColumn;
	
	@UiField
	AudioConcatenatedControl paragraphAudio;
	
	//@UiField
	//ParagraphElement titleText;
	@UiField
	HTMLPanel paragraphWrap;
	@UiField
	ParagraphElement paragraphText;
	
	@UiField
	LabelButton submitButton, backButton;
	
	public int paragraphIndex;
	private Item[] items;
	private Item[] selectedItems;
	private boolean[][] chunksUsed = new boolean[2][];
	private int[] selectedChunkIndices = {-1, -1};
	private boolean mSentenceActive[] = new boolean[3];
	
	private AddEvidenceListItem[] mListItems = new AddEvidenceListItem[3];
	//private HTMLPanel[] mSentenceLabels = new HTMLPanel[3];
	private ArrayList<HandlerRegistration> mLabelRegs;
	private HTMLPanel[] mSentences = new HTMLPanel[3];
	
	private AddEvidencePopup mPopup;
	private RewritePopup mRewritePopup;
	
	public AddEvidence(ParagraphBuilder parent, final int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		initWidget(uiBinder.createAndBindUi(this));
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		items = new Item[claim.reasons[paragraphIndex].reason.items.length];
		if (claim.paragraphsSwapped[paragraphIndex]) {
			items[0] = claim.reasons[paragraphIndex].reason.items[1];
			items[1] = claim.reasons[paragraphIndex].reason.items[0];
		} else {
			items[0] = claim.reasons[paragraphIndex].reason.items[0];
			items[1] = claim.reasons[paragraphIndex].reason.items[1];
		}
		
		getParentPanel().hud.init(items);
		
		selectedItems = new Item[claim.reasons[paragraphIndex].reason.items.length];
		
		mLabelRegs = new ArrayList<HandlerRegistration>(3 * 2);
		
		for(int i=0; i < 3; i++){
			AddEvidenceListItem item = new AddEvidenceListItem(i>0);
			item.label.setInnerText((i>0) ? items[i-1].name:"Reason: " + claim.reasons[paragraphIndex].reason.text);
			sideColumn.add(item);
			
			mSentences[i] = buildSentence(i);
			paragraphText.appendChild(mSentences[i].getElement());
			
			mListItems[i] = item;
			mSentenceActive[i] = i<2;
			
			final int itemIndex = i;
			mLabelRegs.add(item.addDomHandler(new MouseOverHandler() {
				@Override
				public void onMouseOver(MouseOverEvent event) {
					label_onMouseOver(itemIndex);
				}
			}, MouseOverEvent.getType()));
			mLabelRegs.add(item.addDomHandler(new MouseOutHandler() {
				@Override
				public void onMouseOut(MouseOutEvent event) {
					label_onMouseOut(itemIndex);
				}
			}, MouseOutEvent.getType()));
			
			if(i > 0){
				mListItems[i].setItem(items[i-1]);

				final int index = i;
				mListItems[i].setClickHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						onItemSelected(items[index-1], mListItems[index], index);
					}
				});
			}
			
		}
		
		setAudio();
	}
	
	private HTMLPanel buildSentence(int index){
		//TODO: Trim down to evidence adding only
		HTMLPanel sentence;
		sentence = new HTMLPanel("span", getSentenceHTML(index));
		return sentence;
	}
	
	private void onItemSelected(final Item curItem, final AddEvidenceListItem currentItem, final int index){
		getParentPanel().hud.onItemPanelClose();
		
		getParentPanel().hud.setChunkHandler(new EvidenceSidebar.SelectChunkHandler() {
			@Override
			public void onSelectChunk(Item item, int chunkIndex) {
				GWT.log("Fill text: " + item.chunks.get(chunkIndex).sentenceText);
				GWT.log("Selected text: " + item.chunks.get(chunkIndex).itemText);
				chunkSelected(curItem,index,chunkIndex,null);	
			}

			@Override
			public void onSelectChunk(Item item, String chunkText) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		getParentPanel().hud.showItem(curItem,true);
	}
	

	private String getSentenceHTML(int index) {
		RegExp regx = RegExp.compile(ItemChunk.REPLACEMENT_TOKEN_REGEX,"");
		switch (index) {
			case 0: return "<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" + DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text  + " </span>";
			case 1: return "<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" + regx.replace(items[0].sentenceText[0] , selectedChunkIndices[0] == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : "</span><span class=\"player-selected\">" + selectedItems[0].chunks.get(selectedChunkIndices[0]).sentenceText  + "</span><span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" ) + " " + "</span>";
				//items[0].sentenceText.replaceFirst(ItemChunk.REPLACEMENT_TOKEN_REGEX, selectedChunkIndices[0] == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : selectedItems[0].chunks.get(selectedChunkIndices[0]).sentenceText);
			case 2: return "<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" + regx.replace( items[1].sentenceText[0], selectedChunkIndices[1] == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : "</span><span class=\"player-selected\">" + selectedItems[1].chunks.get(selectedChunkIndices[1]).sentenceText + "</span><span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" ) + " " + "</span>";
				//return items[1].sentenceText.replaceFirst(ItemChunk.REPLACEMENT_TOKEN_REGEX, selectedChunkIndices[1] == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : selectedItems[1].chunks.get(selectedChunkIndices[1]).sentenceText);
			default:
				GWT.log("Invalid index: " + index);
				return null;
		}
		
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		getParentPanel().hud.onItemPanelClose();
		getParentPanel().hud.setEvidenceHandler(new EvidenceSidebar.SelectItemHandler() {
			public void onSelectItem(Item item) {
				GWT.log("Item selected");
				useEvidenceItem(item);
			}
		});
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		paragraphWrap.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				submitButton.setEnabled(checkComplete());
				updateScrollbars();
			}},KeyUpEvent.getType());
		
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		checkComplete();
		addScrollbars();
	}
	
	protected void useEvidenceItem(final Item item) {
		if(item!= null){
			for(int i=1; i < 3; i++){
				if(mListItems[i].itemIsSet() && mListItems[i].getItem() == item){
					final int index =i;
					onItemSelected(items[index-1], mListItems[index], index);
				}
			}
		}else{
			GWT.log("Exit from evidence selection");
		}
	}
	
	protected void chunkSelected(Item item,final int itemIndex ,int selectedChunkIndex, String text){
		selectedItems[itemIndex - 1] = item;
		selectedChunkIndices[itemIndex - 1] = selectedChunkIndex;
		

		mSentences[itemIndex].getElement().setInnerHTML(getSentenceHTML(itemIndex) + " ");
		mListItems[itemIndex].setSentence(selectedChunkIndex);
		mListItems[itemIndex].setClearHandler(new AddEvidenceListItem.ClearHandler() {
			@Override
			public void onClear() {
			
				selectedChunkIndices[itemIndex-1] = -1;
				GWT.log("Clearing a sentence chunk from the whassit " + buildSentence(itemIndex).getElement().getInnerHTML());
				
				mSentences[itemIndex].getElement().setInnerHTML(buildSentence(itemIndex).getElement().getInnerHTML()); 
				
				//mListItems[itemIndex].setSentence(itemIndex);
			}
		});
		
		setAudio();
		
		submitButton.setEnabled(checkComplete());
		updateScrollbars();
	}
	
	@Override
	protected void onDetach() {
		for (int i = 0; i < mListItems.length; ++i)
			mListItems[i].destroy();
		for (int i = 0, count = mLabelRegs.size(); i < count; ++i)
			mLabelRegs.get(i).removeHandler();
		mLabelRegs.clear();
		getParentPanel().hud.setEvidenceHandler(null);
		submitButton.destroy();
		super.onDetach();
	}
	
	protected void label_onMouseOver(int labelIndex) {
		mSentences[labelIndex].addStyleName("highlight");
	}
	
	protected void label_onMouseOut(int labelIndex) {
		mSentences[labelIndex].removeStyleName("highlight");
	}
	
	private void setAudio(){
		paragraphAudio.clear();
		
		Reason myReason = DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason;
		paragraphAudio.loadSound(myReason.audioSrc, "reason" + myReason.id);
		
		for(int i=0; i < 2; i++){
			paragraphAudio.loadSound(items[i].initialTextStartAudioURL, "item"+items[i].id+"start");
			if(selectedChunkIndices[i] != -1)
				paragraphAudio.loadSound(items[i].chunks.get(selectedChunkIndices[i]).audioSrc, "item"+items[i].id+"middle" + selectedChunkIndices[i]);
			else
				paragraphAudio.loadSound(DataController.getIssueURL() + "twoSecondsOfNothing.mp3", "blank");
			paragraphAudio.loadSound(items[i].initialTextEndAudioURL, "item"+items[i].id+"end");
		}
		
	}
	
	private void backButton_onClick(){
		getParentPanel().close(this, true);
	}
	
	private void submitButton_onClick() {
		if (mPopup != null || mRewritePopup != null)
			return;
		
		
		
		for(int i = 1; i< mSentences.length; i++ ){
			if(selectedChunkIndices[i-1] != items[i-1].correctChunkIndex || selectedItems[i-1] != items[i-1]){
				Element el = mSentences[i].getElement().getFirstChildElement().getNextSiblingElement();
				if(el != null){
					el.addClassName("incorrect");
					el.removeClassName("correct");
				}
				mListItems[i].mark(false);
			}else{
				Element el = mSentences[i].getElement().getFirstChildElement().getNextSiblingElement();
				if(el != null){
					el.addClassName("correct");
					el.removeClassName("incorrect");
				}
				mListItems[i].mark(true);
			}
		}
		
		GWT.log("Submit for evidence adding thing");
		if(checkCorrect()){
			submitButton.clickHandler = null;
		
			// save the paragraph text
			for(int i = 1; i < mSentences.length; i++){
				items[i-1].customSentenceText = "";
				Element el = mSentences[i].getElement().getFirstChildElement().getNextSiblingElement();
				if(el != null){
					el.removeClassName("correct");
					el.removeClassName("incorrect");
				}
				
				/*
				// Get the sentence's children and strip out the extra html
				Iterator<Widget> children = mSentences[i].iterator();
				
				while(children.hasNext()){
					Widget element = children.next();
					element.getElement().setAttribute("contenteditable","inherit");
					element.removeStyleName("cloze-slot");	
					GWT.log("trying to remove the correct class from a span");
					element.removeStyleName("correct");	
					element.getElement().setInnerHTML(element.getElement().getInnerText());
				}
				*/
				items[i-1].customSentenceText += mSentences[i].getElement().getInnerHTML();
				GWT.log(items[i-1].customSentenceText);
			}
			

			getParentPanel().hud.onItemPanelClose();
			getParentPanel().close(this, false);
		} else {
			GWT.log("Not entirely correct");
		}
	}

	private boolean checkComplete() {
		
		for (int i = 0; i < items.length; ++i)
			if(selectedChunkIndices[i] == -1)
				return false;
		return true;
		
	}
	
	private boolean checkCorrect() {
		
		for(int i=0; i < selectedChunkIndices.length; i++)
			if(selectedChunkIndices[i] != items[i].correctChunkIndex || selectedItems[i] != items[i])
				return false;
		return true;
	}
	
	private native void addScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true,
				useKeyboard:false
			});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar('update');
	}-*/;
	
}
