package org.icivics.arg.ui.paragraphBuilder.client;

import org.icivics.arg.data.model.client.Item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class AddEvidenceListItem extends Composite {
	private static AddEvidenceListItemUiBinder uiBinder = GWT.create(AddEvidenceListItemUiBinder.class);
	interface AddEvidenceListItemUiBinder extends UiBinder<Widget, AddEvidenceListItem> {}
	
	@UiField
	ParagraphElement label;
	@UiField
	HTMLPanel evidenceDiv;
	/*
	@UiField
	HTMLPanel innerEvidenceDiv;
	*/
	@UiField
	HTMLPanel root;
	
	
	HTMLPanel innerEvidenceDiv;
	
	@UiField
	ParagraphElement evidenceTitle,evidenceSummary;
	@UiField
	DivElement icon;
	/*
	@UiField
	DivElement sentenceDiv;
	*/
	
	HTMLPanel sentenceDivs[];
	int sentenceIndex = -1;
	
	HTMLPanel removeBtns[];
	HTMLPanel feedback;
	
	public interface ClearHandler {void onClear();}
	ClearHandler clearHandler;
	
	private HandlerRegistration mButtonClickReg;
	private ClickHandler buttonClickHandler;
	private Item mItem;
	private int mNumSlots;
	public boolean sentenceSet = false;

	
	public AddEvidenceListItem(Boolean evidence) {
		this(evidence ? 1 : 0);
	}
	
	public AddEvidenceListItem(int numSlots){
		initWidget(uiBinder.createAndBindUi(this));
		
		mNumSlots = numSlots;
		
		sentenceDivs = new HTMLPanel[numSlots];
		removeBtns = new HTMLPanel[numSlots];
		
		if(numSlots == 0){
			//evidenceDiv.removeFromParent();
			//sentenceDiv.removeFromParent();
			icon.removeFromParent();
			evidenceTitle.removeFromParent();
			evidenceSummary.removeFromParent();
			evidenceDiv.addStyleName("disabled");
			
		} else {
			label.removeFromParent();
			for(int i = 0; i < mNumSlots; i++){
				HTMLPanel sentenceSlot = new HTMLPanel("");
				sentenceSlot.setStyleName("linked-sentence");
				
				HTMLPanel innerSlot = new HTMLPanel("");
				innerSlot.setStyleName("text");
				
				sentenceSlot.add(innerSlot);
				
				sentenceDivs[i] = innerSlot;

				root.add(sentenceSlot);
				
				removeBtns[i] = new HTMLPanel("");
			}
			innerEvidenceDiv = sentenceDivs[0];
		}
		
		setClickHandler(null);
		mItem = null;
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
	}
	
	@Override
	protected void onDetach() {
		if (mButtonClickReg != null) {
			mButtonClickReg.removeHandler();
			mButtonClickReg = null;
		}
		super.onDetach();
	}
	
	public void setClickHandler(ClickHandler handler) {
		if(handler != null){
			mButtonClickReg = evidenceDiv.addDomHandler(handler, ClickEvent.getType());
		}else if(mButtonClickReg != null){
			mButtonClickReg.removeHandler();
		}
	}
	
	public void setClearHandler(ClearHandler ch){
		clearHandler = ch;
	}
	
	public int getNumSlots(){
		return mNumSlots;
	}
	
	public void setSentence(int index){
		setSentence(mItem.chunks.get(index).itemText);
		/*
		clearHandler = null;
		clearSentence(0);
		
		//This will ensure the itemtext is used consistently, so that double dollar signs aren't an issue
		RegExp regex = RegExp.compile("text","");
		
		innerEvidenceDiv.getElement().setInnerText(regex.replace("text" , mItem.chunks.get(index).itemText));
		
		removeBtns[0] = new HTMLPanel("");
		removeBtns[0].addStyleName("remove-btn");
		innerEvidenceDiv.add(removeBtns[0]);
		
		sentenceSet = true;
		
		removeBtns[0].addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				clearSentence(0);
				event.stopPropagation();
			}}, ClickEvent.getType());
		*/
	}
	
	public void setSentence(String sentenceText){
		for(int i =0; i < sentenceDivs.length; i++){
			if(sentenceText.equals(sentenceDivs[i].getElement().getInnerText()))
				return;
		}
		
		
		sentenceIndex++;
		
		
		
		//sentenceIndex = 0;
		for(int i = 0; i < sentenceDivs.length; i ++)
		{
			if(sentenceDivs[i].getElement().getInnerText().length() == 0){
				sentenceIndex = i;
				i = sentenceDivs.length;
			}
		}
		
		if(sentenceIndex >= sentenceDivs.length)
			sentenceIndex = 0;
		
		clearHandler = null;
		clearSentence(sentenceIndex);
		
		//This will ensure the itemtext is used consistently, so that double dollar signs aren't an issue
		RegExp regex = RegExp.compile("text","");
		
		sentenceDivs[sentenceIndex].getElement().setInnerText(regex.replace("text" , sentenceText));
		
		removeBtns[sentenceIndex] = new HTMLPanel("");
		removeBtns[sentenceIndex].addStyleName("remove-btn");
		sentenceDivs[sentenceIndex].add(removeBtns[sentenceIndex]);
		
		sentenceSet = true;
		
		final int index = sentenceIndex;
		
		removeBtns[sentenceIndex].addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				clearSentence(index);
				event.stopPropagation();
			}}, ClickEvent.getType());
	}
	
	public void clearSentence(int i){
		if(removeBtns[i] != null){
			removeBtns[i].removeFromParent();
			removeBtns[i] = null;
		}
		if(feedback != null && feedback.isAttached()){
			feedback.removeFromParent();
			feedback = null;
		}
		
		sentenceDivs[i].getElement().setInnerText("");
		if(clearHandler != null){
			clearHandler.onClear();
			clearHandler = null;
		}
	}
	
	public void setItem(Item item){
		mItem = item;
		evidenceTitle.setInnerText(item.name);
		evidenceSummary.setInnerText(item.description);
		icon.addClassName("type" + item.type);
	}
	
	public void clearItem(){
		/*
		if(mItem != null)
			icon.removeClassName("type" + mItem.type);
		mItem = null;
		evidenceTitle.setInnerText("");
		evidenceSummary.setInnerText("");
		*/
		GWT.log("Item cleared?!");
		
	}
	
	public Boolean itemIsSet(){
		return mItem != null;
	};
	
	public void destroy() {
		if (mButtonClickReg != null) {
			mButtonClickReg.removeHandler();
			mButtonClickReg = null;
		}
	}

	public Item getItem() {
		return mItem;
	}

	public void addClearHandler(ClearHandler handler) {
		clearHandler = handler;
	}

	public void mark(boolean correct) {
		feedback = new HTMLPanel("");
		feedback.addStyleName("feedback");
		if(correct){
			feedback.addStyleName("correct");
			feedback.getElement().setInnerText("CORRECT");
		}else {
			feedback.addStyleName("incorrect");
			feedback.getElement().setInnerText("INCORRECT");
		}
		innerEvidenceDiv.add(feedback);
	}

}
