package org.icivics.arg.ui.paragraphBuilder.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class ParagraphEdit extends Subpanel<ParagraphBuilder> {
	private static ParagraphEditUiBinder uiBinder = GWT.create(ParagraphEditUiBinder.class);
	interface ParagraphEditUiBinder extends UiBinder<Widget, ParagraphEdit> {}
	
	@UiField
	DivElement titleText;
	@UiField
	AudioControl directionsAudio;
	@UiField
	DivElement labelText;
	@UiField
	DistinguishableTextBox paragraphField;
	@UiField
	LabelButton submitButton;
	
	public int paragraphIndex;
	public int scaffoldLevel;

	public ParagraphEdit(ParagraphBuilder parent, int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		
		scaffoldLevel = getParentPanel().scaffoldLevel;
		
		initWidget(uiBinder.createAndBindUi(this));
		
		switch(scaffoldLevel){
			case 0:
				labelText.setInnerText("PERSONALIZE YOUR PARAGRAPH");
				titleText.setInnerHTML("RE-READ the paragraph.CHANGE the sentences if you want them to be different or ADD your own thoughts about the evidence. CORRECT anything that doesn't look right.");
				directionsAudio.setLocation("Directions_PC1_personalize.mp3");
				break;
			case 1:
				labelText.setInnerText("EDIT YOUR PARAGRAPH");
				titleText.setInnerHTML("RE-READ your paragraph. CHANGE the sentences if you want them to be different or ADD more thoughts about the evidence. CORRECT anything that doesn't look right.");
				directionsAudio.setLocation("Directions_PC2_edit.mp3");
				break;
			case 2:
				labelText.setInnerText("EDIT YOUR PARAGRAPH");
				titleText.setInnerHTML("<ul><li>RE-READ your paragraph.</li><li>CHANGE the sentences if you want them to be different or ADD more thoughts about the evidence.</li><li>CORRECT anything that doesn't look right.</li></ul>");
				directionsAudio.setLocation("Directions_PC2_edit.mp3");
				break;
			default:
				titleText.setInnerText("Body Paragraph #" + (paragraphIndex + 1) + " - Edit your Paragraph");
				break;
		}
		
		
		
		paragraphField.setInnerText(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
		if(scaffoldLevel == 2){
			paragraphField.setInnerText(
						"<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME + "\">" +
						DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text +
						"</span>"
					);
		}
		
		if(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] != null)
			paragraphField.setInnerText(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		getParentPanel().hud.init(claim.reasons[paragraphIndex].reason.items);
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		super.onDetach();
	}
	
	private void submitButton_onClick() {
		GWT.log("Submit clicked");
		submitButton.clickHandler = null;
		
		DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] = paragraphField.getValue();
		getParentPanel().close(this);
	}
}
