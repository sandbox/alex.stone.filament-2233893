package org.icivics.arg.ui.paragraphBuilder.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Glossary;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.TransitionWord;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.DropdownMenu;
import org.icivics.arg.ui.common.client.GlossariedTextPanel;
import org.icivics.arg.ui.common.client.GlossaryPopupContent;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.util.client.ElementUtil;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;;

public class AddTransitions extends Subpanel<ParagraphBuilder> {
	private static AddTransitionsUiBinder uiBinder = GWT.create(AddTransitionsUiBinder.class);
	interface AddTransitionsUiBinder extends UiBinder<Widget, AddTransitions> {}
	
	@UiField
	GlossariedTextPanel titleText;
	@UiField
	AudioControl directionsAudio;
	
	@UiField
	HTMLPanel paragraphText;
	@UiField
	HTMLPanel sideColumn;
	
	@UiField
	LabelButton submitButton, backButton;
	
	private HTMLPanel[] mSentences = new HTMLPanel[3];
	private HTMLPanel[] mTransitionSlots = new HTMLPanel[2];
	private HTMLPanel[] mSlotButtons = new HTMLPanel[2];
	private HandlerRegistration[] mSlotButtonHandlerRegs = new HandlerRegistration[2];
	private Boolean[] mSufficientText = {false, false};
	
	private HTMLPanel[] mLabelHolders = new HTMLPanel[3];
	private DropdownMenu[] mSelectors = new DropdownMenu[2];
	private ArrayList<HandlerRegistration> mButtonRegs;
	
	public int paragraphIndex;
	public int scaffoldLevel;
	private Item[] items;
	
	private Element helpBoxLastTgt;
	private Timer mouseOverTimer;
	
	DistinguishableTextBox freeWriteBox;
	
	public AddTransitions(ParagraphBuilder parent, int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		
		LogUtil.log("Starting to make the add transitions screen");
		
		this.scaffoldLevel = getParentPanel().scaffoldLevel;
		
		initWidget(uiBinder.createAndBindUi(this));
		
		LogUtil.log("Ui binder binded bound whatever");
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		items = new Item[claim.reasons[paragraphIndex].reason.items.length];
		if (claim.paragraphsSwapped[paragraphIndex]) {
			items[0] = claim.reasons[paragraphIndex].reason.items[1];
			items[1] = claim.reasons[paragraphIndex].reason.items[0];
		} else {
			items[0] = claim.reasons[paragraphIndex].reason.items[0];
			items[1] = claim.reasons[paragraphIndex].reason.items[1];
		}
		
		LogUtil.log("Starting to generate the left hand sidebar thing");
		
		List<TransitionWord> transitions = DataController.getTransitions();
		Iterator<String> types = TransitionWord.types.keySet().iterator();
		while(types.hasNext()){
			final String t = types.next();
			HTMLPanel typePanel = new HTMLPanel(" ");
			HTMLPanel typeLabel = new HTMLPanel(t);
			final String helpText = TransitionWord.types.get(t);
			if(helpText != null){
				//This type has help text, add a mouse-over listener
				typeLabel.addDomHandler(new MouseOverHandler(){
					@Override
					public void onMouseOver(MouseOverEvent event) {
						mouseOverHelpfulText(t, helpText, event);
					}}, MouseOverEvent.getType());
				typeLabel.addDomHandler(new MouseOutHandler(){
					@Override
					public void onMouseOut(MouseOutEvent event) {
						mouseOutOfHelpfulText();
					}}, MouseOutEvent.getType());
				typeLabel.addDomHandler(new MouseOutHandler(){
					@Override
					public void onMouseOut(MouseOutEvent event) {
						mouseOutOfHelpfulText();
					}}, MouseOutEvent.getType());
				typeLabel.addStyleName("glossaryWord");
			}
			typePanel.add(typeLabel);
			sideColumn.add(typePanel);
			for(int i=0,count = transitions.size(); i<count; i++){
				if(transitions.get(i).getType().equals(t)){
					HTMLPanel transitionWord = new HTMLPanel("p",transitions.get(i).getWord());
					final String word = transitions.get(i).getWord();
					final String helpTextWord = transitions.get(i).getDescription() + "\n" + transitions.get(i).getExamples();
					transitionWord.addStyleName("glossaryWord");
					transitionWord.addDomHandler(new MouseOverHandler(){
						@Override
						public void onMouseOver(MouseOverEvent event) {
							mouseOverHelpfulText(word, helpTextWord, event);
						}}, MouseOverEvent.getType());
					typePanel.add(transitionWord);
					transitionWord.addDomHandler(new ClickHandler(){

						@Override
						public void onClick(ClickEvent event) {
							Element tgt = Element.as(event.getNativeEvent().getEventTarget());
							GWT.log("Transition word clicked");
							displayHelpfulText(word, helpTextWord, tgt);
							
						}}, ClickEvent.getType());
					transitionWord.addDomHandler(new MouseOutHandler(){
						@Override
						public void onMouseOut(MouseOutEvent event) {
							mouseOutOfHelpfulText();
						}}, MouseOutEvent.getType());
				}
			}
		}
		
		LogUtil.log("Made the left hand sidebar thing, starting to make the paragraph");
		
		mButtonRegs = new ArrayList<HandlerRegistration>();
		
		// Construct the paragraph
		if(scaffoldLevel < 2){
			for(int i=0; i<3; i++){
				
				mSentences[i] = new HTMLPanel("span", getSentenceText(i));
				if(i>0){
					HTMLPanel container = new HTMLPanel("span","&nbsp;");
					mTransitionSlots[i-1] = new HTMLPanel("span","&nbsp;");
					mTransitionSlots[i-1].getElement().setAttribute("contentEditable", "true");
					mTransitionSlots[i-1].getElement().getStyle().setProperty("MozUserSelect", "text");
					mTransitionSlots[i-1].getElement().getStyle().setProperty("KhtmlUserSelect", "text");
					mTransitionSlots[i-1].getElement().getStyle().setProperty("WebkitUserSelect", "text");
					mTransitionSlots[i-1].getElement().getStyle().setProperty("userSelect", "text");
					container.addStyleName("transition-slot");
					container.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
					container.add(mTransitionSlots[i-1]);
					
					addPasteHandler(mTransitionSlots[i-1].getElement().getParentNode());
					
					mSlotButtons[i-1] = new HTMLPanel(" ");
					mSlotButtons[i-1] .addStyleName("remove-btn");
					
					container.add(mSlotButtons[i-1]);
					paragraphText.add(container);
				}
				paragraphText.add(mSentences[i]);
			}
			
			titleText.setInnerText("DECIDE which transitions you will use from the list and TYPE them into your paragraph. If there is a spot where you don't want to use a transition, CLICK the x in the box to remove it.");
			directionsAudio.setLocation("Directions_PC1_2_connect.mp3");
			
		} else {
			titleText.setInnerText("DECIDE which transitions you will use from the list and TYPE them into your paragraph.");
			directionsAudio.setLocation("Directions_PC3_connect.mp3");
			
			freeWriteBox = new DistinguishableTextBox();
			freeWriteBox.setInnerText(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
			paragraphText.add(freeWriteBox);
		}
		
		LogUtil.log("Made the add transitions screen");
		
	}
	
	protected void mouseOverHelpfulText(final String word, final String helpText, MouseOverEvent event) {
		final Element tgt = Element.as(event.getNativeEvent().getEventTarget());

		if(tgt != helpBoxLastTgt){
			if(helpBoxLastTgt != null ){
				if(mouseOverTimer != null){
					mouseOverTimer.cancel();
					mouseOverTimer = null;
					hideHelpfulText();
				}
			}
			
			helpBoxLastTgt = tgt;
			
			mouseOverTimer = new Timer(){
				@Override
				public void run() {
					displayHelpfulText(word, helpText, tgt);
				}};
			mouseOverTimer.schedule(500);
		}
		
	}
	
	protected void mouseOutOfHelpfulText(){
		if(mouseOverTimer != null){
			mouseOverTimer.cancel();
			mouseOverTimer = null;
		}
		hideHelpfulText();
	}

	protected void hideHelpfulText(){
		Glossary.popup.hide();
	}
	
	protected void displayHelpfulText(final String word, final String def, final Element tgt){
		if(def !=null){
			
			GWT.log("HELP TEXT: " + word + " : " + def);
			
			GlossaryPopupContent popupContents = new GlossaryPopupContent(word,def);
			Glossary.popup.setWidget(popupContents);

			Glossary.popup.addDomHandler(new MouseMoveHandler(){
				@Override
				public void onMouseMove(MouseMoveEvent event) {
					mouseOutOfHelpfulText();
				}}, MouseMoveEvent.getType());
			
			Glossary.popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
				
				@Override
				public void setPosition(int offsetWidth, int offsetHeight) {
					int pos_x;
					int pos_y;
					
					int x = tgt.getAbsoluteLeft();
					int fallbacky = tgt.getAbsoluteTop();
					int fallbackx = tgt.getAbsoluteRight();
					int y = tgt.getAbsoluteBottom();
					
					if(y + offsetHeight > 705 ){
						pos_y = fallbacky - offsetHeight;
					}else{
						pos_y = y;
					}
					if(x + offsetWidth > 940 ){
						pos_x = fallbackx - offsetWidth;
					}else{
						pos_x = x;
					}
					
					Glossary.popup.setPopupPosition(pos_x,pos_y);
				}
			});
			ElementUtil.setTextScaling();
		} 
	}
	
	protected void removeButton_onClick(final int index) {
		mTransitionSlots[index].getElement().setInnerText("");
		mTransitionSlots[index].getParent().removeStyleName("transition-slot");
		mTransitionSlots[index].getParent().addStyleName("transition-slot-collapsed");
		mSlotButtons[index].removeStyleName("remove-btn");
		mSlotButtons[index].addStyleName("add-btn");
		mSlotButtonHandlerRegs[index].removeHandler();
		
		mSufficientText[index] = true;
		submitButton.setEnabled(checkComplete());
		
		mSlotButtonHandlerRegs[index] = mSlotButtons[index].addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addButton_onClick(index);
			}}, ClickEvent.getType());
		
	}

	protected void addButton_onClick(final int index) {
		mTransitionSlots[index].getParent().removeStyleName("transition-slot-collapsed");
		mTransitionSlots[index].getParent().addStyleName("transition-slot");
		mSlotButtons[index].removeStyleName("add-btn");
		mSlotButtons[index].addStyleName("remove-btn");
		mSlotButtonHandlerRegs[index].removeHandler();
		
		mSufficientText[index] = false;
		submitButton.setEnabled(checkComplete());
		
		mSlotButtonHandlerRegs[index] = mSlotButtons[index].addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				removeButton_onClick(index);
			}}, ClickEvent.getType());
	}

	@Override
	protected void onAttach() {
		super.onAttach();
		
		if(scaffoldLevel != 2)
		for( int i = 1; i<3; i++){
			final int index = i;
			
			mSlotButtonHandlerRegs[i-1] = mSlotButtons[i-1].addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					removeButton_onClick(index -1);
				}}, ClickEvent.getType());
			
			mTransitionSlots[i-1].addDomHandler(new KeyUpHandler(){
				@Override
				public void onKeyUp(KeyUpEvent event) {
					//mTransitionSlots[index-1].getElement().setInnerHTML(mTransitionSlots[index-1].getElement().getInnerText());
					if(mTransitionSlots[index-1].getElement().getInnerText().replaceAll("\\W", "").length() > 0)
						mSufficientText[index-1] = true;
					else
						mSufficientText[index-1] = false;
					if(mTransitionSlots[index-1].getElement().getInnerText().length() == 0){
						mTransitionSlots[index-1].getElement().setInnerHTML("&nbsp;");
					}
					submitButton.setEnabled(checkComplete());
					updateScrollbars();
				}},KeyUpEvent.getType());
		}
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		getParentPanel().hud.init();
		
		addScrollbars();
		submitButton.setEnabled(checkComplete());
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		super.onDetach();
	}
	
	private String getModifiedText(int index) {
		String base = getSentenceText(index);
		
		GWT.log(base);
		
		if (index == 0) {
			return base;
		} else {
			String prefix = mTransitionSlots[index-1].getElement().getInnerText();
			prefix = new SafeHtmlBuilder().appendEscaped(prefix).toSafeHtml().asString();
			
			if (prefix.replaceAll("\\W", "").length() == 0) {
				return base;
			} else {
				RegExp regex = RegExp.compile("[^a-zA-Z]+$");
				prefix = regex.replace(prefix, "");
				
				regex = RegExp.compile("^[^a-zA-Z]+");
				prefix = regex.replace(prefix, "");
				
				prefix = "<span class=\"" + DistinguishableTextBox.STUDENTCLASSNAME +"\">" + 
						prefix +
						", </span>";
				
				HTMLPanel tempPanel = new HTMLPanel(base);
				base = tempPanel.getElement().getFirstChildElement().getInnerText();

				base = Character.toLowerCase(base.charAt(0)) + base.substring(1);

				tempPanel.getElement().getFirstChildElement().setInnerText(base);
				
				base = tempPanel.getElement().getInnerHTML();

				return prefix + base;
			}
		}
	}
	
	private String getSentenceText(int index) {
		switch (index) {
			case 0: return "<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" + DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text + " </span>";
			case 1: return items[0].getFullSentenceText();
			case 2: return items[1].getFullSentenceText();
			default:
				GWT.log("Invalid index: " + index);
				return null;
		}
	}
	
	private boolean checkComplete() {
		if(scaffoldLevel > 1)
			return true;
		
		for( int i = 0; i < 2; i++){
			if(!mSufficientText[i])
				return false;
		}
		return true;

	}
	
	protected void label_onMouseOver(int labelIndex) {
		mSentences[labelIndex].addStyleName("highlight");
	}
	
	protected void label_onMouseOut(int labelIndex) {
		mSentences[labelIndex].removeStyleName("highlight");
	}
	
	private void selector_onSelect(int selectorIndex) {
		mSentences[selectorIndex + 1].getElement().setInnerText(getModifiedText(selectorIndex + 1));
		if (checkComplete()) {
			submitButton.setEnabled(true);
		}
	}
	
	private void submitButton_onClick() {
		GWT.log("Submit clicked");

		submitButton.clickHandler = null;
		
		DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] = null;
		
		if(scaffoldLevel < 2){
			DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex] = getModifiedText(0);
			for (int i = 1; i < mLabelHolders.length; ++i)
				DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex] += " " + getModifiedText(i);
			getParentPanel().close(this, false);
		} else {
			DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] = freeWriteBox.getValue();
			getParentPanel().close(this, false);
		}
		
		
	}
	
	private void backButton_onClick(){
		getParentPanel().close(this, true);
	}
	
	private native void addPasteHandler(Node n) /*-{
		$wnd.$(n).bind('paste',function(e){
		    e.preventDefault();
		});
	}-*/;
	
	private native void addScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true,
				useKeyboard:false
			});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar('update');
	}-*/;
}
