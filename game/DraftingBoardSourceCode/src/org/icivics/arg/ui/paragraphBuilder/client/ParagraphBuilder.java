package org.icivics.arg.ui.paragraphBuilder.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.json.client.ParagraphBuilderProgress;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ReasonInstance;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class ParagraphBuilder extends ModuleUI<ParagraphBuilder> {
	protected int paragraphIndex;
	protected int scaffoldLevel;
	protected int numOrderFailures = 0;
	protected ParagraphBuilderProgress userData;
	
	protected EvidenceSidebar hud;
	
	public ParagraphBuilder(int paragraphIndex, EvidenceSidebar hud) {
		this.paragraphIndex = paragraphIndex;
		
		this.scaffoldLevel = ParagraphBuilderProgress.SCAFFOLDING_LEVELS[DataController.getCurrentIssue().studentLevel][paragraphIndex];
		
		hud.init(DataController.getCurrentClaim());
		this.hud = hud;
	}
	
	protected void commitScore() {
		double orderScore = 1.0 / (1.0 + numOrderFailures);
		DataController.getCurrentIssue().scoreParagraphs[paragraphIndex] = orderScore;
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		userData = DataController.getModuleProgress().forParagraphBuilder(paragraphIndex);
		openUI(getSavedUI());
	}
	
	private Subpanel<ParagraphBuilder> getSavedUI() {
		int stepIndex = userData.getStepIndex();
		DataController.moduleStepIndex = stepIndex;
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		Item[] items = new Item[claim.reasons[paragraphIndex].reason.items.length];
		
		items[0] = claim.reasons[paragraphIndex].reason.items[1];
		items[1] = claim.reasons[paragraphIndex].reason.items[0];
		
		hud.init(items);
		
		scaffoldLevel = ParagraphBuilderProgress.SCAFFOLDING_LEVELS[DataController.getCurrentIssue().studentLevel][paragraphIndex];
		int prevScaffoldLevel = userData.getParagraphLevel();
		if(prevScaffoldLevel != scaffoldLevel)
			stepIndex = 0;
		
		if(stepIndex == 0)
			userData.setParagraphLevel(scaffoldLevel);
		
		GWT.log("Paragraph builder for " + paragraphIndex + " at level " + scaffoldLevel);
		
		switch (scaffoldLevel){
		case 0: switch (stepIndex) {
					case 0: return new SentenceOrdering(this, paragraphIndex);
					case 1: return new AddEvidence(this, paragraphIndex);
					case 2: return new AddTransitions(this, paragraphIndex);
					case 3: return new ParagraphEdit(this, paragraphIndex);
					case 4: return null;
					default:
						DataController.moduleStepIndex = 0;
						return new SentenceOrdering(this, paragraphIndex);
				}
		case 1: switch (stepIndex) {
					case 0: return new SentenceOrdering(this, paragraphIndex);
					case 1: return new ClozeParagraph(this, paragraphIndex, 0);
					case 2: return new ClozeParagraph(this, paragraphIndex, 1);
					case 3: return new AddTransitions(this, paragraphIndex);
					case 4: return new ParagraphEdit(this, paragraphIndex);
					case 5: return new ParagraphChecklist(this, paragraphIndex);
					case 6: return null;
					default:
						DataController.moduleStepIndex = 0;
						return new SentenceOrdering(this, paragraphIndex);
				}
		case 2:switch (stepIndex) {
					case 0: return new ParagraphNotes(this, paragraphIndex);
					case 1: return new AddTransitions(this,paragraphIndex);
					case 2: return new ParagraphChecklist(this, paragraphIndex);
					case 3: return null;
					default:
						DataController.moduleStepIndex = 0;
						return new ParagraphChecklist(this, paragraphIndex);
				}
		default:
			DataController.moduleStepIndex = 0;
			return new ParagraphChecklist(this, paragraphIndex);
		}
		
		/*
		if (paragraphIndex < 2) {
			if (stepIndex > 0) {
				numOrderFailures = userData.getOrderFailures();
			}
			
			switch (stepIndex) {
				case 0: return new SentenceOrdering(this, paragraphIndex);
				case 1: return new AddEvidence(this, paragraphIndex);
				case 2: return new AddTransitions(this, paragraphIndex);
				case 3: return new ParagraphEdit(this, paragraphIndex);
				case 4: return new ParagraphChecklist(this, paragraphIndex);
				case 5: return null;
				default:
					DataController.moduleStepIndex = 0;
					return new SentenceOrdering(this, paragraphIndex);
			}
		} else {
			switch (stepIndex) {
				case 0: return new ParagraphEdit(this, paragraphIndex);
				case 1: return new ParagraphChecklist(this, paragraphIndex);
				case 2: return null;
				default:
					DataController.moduleStepIndex = 0;
					return new ParagraphChecklist(this, paragraphIndex);
			}
		}
		*/
	}
	
	void close(SentenceOrdering ui) {
		userData.setParagraphSwapped(DataController.getCurrentIssue().claim.paragraphsSwapped[paragraphIndex]);
		userData.setOrderFailures(numOrderFailures);
		
		switch(scaffoldLevel){
		case 0:
			DataController.saveModuleProgress(1);
			transitionTo(new AddEvidence(this, paragraphIndex));
			break;
			
		case 1:
			DataController.saveModuleProgress(1);
			transitionTo(new ClozeParagraph(this,paragraphIndex,0));
			break;
			
		default:
			GWT.log("This scaffold level should not have been in a sentence ordering screen to begin with: " + scaffoldLevel);
		}
		
		/*
		DataController.saveModuleProgress(1);
		if(paragraphIndex == 0)
			transitionTo(new AddEvidence(this, paragraphIndex));
		else
			transitionTo(new ClozeParagraph(this,paragraphIndex,0));
		*/
	}
	
	void close(AddEvidence ui, boolean goingBack) {
		assert scaffoldLevel < 1 : "Cannot add evidence for scaffold level " + scaffoldLevel;
		
		if(goingBack){
			DataController.saveModuleProgress(0);
			transitionTo(new SentenceOrdering(this, paragraphIndex));
		}else{
			ReasonInstance reason = DataController.getCurrentIssue().claim.reasons[paragraphIndex];
			for (int i = 0; i < reason.reason.items.length; ++i) {
				userData.setCustomItemText(i, reason.reason.items[i].customSentenceText);
			}
	
			DataController.saveModuleProgress(2);
			transitionTo(new AddTransitions(this, paragraphIndex));
		}
	}
	
	void close(AddTransitions ui, boolean goingBack) {
		
		if(goingBack){
			switch(scaffoldLevel){
			case 0:
				DataController.saveModuleProgress(1);
				transitionTo(new AddEvidence(this,paragraphIndex));
				break;
			case 1:
				DataController.saveModuleProgress(2);
				transitionTo(new ClozeParagraph(this,paragraphIndex,scaffoldLevel));
				break;
			case 2:
				DataController.saveModuleProgress(0);
				transitionTo(new ParagraphNotes(this, paragraphIndex));
				break;
			default:
				GWT.log("This scaffold level is invalid, and could not have had transitions added to it: " + scaffoldLevel);
			}
		}else{
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
			
			switch(scaffoldLevel){
			case 0:
				DataController.saveModuleProgress(3);
				transitionTo(new ParagraphEdit(this, paragraphIndex));
				break;
			case 1:
				DataController.saveModuleProgress(4);
				transitionTo(new ParagraphEdit(this, paragraphIndex));
				break;
			case 2:
				DataController.saveModuleProgress(2);
				transitionTo(new ParagraphChecklist(this,paragraphIndex));
				break;
			default:
				GWT.log("This scaffold level is invalid, and could not have had transitions added to it: " + scaffoldLevel);
			}
		}
	}
	
	void close(ParagraphEdit ui) {
		userData.setFinalParagraph(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
		
		
		
		/*
		if(paragraphIndex > 0){
			transitionTo(new ParagraphChecklist(this,paragraphIndex));
		}else {
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
			userData.setFinalParagraph(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
			
			hud.init(DataController.getCurrentClaim());
			DataController.saveModuleProgress(1);
			finish();
		}
		*/
		switch(scaffoldLevel){
			case 0:
				DataController.saveModuleProgress(4);
				userData.setOriginalParagraph(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
				userData.setFinalParagraph(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
				
				hud.init();
				DataController.saveModuleProgress(1);
				finish();
				break;
			case 1:
				DataController.saveModuleProgress(5);
				transitionTo(new ParagraphChecklist(this,paragraphIndex));
				break;
			case 2:
				DataController.saveModuleProgress(2);
				transitionTo(new AddTransitions(this,paragraphIndex));
				break;
		}
	}
	
	void close(ParagraphChecklist ui, boolean goingForward) {
		if(goingForward){
			switch(scaffoldLevel){
			case 0:
				GWT.log("This scaffold level should not have had a paragraphChecklist to begin with " + scaffoldLevel);
				break;
			case 1:
				// This may be pointless
				DataController.saveModuleProgress(6);
				break;
			case 2:
				// This may be pointless
				DataController.saveModuleProgress(3);
				break;
				
			default:
				GWT.log("This scaffold level is invalid and should not have had a paragraphChecklist to begin with " + scaffoldLevel);
			}
			
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
			userData.setFinalParagraph(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
			hud.init();
			DataController.saveModuleProgress(1);
			finish();
		} else {
			//userData.setOriginalParagraph(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
			
			switch(scaffoldLevel){
			case 0:
				GWT.log("This scaffold level should not have had a paragraphChecklist to begin with " + scaffoldLevel);
				break;
			case 1:
				DataController.saveModuleProgress(4);
				transitionTo(new ParagraphEdit(this, paragraphIndex));
				break;
			case 2:
				DataController.saveModuleProgress(1);
				transitionTo(new ParagraphEdit(this, paragraphIndex));
				break;
				
			default:
				GWT.log("This scaffold level is invalid and should not have had a paragraphChecklist to begin with " + scaffoldLevel);
			}
			
			
		}
	}
	
	void close(ClozeParagraph ui, boolean finished){
		assert scaffoldLevel == 1 : "This scaffold level should not have had a cloze paragraph: " + scaffoldLevel;
		if(finished){
			ReasonInstance reason = DataController.getCurrentIssue().claim.reasons[paragraphIndex];
			for (int i = 0; i < reason.reason.items.length; ++i) {
				userData.setCustomItemText(i, reason.reason.items[i].customSentenceText);
			}
			
			DataController.saveModuleProgress(3);
			transitionTo(new AddTransitions(this, paragraphIndex));
		}else{
			DataController.saveModuleProgress(0);
			transitionTo(new SentenceOrdering(this, paragraphIndex));
		}
	}

	public void close(ParagraphNotes paragraphNotes) {
		assert scaffoldLevel == 2: "This scaffold level should not have had a notes screen: " + scaffoldLevel;
		
		userData.setOriginalParagraph(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
		userData.setFinalParagraph(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
		
		DataController.saveModuleProgress(1);
		transitionTo(new AddTransitions(this,paragraphIndex));
	}
}
