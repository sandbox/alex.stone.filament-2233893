package org.icivics.arg.ui.paragraphBuilder.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

public class ParagraphNotes extends Subpanel<ParagraphBuilder> {
	private static ParagraphNotesUiBinder uiBinder = GWT.create(ParagraphNotesUiBinder.class);
	interface ParagraphNotesUiBinder extends UiBinder<Widget, ParagraphNotes> {}

	@UiField
	ParagraphElement titleText;
	@UiField
	DistinguishableTextBox paragraphField;
	@UiField
	LabelButton submitButton;
	@UiField
	HTMLPanel sideColumn;
	
	public int paragraphIndex;

	private Item[] items;
	private AddEvidenceListItem[] mListItems = new AddEvidenceListItem[3];
	
	
	public ParagraphNotes(ParagraphBuilder parent, int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		initWidget(uiBinder.createAndBindUi(this));
		//titleText.setInnerText("Body Paragraph #" + (paragraphIndex + 1) + " - Edit your Paragraph");
		//paragraphField.setInnerText(DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex]);
		
		paragraphField.setInnerText(
					"<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME + "\">" +
					DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text +
					"</span>"
				);
		
		if(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] != null)
			paragraphField.setInnerText(DataController.getCurrentIssue().claim.paragraphs[paragraphIndex]);
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		items = new Item[claim.reasons[paragraphIndex].reason.items.length];
		if (claim.paragraphsSwapped[paragraphIndex]) {
			items[0] = claim.reasons[paragraphIndex].reason.items[1];
			items[1] = claim.reasons[paragraphIndex].reason.items[0];
		} else {
			items[0] = claim.reasons[paragraphIndex].reason.items[0];
			items[1] = claim.reasons[paragraphIndex].reason.items[1];
		}
		
		getParentPanel().hud.init(items);
		
		//selectedItems = new Item[claim.reasons[paragraphIndex].reason.items.length];
		
		//mLabelRegs = new ArrayList<HandlerRegistration>(3 * 2);
		
		for(int i=0; i < 3; i++){
			AddEvidenceListItem item = new AddEvidenceListItem(i>0? 3 : 0);
			item.label.setInnerText((i>0) ? items[i-1].name:"Reason: " + claim.reasons[paragraphIndex].reason.text);
			sideColumn.add(item);
			
			//mSentences[i] = buildSentence(i);
			//paragraphText.appendChild(mSentences[i].getElement());
			
			mListItems[i] = item;
			//mSentenceActive[i] = i<2;
			
			final int itemIndex = i;
			/*
			mLabelRegs.add(item.addDomHandler(new MouseOverHandler() {
				@Override
				public void onMouseOver(MouseOverEvent event) {
					label_onMouseOver(itemIndex);
				}
			}, MouseOverEvent.getType()));
			mLabelRegs.add(item.addDomHandler(new MouseOutHandler() {
				@Override
				public void onMouseOut(MouseOutEvent event) {
					label_onMouseOut(itemIndex);
				}
			}, MouseOutEvent.getType()));
			*/
			
			if(i > 0){
				mListItems[i].setItem(items[i-1]);

				final int index = i;
				mListItems[i].setClickHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						onItemSelected(items[index-1], index);
					}
				});
			}
			
		}
		 
		 
		 
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		getParentPanel().hud.setEvidenceHandler(new EvidenceSidebar.SelectItemHandler() {
			public void onSelectItem(Item item) {
				GWT.log("Item selected");
				useEvidenceItem(item);
			}
		});
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		addScrollbars();
		updateScrollbars();
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		
		super.onDetach();
	}
	
	/*
	private void onItemSelected(final Item curItem, final AddEvidenceListItem currentItem, final int index){
		getParentPanel().hud.onItemPanelClose();
		
		getParentPanel().hud.setChunkHandler(new EvidenceSidebar.SelectChunkHandler() {
			@Override
			public void onSelectChunk(Item item, int chunkIndex) {
				
			}

			@Override
			public void onSelectChunk(Item item, String chunkText) {
				// TODO Auto-generated method stub
				GWT.log("Text text text: " + chunkText);
				chunkSelected(chunkText);
			}
		});
		
		getParentPanel().hud.showItem(curItem,true, true);
	}
	*/
	
	
	protected void useEvidenceItem(final Item item) {
		
		GWT.log("Item has been selected");
		
		if(item!= null){
			for(int i=1; i < 3; i++){
				if(mListItems[i].itemIsSet() && mListItems[i].getItem() == item){
					onItemSelected(item, i);
					i=3;
					GWT.log("item has been found as an option");
				}
			}
			
		}else{
			GWT.log("Exit from evidence selection");
		}
	}
	
	 private void onItemSelected(final Item curItem, final int index){
		getParentPanel().hud.onItemPanelClose();
		
		getParentPanel().hud.setChunkHandler(new EvidenceSidebar.SelectChunkHandler() {
			@Override
			public void onSelectChunk(Item item, int chunkIndex) {
				GWT.log("Fill text: " + item.chunks.get(chunkIndex).sentenceText);
				GWT.log("Selected text: " + item.chunks.get(chunkIndex).itemText);
				chunkSelected(chunkIndex, item.chunks.get(chunkIndex).itemText);	
			}

			@Override
			public void onSelectChunk(Item item, String chunkText) {
				// TODO Auto-generated method stub
				int itemIndex = 1;
				for(int i = 1; i < mListItems.length; i++){
					if(item == mListItems[i].getItem()){
						itemIndex = i;
					}
				}
				chunkSelected(itemIndex, chunkText);	
			}
			
		});
		
		getParentPanel().hud.showItem(curItem,true,true);
	}
	 
	
	protected void chunkSelected(final int itemIndex , String text){
		//selectedItems[itemIndex - 1] = item;
		//selectedChunkIndices[itemIndex - 1] = selectedChunkIndex;
		
		/*
		if(paragraphIndex == 0){
			mSentences[itemIndex].getElement().setInnerHTML(getSentenceHTML(itemIndex) + " ");
			mListItems[itemIndex].setSentence(selectedChunkIndex);
			mListItems[itemIndex].setClearHandler(new AddEvidenceListItem.ClearHandler() {
				@Override
				public void onClear() {
				
					selectedChunkIndices[itemIndex-1] = -1;
					GWT.log("Clearing a sentence chunk from the whassit " + buildSentence(itemIndex).getElement().getInnerHTML());
					
					mSentences[itemIndex].getElement().setInnerHTML(buildSentence(itemIndex).getElement().getInnerHTML()); 
					
					//mListItems[itemIndex].setSentence(itemIndex);
				}
			});
		}else if(paragraphIndex == 1){
			if(mSentenceActive[itemIndex]){
				mListItems[itemIndex].setSentence(selectedChunkIndex);
				mListItems[itemIndex].clearHandler = new AddEvidenceListItem.ClearHandler() {
					@Override
					public void onClear() {
						selectedChunkIndices[itemIndex-1] = -1;
					}
				};
			}
		}else
		*/
		{
			mListItems[itemIndex].setSentence(text);
			//items[itemIndex - 1].customSentenceText = items[itemIndex - 1].sentenceText[0].replaceFirst(ItemChunk.REPLACEMENT_TOKEN_REGEX, text);
			//mSentences[itemIndex].getElement().setInnerHTML(items[itemIndex - 1].customSentenceText + " ");
		}
		
		//submitButton.setEnabled(checkComplete());
		updateScrollbars();
	}
	
	
	private void submitButton_onClick() {
		GWT.log("Submit clicked");
		submitButton.clickHandler = null;
		
		getParentPanel().hud.setEvidenceHandler(null);
		
		DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex] = paragraphField.getValue();
		DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] = paragraphField.getValue();
		getParentPanel().close(this);
	}
	
	private native void addScrollbars() /*-{
	$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar({
			wheelSpeed: 30,
			wheelPropagation: true,
			suppressScrollX: true,
			useKeyboard:false
		});
		
		$wnd.$(".paragraph-constructor .panel-container .side-panel").perfectScrollbar({
			wheelSpeed: 30,
			wheelPropagation: true,
			suppressScrollX: true,
			useKeyboard:false
		});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar('update');
		$wnd.$(".paragraph-constructor .panel-container .side-panel").perfectScrollbar('update');
	}-*/;
}
