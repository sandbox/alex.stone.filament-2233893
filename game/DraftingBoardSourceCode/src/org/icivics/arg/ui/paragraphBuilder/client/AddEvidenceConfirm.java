package org.icivics.arg.ui.paragraphBuilder.client;

import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.paragraphBuilder.client.AddEvidenceConfirm.Handler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class AddEvidenceConfirm extends Composite{
	private static AddEvidenceConfirmUiBinder uiBinder = GWT.create(AddEvidenceConfirmUiBinder.class);
	interface AddEvidenceConfirmUiBinder extends UiBinder<Widget, AddEvidenceConfirm> {}

	interface Handler {void onCancel(); void onConfirm(); void onRewriteConfirm(String text);}
	Handler handler;
	
	Item mItem;
	String sentence = "";
	int selectedIndex = -1;
	
	@UiField
	HTMLPanel sentenceText;
	
	@UiField
	HTMLPanel rewriteDiv;
	@UiField
	HTMLPanel rewriteText;
	
	@UiField
	LabelButton cancelButton;
	@UiField
	LabelButton submitButton;

	private Boolean rewrite;
	private String playerText = "HELLO";
	
	HTMLPanel blankSpace;
	HandlerRegistration focusReg;
	
	public AddEvidenceConfirm() {
		initWidget(uiBinder.createAndBindUi(this));
		
	}

	
	@Override
	protected void onAttach() {
		super.onAttach();
		cancelButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				if(handler!= null)
					handler.onCancel();
			}
		};
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				if(handler!= null)
					if(!rewrite){
						handler.onConfirm();
					}else{
						playerText = "</span><span class=\""+ DistinguishableTextBox.STUDENTCLASSNAME +"\">" +
									 blankSpace.getElement().getInnerText().replaceAll("<", "&lt;").replaceAll(">","&gt;") + "</span>" +
									 "<span class=\""+ DistinguishableTextBox.DRAFTINGBOARDCLASSNAME +"\">";
						handler.onRewriteConfirm(playerText);
					}
			}
		};
	}
	
	private void updateSentenceText() {
		String str = sentence;
		blankSpace = new HTMLPanel("span","");
		blankSpace.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
		blankSpace.getElement().getStyle().setProperty("minWidth","6em");
		blankSpace.getElement().getStyle().setProperty("minHeight","1em");
		focusReg = blankSpace.addDomHandler(new FocusHandler(){
			@Override
			public void onFocus(FocusEvent event) {
				blankSpace.getElement().setInnerHTML("");
				focusReg.removeHandler();
			}}, FocusEvent.getType());
		if(!rewrite){
			RegExp regx = RegExp.compile(ItemChunk.REPLACEMENT_TOKEN_REGEX,"g");
			str = regx.replace(str, selectedIndex == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : "<span>" + mItem.chunks.get(selectedIndex).sentenceText + "</span>");
			//str = str.replaceAll(ItemChunk.REPLACEMENT_TOKEN_REGEX, selectedIndex == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : "<span>" + mItem.chunks.get(selectedIndex).sentenceText + "</span>");
			sentenceText.getElement().setInnerHTML(str);
		} else {
			sentenceText.getElement().setInnerHTML(selectedIndex == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : "<span>" + mItem.chunks.get(selectedIndex).itemText + "</span>");
			str = str.replaceAll(ItemChunk.REPLACEMENT_TOKEN_REGEX,  "<span id=\"editableBit\">" + ItemChunk.REPLACEMENT_UNKNOWN_TEXT + "</span>");
			rewriteText.getElement().setInnerHTML(str);
			
			
			blankSpace.getElement().setAttribute("contenteditable", "true");
			
			//FIXME: Figure out how to use the non-depriceated method. Also how to spell depreceated.
			rewriteText.addAndReplaceElement(blankSpace, rewriteText.getElementById("editableBit"));
		}
	}

	public void setSentence(String sentence){
		setSentence(sentence, false);
	}
	
	public void setSentence(String sentence, Boolean rewrite){
		this.sentence = sentence;
		this.rewrite = rewrite;
		rewriteDiv.setVisible(rewrite);
		updateSentenceText();
	}
	
	public void setItem(Item item){
		mItem = item;
		if(item == null){
			submitButton.setEnabled(false);
			selectedIndex = -1;
		}
	}

	public void selectChunk(int chunkIndex) {
		selectedIndex = chunkIndex;
		updateSentenceText();
		GWT.log(mItem.chunks.get(chunkIndex).sentenceText + chunkIndex);
		
		submitButton.setEnabled(true);
	}

	public void addHandler(Handler handler) {
		this.handler = handler;
	}


	public void clearHandler() {
		handler = null;
	}

}
