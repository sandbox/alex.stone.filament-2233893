package org.icivics.arg.ui.paragraphBuilder.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.util.client.ElementUtil;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ClozeParagraph extends Subpanel<ParagraphBuilder> {
	private static ClozeParagraphUiBinder uiBinder = GWT.create(ClozeParagraphUiBinder.class);
	interface ClozeParagraphUiBinder extends UiBinder<Widget, ClozeParagraph> {}

	@UiField
	SimplePanel popupHolder;
	AddEvidenceConfirm popup;
	
	@UiField
	HTMLPanel sideColumn;
	
	//@UiField
	//ParagraphElement titleText;
	@UiField
	HTMLPanel paragraphWrap;
	@UiField
	ParagraphElement paragraphText;
	
	@UiField
	LabelButton submitButton, backButton;
	
	public int paragraphIndex;
	public int sentenceIndex;
	private Item[] items;
	private Item[] selectedItems;
	private ArrayList<HandlerRegistration> mLabelRegs;
	private HTMLPanel[] mSentences = new HTMLPanel[3];
	private AddEvidenceListItem[] mListItems;
	//private boolean mSentenceActive[] = new boolean[3];
	
	int sentenceFillIndex = 0;
	
	private int[] selectedChunkIndices = {-1, -1};
	
	public ClozeParagraph(ParagraphBuilder parent, final int paragraphIndex, final int sentenceIndex) {
		super(parent);
		
		//TODO: Most of this is copied over from AddEvidence. Should prbably be a subclass.
		this.paragraphIndex = paragraphIndex;
		initWidget(uiBinder.createAndBindUi(this));
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		items = new Item[claim.reasons[paragraphIndex].reason.items.length];
		if (claim.paragraphsSwapped[paragraphIndex]) {
			items[0] = claim.reasons[paragraphIndex].reason.items[1];
			items[1] = claim.reasons[paragraphIndex].reason.items[0];
		} else {
			items[0] = claim.reasons[paragraphIndex].reason.items[0];
			items[1] = claim.reasons[paragraphIndex].reason.items[1];
		}
		
		getParentPanel().hud.init(items);
		
		selectedItems = new Item[claim.reasons[paragraphIndex].reason.items.length];
		mLabelRegs = new ArrayList<HandlerRegistration>(3 * 2);
		
		displayParagraph(sentenceIndex);
		
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		GWT.log("Attaching the cloze sentence screen");
		
		getParentPanel().hud.onItemPanelClose();
		getParentPanel().hud.setEvidenceHandler(new EvidenceSidebar.SelectItemHandler() {
			public void onSelectItem(Item item) {
				GWT.log("Item selected");
				useEvidenceItem(item);
			}
		});
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		paragraphWrap.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				submitButton.setEnabled(checkComplete());
				updateScrollbars();
			}},KeyUpEvent.getType());
		
		//checkComplete();
		addScrollbars();
	}
	
	@Override
	protected void onDetach() {
		GWT.log("Detaching the cloze sentence screen");
		
		for (int i = 0; i < mListItems.length; ++i)
			mListItems[i].destroy();
		for (int i = 0, count = mLabelRegs.size(); i < count; ++i)
			mLabelRegs.get(i).removeHandler();
		mLabelRegs.clear();
		// Detachment may after the next screen attaches, so clearing the evidence handler is bad
		
		submitButton.destroy();
		super.onDetach();
	}
	
	private HTMLPanel buildSentence(int index){
		HTMLPanel sentence;

		sentence = new HTMLPanel("span", "");
		String text;
		if(index == 0){
			text = DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text + " ";
			sentence.addStyleName(DistinguishableTextBox.DRAFTINGBOARDCLASSNAME);
			sentence.getElement().setInnerHTML(text);
			
		}else if(index == this.sentenceIndex + 1){
			text = items[index-1].sentenceText[1] + " ";
			String[] textArray = text.split(ItemChunk.REPLACEMENT_TOKEN_REGEX);
			text = "";
			for(int i=0, count=textArray.length; i<count; i++){
				HTMLPanel dbContent = new HTMLPanel("span",textArray[i]);
				dbContent.addStyleName(DistinguishableTextBox.DRAFTINGBOARDCLASSNAME);
				sentence.add(dbContent);
				if(i+1 != count){
					HTMLPanel blank = new HTMLPanel("span", " ");
					blank.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
					blank.addStyleName("cloze-slot");
					blank.getElement().setAttribute("contenteditable","true");
					blank.getElement().setAttribute("contentEditable", "true");
					blank.getElement().getStyle().setProperty("MozUserSelect", "text");
					blank.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
					blank.getElement().getStyle().setProperty("WebkitUserSelect", "text");
					blank.getElement().getStyle().setProperty("userSelect", "text");
					sentence.add(blank);
				}
			}
		} else if(index < this.sentenceIndex + 1){
			text = items[index-1].customSentenceText;
			
			sentence.getElement().setInnerHTML(text);
		}
	
		return sentence;
	}
	
	
	protected void displayParagraph(int sentenceIndex){
		this.sentenceIndex = sentenceIndex;
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		
		mListItems = new AddEvidenceListItem[sentenceIndex+2];
		
		paragraphText.removeAllChildren();
		sideColumn.clear();
		
		for(int i=0; i < 3; i++){	
			mSentences[i] = buildSentence(i);
			
			paragraphText.appendChild(mSentences[i].getElement());
			
			if(i < mListItems.length){
				AddEvidenceListItem item = new AddEvidenceListItem(i == sentenceIndex + 1 ? 2 : 0);
				item.label.setInnerText((i>0) ? items[i-1].name:"Reason: " + claim.reasons[paragraphIndex].reason.text);
				sideColumn.add(item);
				mListItems[i] = item;
				
				final int itemIndex = i;
				mLabelRegs.add(item.addDomHandler(new MouseOverHandler() {
					@Override
					public void onMouseOver(MouseOverEvent event) {
						label_onMouseOver(itemIndex);
					}
				}, MouseOverEvent.getType()));
				mLabelRegs.add(item.addDomHandler(new MouseOutHandler() {
					@Override
					public void onMouseOut(MouseOutEvent event) {
						label_onMouseOut(itemIndex);
					}
				}, MouseOutEvent.getType()));
			}
			
			if(i > 0 && i < mListItems.length){
				mListItems[i].setItem(items[i-1]);

				final int index = i;
				mListItems[i].setClickHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						useEvidenceItem(items[index-1]);
					}
				});
			}
			
		}
		
		submitButton.setEnabled(checkComplete());
		updateScrollbars();
		ElementUtil.setTextScaling();
		getParentPanel().hud.onItemPanelClose();
	}
	
	protected void useEvidenceItem(final Item item) {
		
		GWT.log("Item has been selected");
		
		if(item!= null){
			for(int i=1; i < mListItems.length; i++){
				if(mListItems[i].itemIsSet() && mListItems[i].getItem() == item && mListItems[i].getNumSlots() != 0){
					onItemSelected(item);
					i=3;
					GWT.log("item has been found as an option");
				}
			}
			
		}else{
			GWT.log("Exit from evidence selection");
		}
	}
	
	private void onItemSelected(final Item curItem){
		getParentPanel().hud.onItemPanelClose();
		
		getParentPanel().hud.setChunkHandler(new EvidenceSidebar.SelectChunkHandler() {
			@Override
			public void onSelectChunk(Item item, int chunkIndex) {
				
			}

			@Override
			public void onSelectChunk(Item item, String chunkText) {
				// TODO Auto-generated method stub
				GWT.log("Text text text: " + chunkText);
				chunkSelected(chunkText);
			}
		});
		
		getParentPanel().hud.showItem(curItem,true, true);
	}
	
	protected void chunkSelected(String text){
		
		mListItems[1 + sentenceIndex].setSentence(text);
		
		updateScrollbars();
	}
	
	private boolean checkComplete() {
		boolean slotsFilled = true;
		for(int i=1; i<mSentences.length;i++){
			Iterator<Widget> children = mSentences[i].iterator();
			items[i-1].customSentenceText = "";
			while(children.hasNext()){
				Widget element = children.next();
				if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains("cloze-slot") &&
						element.getElement().getInnerText().replaceAll("\\s", "").length() == 0){
					slotsFilled = false;
				}
			}
		}
		return slotsFilled;
	}
	
	protected void label_onMouseOver(int labelIndex) {
		mSentences[labelIndex].addStyleName("highlight");
	}
	
	protected void label_onMouseOut(int labelIndex) {
		mSentences[labelIndex].removeStyleName("highlight");
	}

	private void submitButton_onClick() {
		LogUtil.log("Submit of cloze sentence screen");
		
		// save the paragraph text
		for(int i = 1; i < mSentences.length; i++){
			// Get the sentence's children and strip out the extra html
			Iterator<Widget> children = mSentences[i].iterator();
			items[i-1].customSentenceText = "";
			while(children.hasNext()){
				Widget element = children.next();
				element.getElement().removeAttribute("contenteditable");
				element.getElement().removeAttribute("contentEditable");
				element.getElement().getStyle().clearProperty("MozUserSelect");
				element.getElement().getStyle().clearProperty("KhtmlUserSelect");
				element.getElement().getStyle().clearProperty("WebkitUserSelect");
				element.getElement().getStyle().clearProperty("userSelect");
				element.removeStyleName("cloze-slot");	
				element.removeStyleName("correct");	
				
				SafeHtmlBuilder sanitizer = new SafeHtmlBuilder();
				sanitizer.appendEscaped(element.getElement().getInnerText());
				
				element.getElement().setInnerHTML(sanitizer.toSafeHtml().asString());
			}
			
			items[i-1].customSentenceText += mSentences[i].getElement().getInnerHTML() ;
			GWT.log(items[i-1].customSentenceText);
		}
		
		
		if(sentenceIndex == 0){
			displayParagraph(1);
		} else {
			submitButton.clickHandler = null;
			getParentPanel().hud.onItemPanelClose();
			getParentPanel().hud.setEvidenceHandler(null);
			getParentPanel().close(this, true);
		}
	}
	
	private void backButton_onClick(){
		if(sentenceIndex == 0){
			//Go back a screen
			GWT.log("trying to back up a screen");
			getParentPanel().close(this, false);
		} else {
			//Back up to the previous sentence
			GWT.log("trying to back up a sentence");
			displayParagraph(0);
		}
	}
	
	private native void addScrollbars() /*-{
	$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true,
				useKeyboard:false
			});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar('update');
	}-*/;

}
