package org.icivics.arg.ui.paragraphBuilder.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceOrdering extends Subpanel<ParagraphBuilder> {
	private static SentenceOrderingUiBinder uiBinder = GWT.create(SentenceOrderingUiBinder.class);
	interface SentenceOrderingUiBinder extends UiBinder<Widget, SentenceOrdering> {}
	
	@UiField
	SentenceOrderingListItem item1;
	@UiField
	SentenceOrderingListItem item2;
	@UiField
	SentenceOrderingListItem item3;
	
	//@UiField
	//DivElement titleText;
	@UiField
	ParagraphElement paragraphText;
	
	@UiField
	SimplePanel panelHolder;
	
	@UiField
	LabelButton submitButton;
	
	private SentenceOrderingListItem[] mListItems = new SentenceOrderingListItem[3];
	private SpanElement[] mSentences = new SpanElement[3];
	
	private ArrayList<HandlerRegistration> mLabelRegs;
	private ArrayList<HandlerRegistration> mArrowClickRegs;
	private TextPopup mFeedbackPopup;
	
	private int[] mIndices = {0, 1, 2};
	
	public int paragraphIndex;
	private int scaffoldLevel;
	
	public SentenceOrdering(ParagraphBuilder parent, int paragraphIndex) {
		super(parent);
		this.paragraphIndex = paragraphIndex;
		this.scaffoldLevel = getParentPanel().scaffoldLevel;
		
		initWidget(uiBinder.createAndBindUi(this));
		//titleText.setInnerText("Body Paragraph #" + (paragraphIndex + 1) + " - Arrange the Sentences");
		
		mListItems[0] = item1;
		mListItems[1] = item2;
		mListItems[2] = item3;
		
		item1.disableUpButton();
		item3.disableDownButton();
		
		for (int i = 0; i < mListItems.length; ++i) {
			mListItems[i].setLabel(getLabel(i));
			mSentences[i] = Document.get().createSpanElement();
			mSentences[i].setInnerText(getSentenceText(i));
			paragraphText.appendChild(mSentences[i]);
			paragraphText.appendChild(Document.get().createTextNode(" "));
		}
		
		ElementUtil.setTextScaling();
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		mArrowClickRegs = new ArrayList<HandlerRegistration>(mListItems.length * 2 - 2);
		mLabelRegs = new ArrayList<HandlerRegistration>(mListItems.length * 2);
		for (int i = 0; i < mListItems.length; ++i) {
			final int itemIndex = i;
			if (i != 0) {
				mArrowClickRegs.add(mListItems[i].upButton.addDomHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						itemUpButton_onClick(itemIndex);
					}
				}, ClickEvent.getType()));
			}
			if (i != mListItems.length - 1) {
				mArrowClickRegs.add(mListItems[i].downButton.addDomHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						itemDownButton_onClick(itemIndex);
					}
				}, ClickEvent.getType()));
			}
			mLabelRegs.add(mListItems[i].addDomHandler(new MouseOverHandler() {
				@Override
				public void onMouseOver(MouseOverEvent event) {
					label_onMouseOver(itemIndex);
				}
			}, MouseOverEvent.getType()));
			mLabelRegs.add(mListItems[i].addDomHandler(new MouseOutHandler() {
				@Override
				public void onMouseOut(MouseOutEvent event) {
					label_onMouseOut(itemIndex);
				}
			}, MouseOutEvent.getType()));
		}
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		GWT.log("Sentence swapper attaching");
		ElementUtil.setTextScaling();
		
		getParentPanel().hud.init(DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.items);
	}
	
	@Override
	protected void onDetach() {
		if (mFeedbackPopup != null) {
			mFeedbackPopup.clearHandler();
			mFeedbackPopup.removeFromParent();
			mFeedbackPopup = null;
		}
		for (int i = 0, count = mArrowClickRegs.size(); i < count; ++i)
			mArrowClickRegs.get(i).removeHandler();
		mArrowClickRegs = null;
		submitButton.destroy();
		super.onDetach();
	}
	
	private boolean checkCorrect() {
		return mIndices[0] == 0;
	}
	
	private String getLabel(int index) {
		index = mIndices[index];
		switch (index) {
			case 0: return "Reason: " + DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text;
			case 1: return DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.items[0].name;
			case 2: return DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.items[1].name;
			default:
				GWT.log("Invalid index: " + index);
				return null;
		}
	}
	
	private String getSentenceText(int index) {
		index = mIndices[index];
		switch (index) {
			case 0: return DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.text;
			case 1: return DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.items[0].sentenceText[scaffoldLevel].replaceAll(ItemChunk.REPLACEMENT_TOKEN_REGEX, ItemChunk.REPLACEMENT_UNKNOWN_TEXT);
			case 2: return DataController.getCurrentIssue().claim.reasons[paragraphIndex].reason.items[1].sentenceText[scaffoldLevel].replaceAll(ItemChunk.REPLACEMENT_TOKEN_REGEX, ItemChunk.REPLACEMENT_UNKNOWN_TEXT);
			default:
				GWT.log("Invalid index: " + index);
				return null;
		}
	}
	
	protected void label_onMouseOver(int labelIndex) {
		mSentences[labelIndex].addClassName("highlight");
	}
	
	protected void label_onMouseOut(int labelIndex) {
		mSentences[labelIndex].removeClassName("highlight");
	}
	
	private void itemUpButton_onClick(int itemIndex) {
		if (mFeedbackPopup != null)
			return;
		
		int temp = mIndices[itemIndex];
		mIndices[itemIndex] = mIndices[itemIndex - 1];
		mIndices[itemIndex - 1] = temp;
		
		mListItems[itemIndex].setLabel(getLabel(itemIndex));
		mListItems[itemIndex - 1].setLabel(getLabel(itemIndex - 1));
		
		mSentences[itemIndex].setInnerText(getSentenceText(itemIndex));
		mSentences[itemIndex - 1].setInnerText(getSentenceText(itemIndex - 1));
	}
	
	private void itemDownButton_onClick(int itemIndex) {
		if (mFeedbackPopup != null)
			return;
		
		int temp = mIndices[itemIndex];
		mIndices[itemIndex] = mIndices[itemIndex + 1];
		mIndices[itemIndex + 1] = temp;
		
		mListItems[itemIndex].setLabel(getLabel(itemIndex));
		mListItems[itemIndex + 1].setLabel(getLabel(itemIndex + 1));
		
		mSentences[itemIndex].setInnerText(getSentenceText(itemIndex));
		mSentences[itemIndex + 1].setInnerText(getSentenceText(itemIndex + 1));
	}
	
	private void feedbackPopup_onClose(TextPopup popup) {
		popup.clearHandler();
		popup.removeFromParent();
		mFeedbackPopup = null;
	}
	
	private void submitButton_onClick() {
		if (mFeedbackPopup != null)
			return;
		
		if (checkCorrect()) {
			GWT.log("Submit correct");
			submitButton.clickHandler = null;
			//Commit reordering
			DataController.getCurrentIssue().claim.paragraphsSwapped[paragraphIndex] = mIndices[1] == 2;
			getParentPanel().close(this);
		} else {
			++getParentPanel().numOrderFailures;
			
			GWT.log("Submit incorrect");
			mFeedbackPopup = new TextPopup();
			mFeedbackPopup.setTitle("Invalid Order");
			mFeedbackPopup.setBody("Your reason sentence must be the first sentence in your paragraph.");
			mFeedbackPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			panelHolder.add(mFeedbackPopup);
		}
	}
}
