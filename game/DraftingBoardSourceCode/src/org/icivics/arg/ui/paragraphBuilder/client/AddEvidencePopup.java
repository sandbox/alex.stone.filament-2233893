package org.icivics.arg.ui.paragraphBuilder.client;

import java.util.ArrayList;

import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.HTMLPanel;

public class AddEvidencePopup extends EvidenceDetailPanel {
	private HTMLPanel[] chunks;
	private boolean[] chunksUsed;
	private ArrayList<HandlerRegistration> chunkRegs;
	private int selectedIndex = -1;
	
	public static interface Handler extends EvidenceDetailPanel.Handler {
		void onSelect(int chunkIndex);
	}
	
	public AddEvidencePopup(Item item, boolean[] chunksUsed) {
		super(item, true);
		this.chunksUsed = chunksUsed;
		updateSentenceText();
		showInstructions();
		
		String[] pieces = item.getTextPieces();
		chunks = new HTMLPanel[item.chunks.size()];
		String rawHTML = pieces[0];
		
		for (int i = 0; i < chunks.length; ++i) {
			chunks[i] = new HTMLPanel("span", item.chunks.get(i).itemText);
			chunks[i].addStyleName(chunksUsed[i] ? "disabled" : "choice");
			rawHTML += "<span id=\"chunk" + i + "\"></span>" + pieces[i + 1];
		}
		bodyText.setInnerHTML(rawHTML);
		
		chunkRegs = new ArrayList<HandlerRegistration>(chunks.length);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		confirmButton.addStyleName("hidden");
		
		for (int i = 0; i < chunks.length; ++i) {
			getRoot().add(chunks[i], DOM.getElementById("chunk" + i));
			final int chunkIndex = i;
			if (!chunksUsed[i]) {
				chunkRegs.add(chunks[i].addDomHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						chunk_onClick(chunkIndex);
					}
				}, ClickEvent.getType()));
			}
		}
	}
	
	@Override
	protected void onDetach() {
		for (int i = 0, count = chunkRegs.size(); i < count; ++i)
			chunkRegs.get(i).removeHandler();
		chunkRegs.clear();
		for (int i = 0; i < chunks.length; ++i)
			chunks[i].removeFromParent();
		super.onDetach();
	}
	
	public Item getItem() {
		return mItem;
	}
	
	private void updateSentenceText() {
		sentenceText.setInnerText(mItem.sentenceText[0].replaceAll(ItemChunk.REPLACEMENT_TOKEN_REGEX, selectedIndex == -1 ? ItemChunk.REPLACEMENT_UNKNOWN_TEXT : mItem.chunks.get(selectedIndex).sentenceText));
	}
	
	private void chunk_onClick(int index) {
		if (selectedIndex != -1) {
			chunks[selectedIndex].removeStyleName("selected");
		}
		selectedIndex = index;
		chunks[selectedIndex].addStyleName("selected");
		confirmButton.removeStyleName("hidden");
		confirmButton.setEnabled(true);
		
		updateSentenceText();
	}
	
	@Override
	protected void confirmButton_onClick() {
		if (selectedIndex == -1)
			return;
		
		removeFromParent();
		if (handler != null && handler instanceof Handler)
			((Handler) handler).onSelect(selectedIndex);
	}
	
	public void destroy() {
		removeFromParent();
		super.destroy();
	}
}
