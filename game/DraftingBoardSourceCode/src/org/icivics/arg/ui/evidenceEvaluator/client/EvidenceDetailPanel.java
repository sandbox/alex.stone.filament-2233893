package org.icivics.arg.ui.evidenceEvaluator.client;

import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.ui.common.client.BasicButton;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceDetailPanel extends UIPanel {
	private static EvidenceDetailPanelUiBinder uiBinder = GWT.create(EvidenceDetailPanelUiBinder.class);
	interface EvidenceDetailPanelUiBinder extends UiBinder<Widget, EvidenceDetailPanel> {}
	
	protected Item mItem;
	public Item getItem() { return mItem; }
	
	@UiField
	protected DivElement panel;
	
	@UiField
	protected DivElement instructionHolder;
	
	@UiField
	protected DivElement photoImageHolder;
	@UiField
	protected ImageElement photoImage;
	
	@UiField
	protected ParagraphElement titleText;
	@UiField
	protected DivElement bodyTextHolder;
	@UiField
	protected DivElement bodyText;
	@UiField
	protected ParagraphElement sentenceText;
	
	//@UiField
	//protected BasicButton audioButton;
	@UiField
	protected BasicButton closeButton;
	@UiField
	protected LabelButton confirmButton;
	
	public static interface Handler {
		void onClose();
		void onConfirm();
	}
	public Handler handler;
	
	private boolean mUseConfirm;
	
	public EvidenceDetailPanel(Item item, boolean useConfirm) {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		mItem = item;
		mUseConfirm = useConfirm;
		
		initDisplay();
	}
	
	private void initDisplay() {
		panel.addClassName("type" + mItem.type);
		titleText.setInnerText(mItem.name);
		bodyText.setInnerHTML(mItem.getFullText());
		if (mItem.imageSrc == null) {
			photoImageHolder.addClassName("hidden");
		} else {
			photoImage.setSrc(mItem.imageSrc);
			//audioButton.addStyleName("photo");
			bodyTextHolder.addClassName("photo");
		}
		if (mItem.audioSrc == null) {
			//audioButton.addStyleName("disabled");
		} else {
			
		}
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		closeButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}
		};
		
		confirmButton.removeStyleName("hidden");
		if (mUseConfirm) {
			confirmButton.clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					confirmButton_onClick();
				}
			};
		} else {
			confirmButton.addStyleName("hidden");
		}
	}
	
	@Override
	protected void onDetach() {
		closeButton.destroy();
		confirmButton.destroy();
		super.onDetach();
	}
	
	public void setSentenceText(String text) {
		sentenceText.setInnerText(text);
	}
	
	public void showInstructions() {
		instructionHolder.addClassName("completer_instructions");
	}
	
	protected void closeButton_onClick() {
		if (handler != null)
			handler.onClose();
	}
	
	protected void confirmButton_onClick() {
		if (handler != null)
			handler.onConfirm();
	}
	
	public void destroy() {
		handler = null;
		mItem = null;
	}
}
