package org.icivics.arg.ui.evidenceEvaluator.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.ui.global.client.CloseHandler;
import org.icivics.arg.ui.global.client.NavAccessiblePanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceListPanel extends UIPanel implements NavAccessiblePanel {
	private static EvidenceListPanelUiBinder uiBinder = GWT.create(EvidenceListPanelUiBinder.class);
	interface EvidenceListPanelUiBinder extends UiBinder<Widget, EvidenceListPanel> {}
	
	private static final int ITEMS_PER_PAGE = 6;
	
	private ArrayList<HandlerRegistration> mClickRegs = new ArrayList<HandlerRegistration>();
	private HandlerRegistration mPrevClickReg;
	private HandlerRegistration mNextClickReg;
	private HandlerRegistration mCloseClickReg;
	private EvidenceDetailPanel mItemPanel;
	private int mListOffset = 0;
	
	private CloseHandler mCloseHandler;
	private ArrayList<Item> mItems;
	public String sentenceText;
	
	public interface Handler { void onSelectItem(Item item); }
	
	public ArrayList<Item> disabledItems = new ArrayList<Item>();
	Handler handler;
	
	
	@UiField
	protected HTMLPanel itemList;
	@UiField
	protected HTMLPanel prevButton;
	@UiField
	protected HTMLPanel nextButton;
	@UiField
	protected HTMLPanel closeButton;
	@UiField
	protected SimplePanel panelMount;

	public EvidenceListPanel() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		
		Item[] srcItems = DataController.getCurrentIssue().issue.items;
		mItems = new ArrayList<Item>();
		for (int i = 0; i < srcItems.length && srcItems[i] != null; ++i)
			mItems.add(srcItems[i]);
	}
	
	public EvidenceListPanel(Claim claim) {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		
		Reason[] reasons = claim.reasons;
		mItems = new ArrayList<Item>();
		for (int i = 0; i < reasons.length; ++i)
			for (int j = 0; j < reasons[j].items.length; ++j)
				mItems.add(reasons[i].items[j]);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		showPage(0);
		
		mCloseClickReg = closeButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick(event);
			}
		}, ClickEvent.getType());
	}
	
	@Override
	protected void onDetach() {
		for (int i = 0, count = mClickRegs.size(); i < count; ++i)
			mClickRegs.get(i).removeHandler();
		mClickRegs.clear();
		if (mCloseClickReg != null) {
			mCloseClickReg.removeHandler();
			mCloseClickReg = null;
		}
		clearNavListeners();
		super.onDetach();
	}
	
	public void setHandler(Handler handler) {
		this.handler = handler;
	}
	
	public void clearHandler() {
		this.handler = null;
	}
	
	private void clearNavListeners() {
		prevButton.removeStyleName("btn_disabled");
		nextButton.removeStyleName("btn_disabled");
		if (mPrevClickReg != null) {
			mPrevClickReg.removeHandler();
			mPrevClickReg = null;
		}
		if (mNextClickReg != null) {
			mNextClickReg.removeHandler();
			mNextClickReg = null;
		}
	}
	
	private void initPageListeners() {
		for (int i = 0, count = mClickRegs.size(); i < count; ++i)
			mClickRegs.get(i).removeHandler();
		mClickRegs.clear();
		
		int count = ITEMS_PER_PAGE;
		for (int i = 0; i < count; ++i) {
			final int buttonIndex = i;
			HTMLPanel btn = (HTMLPanel) itemList.getWidget(i);
			btn.removeStyleName("disabled");
			if (mListOffset + i < mItems.size() && !disabledItems.contains(mItems.get(mListOffset + i))) {
				mClickRegs.add(btn.addDomHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						itemButton_onClick(buttonIndex);
					}
				}, ClickEvent.getType()));
			} else {
				btn.addStyleName("disabled");
			}
		}
	}
	
	private void showPage() {
		for (int i = 0, count = ITEMS_PER_PAGE; i < count; ++i) {
			HTMLPanel display = (HTMLPanel) itemList.getWidget(i);
			ParagraphElement text = (ParagraphElement) display.getElement().getElementsByTagName("p").getItem(0);
			DivElement icon = (DivElement) display.getElement().getElementsByTagName("div").getItem(0);
			display.removeStyleName("hidden");
			if (mListOffset + i < mItems.size()) {
				Item item = mItems.get(mListOffset + i);
				text.setInnerText(item.name);
				icon.setClassName("icon type" + item.type);
			} else {
				text.setInnerText("");
				icon.setClassName("icon");
				display.addStyleName("hidden");
			}
		}
		initPageListeners();
		clearNavListeners();
		if (mListOffset > 0) {
			mPrevClickReg = prevButton.addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					prevButton_onClick(event);
				}
			}, ClickEvent.getType());
		} else {
			prevButton.addStyleName("btn_disabled");
		}
		if (mItems.size() > mListOffset + ITEMS_PER_PAGE) {
			mNextClickReg = nextButton.addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					nextButton_onClick(event);
				}
			}, ClickEvent.getType());
		} else {
			nextButton.addStyleName("btn_disabled");
		}
	}
	
	private void showPage(int page) {
		mListOffset = page * ITEMS_PER_PAGE;
		showPage();
	}
	
	
	public void showItem(Item item) {
		mItemPanel = new EvidenceDetailPanel(item, handler != null);
		if (sentenceText != null) {
			mItemPanel.setSentenceText(sentenceText);
		}
		mItemPanel.handler = new EvidenceDetailPanel.Handler() {
			@Override
			public void onConfirm() {
				onItemPanelConfirm();
			}
			
			@Override
			public void onClose() {
				onItemPanelClose();
			}
		};
		panelMount.add(mItemPanel);
	}
	
	private void closeItemPanel() {
		if (mItemPanel == null)
			return;
		
		panelMount.remove(mItemPanel);
		mItemPanel.destroy();
		mItemPanel = null;
	}
	
	public void onItemPanelClose() {
		closeItemPanel();
	}
	
	public void onItemPanelConfirm() {
		if (handler != null) {
			handler.onSelectItem(mItemPanel.getItem());
		}
		closeItemPanel();
	}
	
	private void itemButton_onClick(int buttonIndex) {
		Item item = mItems.get(mListOffset + buttonIndex);
		//Ensure that this button is supposed to be clickable before executing
		if (item != null && mItemPanel == null) {
			showItem(item);
		}
	}
	
	private void prevButton_onClick(ClickEvent e) {
		if (mListOffset > 0) {
			mListOffset -= ITEMS_PER_PAGE;
			showPage();
		}
	}
	
	private void nextButton_onClick(ClickEvent e) {
		if (mItems.size() > mListOffset + ITEMS_PER_PAGE) {
			mListOffset += ITEMS_PER_PAGE;
			showPage();
		}
	}
	
	private void closeButton_onClick(ClickEvent e) {
		close();
	}
	
	private void close() {
		removeFromParent();
		if (mCloseHandler != null) {
			mCloseHandler.onClose(this);
			mCloseHandler = null;
		}
	}

	@Override
	public void setCloseHandler(CloseHandler handler) {
		mCloseHandler = handler;
	}
}
