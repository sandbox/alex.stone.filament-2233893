package org.icivics.arg.ui.evidenceEvaluator.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.Glossary;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.GlossariedTextPanel;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceListPanel.Handler;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar.SelectChunkHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceSidebar extends UIPanel {
	
	
	private static EvidenceSidebarUiBinder uiBinder = GWT.create(EvidenceSidebarUiBinder.class);
	interface EvidenceSidebarUiBinder extends UiBinder<Widget, EvidenceSidebar> {}

	private ArrayList<Item> mItems;
	private ArrayList<HandlerRegistration> mClickRegs = new ArrayList<HandlerRegistration>();
	private ArrayList<HandlerRegistration> chunkRegs = new ArrayList<HandlerRegistration>();
	
	private EvidenceDetailPanel mItemPanel;
	
	public interface ViewProgressHandler {void onViewProgress();}
	ViewProgressHandler progressHandler;
	
	public interface SelectItemHandler { void onSelectItem(Item item); }
	SelectItemHandler selectHandler;
	public interface SelectChunkHandler {void onSelectChunk(Item item, int chunkIndex); void onSelectChunk(Item item, String chunkText);}
	SelectChunkHandler chunkHandler;
	public String sentenceText;
	
	private Boolean viewingItem = false;
		
	private static class nonRandomNumber {
		private static int pos = 0;
		private static double[] notRandom = {
			0.9416580998,
			0.4197498646,
			0.2250091824,
			0.4048365468,
			0.9366917809,
			0.9072792036,
			0.1293503856,
			0.0464706768,
			0.1109572070,
			0.7296857655,
			0.7543672095,
			0.9981714065,
			0.2492912975,
			0.6675723008,
			0.8143805620,
			0.8770149430,
			0.2938728737,
			0.8845859924,
			0.5162195128,
			0.0682359176,
			0.7513745327,
			0.7111013928,
			0.5319584199,
			0.1984843234,
			0.9346577111,
			0.3585057994,
			0.9738510630,
			0.8028284940,
			0.0083721567,
			0.2394487061
		};
		
		public static void reset(int seed){
			pos = seed%notRandom.length;
		}
		
		public static float getNum(){
			pos++;
			if(pos >= notRandom.length)
				pos = 0;
			return (float) notRandom[pos];
		}
	}
	
	@UiField
	SimplePanel contentContainer;
	
	@UiField
	HTMLPanel itemListHolder, evidenceHeader;
	
	@UiField 
	SimplePanel panelMount;
	
	@UiField
	HTMLPanel backButton;
	
	@UiField
	HTMLPanel evidenceContentHolder;
	
	@UiField
	Element sidebar;
	
	@UiField
	public HTMLPanel contentStyleContainer;
	
	@UiField
	HTMLPanel progressSoFar;

	HTMLPanel itemPanelWrapper;
	ArrayList<HTMLPanel> itemPanelList;
	int lastScrollPosition;
	private float mWidth;
	
	private HandlerRegistration progressReg;
	
	public EvidenceSidebar(){
		super();
		itemPanelList = new ArrayList<HTMLPanel>();
		initWidget(uiBinder.createAndBindUi(this));
		lastScrollPosition = 0;
	}

	@Override
	protected void onAttach(){
		super.onAttach();
		
		mWidth = getCurrentWidthPct();
		GWT.log("Current width is " + mWidth);
		hide(true);
		//sidebar.getStyle().setLeft(100, Unit.PCT);
		//contentStyleContainer.getElement().getStyle().setWidth(100,Unit.PCT);
		
		enableProgressButton(true);
	}
	
	public void init(){
		GWT.log("Sidebar init with nothing");
		Item[] srcItems = new Item[0];//DataController.getCurrentIssue().issue.items;
		mItems = new ArrayList<Item>();
		
		for (int i = 0; i < srcItems.length && srcItems[i] != null; ++i)
			mItems.add(srcItems[i]);
		
		if(mItems.size() > 0){
			shuffle(mItems);
		}
		
		initInterface();
	}
	
	public void init(Claim claim) {
		GWT.log("Sidebar init with claim");
		Reason[] reasons = claim.reasons;
		mItems = new ArrayList<Item>();
		for (int i = 0; i < reasons.length; ++i)
			for (int j = 0; j < reasons[j].items.length; ++j)
				mItems.add(reasons[i].items[j]);
		
		if(mItems.size() > 0){
			shuffle(mItems);
		}
		
		initInterface();
	}
	
	private void shuffle(ArrayList<Item> list){
		int count = list.size();
		
		if(count > 0)
			nonRandomNumber.reset(13);//nonRandomNumber.reset(Integer.parseInt(list.get(0).id));
		// More shuffle -> more random, right?
		for(int k = 0; k < 3; k++)
			for(int i = 0, j=0; i<count; i++){
				j = (int) Math.floor(count*nonRandomNumber.getNum());
				//GWT.log("Swapping " + i + " and " + j + " On the " + (k+1) + "'th time through the list");
				Collections.swap(list, i, j);
			}
	}
	
	public void init(Item items[]){
		GWT.log("Sidebar init with array");
		mItems = new ArrayList<Item>();
		int count = items.length;
		for (int i = 0; i < count; ++i)
			mItems.add(items[i]);
		initInterface();
	}
	
	private void initInterface(){
		// Make sure the item panel is closed
		onItemPanelClose();
		
		// Disable the back button
		showBackButton(false);

		// Clear click handlers, just in case
		for (int i = 0, count = mClickRegs.size(); i < count; ++i)
			mClickRegs.get(i).removeHandler();
		mClickRegs.clear();
		
		itemPanelList = new ArrayList<HTMLPanel>();
		itemPanelWrapper = new HTMLPanel("");
		itemPanelWrapper.setStyleName("evidence-item-wrapper");
		
		itemListHolder.clear();
		
		int count = mItems.size();
		for(int i = 0; i < count; ++i){
			final int itemIndex = i;
			
			Item item = mItems.get(i);
			
			String imgsrc = DataController.getIconPath(item.type);
			
			HTMLPanel itemPanel = 
				new HTMLPanel("<div class=\"evidence-item-inner\"><div class=\"evidence-icon\"><img src=\"" + imgsrc +"\"/></div><div class=\"text\"><p></p><p></p></div><div class=\"evidence-arrow\"></div>");
			
			// For some reason this messes all sorts of things up: <div class=\"link\"><a href=\"#\"></a></div>
			// <img src=\"images/evidence_gavel.png\"/>
			
			itemPanel.setStyleName("evidence-item");
			itemPanel.getElement().setId("evidenceBtn" + item.id);
			
			
			ParagraphElement text = (ParagraphElement) itemPanel.getElement().getElementsByTagName("p").getItem(0);
			ParagraphElement text2 = (ParagraphElement) itemPanel.getElement().getElementsByTagName("p").getItem(1);
			DivElement icon = (DivElement) itemPanel.getElement().getElementsByTagName("div").getItem(1);
			
			text.setInnerText(item.name);
			text.setClassName("title");
			text2.setInnerText(item.description);
			text2.setClassName("summary");
			icon.setClassName("evidence-icon type" + item.type);
			
			// Set up click handling
			ClickHandler aClickHandler = new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							itemButton_onClick(itemIndex);
						}
					};
			mClickRegs.add(itemPanel.addDomHandler(aClickHandler, ClickEvent.getType()));
			itemPanel.addStyleName("clickable");
			
			itemPanelWrapper.add(itemPanel);
			
			itemPanelList.add(itemPanel);
		}
		itemListHolder.add(itemPanelWrapper);
		
		// Setup the scrolling thing, now that the evidence panel exists for sure
		setUpScrollBars();
		setTextScaling();
	}
	
	private void restoreInterface(){
		// Disable the back button
		showBackButton(false);
		
		evidenceContentHolder.add(itemListHolder);
		
		setUpScrollBars(lastScrollPosition);
		setTextScaling();
	}
	
	public SimplePanel getContentContainer() {
		return contentContainer;
	}
	 
	private void itemButton_onClick(int itemIndex){
		Item item = mItems.get(itemIndex);
		
		if (item != null) {
			showItem(item);
		}
	}
	
	public void showItem(Item item){
		showItem(item,false, false);
	}
	
	public void showItem(Item item, Boolean highlightElements){
		showItem(item, highlightElements, false);
	}
	
	public void showItem(final Item item, Boolean highlightElements, boolean splitBySentence) {
		HTMLPanel listPanel = new HTMLPanel("");
		listPanel.addStyleName("evidence-list");
		
		HTMLPanel detailWrapPanel = new HTMLPanel("");
		detailWrapPanel.addStyleName("evidence-detail-wrap");
		
		HTMLPanel detailPanel = new HTMLPanel("");
		detailPanel.addStyleName("evidence-detail");
		
		AudioControl audioPanel = new AudioControl();
		audioPanel.loadSound(item.audioSrc, "evidenceSound" + item.id);
		
		HTMLPanel imagePanel = new HTMLPanel("<img src=\"" + item.imageSrc + "\" alt=\"\"/>");
		imagePanel.addStyleName("image");
		
		HTMLPanel titlePanel = new HTMLPanel("<p>" + item.name + "</p>");
		titlePanel.addStyleName("title");
		
		
		GlossariedTextPanel contentPanel;
		String contentStr;
		if(highlightElements && splitBySentence){
			contentStr = item.getTextForSegmentation();
			for (int i = 0, count = item.chunks.size(); i < count; ++i) {
				RegExp regex = RegExp.compile("\\$\\$","g");
				String inner = regex.replace(item.chunks.get(i).itemText,"$$" );
				
				regex = RegExp.compile(ItemChunk.REPLACEMENT_TOKEN_REGEX,"");
				contentStr = regex.replace(contentStr, inner);
				
				//
				regex = RegExp.compile(ItemChunk.DELIMITER_TOKEN_START_REGEX,"g");
				contentStr = regex.replace(contentStr, "<span class=\"choice\">");
				
				regex = RegExp.compile(ItemChunk.DELIMITER_TOKEN_END_REGEX,"g");
				contentStr = regex.replace(contentStr, "</span>");
				//
				
				regex = RegExp.compile(ItemChunk.CLASS_TOKEN_REGEX, "g");
				contentStr = regex.replace(contentStr, "class=\"choice\"");
				
				//contentStr = contentStr.replaceFirst(ItemChunk.REPLACEMENT_TOKEN_REGEX, 
				//		"<span id=\"chunk" + i + "\"> </span>");
			}
			
			contentStr = "<p>" + contentStr + "</p>";
		} else if(highlightElements){
			contentStr = item.getTextForPC();
			for (int i = 0, count = item.chunks.size(); i < count; ++i) {
				RegExp regex = RegExp.compile(ItemChunk.REPLACEMENT_TOKEN_REGEX,"");
				contentStr = regex.replace(contentStr, "<span id=\"chunk" + i + "\"> </span>");
				//contentStr = contentStr.replaceFirst(ItemChunk.REPLACEMENT_TOKEN_REGEX, 
				//		"<span id=\"chunk" + i + "\"> </span>");
			}
			contentStr = "<p>" + contentStr + "</p>";
		}
		else{
			contentStr = "<p>" + item.getFullText() + "</p>";
		}
		
		contentPanel = new GlossariedTextPanel(contentStr);
		contentPanel.addStyleName("evidence-detail-content");
		
		
		
		detailWrapPanel.add(detailPanel);
		detailPanel.add(audioPanel);
		detailPanel.add(imagePanel);
		detailPanel.add(titlePanel);
		detailPanel.add(contentPanel);
		
		
		HTMLPanel spacer = new HTMLPanel("Some text that I shall hide away later");
		spacer.addStyleName("spacer");
		spacer.getElement().getStyle().setHeight(400,Unit.PX);
		spacer.getElement().getStyle().setTextIndent(-9999, Unit.PX);
		spacer.getElement().getStyle().setProperty("display", "table-cell");
		detailPanel.add(spacer);
		
		
		listPanel.add(detailWrapPanel);
		
		lastScrollPosition = getCurrentScrollPosition();
		evidenceContentHolder.clear();

		evidenceContentHolder.add(listPanel);
		
		showBackButton(true);
		/*
		backButton.removeStyleName("disabled");
		backButton.addStyleName("clickable");
		*/
		
		//Add back button event listener
		backButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onItemPanelClose();
			}
		}, ClickEvent.getType());
		
		
		if(highlightElements){
			if(splitBySentence){
				GWT.log("Highlighting elements");
				if(chunkHandler != null){
					chunkRegs.add(contentPanel.addDomHandler(new ClickHandler(){
						@Override
						public void onClick(ClickEvent event) {
							Element tgt = Element.as(event.getNativeEvent().getEventTarget());
							Element tgtParent = tgt.getParentElement();
							String txt = tgt.getInnerText();
							if(tgt.hasClassName("choice") ){
								chunkHandler.onSelectChunk(item, txt);
							} else if(tgtParent != null && tgtParent.hasClassName("choice")){
								txt = tgtParent.getInnerText();
								chunkHandler.onSelectChunk(item, txt);
							}
						}
					}, ClickEvent.getType()));
				}
			} else {
				for (int i = 0, count = item.chunks.size(); i < count; ++i) {
					GWT.log("Highlighting elements");
					final int chunkIndex = i;
					Element chunk = contentPanel.getElementById("chunk" + i);
					RegExp regex = RegExp.compile("\\$\\$","g");
					HTMLPanel inner = new HTMLPanel("span", regex.replace(item.chunks.get(i).itemText,"$$" ));
					inner.addStyleName("choice");
					getRoot().add(inner, chunk);
					
					if(chunkHandler != null){
						chunkRegs.add(inner.addDomHandler(new ClickHandler(){
							@Override
							public void onClick(ClickEvent event) {
								chunkHandler.onSelectChunk(item, chunkIndex);
							}}, ClickEvent.getType()
						));
					} 
					
				}
			}
		}
		setTextScaling();
		setUpScrollBars();
		viewingItem = true;
		
		if(selectHandler != null && !highlightElements)
			selectHandler.onSelectItem(item);
	}
	
	public void onItemPanelClose(boolean supressHandler){
		if(!supressHandler){
			if (selectHandler != null && viewingItem){
				viewingItem = false;
				selectHandler.onSelectItem(null);
			}
		}
		
		GWT.log("Closing evidence panel");
		
		for(int i = 0; i < chunkRegs.size(); i++){
			chunkRegs.get(i).removeHandler();
		}
		chunkRegs.clear();
		clearChunkHandler();
		showBackButton(false);
		evidenceContentHolder.clear();

		restoreInterface();
	}
	
	public void onItemPanelClose() {
		onItemPanelClose(false);
	}

	public void setEvidenceHandler(EvidenceSidebar.SelectItemHandler handler2) {
		selectHandler = handler2;
	}
	
	public void setChunkHandler(SelectChunkHandler selectChunkHandler) {
		chunkHandler = selectChunkHandler;
	}

	public void setProgressHandler(ViewProgressHandler handler){
		progressHandler = handler;
	}
	
	public void clearEvidenceHandler() {
		selectHandler = null;
	}

	public void clearChunkHandler(){
		chunkHandler = null;
	}
	
	public void clearProgressHandler(){
		progressHandler = null;
	}
	
	private void showBackButton(boolean show){
		if(show){
			// Add the back button to the top bit
			backButton.removeStyleName("disabled");
			backButton.addStyleName("clickable");
			
			evidenceHeader.add(backButton);
		} else {
			// Remove the back button from the top bit
			backButton.removeFromParent();
		}
	}
	
	public void markItem(Item item, boolean mark) {
		// Find the right div
		Element element = itemListHolder.getElementById("evidenceBtn" + item.id);
		// Apply the style to it
		if(mark && element != null)
			element.addClassName("used");
		else
			element.removeClassName("used");
	}


	public void clearMarked() {
		for(int i =0; i < mItems.size(); i++){
			markItem(mItems.get(i), false);
		}
	}
	
	public static void setUpScrollBars(){
		setUpScrollBars(0);
	}
	
	public void hide(boolean full){
		mWidth = 35; //Hardcoded width: should probably be read off from the css
		sidebar.getStyle().setLeft(100, Unit.PCT);
		contentStyleContainer.getElement().getStyle().setLeft(0, Unit.PCT);
		//contentStyleContainer.getElement().getStyle().setWidth(100,Unit.PCT);
		GWT.log("Making content area full screen");
		if(full){
			contentStyleContainer.addStyleName("content-container-full");
			contentStyleContainer.removeStyleName("content-container");
		} else {
			contentStyleContainer.removeStyleName("content-container-full");
			contentStyleContainer.addStyleName("content-container");
		}
	}
	
	public void hide(){
		hide(false);
	}
	
	public void show(){
		GWT.log("Showing the sidebar? " + mWidth + " Should be set to a width of ");
		
		sidebar.getStyle().setLeft(100 - mWidth, Unit.PCT);
		contentStyleContainer.getElement().getStyle().setLeft(0, Unit.PCT);
		contentStyleContainer.removeStyleName("content-container-full");
		contentStyleContainer.addStyleName("content-container");
		//contentStyleContainer.getElement().getStyle().setWidth(100 - mWidth,Unit.PCT);
	}
	
	public void enableProgressButton(boolean enable){
		if(enable){
			progressReg = progressSoFar.addDomHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					onItemPanelClose();
					progressHandler.onViewProgress();
				}},ClickEvent.getType());
			
			progressSoFar.removeStyleName("disabled");
		}else{
			progressReg.removeHandler();
			progressSoFar.addStyleName("disabled");
		}
	}
	
	public static native void setUpScrollBars(int pos)/*-{
		$wnd.$('.evidence-list').perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true,
				useKeyboard: false
			});
		
		$wnd.$('.evidence-list').scrollTop(pos);
		$wnd.$('.evidence-list').perfectScrollbar('update');
		
	}-*/;
	
	private static native void setTextScaling()/*-{
		$wnd.scaleText();
		
		$wnd.$('.evidence-list').perfectScrollbar('update');
	}-*/;
	
	private static native int getCurrentScrollPosition()/*-{
		return $wnd.$('.evidence-list').scrollTop();
	}-*/;
	
	private static native float getCurrentWidthPct()/*-{
		return parseFloat($wnd.$('.evidence-container').css('width')) / parseFloat($wnd.$('.argumentation').css('width'));
	}-*/;
	
	private static native void animateToWidth(int newWidthPct)/*-{
		$wnd.$(".evidence-container").animate({left:(100 - newWidthPct) + "%"},{duration:500, complete:$wnd.scaleText});
		$wnd.$(".content-container").animate({width: (100 - newWidthPct)+"%"},500);
	}-*/;

	
}
