package org.icivics.arg.ui.common.client;

import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.dom.client.Style.Position;

public class Subpanel<Parent extends UIPanel> extends UIPanel {
	private Parent parent;
	
	public Subpanel() {
		super();
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		getElement().getStyle().setPosition(Position.ABSOLUTE);
		//setUpScrollBars();
		setTextScaling();
	}
	
	public Parent getParentPanel() {
		return parent;
	}
	
	public void setParentPanel(Parent v) {
		parent = v;
	}
	
	public void finishShowing() {
		setTextScaling();
	}
	
	public void startHiding() {
		
	}
	
	public Subpanel(Parent parent) {
		this.parent = parent;
	}
	
	public static native void setUpScrollBars()/*-{
		$wnd.$('.evidence-list').scrollTop('0');
		$wnd.$('.evidence-list').perfectScrollbar('update');
	}-*/;
	
	private static native void setTextScaling()/*-{
		$wnd.scaleText();
	}-*/;
}
