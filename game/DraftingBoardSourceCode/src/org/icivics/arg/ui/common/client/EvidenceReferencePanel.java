package org.icivics.arg.ui.common.client;

import org.icivics.arg.data.model.client.Item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceReferencePanel extends UIPanel {
	private static EvidenceReferencePanelUiBinder uiBinder = GWT.create(EvidenceReferencePanelUiBinder.class);
	interface EvidenceReferencePanelUiBinder extends UiBinder<Widget, EvidenceReferencePanel> {}
	
	@UiField
	ParagraphElement titleText;
	@UiField
	ParagraphElement bodyText;
	
	@UiField
	protected HTMLPanel closeButton;
	
	public static interface Handler { void onClose(); }
	public Handler handler;
	
	private HandlerRegistration mCloseClickReg;
	
	private Item mItem;
	public Item getItem() { return mItem; }
	
	public EvidenceReferencePanel(Item item) {
		mItem = item;
		initWidget(uiBinder.createAndBindUi(this));
		titleText.setInnerText(item.name);
		bodyText.setInnerText(item.getFullText());
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		mCloseClickReg = closeButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}
		}, ClickEvent.getType());
	}
	
	@Override
	protected void onDetach() {
		if (mCloseClickReg != null) {
			mCloseClickReg.removeHandler();
			mCloseClickReg = null;
		}
		super.onDetach();
	}

	protected void closeButton_onClick() {
		removeFromParent();
		if (handler != null)
			handler.onClose();
	}
	
	public void destroy() {
		handler = null;
		mItem = null;
	}
}
