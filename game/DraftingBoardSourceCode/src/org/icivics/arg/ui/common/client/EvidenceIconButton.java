package org.icivics.arg.ui.common.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class EvidenceIconButton extends BasicButton {
	private static EvidenceIconButtonUiBinder uiBinder = GWT.create(EvidenceIconButtonUiBinder.class);
	interface EvidenceIconButtonUiBinder extends UiBinder<Widget, EvidenceIconButton> {}
	
	@UiField
	protected DivElement iconHolder;
	
	protected int mType;
	
	public EvidenceIconButton() {
		super(uiBinder);
		initWidget(uiBinder.createAndBindUi(this));
		initState();
	}
	
	@Override
	protected void initState() {
		super.initState();
		setType(1);
	}
	
	public int getType() {
		return mType;
	}
	
	public void setType(int type) {
		iconHolder.replaceClassName("type" + mType, "type" + type);
		mType = type;
	}
}
