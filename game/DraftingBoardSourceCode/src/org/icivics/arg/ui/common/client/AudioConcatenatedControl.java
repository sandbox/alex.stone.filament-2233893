package org.icivics.arg.ui.common.client;

import java.util.ArrayList;

import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;



public class AudioConcatenatedControl extends AudioControl {
	@UiTemplate("AudioControl.ui.xml")
	interface AudioConcatenatedControlBinder extends UiBinder<Widget,AudioConcatenatedControl>{}
	private static AudioConcatenatedControlBinder uiBinder = GWT.create(AudioConcatenatedControlBinder.class);

	private ArrayList<String> soundIds;
	private ArrayList<Float> durations;
	private float mTotalDuration;
	private int mCurrentSoundIndex;
	private float mCurrentSoundPosition;
	
	private boolean swappingSound=false;
	
	
	public AudioConcatenatedControl() {
		//initWidget(uiBinder.createAndBindUi(this));
		super();
		soundIds = new ArrayList<String>();
		durations = new ArrayList<Float>();
		mTotalDuration = 0;
		mCurrentSoundIndex = -1;
	}
	
	public void clear(){
		if(mCurrentSoundIndex != -1)
			pauseSound(soundIds.get(mCurrentSoundIndex));
		soundIds = new ArrayList<String>();
		durations = new ArrayList<Float>();
		mTotalDuration = 0;
		mCurrentSoundIndex = -1;
		mCurrentSoundPosition = 0;
		progressBar.getElement().getStyle().setWidth(0, Unit.PCT);
	}
	
	@Override
	public void loadSound(String url, String id){
		super.loadSound(url,id);
		soundIds.add(id);
		GWT.log("Loading sound " + id);
		durations.add(getSoundDuration(id));
		recomputeDuration();
		if(mCurrentSoundIndex == -1)
			mCurrentSoundIndex = 0;
	}
	
	@Override
	protected void playPauseBtn_onClick(ClickEvent event){
		progressWrapper.removeStyleName("hidden");
		if(mCurrentSoundIndex > -1){
			if(!playing){
				playSound(soundIds.get(mCurrentSoundIndex));
				playPauseBtn.addStyleName("pause");
				playing = true;
			} else {
				pauseSound(soundIds.get(mCurrentSoundIndex));
				playPauseBtn.removeStyleName("pause");
				playing = false;
			}
		}
	}
	
	@Override
	protected void backBtn_onClick(ClickEvent event){
		if(mCurrentSoundIndex != -1){
			pauseSound(soundIds.get(mCurrentSoundIndex));
			mCurrentSoundIndex = 0;
			seekSoundTo(soundIds.get(mCurrentSoundIndex),0);
			if(playing)
				playSound(soundIds.get(mCurrentSoundIndex));
		}
	}
	
	@Override
	protected void progressBar_onClick(double x){
		final boolean restart = playing; 
		if(mCurrentSoundIndex != -1)
			pauseSound(soundIds.get(mCurrentSoundIndex));
		progressBar_onDrag(x);
		progressBarRegs[0].removeHandler();
		progressBarRegs[0] = progressWrapper.addDomHandler(new MouseMoveHandler(){
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				progressBar_onDrag(event.getRelativeX(progressWrapper.getElement()));
			}
		}, MouseMoveEvent.getType());
		
		
		progressBarRegs[1] = RootPanel.get().addDomHandler(new MouseUpHandler(){
			@Override
			public void onMouseUp(MouseUpEvent event) {
				progressBar_onRelease();
				if(restart && mCurrentSoundIndex != -1){
					playing = true;
					playSound(soundIds.get(mCurrentSoundIndex));
				}
		}}, MouseUpEvent.getType());
	}
	
	@Override
	protected void progressBar_onDrag(double x){
		double pos;
		pos = x/progressWrapper.getElement().getClientWidth();
		
		float posInSeconds = (float) (pos*mTotalDuration);
		int sndId = 0;
		while(posInSeconds - durations.get(sndId) > 0 && sndId < soundIds.size()){
			posInSeconds -= durations.get(sndId);
			sndId++;
		}
		if(mCurrentSoundIndex != -1){
			GWT.log("Current sound swapped from "+mCurrentSoundIndex+" to "+sndId);
			mCurrentSoundIndex = sndId;
			seekSoundTo(soundIds.get(sndId),posInSeconds/durations.get(sndId));
		}
		//progressBar.getElement().getStyle().setWidth(pos*100, Unit.PCT);
	}
	
	@Override
	protected void progressBar_onRelease(){
		int count = progressBarRegs.length;
		for(int i=0; i < count; i++)
			if(progressBarRegs[i] != null)
				progressBarRegs[i].removeHandler();
		
		progressBarRegs[0] = progressWrapper.addDomHandler(new MouseDownHandler(){
			@Override
			public void onMouseDown(MouseDownEvent event) {
				progressBar_onClick(event.getRelativeX(progressWrapper.getElement()));
		}},MouseDownEvent.getType());
		
		
	}
	
	@Override
	protected void whilePlaying(float posPct){
		mCurrentSoundPosition = posPct;
		
		if(posPct == 1)
			GWT.log("Played to the very end");
		
		GWT.log("Playing callback called for sound " + mCurrentSoundIndex);
		recomputeDuration();
		float totPos = 0;
		for(int i=0; i < mCurrentSoundIndex; i++){
			totPos += durations.get(i);
		}
		if(mCurrentSoundIndex != -1)
			totPos += posPct*durations.get(mCurrentSoundIndex);
		
		progressBar.getElement().getStyle().setWidth((totPos/mTotalDuration)*100, Unit.PCT);
		if(playing)
			playPauseBtn.addStyleName("pause");
	}
	
	@Override
	protected void onPause(){
		if(!swappingSound){
			GWT.log("Sound paused");
			playing=false;
			playPauseBtn.removeStyleName("pause");
		}
	}
	
	@Override
	protected void whenFinished(){
		GWT.log("Finished playing a sound: " + mCurrentSoundIndex);
		if(mCurrentSoundIndex +1 == soundIds.size()){
			//Last file in the set, go to beginning
			mCurrentSoundIndex = 0;
			playing = false;
			playPauseBtn.removeStyleName("pause");
			seekSoundTo(soundIds.get(mCurrentSoundIndex),0);
			for(int i=0,k=soundIds.size();i<k;i++){
				String curId = soundIds.get(i);
				
				swappingSound = true;
				seekSoundTo(curId,0);
				playSound(curId);
				pauseSound(curId);
				seekSoundTo(curId,0);
				swappingSound = false;
			}
		} else if(mCurrentSoundIndex != -1){
			String curId = soundIds.get(mCurrentSoundIndex);
			mCurrentSoundIndex++;
			seekSoundTo(soundIds.get(mCurrentSoundIndex),0);
			
			swappingSound = true;
			seekSoundTo(curId,0);
			playSound(curId);
			pauseSound(curId);
			seekSoundTo(curId,0);
			swappingSound = false;
			
			playSound(soundIds.get(mCurrentSoundIndex));
		}
	}
	
	private void recomputeDuration(){
		float dur = 0;
		for(int i=0,num = durations.size();i<num;i++){
			durations.set(i, getSoundDuration(soundIds.get(i)));
			dur+=durations.get(i);
		}
		mTotalDuration = dur;
	}
	
	private native float getSoundDuration(String soundId)/*-{
		var sound = $wnd.soundManager.getSoundById(soundId);
		
		var dur;
		if(sound != null)
			dur = sound.duration;
		if(dur != null)
			return dur;
		else 
			return 0;
	}-*/;

	public int getPlayingFileIndex() {
		return mCurrentSoundIndex;
	}

	public float getPlayingFilePosition() {
		return mCurrentSoundPosition;
	}

	public void playAt(int index, float position, boolean wasPlaying) {
		LogUtil.log("trying to play a thing again " + index + " " +soundIds.size() + " " + position + " " + wasPlaying);
		
		if(index < 0 || index > soundIds.size() || soundIds.size() == 0){
			GWT.log("Trying to play nonexistant file");
			LogUtil.log("File does not seem to be in the list of files");
		} else if(position < 0 || position > 1){
			GWT.log("Trying to play existing file at non-existing position");
			LogUtil.log("The position does not make sense");
		} else {
			/*
			for(int i=0,k=soundIds.size();i<k;i++){
				String curId = soundIds.get(i);
				swappingSound = true;
				seekSoundTo(curId,0);
				playSound(curId);
				pauseSound(curId);
				seekSoundTo(curId,0);
				swappingSound = false;
			}
			*/
			LogUtil.log("Actually trying to play a thing");
			LogUtil.log("trying to play sound number " + index + ": " + soundIds.get(index) + "which replaces" + mCurrentSoundIndex + " at position " + position + " it was playing before:" + wasPlaying);
			
			String id = soundIds.get(index);
			mCurrentSoundIndex = index;
			if(wasPlaying){
				playSound(soundIds.get(mCurrentSoundIndex));
				playing = true;
				playPauseBtn.addStyleName("pause");
			}
			seekSoundTo(id,position);
		}
			
		LogUtil.log("trying to play sound number " + index + ": " + soundIds.get(index) + "which replaces" + mCurrentSoundIndex + " at position " + position + " it was playing before:" + wasPlaying);
		
	}
}
