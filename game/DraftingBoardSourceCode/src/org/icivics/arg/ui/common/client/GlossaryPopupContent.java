package org.icivics.arg.ui.common.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class GlossaryPopupContent extends Composite {

	private static GlossaryPopupContentUiBinder uiBinder = GWT.create(GlossaryPopupContentUiBinder.class);
	interface GlossaryPopupContentUiBinder extends UiBinder<Widget, GlossaryPopupContent> {}

	@UiField
	HTMLPanel termField;
	@UiField
	HTMLPanel definitionField;
	
	
	public GlossaryPopupContent(String term, String definition) {
		initWidget(uiBinder.createAndBindUi(this));
		termField.getElement().setInnerText(term);
		definitionField.getElement().setInnerHTML(definition);
	}


	public GlossaryPopupContent(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
	}


}
