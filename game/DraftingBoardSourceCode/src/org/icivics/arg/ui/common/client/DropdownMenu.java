package org.icivics.arg.ui.common.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class DropdownMenu extends UIPanel {
	private static RolloverMenuUiBinder uiBinder = GWT.create(RolloverMenuUiBinder.class);
	interface RolloverMenuUiBinder extends UiBinder<Widget, DropdownMenu> {}
	
	@UiField
	LabelButton button;
	@UiField
	UListElement styleHolder;
	@UiField
	UListElement optionsHolder;
	
	public static interface Handler { void onSelect(); }
	public Handler handler;
	
	private ArrayList<DropdownMenuItem> mItems = new ArrayList<DropdownMenuItem>();
	private ArrayList<HandlerRegistration> mItemRegs = new ArrayList<HandlerRegistration>();
	private String mDefaultText = "";
	private boolean mEnabled = true;
	
	private boolean mExpanded = false;
	private int mSelectedIndex = -1;
	
	private HandlerRegistration mMouseOutReg;
	
	public DropdownMenu() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		if (mEnabled) {
			attachListeners();
		}
	}
	
	@Override
	protected void onDetach() {
		if (mEnabled) {
			detachListeners();
		}
		super.onDetach();
	}
	
	private void attachListeners() {
		mMouseOutReg = getRoot().addDomHandler(new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {
				root_onMouseOut();
			}
		}, MouseOutEvent.getType());
		button.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				button_onClick();
			}
		};
	}
	private void detachListeners() {
		if (mMouseOutReg != null) {
			mMouseOutReg.removeHandler();
			mMouseOutReg = null;
		}
		button.clickHandler = null;
	}
	
	public String getLabelText() {
		return mDefaultText;
	}
	public void setLabelText(String text) {
		mDefaultText = text;
		if (mSelectedIndex == -1)
			doSetLabelText(text);
	}
	
	public String getCurrentLabelText() {
		return button.getText();
	}
	private void doSetLabelText(String text) {
		button.setText(text);
	}
	
	public void setInnerClass(String text) {
		styleHolder.setClassName(text);
	}
	
	public boolean getEnabled() {
		return mEnabled;
	}
	public void setEnabled(boolean v) {
		if (mEnabled != v) {
			mEnabled = v;
			if (v) {
				removeStyleName("disabled");
			} else {
				addStyleName("disabled");
			}
		}
	}
	
	public String getValue() {
		return mSelectedIndex == -1 ? null : mItems.get(mSelectedIndex).getValue();
	}
	
	public int getSelectedIndex() {
		return mSelectedIndex;
	}
	
	public void selectItem(int index) {
		mSelectedIndex = index;
		doSetLabelText(mItems.get(index).getLabelText());
		if (handler != null)
			handler.onSelect();
	}
	
	public void clearSelection() {
		mSelectedIndex = -1;
		doSetLabelText(mDefaultText);
	}
	
	public DropdownMenuItem addItem(String value) {
		return addItem(value, value);
	}
	
	public DropdownMenuItem addItem(String value, String labelText) {
		final int index = mItems.size();
		DropdownMenuItem option = new DropdownMenuItem(value, labelText);
		mItems.add(option);
		getRoot().add(option, optionsHolder);
		mItemRegs.add(option.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				item_onClick(index);
			}
		}, ClickEvent.getType()));
		return option;
	}
	
	public DropdownMenuItem removeItem(String value) {
		for (int i = 0, count = mItems.size(); i < count; ++i) {
			DropdownMenuItem option = mItems.get(i);
			if (option.getValue().equals(value)) {
				return removeItem(i);
			}
		}
		return null;
	}
	
	public DropdownMenuItem removeItem(int index) {
		mItemRegs.get(index).removeHandler();
		mItemRegs.remove(index);
		
		mItems.get(index).removeFromParent();
		return mItems.remove(index);
	}
	
	protected void button_onClick() {
		if (mExpanded)
			collapseMenu();
		else
			expandMenu();
	}
	
	protected void root_onMouseOut() {
		collapseMenu();
	}
	
	protected void item_onClick(int index) {
		collapseMenu();
		selectItem(index);
	}
	
	protected void expandMenu() {
		if (mExpanded)
			return;
		
		mExpanded = true;
		optionsHolder.removeClassName("hidden");
	}
	
	protected void collapseMenu() {
		if (!mExpanded)
			return;
		
		mExpanded = false;
		optionsHolder.addClassName("hidden");
	}
	
	public void destroy() {
		optionsHolder.removeClassName("hidden");
		removeStyleName("disabled");
		mSelectedIndex = -1;
		mEnabled = false;
		handler = null;
		
		int i = mItems.size();
		while (i-- > 0) {
			mItemRegs.get(i).removeHandler();
			mItems.get(i).removeFromParent();
		}
		mItemRegs = null;
		mItems = null;
		
		button.destroy();
	}
}
