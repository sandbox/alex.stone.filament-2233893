package org.icivics.arg.ui.common.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

public class UIPanel extends Composite {
	public HTMLPanel getRoot() {
		return (HTMLPanel) getWidget();
	}

	public UIPanel() {
		
	}
}
