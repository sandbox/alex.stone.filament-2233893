package org.icivics.arg.ui.common.client;

import org.icivics.arg.anim.client.Ease;
import org.icivics.arg.anim.client.Tween;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.ui.global.client.Reward;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ModuleUI<T extends ModuleUI<T>> extends UIPanel {
	private static final double DISTANCE = 1080.0;
	private static final int TRANSITION_MS = 2000;
	
	private static ModuleUIUiBinder uiBinder = GWT.create(ModuleUIUiBinder.class);
	interface ModuleUIUiBinder extends UiBinder<Widget, ModuleUI> {}
	
	private Subpanel<T> mCurrentUI;
	private Subpanel<T> mNextUI;
	private Label mInputBlocker;
	
	private Tween mTransition;
	
	public static interface Handler { void onComplete(); }
	public Handler handler;
	
	public EvidenceSidebar sidebar;
	
	public ModuleUI() {
		initWidget(uiBinder.createAndBindUi(this));
		mInputBlocker = new Label();
		
		mInputBlocker.getElement().getStyle().setPosition(Position.ABSOLUTE);
		mInputBlocker.getElement().getStyle().setLeft(0, Unit.PX);
		mInputBlocker.getElement().getStyle().setTop(0, Unit.PX);
		mInputBlocker.getElement().getStyle().setWidth(940, Unit.PX);
		mInputBlocker.getElement().getStyle().setHeight(640, Unit.PX);
		
	}
	
	public ModuleUI(Handler handler) {
		this();
		this.handler = handler;
	}
	
	@Override
	protected void onDetach() {
		closeUI();
		super.onDetach();
	}
	
	protected void commitScore() {
		
	}
	
	protected void finish() {
		commitScore();
		if(sidebar!=null)
			sidebar.hide();
		transitionTo(new Reward<T>(new Reward.Handler() {
			@Override
			public void onComplete() {
				reward_onComplete();
			}
		}));
	}
	
	public void exit() {
		removeFromParent();
		if (handler != null) {
			handler.onComplete();
			handler = null;
		}
	}
	
	protected static void setBackgroundX(int x) {
		if (RootPanel.get("screen") == null)
			GWT.log("RootPanel.get(screen) is null");
		else if (RootPanel.get("screen").getElement() == null)
			GWT.log("RootPanel.get(screen).getElement() is null");
		else if (RootPanel.get("screen").getElement().getStyle() == null)
			GWT.log("RootPanel.get(screen).getElement().getStyle() is null");
		else
			RootPanel.get("screen").getElement().getStyle().setProperty("backgroundPosition", x + "px 0px");
	}
	
	protected static void setBackgroundX(double x) {
		setBackgroundX((int) Math.round(x));
	}
	
	protected void transitionTo(Subpanel<T> ui) {
		assert mTransition == null : "Transition already in progress.";
		assert mNextUI == null : "Next UI: " + mNextUI;
		if (mTransition != null)
			return;
		
		mNextUI = ui;
		mNextUI.getElement().getStyle().setLeft(DISTANCE, Unit.PX);
		
		mTransition = new Tween(Ease.ACCEL_DECEL) {
			protected void onUpdate(double progress) {
				if (mCurrentUI != null)
					mCurrentUI.getElement().getStyle().setLeft(-DISTANCE * progress, Unit.PX);
				mNextUI.getElement().getStyle().setLeft(DISTANCE * (1.0 - progress), Unit.PX);
				setBackgroundX(-DISTANCE * progress);
			}
			
			protected void onComplete() {
				super.onComplete();
				if (mCurrentUI != null)
					getRoot().remove(mCurrentUI);
				mInputBlocker.removeFromParent();
				setBackgroundX(0);
				mNextUI.finishShowing();
				mCurrentUI = mNextUI;
				mNextUI = null;
				mTransition = null;
				
			}
		};
		getRoot().add(mNextUI);
		getRoot().add(mInputBlocker);
		
		if (mCurrentUI != null)
			mCurrentUI.startHiding();
		mTransition.run(TRANSITION_MS);
	}
	
	protected void openUI(Subpanel<T> ui) {
		if (ui == null) {
			openUI(new Reward<T>(new Reward.Handler() {
				@Override
				public void onComplete() {
					reward_onComplete();
				}
			}));
		} else {
			mCurrentUI = ui;
			getRoot().add(mCurrentUI);
			mCurrentUI.finishShowing();
		}
	}
	
	protected void closeUI(Subpanel<T> ui) {
		assert mCurrentUI == ui : mCurrentUI;
		
		mCurrentUI.startHiding();
		getRoot().remove(mCurrentUI);
		mCurrentUI = null;
	}
	
	private void closeUI() {
		if (mCurrentUI != null) {
			getRoot().remove(mCurrentUI);
			mCurrentUI = null;
		}
	}
	
	public void reward_onComplete() {
		if (mCurrentUI != null)
			closeUI(mCurrentUI);
		exit();
	}
}
