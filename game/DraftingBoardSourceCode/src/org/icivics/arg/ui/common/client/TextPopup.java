package org.icivics.arg.ui.common.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class TextPopup extends UIPanel {
	private static TextPopupUiBinder uiBinder = GWT.create(TextPopupUiBinder.class);
	interface TextPopupUiBinder extends UiBinder<Widget, TextPopup> {}
	
	public interface PopupHandler { void onClose(TextPopup popup); }
	
	private PopupHandler mHandler;
	
	@UiField
	protected DivElement dimmer;
	@UiField
	protected ParagraphElement titleText;
	@UiField
	protected DivElement bodyText;
	@UiField
	protected LabelButton closeButton;
	
	public TextPopup() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		closeButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick(event);
			}
		};
	}
	
	private void closeButton_onClick(ClickEvent e) {
		GWT.log("Close clicked");
		close();
	}
	
	public void setTitle(String text) {
		titleText.setInnerText(text);
	}
	
	public void setBody(String text) {
		bodyText.setInnerHTML(text);
	}
	
	public void setDimmerVisible(boolean visible) {
		dimmer.removeClassName("hidden");
		if (!visible) {
			dimmer.addClassName("hidden");
		}
	}
	
	public void setHandler(PopupHandler handler) {
		mHandler = handler;
	}
	
	public void clearHandler() {
		setHandler(null);
	}
	
	public void close() {
		closeButton.destroy();
		removeFromParent();
		if (mHandler != null) {
			mHandler.onClose(this);
			clearHandler();
		}
	}
}
