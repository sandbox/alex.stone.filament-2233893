package org.icivics.arg.ui.common.client;

import java.util.ArrayList;

import org.icivics.arg.client.ICivicsArgumentation;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class ConfirmPopup extends Composite {
	private static ConfirmPopupUiBinder uiBinder = GWT.create(ConfirmPopupUiBinder.class);
	interface ConfirmPopupUiBinder extends UiBinder<Widget, ConfirmPopup> {}
	
	public interface Handler {
		void onConfirm(ConfirmPopup popup);
		void onCancel(ConfirmPopup popup);
	}
	
	private Handler mHandler;
	
	@UiField
	protected DivElement dimmer;
	
	@UiField
	protected AudioConcatenatedControl audio;
	
	@UiField
	protected ParagraphElement titleText;
	@UiField
	protected DivElement bodyText;
	
	@UiField
	protected LabelButton confirmButton;
	@UiField
	protected LabelButton cancelButton;
	
	public ConfirmPopup() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public ConfirmPopup(String confirmButtonText) {
		this();
		confirmButton.setText(confirmButtonText);
	}
	
	public ConfirmPopup(String confirmButtonText, String cancelButtonText) {
		this(confirmButtonText);
		cancelButton.setText(cancelButtonText);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		confirmButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				confirmButton_onClick(event);
			}
		};
		cancelButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				cancelButton_onClick(event);
			}
		};
		
		ICivicsArgumentation.scaleText();
	}
	
	private void confirmButton_onClick(ClickEvent e) {
		GWT.log("Confirm clicked");
		Handler handler = mHandler;
		close();
		if (handler != null)
			handler.onConfirm(this);
	}
	
	private void cancelButton_onClick(ClickEvent e) {
		GWT.log("Cancel clicked");
		Handler handler = mHandler;
		close();
		if (handler != null)
			handler.onCancel(this);
	}
	
	public void setTitle(String text) {
		titleText.setInnerText(text);
	}
	
	public void setAudio(ArrayList<String> urls, String baseId){
		
		if(urls == null || urls.size() == 0){
			//Clear the audio stuff
		
			audio.removeFromParent();
		}else{
		
			//load the audio clips
			int count=urls.size();
			for(int i=0;i<count;i++){
				audio.loadSound(urls.get(i), baseId+i);
			}
		}
		
	}
	
	public void setBody(String text) {
		bodyText.setInnerText(text);
	}
	
	public void setBodyHTML(String htmlString){
		bodyText.setInnerHTML(htmlString);
	}
	
	public void setDimmerVisible(boolean visible) {
		dimmer.removeClassName("hidden");
		if (!visible) {
			dimmer.addClassName("hidden");
		}
	}
	
	public void setHandler(Handler handler) {
		mHandler = handler;
	}
	
	public void clearHandler() {
		setHandler(null);
	}
	
	public void close() {
		confirmButton.destroy();
		cancelButton.destroy();
		removeFromParent();
		audio.clear();
		if (mHandler != null) {
			clearHandler();
		}
	}
	
	public void styleForEvidence(){
		confirmButton.setInnerClass("submit-btn");
	}
}
