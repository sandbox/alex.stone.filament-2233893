package org.icivics.arg.ui.common.client;

import java.util.ArrayList;

import org.icivics.arg.data.model.client.Glossary;




import org.icivics.arg.util.client.ElementUtil;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.dom.client.NodeList;

public class GlossariedTextPanel extends HTMLPanel {
	
	private static String DEFINEDTERMCLASS = "glossaryWord";
	
	private Element lastTarget = null;
	private Timer mouseOverTimer;
	
	public GlossariedTextPanel(String html) {
		super(" ");
		Glossary.popup.getElement().getStyle().setZIndex(2);
		
		setInnards(html);
		
		
	}

	@Override
	protected void onAttach(){
		super.onAttach();
		
		this.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				Element tgt = Element.as(event.getNativeEvent().getEventTarget());
				String txt = tgt.getInnerText();
				//tgt.hasTagName(AnchorElement.TAG);
				//tgt.hasClassName(DEFINEDTERMCLASS);
				if(tgt.getTagName().toLowerCase().equals(AnchorElement.TAG.toLowerCase()) && tgt.hasClassName(DEFINEDTERMCLASS)){
					String def = Glossary.getDefinition(txt);
					int x = tgt.getAbsoluteLeft();
					int fallbacky = tgt.getAbsoluteTop();
					int fallbackx = tgt.getAbsoluteRight();
					int y = tgt.getAbsoluteBottom();
					showGlossary(txt,def,x,y,fallbacky,fallbackx);
				}
			}}, ClickEvent.getType());
		
		// TODO: Consider RootPanel.get() as a better target for these events
		this.addDomHandler(new MouseMoveHandler(){
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				mouseMovement(event);
			}}, MouseMoveEvent.getType());
		
	}
	
	public GlossariedTextPanel(SafeHtml safeHtml) {
		super(safeHtml);
		setInnards(safeHtml.asString());
	}

	public GlossariedTextPanel(String tag, String html) {
		super(tag, html);
		setInnards(html);
	}

	public void setInnerText(String text){
		setInnards(text);
	}
	
	public void update(){
		setInnards(this.getElement().getInnerHTML());
	}
	
	private void setInnards(String html){
		GWT.log(html);
		ArrayList<String> terms = Glossary.getTerms();
		for(String definedTerm:terms){
			GWT.log("defined term being processed: " + definedTerm);
			//Java regexp does not work all that great. Does sometimes, but fails other times.
			//The javascript and java syntax is different
			
			//This works: java declaration, javascript regex style
			RegExp rgex = RegExp.compile("\\b(" + definedTerm +")\\b", "gi");
			
			MatchResult match;
			String word;
			
			while((match = rgex.exec(html)) != null){
				int index = rgex.getLastIndex();
				word = match.getGroup(1);
				
				GWT.log("defined matched: " + definedTerm);
				
				if(html.lastIndexOf("<a class=\"" + DEFINEDTERMCLASS + "\">", index - ("<a class=\"" + DEFINEDTERMCLASS + "\">").length()) <= 
				   html.lastIndexOf("</a>", index - "</a>".length())){
					// Not inside an anchor tag
					html =
							html.substring(0,index-definedTerm.length()) + 
							"<a class=\"" + DEFINEDTERMCLASS + "\">" + 
							word + 
							"</a>" +
							html.substring(index)
					;
					//TODO: Make it to that it doesn't look for any more of the same term
					index = html.length();
				}
				rgex.setLastIndex(index + ("<a class=\"" + DEFINEDTERMCLASS + "\">" + "</a>").length());
			}
		}

		this.getElement().setInnerHTML(html);
	}
	
	private void mouseMovement(MouseMoveEvent event){
				
		Element tgt = Element.as(event.getNativeEvent().getEventTarget());
		if(tgt != lastTarget){
			if(lastTarget != null && lastTarget.getTagName().toLowerCase().equals(AnchorElement.TAG.toLowerCase()) && lastTarget.hasClassName(DEFINEDTERMCLASS)){
				//String txt = lastTarget.getInnerText();
				if(mouseOverTimer != null){
					mouseOverTimer.cancel();
					mouseOverTimer = null;
					hideGlossary();
				}
			}
			
			lastTarget = tgt;
			
			if(tgt.getTagName().toLowerCase().equals(AnchorElement.TAG.toLowerCase()) && tgt.hasClassName(DEFINEDTERMCLASS)){
				final String txt = tgt.getInnerText();
				final String def = Glossary.getDefinition(txt);
				final int x = tgt.getAbsoluteLeft();
				final int fallbacky = tgt.getAbsoluteTop();
				final int fallbackx = tgt.getAbsoluteRight();
				final int y = tgt.getAbsoluteBottom();
				mouseOverTimer = new Timer(){
					@Override
					public void run() {
						showGlossary(txt,def, x, y, fallbacky, fallbackx);
					}};
				mouseOverTimer.schedule(500);
			}
		}
	}
	
	private void hideGlossary(){
		Glossary.popup.hide();
	}
	
	private void showGlossary(String txt, String def, final int x, final int y, final int fallbacky, final int fallbackx){
		if(def !=null){
			//GWT.log("Text of clicked anchor: " + txt + " Defined as " + def);
			GlossaryPopupContent popupContents = new GlossaryPopupContent(Glossary.getCapitalizedTerm(txt),def);
			Glossary.popup.setWidget(popupContents);
			//Glossary.popup.setGlassEnabled(true);
			Glossary.popup.addDomHandler(new MouseMoveHandler(){
				@Override
				public void onMouseMove(MouseMoveEvent event) {
					mouseMovement(event);
				}}, MouseMoveEvent.getType());
			
			Glossary.popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
				
				@Override
				public void setPosition(int offsetWidth, int offsetHeight) {
					int pos_x;
					int pos_y;
					if(y + offsetHeight > 705 /*Window.getClientHeight()*/){
						pos_y = fallbacky - offsetHeight;
					}else{
						pos_y = y;
					}
					if(x + offsetWidth > 940 /*Window.getClientWidth()*/){
						pos_x = fallbackx - offsetWidth;
					}else{
						pos_x = x;
					}
					Glossary.popup.setPopupPosition(pos_x,pos_y);
				}
			});
			ElementUtil.setTextScaling();
		} else
			GWT.log("Text of clicked anchor: " + txt + " No definition found.");
	}
	
}
