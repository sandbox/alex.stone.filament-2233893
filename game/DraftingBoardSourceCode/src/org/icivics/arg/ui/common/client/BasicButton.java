package org.icivics.arg.ui.common.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class BasicButton extends Composite {
	private static BasicButtonUiBinder uiBinder = GWT.create(BasicButtonUiBinder.class);
	interface BasicButtonUiBinder extends UiBinder<Widget, BasicButton> {}
	
	protected static final String DISABLED_CSS = "btn_disabled";
	
	protected boolean mEnabled;
	protected HandlerRegistration mClickReg;
	public ClickHandler clickHandler;
	
	public BasicButton() {
		initWidget(uiBinder.createAndBindUi(this));
		initState();
	}
	
	public BasicButton(UiBinder<? extends Widget, ? extends BasicButton> uiBinder) {
		
	}
	
	protected void initState() {
		enableDomHandlers();
		mEnabled = true;
	}
	
	/**
	 * Returns whether the button's clickHandler is triggered when the button is clicked.
	 * When false, the "btn_disabled" CSS class is applied to the button.
	 * @return true if enabled, false if not
	 */
	public boolean getEnabled() {
		return mEnabled;
	}
	
	/**
	 * Sets whether the button's clickHandler is triggered when the button is clicked.
	 * When false, the "btn_disabled" CSS class is applied to the button.
	 * @param v true if enabled, false if not
	 */
	public void setEnabled(boolean v) {
		if (v == mEnabled)
			return;
		mEnabled = v;
		
		if (v) {
			enableDomHandlers();
			applyEnabledStyle();
		} else {
			disableDomHandlers();
			applyDisabledStyle();
		}
	}
	
	protected void applyEnabledStyle() {
		getWidget().removeStyleName(DISABLED_CSS);
	}
	
	protected void applyDisabledStyle() {
		getWidget().addStyleName(DISABLED_CSS);
	}
	
	protected void enableDomHandlers() {
		if (mClickReg != null)
			return;
		
		mClickReg = getWidget().addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				widget_onClick(event);
				
			}
		}, ClickEvent.getType());
	}
	
	protected void disableDomHandlers() {
		if (mClickReg != null) {
			mClickReg.removeHandler();
			mClickReg = null;
		}
	}
	
	protected void widget_onClick(ClickEvent event) {
		if (clickHandler != null) {
			clickHandler.onClick(event);
		}
	}
	
	public void destroy() {
		setEnabled(false);
		clickHandler = null;
	}
}
