package org.icivics.arg.ui.common.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class DistinguishableTextBox extends Composite {
	private static DistinguishableTextBoxUiBinder uiBinder = GWT.create(DistinguishableTextBoxUiBinder.class);
	interface DistinguishableTextBoxUiBinder extends UiBinder<Widget, DistinguishableTextBox> {}

	public static String DRAFTINGBOARDCLASSNAME = "draftingboard-content";
	public static String STUDENTSELECTEDCLASSNAME ="player-selected";
	public static String STUDENTCLASSNAME = "student-content";
	
	public static int[] ALLOWEDKEYCODES = {37, 38, 39, 40, // Arrow keys
		8, 46,   //Backspace and delete
		35, 36,  //Insert and home
		33,34,35, // page up, page down, end
		16, 20, //Shift and capslock
		17, 18, 91 // ctrl, alt, windows key
	};
	
	@UiField
	HTMLPanel textBox;
	HandlerRegistration textBoxKeyDownReg;
	
	public DistinguishableTextBox() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	protected void onAttach(){
		super.onAttach();
		textBox.getElement().setAttribute("contentEditable", "true");
		textBox.getElement().getStyle().setProperty("MozUserSelect", "text");
		textBox.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
		textBox.getElement().getStyle().setProperty("WebkitUserSelect", "text");
		textBox.getElement().getStyle().setProperty("userSelect", "text");
		
		addPasteHandler(textBox.getElement().getParentNode());
		
		deSpellcheck();
		
		textBoxKeyDownReg = textBox.addDomHandler(new KeyDownHandler(){
			@Override
			public void onKeyDown(KeyDownEvent event) {
				
				// Check to see if this key is okay
				boolean input = true;
				int count = ALLOWEDKEYCODES.length;
				int code = event.getNativeKeyCode();
				for(int i=0; i<count; i++){
					if(code == ALLOWEDKEYCODES[i])
						input = false;
				}
				if(input){
					// Ugly hackery between here and the JSNI code to prevent webkit from collapsing "empty" span
					boolean spacebar = code == 32;
					boolean prevent = onInput(spacebar);
					if(prevent){
						event.preventDefault();
					}
					
				}
				//scrubHTML();
				deSpellcheck();
			}}, KeyDownEvent.getType());
		textBox.addDomHandler(new KeyUpHandler(){

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(textBox.getElement().getInnerHTML().length() == 0)
					textBox.getElement().setInnerHTML("<span class=\"" + DRAFTINGBOARDCLASSNAME + "\">&nbsp;</span>");
			}
			
		}, KeyUpEvent.getType());
	}
	
	@Override
	protected void onDetach(){
		textBoxKeyDownReg.removeHandler();
		super.onDetach();
	}
	
	protected boolean onInput(boolean spaceKey){
		GWT.log("Allowable key pressed");
		return splitIfNeeded(textBox.getElement().getParentNode(), spaceKey);
	}
	
	protected native boolean splitIfNeeded(Node n, boolean spaceKey) /*-{
		var DBCLASSNAME = @org.icivics.arg.ui.common.client.DistinguishableTextBox::DRAFTINGBOARDCLASSNAME;
		var DBSTUCLASSNAME = @org.icivics.arg.ui.common.client.DistinguishableTextBox::STUDENTSELECTEDCLASSNAME;
		var STUDENTCLASSNAME = @org.icivics.arg.ui.common.client.DistinguishableTextBox::STUDENTCLASSNAME;

		var NUM = 0;
		
		var WND = $wnd;
		
		// Get the current user selection
		var selection = $wnd.rangy.getSelection();
		
		// If stuff is selected, delete it! TODO: Check if this actually works as expected
		selection.deleteFromDocument();
		
		// Collapse to just one thing
		selection.collapseToStart();
		
		var element = selection.anchorNode;
		
		// Check if the element the cursor in is draftingboard content
		var spanClass = STUDENTCLASSNAME;
				
		if($wnd.$(element).parent().hasClass(DBCLASSNAME)){
			spanClass = DBCLASSNAME;
		}else if ($wnd.$(element).parent().hasClass(DBSTUCLASSNAME)){
			spanClass = DBSTUCLASSNAME;
		}else if ($wnd.$(element).parent().hasClass("distinguishable-text")){
			spanClass = "distinguishable-text";
		}
				
		if(spanClass == DBCLASSNAME || spanClass == DBSTUCLASSNAME){
			
			var range1 = $wnd.rangy.createRange();
				
			range1.setStart(element,0);
			range1.setEnd(element,selection.anchorOffset);
			
			var range2 = $wnd.rangy.createRange();
			range2.selectNodeContents(element);
			range2.setStart(element,selection.anchorOffset); 
			
			var firstHalf = $wnd.$("<span>" + range1.toString() + "</span>");
			
			var secondHalf = $wnd.$("<span>" + range2.toString() + "</span>");

			//Student bit needs to be created by the document, so it can be used by the selection
			var thirdHalfElement = $doc.createElement("span");
			// For some reason, an empty span or a " " body does not work correctly in chrome
			var thirdHalf = $wnd.$(thirdHalfElement).html("&nbsp;");
			
			
			$wnd.$(element).parent().replaceWith(firstHalf);
			if(secondHalf.text().length > 0)
				firstHalf.after(secondHalf);
			firstHalf.after(thirdHalf);
			
			
			selection.selectAllChildren(thirdHalfElement);
			if(spaceKey)
				selection.collapseToEnd();
			
			thirdHalf.addClass(STUDENTCLASSNAME);
			secondHalf.addClass(spanClass);
			firstHalf.addClass(spanClass);
			
			return spaceKey;
		} else if(spanClass == "distinguishable-text"){
			var thirdHalfElement = $doc.createElement("span");
			var thirdHalf = $wnd.$(thirdHalfElement).html("&nbsp;");
			
			thirdHalf.addClass(STUDENTCLASSNAME);
			
			//TODO: Create range
			var myRange = selection.getRangeAt(0);
			myRange.insertNode(thirdHalfElement);
			
			selection.removeAllRanges();
			
			selection.selectAllChildren(thirdHalfElement);
			if(spaceKey)
				selection.collapseToEnd();
				
			selection.refresh();
			
			return spaceKey;
		} else {
			return false;
		}
		
	}-*/;

	private native void addPasteHandler(Node n) /*-{
		$wnd.$(n).bind('paste',function(e){
		    e.preventDefault();
		});
	}-*/;
	
	private native void deSpellcheck() /*-{
		$wnd.$("." + @org.icivics.arg.ui.common.client.DistinguishableTextBox::DRAFTINGBOARDCLASSNAME).each(function () {
			$wnd.$(this).prop({spellcheck:false, autocorrect:"OFF", autocomplete:"OFF", autocapitalize:"OFF"});
		});
	}-*/;
	
	public void setInnerText(String string) {
		// I think adding this wrapping span is a mistake
		//textBox.getElement().setInnerHTML("<span class=\"" + DRAFTINGBOARDCLASSNAME + "\">"+string+"</span>");
		textBox.getElement().setInnerHTML(string);
		
		deSpellcheck();
	}

	public String getValue() {
		scrubHTML();
		return textBox.getElement().getInnerHTML();
	}
	
	private void scrubHTML(){
		Element el = textBox.getElement().getFirstChildElement();
		if(el != null){ // A textbox might not actually have any children
			if(el.getInnerText().length() > 0)
				el.setInnerHTML(new SafeHtmlBuilder().appendEscaped(el.getInnerText()).toSafeHtml().asString());
				//el.setInnerHTML(el.getInnerText());
			while((el = el.getNextSiblingElement()) != null){
				if(el.getInnerText().length() > 0)
					el.setInnerHTML(new SafeHtmlBuilder().appendEscaped(el.getInnerText()).toSafeHtml().asString());
			}
		}
	}
	
	
}
