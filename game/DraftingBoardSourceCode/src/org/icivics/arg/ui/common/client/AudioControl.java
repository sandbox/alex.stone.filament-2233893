package org.icivics.arg.ui.common.client;

import org.icivics.arg.data.client.DataController;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.http.client.URL;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class AudioControl extends Composite {
	private static AudioControlUiBinder uiBinder = GWT.create(AudioControlUiBinder.class);
	interface AudioControlUiBinder extends UiBinder<Widget, AudioControl> {}

	private String mSoundID;
	public boolean playing = false;
	
	@UiField
	HTMLPanel backBtn;
	HandlerRegistration backBtnReg;
	@UiField
	HTMLPanel playPauseBtn;
	HandlerRegistration playPauseBtnReg;
	@UiField
	HTMLPanel progressBar;
	
	@UiField
	HTMLPanel progressWrapper;
	HandlerRegistration[] progressBarRegs = new HandlerRegistration[2];
	
	
	
	
	public AudioControl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	protected void onAttach(){
		super.onAttach();
		
		//Event listeners
		backBtnReg = backBtn.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backBtn_onClick(event);
			}
		}, ClickEvent.getType());
		playPauseBtnReg = playPauseBtn.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				playPauseBtn_onClick(event);
			}
		}, ClickEvent.getType());
		
		progressBarRegs[0] = progressWrapper.addDomHandler(new MouseDownHandler(){
			@Override
			public void onMouseDown(MouseDownEvent event) {
				progressBar_onClick(event.getRelativeX(progressWrapper.getElement()));
			}},MouseDownEvent.getType());
				
		progressBar.getElement().getStyle().setWidth(0, Unit.PCT);
	}
	
	@Override
	protected void onDetach(){
		pauseSound(mSoundID);
		
		//Get rid of event listeners
		if(backBtnReg != null)
			backBtnReg.removeHandler();
		if(playPauseBtnReg != null)
			playPauseBtnReg.removeHandler();
		int count = progressBarRegs.length;
		for(int i=0; i < count; i++)
			if(progressBarRegs[i] != null)
				progressBarRegs[i].removeHandler();
		super.onDetach();
	}
	
	protected void playPauseBtn_onClick(ClickEvent event){
		progressWrapper.removeStyleName("hidden");
		if(mSoundID != null){
			if(!playing){
				playSound(mSoundID);
				playPauseBtn.addStyleName("pause");
				playing = true;
			} else {
				pauseSound(mSoundID);
				playing = false;
			}
		}
	}
	
	protected void backBtn_onClick(ClickEvent event){
		if(mSoundID != null){
			seekSoundTo(mSoundID,0);
		}
	}
	
	public void loadSound(String url){
		loadSound(url,null);
	}
	
	public void loadSound(String url, String id){
		if(id == null)
			id = url;
		if(url != null)
		mSoundID = initSound(url, id);
		//A hack to put the sound into a state that sends "while playing" events when the position changes
		/*
		playSound(mSoundID);
		pauseSound(mSoundID);
		seekSoundTo(mSoundID,0);
		*/
	}
	
	protected void progressBar_onClick(double x){
		progressBar_onDrag(x);
		progressBarRegs[0].removeHandler();
		progressBarRegs[0] = progressWrapper.addDomHandler(new MouseMoveHandler(){
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				progressBar_onDrag(event.getRelativeX(progressWrapper.getElement()));
			}
		}, MouseMoveEvent.getType());
		final boolean restart = playing;
		progressBarRegs[1] = RootPanel.get().addDomHandler(new MouseUpHandler(){
			@Override
			public void onMouseUp(MouseUpEvent event) {
				progressBar_onRelease();
				if(restart){
					playing = true;
					playSound(mSoundID);
				}
		}}, MouseUpEvent.getType());
		pauseSound(mSoundID);
	}
	
	protected void progressBar_onDrag(double x){
		double pos;
		pos = x/progressWrapper.getElement().getClientWidth();
		seekSoundTo(mSoundID,pos);
	}
	
	protected void progressBar_onRelease(){
		int count = progressBarRegs.length;
		for(int i=0; i < count; i++)
			if(progressBarRegs[i] != null)
				progressBarRegs[i].removeHandler();
		
		progressBarRegs[0] = progressWrapper.addDomHandler(new MouseDownHandler(){
			@Override
			public void onMouseDown(MouseDownEvent event) {
				progressBar_onClick(event.getRelativeX(progressWrapper.getElement()));
		}},MouseDownEvent.getType());
	}
	
	protected void whilePlaying(float progressPct){
		if(playing)
			playPauseBtn.addStyleName("pause");
		progressBar.getElement().getStyle().setWidth(progressPct*100, Unit.PCT);
	}
	
	protected void onPause(){
		GWT.log("Sound paused");
		playing=false;
		playPauseBtn.removeStyleName("pause");
	}
	
	protected void whenFinished(){
		GWT.log("Sound finished playing");
		
		playing=false;
		playPauseBtn.removeStyleName("pause");
		//A hack to put the sound into a state that sends "while playing" events when the position changes
		seekSoundTo(mSoundID,0);
		playSound(mSoundID);
		pauseSound(mSoundID);
		seekSoundTo(mSoundID,0);
		
	}
	
	public void setLocation(String soundloc){
		soundloc = DataController.getIssueURL() + soundloc;
		soundloc = URL.encode(soundloc);
		GWT.log("Trying to load sound " + soundloc + " from the xml");
		loadSound(soundloc,soundloc);
	}
	
	protected native void playSound(String id) /*-{
	//if(console)
	//		console.log("Sound " + id + " playing, and hopefully setting up the listener functions");
		var control = this;
		var mySound = $wnd.soundManager.getSoundById(id);
		$wnd.soundManager.pauseAll();
		if(mySound)
		mySound.play({
					multiShotEvents:true,
					whileplaying:function(){
					control.@org.icivics.arg.ui.common.client.AudioControl::whilePlaying(F)(this.position/this.duration);
					},
					onpause:function(){
					control.@org.icivics.arg.ui.common.client.AudioControl::onPause()();
					},
					onfinish:function(){
						//$wnd.console.log("finnishplayink");
					control.@org.icivics.arg.ui.common.client.AudioControl::whenFinished()();
					}
					});
		
	}-*/;
	
	protected native void pauseSound(String id) /*-{
		$wnd.soundManager.pause(id);
	}-*/;
	
	protected native void seekSoundTo(String id, double position) /*-{
		var snd = $wnd.soundManager.getSoundById(id);
		if(snd)
			snd.setPosition($wnd.Math.floor(position * snd.duration));
	}-*/;
	
	private native String initSound(String soundURL, String soundID) /*-{
		//Expect soundmanager to already be set up in the .html
		var control = this;
		var mySound = $wnd.soundManager.getSoundById(soundID);
		if(mySound == null){
			mySound = $wnd.soundManager.createSound({
				url:soundURL,
				id:soundID,
				autoLoad:true,
				whileplaying:function(){
					control.@org.icivics.arg.ui.common.client.AudioControl::whilePlaying(F)(this.position/this.duration);
				},
				onfinish:function(){
					control.@org.icivics.arg.ui.common.client.AudioControl::whenFinished()();
				},
				onpause:function(){
					control.@org.icivics.arg.ui.common.client.AudioControl::onPause()();
				}
			});
			//if(console)
			//	console.log("Sound " + soundID + " not found, loading");
		} else {
			//if(console)
			//	console.log("Sound " + soundID + " retrieved succesfully, resetting sound position");
			mySound.setPosition(0);
		}
		return mySound.id;
		
	}-*/;
}
