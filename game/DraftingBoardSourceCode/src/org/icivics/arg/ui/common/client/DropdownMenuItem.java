package org.icivics.arg.ui.common.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class DropdownMenuItem extends UIPanel {
	private static DropdownMenuItemUiBinder uiBinder = GWT.create(DropdownMenuItemUiBinder.class);
	interface DropdownMenuItemUiBinder extends UiBinder<Widget, DropdownMenuItem> {}
	
	@UiField
	ParagraphElement label;
	
	protected String mValue;
	
	public DropdownMenuItem() {
		this("");
	}
	
	public DropdownMenuItem(String value) {
		this(value, value);
	}
	
	public DropdownMenuItem(String value, String labelText) {
		initWidget(uiBinder.createAndBindUi(this));
		setValue(value);
		setLabelText(labelText);
	}
	
	public String getValue() {
		return mValue;
	}
	public void setValue(String value) {
		mValue = value;
	}
	
	public String getLabelText() {
		return label.getInnerText();
	}
	public void setLabelText(String text) {
		label.setInnerText(text);
	}
}
