package org.icivics.arg.ui.common.client;

import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class RewritePopup extends UIPanel {
	private static RewritePopupUiBinder uiBinder = GWT.create(RewritePopupUiBinder.class);
	interface RewritePopupUiBinder extends UiBinder<Widget, RewritePopup> {}
	
	@UiField
	ParagraphElement baseText;
	@UiField
	TextAreaElement textField;
	@UiField
	HTMLPanel itemButton;
	@UiField
	DivElement itemIcon;
	@UiField
	LabelButton submitButton;
	
	public static interface Handler { void onSubmit(String text); }
	public Handler handler;
	
	private Item mItem;
	private HandlerRegistration mItemClickReg;
	private EvidenceDetailPanel mItemPopup;
	
	public RewritePopup(Item item) {
		mItem = item;
		initWidget(uiBinder.createAndBindUi(this));
		baseText.setInnerText(item.sentenceText[0].replaceFirst(ItemChunk.REPLACEMENT_TOKEN_REGEX, ItemChunk.REPLACEMENT_UNKNOWN_TEXT));
		textField.setValue(item.getCorrectChunk().sentenceText);
		itemIcon.addClassName("type" + item.type);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		mItemClickReg = itemButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				itemButton_onClick();
			}
		}, ClickEvent.getType());
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		textField.focus();
		textField.select();
	}
	
	@Override
	protected void onDetach() {
		if (mItemPopup != null) {
			mItemPopup.removeFromParent();
			mItemPopup.destroy();
			mItemPopup = null;
		}
		if (mItemClickReg != null) {
			mItemClickReg.removeHandler();
			mItemClickReg = null;
		}
		submitButton.destroy();
		mItem = null;
		super.onDetach();
	}

	private void itemButton_onClick() {
		if (mItemPopup != null)
			return;
		
		mItemPopup = new EvidenceDetailPanel(mItem, false);
		mItemPopup.handler = new EvidenceDetailPanel.Handler() {
			@Override
			public void onConfirm() {
				itemPopup_onClose();
			}
			
			@Override
			public void onClose() {
				itemPopup_onClose();
			}
		};
		getRoot().add(mItemPopup);
	}
	
	private void submitButton_onClick() {
		if (mItemPopup != null)
			return;
		
		if (mItemClickReg != null) {
			mItemClickReg.removeHandler();
			mItemClickReg = null;
		}
		submitButton.destroy();
		String text = textField.getValue();
		removeFromParent();
		if (handler != null) {
			handler.onSubmit(text);
			handler = null;
		}
	}
	
	private void itemPopup_onClose() {
		if (mItemPopup == null)
			return;
		
		mItemPopup.removeFromParent();
		mItemPopup.destroy();
		mItemPopup = null;
	}
}
