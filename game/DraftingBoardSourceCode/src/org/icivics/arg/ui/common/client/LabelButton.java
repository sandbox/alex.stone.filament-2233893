package org.icivics.arg.ui.common.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class LabelButton extends BasicButton {
	private static LabelButtonUiBinder uiBinder = GWT.create(LabelButtonUiBinder.class);
	interface LabelButtonUiBinder extends UiBinder<Widget, LabelButton> {}
	
	@UiField
	protected HTMLPanel innerDiv;
	@UiField
	protected ParagraphElement labelText;
	
	public LabelButton() {
		super(uiBinder);
		initWidget(uiBinder.createAndBindUi(this));
		initState();
	}
	
	/**
	 * The class name applied to the DivElement inside the button.
	 * @param v class name
	 */
	public void setInnerClass(String v) {
		if (mEnabled) {
			innerDiv.setStyleName(v);
		} else {
			innerDiv.setStyleName(v + " " + DISABLED_CSS);
		}
	}
	
	public String getText() {
		return labelText.getInnerText();
	}
	/**
	 * Sets the text displayed on the button face.
	 * @param v the text to display
	 */
	public void setText(String v) {
		labelText.setInnerText(v);
	}
	
	@Override
	protected void enableDomHandlers() {
		if (mClickReg != null)
			return;
		
		mClickReg = innerDiv.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				widget_onClick(event);
				
			}
		}, ClickEvent.getType());
	}
	
	@Override
	protected void applyEnabledStyle() {
		innerDiv.removeStyleName(DISABLED_CSS);
	}
	
	@Override
	protected void applyDisabledStyle() {
		innerDiv.addStyleName(DISABLED_CSS);
	}
}
