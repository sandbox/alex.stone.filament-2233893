package org.icivics.arg.ui.issueAnalyzer.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class BlankFillerEvidenceButton extends Composite {
	private static BlankFillerEvidenceButtonUiBinder uiBinder = GWT.create(BlankFillerEvidenceButtonUiBinder.class);
	interface BlankFillerEvidenceButtonUiBinder extends UiBinder<Widget, BlankFillerEvidenceButton> {}
	
	@UiField
	protected DivElement typeDiv;
	@UiField
	protected ParagraphElement nameText;
	
	protected int mEvidenceType = 1;
	protected boolean mEnabled = true;
	
	public BlankFillerEvidenceButton() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setType(int type) {
		typeDiv.removeClassName("type" + mEvidenceType);
		mEvidenceType = type;
		typeDiv.addClassName("type" + mEvidenceType);
	}
	
	public void setName(String name) {
		nameText.setInnerText(name);
	}
	
	public boolean getEnabled() {
		return mEnabled;
	}
	
	public void setEnabled(boolean enabled) {
		getWidget().removeStyleName("disabled");
		mEnabled = enabled;
		if (!enabled) {
			getWidget().addStyleName("disabled");
		}
	}
}
