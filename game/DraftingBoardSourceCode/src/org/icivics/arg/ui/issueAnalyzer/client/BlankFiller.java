package org.icivics.arg.ui.issueAnalyzer.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.data.model.client.StoryChunk;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceDetailPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class BlankFiller extends Subpanel<IssueAnalyzer> {
	private static BlankFillerUiBinder uiBinder = GWT.create(BlankFillerUiBinder.class);
	interface BlankFillerUiBinder extends UiBinder<Widget, BlankFiller> {}
	
	private static final int ITEMS_PER_EVENT = 3;
	
	@UiField
	DivElement blankList;
	
	@UiField
	DivElement progressBar;
	@UiField
	SimplePanel panelMount;
	@UiField
	LabelButton submitButton;
	@UiField 
	LabelButton returnButton;
	@UiField
	AudioConcatenatedControl audioControl;
	
	private BlankFillerListItem[] mBlanks;
	private BlankFillerEvidenceButton[] mEvidenceButtons;
	private EvidenceDetailPanel mItemPanel;
	private TextPopup mPopup;
	
	private HandlerRegistration[] mEvidenceClickRegs;
	
	private boolean[] mCorrect;
	private boolean[] mRedo;
	private int mCurrentChunkIndex = -1;
	
	public BlankFiller(IssueAnalyzer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		ArrayList<StoryChunk> chunks = DataController.getCurrentIssue().issue.storyChunks;
		int count = chunks.size();
		
		parent.chunkChoices = new String[count];
		
		mBlanks = new BlankFillerListItem[count];
		mRedo = new boolean[count];
		mCorrect = new boolean[count];
		for (int i = 0; i < count; ++i) {
			mCorrect[i] = false;
			mRedo[i] = false;
			mBlanks[i] = new BlankFillerListItem();
			mBlanks[i].setText(chunks.get(i).textBase.replaceAll(StoryChunk.REPLACEMENT_TOKEN, "<span class=\"highlight-white\">" + StoryChunk.REPLACEMENT_UNKNOWN_TEXT + "</span>"));
			//<span class=\"highlight-white\">" + chunk.options[i].text + "</span>
			getRoot().add(mBlanks[i], blankList);
		}
		progressBar.getStyle().setProperty("backgroundPosition", "-200px 0px");
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		audioControl.getElement().getStyle().setZIndex(2);
		
		getParentPanel().hud.setEvidenceHandler(new EvidenceSidebar.SelectItemHandler() {
			@Override
			public void onSelectItem(Item item) {
				onItemPanelConfirm(item);
			}
		});
		
		GWT.log("BlankFiller attaching, with currentChunk " + mCurrentChunkIndex);
	
		nextEvent(true);
	}
	
	@Override
	protected void onDetach() {
		
		clearEvidenceButtons();
		submitButton.destroy();
		mCurrentChunkIndex = -1;
		
		for(int i = 0, count = mCorrect.length; i < count; i++)
			if(!mCorrect[i]){
				mCurrentChunkIndex = i-1;
				mBlanks[i].setActive(true);
				mBlanks[i].setComplete(false);
				break;
			}
		
		getParentPanel().hud.clearEvidenceHandler();
		
		GWT.log("Blank filler UI detaching, with currentChunk " + mCurrentChunkIndex);
		
		super.onDetach();
	}
	
	private int countCorrect() {
		int numCorrect = 0;
		for (int i = 0; i < mCorrect.length; ++i)
			if (mCorrect[i])
				++numCorrect;
		return numCorrect;
	}
	
	private void clearEvidenceButtons() {
		if (mEvidenceButtons != null && mEvidenceClickRegs != null) {
			for (int i = 0, count = mEvidenceButtons.length; i < count; ++i) {
				mEvidenceClickRegs[i].removeHandler();
				mEvidenceButtons[i].removeFromParent();
			}
		}
		mEvidenceButtons = null;
		mEvidenceClickRegs = null;
	}
	
	
	private void evidenceSelectedConfirmation(){
		LogUtil.log("Evidence In the selected confirmation bit");
		ArrayList<StoryChunk> chunks = DataController.getCurrentIssue().issue.storyChunks;
		LogUtil.log("Found the chunks");
		enableSubmitButton();
		LogUtil.log("enabled the submit button");
		float progressBarWidth = progressBar.getClientWidth();
		
		// Count the number of incorrect chunks from here to the end
		int numIncorrect = 0;
		for(int i = mCurrentChunkIndex; i < mCorrect.length; i++){
			if(!mCorrect[i])
				numIncorrect++;
		}
		int bgPos = Math.round(progressBarWidth*(mCorrect.length - numIncorrect) / chunks.size());
		
		progressBar.getStyle().setProperty("MsBackgroundPositionX", bgPos+"px");
		progressBar.getStyle().setProperty("backgroundPosition", bgPos + "px 0px");
		LogUtil.log("Tried to set the progress bar");
	}
	
	
	private void nextEvent(boolean forwards) {
		ArrayList<StoryChunk> chunks = DataController.getCurrentIssue().issue.storyChunks;
		if (mCurrentChunkIndex >= 0) {
			mBlanks[mCurrentChunkIndex].setActive(false);
			mBlanks[mCurrentChunkIndex].setComplete(true);
		}
		clearEvidenceButtons();
		if(forwards)
			++mCurrentChunkIndex;
		else
			--mCurrentChunkIndex;
		
		getParentPanel().hud.onItemPanelClose();
		enableReturnButton();
		
		if(mCurrentChunkIndex < 0){
			GWT.log("Going back to story");
			getParentPanel().close(this, true);
			return;
		}
		
		if(mBlanks.length <= mCurrentChunkIndex){
			close();
		} else if (!mCorrect[mCurrentChunkIndex]) {
			float progressBarWidth = progressBar.getClientWidth();

			// Count the number of incorrect chunks from here to the end
			int numIncorrect = 0;
			for(int i = mCurrentChunkIndex; i < mCorrect.length; i++){
				if(!mCorrect[i])
					numIncorrect++;
			}
			int bgPos = Math.round(progressBarWidth*(mCorrect.length - numIncorrect) / chunks.size());
			
			
			progressBar.getStyle().setProperty("MsBackgroundPositionX", bgPos+"px");
			progressBar.getStyle().setProperty("backgroundPosition", bgPos + "px 0px");
			StoryChunk chunk = DataController.getCurrentIssue().issue.storyChunks.get(mCurrentChunkIndex);
			
			Item[] items = new Item[3];
			for(int i=0; i<3; i++)
				items[i] = chunk.options[i].item;
			if(Math.random() > 0.5){
				Item temp = items[0];
				items[0] = items[1];
				items[1] = temp;
			}
			if(Math.random() > 0.5){
				Item temp = items[2];
				items[2] = items[1];
				items[1] = temp;
			}
			
			getParentPanel().hud.init(items);
			
			int count = ITEMS_PER_EVENT;
			mEvidenceButtons = new BlankFillerEvidenceButton[count];
			mEvidenceClickRegs = new HandlerRegistration[count];
			
			Item item = null;
			for (int i = 0; i < count; ++i) {
				// Pre-load the audio files
				audioControl.loadSound(chunk.options[i].audioUrl, "chunk"+mCurrentChunkIndex+"option" + i);
				
				final int evidenceIndex = i;
				
				mEvidenceButtons[i] = new BlankFillerEvidenceButton();
				mEvidenceButtons[i].setName(chunk.options[i].item.name);
				mEvidenceButtons[i].setType(chunk.options[i].item.type);
				mEvidenceClickRegs[i] = mEvidenceButtons[i].addDomHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						evidenceButton_onClick(evidenceIndex);
					}
				}, ClickEvent.getType());
				// These evidence buttons are no longer in the screen
				/*
				getRoot().add(mEvidenceButtons[i], evidenceList);
				*/
				
				if(getParentPanel().chunkChoices[mCurrentChunkIndex] != null && 
						chunk.options[i].item.id == getParentPanel().chunkChoices[mCurrentChunkIndex])
					item = chunk.options[i].item;
			}
			
			audioControl.clear();
			audioControl.loadSound(chunk.audioUrlA,"chunk"+mCurrentChunkIndex+"A");
			audioControl.loadSound(DataController.getIssueURL() + "twoSecondsOfNothing.mp3", "blank");
			audioControl.loadSound(chunk.audioUrlB,"chunk"+mCurrentChunkIndex+"B");
			
			mBlanks[mCurrentChunkIndex].setActive(true);
			mBlanks[mCurrentChunkIndex].setComplete(false);
			
			if(getParentPanel().chunkChoices[mCurrentChunkIndex] != null){
				// Pretend the already selected evidence was clicked again
				
				/*
				if(item != null)
					showItem(item);
				*/
				
				// If this is a chunk that is being redone, disable the submit button
				if(mRedo[mCurrentChunkIndex]){
					disableSubmitButton();
					mRedo[mCurrentChunkIndex] = false;
				} else {
					showItem(item);
				}
			}
		} else {
			mRedo[mCurrentChunkIndex] = false;
			nextEvent(forwards);
		}
		setTextScaling();
	}
	
	public void showItem(Item item) {
		getParentPanel().hud.showItem(item);
	}
	
	private void closeItemPanel() {
		if (mItemPanel == null)
			return;
		
		panelMount.remove(mItemPanel);
		mItemPanel.destroy();
		mItemPanel = null;
	}
	
	public void onItemPanelClose() {
		closeItemPanel();
	}
	
	public void onItemPanelConfirm(Item item) {
		if(item == null)
			return;
		
		StoryChunk chunk = DataController.getCurrentIssue().issue.storyChunks.get(mCurrentChunkIndex);
		closeItemPanel();
		
		for (int i = 0; i < ITEMS_PER_EVENT; ++i) {
			if (chunk.options[i].item.id.equals(item.id)) {
				GWT.log("Replacement text: " + chunk.options[i].text);
				RegExp regex = RegExp.compile(StoryChunk.REPLACEMENT_TOKEN,"");
				if(mRedo[mCurrentChunkIndex]){
					mBlanks[mCurrentChunkIndex].setText(regex.replace(chunk.textBase,"<span class=\"highlight-red\">" + chunk.options[i].text + "</span>"));
				} else {
					mBlanks[mCurrentChunkIndex].setText(regex.replace(chunk.textBase,"<span class=\"highlight-white\">" + chunk.options[i].text + "</span>"));
				}
				getParentPanel().chunkChoices[mCurrentChunkIndex] = item.id;
				
				GWT.log(chunk.audioUrlA);
				// TODO: save the state of the audio control
				int currentFileIndex = audioControl.getPlayingFileIndex();
				float currentPosition = audioControl.getPlayingFilePosition();
				boolean wasPlaying = audioControl.playing;
				
				audioControl.clear();
				audioControl.loadSound(chunk.audioUrlA,"chunk"+mCurrentChunkIndex+"A");
				audioControl.loadSound(chunk.options[i].audioUrl, "chunk"+mCurrentChunkIndex+"option" + i);
				audioControl.loadSound(chunk.audioUrlB,"chunk"+mCurrentChunkIndex+"B");
				
				LogUtil.log("Loaded the audio files");
				
				// TODO: return the audio control back to its original state
				if(currentFileIndex == 1){
					//The file that has been swapped out was playing
					audioControl.playAt(currentFileIndex, (float) 0.0, wasPlaying);
				} else {
					//The file that has been playing was not changed
					audioControl.playAt(currentFileIndex, currentPosition, wasPlaying);
				}
				
				LogUtil.log("Reset the audio file status");
				
				evidenceSelectedConfirmation();
				return;
			}
		}
		// TODO: save the state of the audio control
		int currentFileIndex = audioControl.getPlayingFileIndex();
		float currentPosition = audioControl.getPlayingFilePosition();
		boolean wasPlaying = audioControl.playing;
		
		audioControl.clear();
		audioControl.loadSound(chunk.audioUrlA,"chunk"+mCurrentChunkIndex+"A");
		audioControl.loadSound("./audio/twoSecondsOfNothing.mp3", "blank");
		audioControl.loadSound(chunk.audioUrlB,"chunk"+mCurrentChunkIndex+"B");
		
		// TODO: return the audio control back to its original state
		if(currentFileIndex == 1){
			//The file that has been swapped out was playing
			audioControl.playAt(currentFileIndex, (float) 0.0, wasPlaying);
		} else {
			//The file that has been playing was not changed
			audioControl.playAt(currentFileIndex, currentPosition, wasPlaying);
		}
	}
	
	private void evidenceButton_onClick(int evidenceIndex) {
		if (!mEvidenceButtons[evidenceIndex].getEnabled() || mPopup != null)
			return;
			
		LogUtil.log("Evidence " + evidenceIndex + " clicked");
		showItem(DataController.getCurrentIssue().issue.storyChunks.get(mCurrentChunkIndex).options[evidenceIndex].item);
	}
	
	private void enableSubmitButton() {
		submitButton.setEnabled(true);
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick(event);
			}
		};
	}
	
	private void disableSubmitButton() {
		submitButton.clickHandler = null;
		submitButton.setEnabled(false);
	}
	
	
	private void submitButton_onClick(ClickEvent e) {
		if (mPopup != null)
			return;

		GWT.log("Continue clicked");
		disableSubmitButton();
		nextEvent(true);
	}
	
	private void enableReturnButton(){
		returnButton.setEnabled(true);
		returnButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				returnButton_onClick(event);
			}
		};
	}
	
	private void disableReturnButton(){
		returnButton.clickHandler = null;
		returnButton.setEnabled(false);
	}
	
	
	private void returnButton_onClick(ClickEvent event){
		disableReturnButton();
		
		GWT.log("I am trying to go back to chunk " + mCurrentChunkIndex);
		disableSubmitButton();
		nextEvent(false);
		
	}
	private void close() {
		for(int i = 0, k = mRedo.length; i<k; i++)
			mRedo[i] = true;
		//getParentPanel().numCorrectChunks = countCorrect();
		getParentPanel().close(this, false);
	}

	public void setCorrect(int i) {
		if(i > -1&& i < mCorrect.length){
			mCorrect[i] = true;
			GWT.log("making a chunk true");
		}
	}
	
	//TODO: This should be a static method of some sort of utility class
	private static native void setTextScaling()/*-{
		$wnd.scaleText();
	}-*/;
}
