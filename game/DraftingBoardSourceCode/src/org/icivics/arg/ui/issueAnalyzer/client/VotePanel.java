package org.icivics.arg.ui.issueAnalyzer.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.IssueInstance;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

public class VotePanel extends Subpanel<IssueAnalyzer> {
	private static VotePanelUiBinder uiBinder = GWT.create(VotePanelUiBinder.class);
	interface VotePanelUiBinder extends UiBinder<Widget, VotePanel> {}
	
	private static final int NUM_OPTIONS = 2;
	private static final String BUTTON_SELECTED_CSS = "selected";
	
	private HTMLPanel[] optionButtons;
	private ImageElement[] optionImages;
	private ParagraphElement[] optionTexts;

	private HandlerRegistration[] mOptionClickRegs;
	private HandlerRegistration expFieldReg;
	private HandlerRegistration expFieldFocusReg;
	private HandlerRegistration mSubmitClickReg;
	
	@UiField
	ParagraphElement questionText;
	
	@UiField
	HTMLPanel option1button;
	@UiField
	ImageElement option1image;
	@UiField
	ParagraphElement option1text;
	
	@UiField
	HTMLPanel responseField;
	
	@UiField
	HTMLPanel option2button;
	@UiField
	ImageElement option2image;
	@UiField
	ParagraphElement option2text;
	
	@UiField
	HTMLPanel submitButton;

	DivElement explanationHolder;
	
	/*
	@UiField
	TextArea explanationField;
	*/
	
	@UiField
	AudioControl audioControl;
	
	public VotePanel(IssueAnalyzer parent) {
		super(parent);
		
		initWidget(uiBinder.createAndBindUi(this));
		optionButtons = new HTMLPanel[NUM_OPTIONS];
		optionButtons[0] = option1button;
		optionButtons[1] = option2button;
		optionImages = new ImageElement[NUM_OPTIONS];
		optionImages[0] = option1image;
		optionImages[1] = option2image;
		optionTexts = new ParagraphElement[NUM_OPTIONS];
		optionTexts[0] = option1text;
		optionTexts[1] = option2text;
		
		questionText.setInnerText(DataController.getCurrentIssue().issue.shortText);
		audioControl.loadSound("./audio/question.mp3");
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		//explanationField = (TextAreaElement) Document.get().getElementById("explanation_text");
		//cols="50" rows="5"  onFocus="this.value=''; this.onfocus=null;"
		explanationHolder = (DivElement) Document.get().getElementById("explanation");
		explanationHolder.addClassName("disabled");
		
		ElementUtil.addScrollbars(explanationHolder);
		
		responseField.getElement().getStyle().setProperty("minWidth","6em");
		responseField.getElement().getStyle().setProperty("minHeight","1em");
		responseField.getElement().setAttribute("contentEditable", "true");
		
		responseField.getElement().getStyle().setProperty("MozUserSelect", "text");
		responseField.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
		responseField.getElement().getStyle().setProperty("WebkitUserSelect", "text");
		responseField.getElement().getStyle().setProperty("userSelect", "text");
		
		mOptionClickRegs = new HandlerRegistration[NUM_OPTIONS];
		
		for (int i = 0; i < NUM_OPTIONS; ++i) {
			final int buttonIndex = i;
			optionImages[i].setSrc(DataController.getCurrentIssue().issue.claims[i].imageSrc);
			optionTexts[i].setInnerText(DataController.getCurrentIssue().issue.claims[i].text);
			mOptionClickRegs[i] = optionButtons[i].addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					optionButton_onClick(event, buttonIndex);
				}
			}, ClickEvent.getType());
		}
		submitButton.addStyleName("btn_disabled");
		
		expFieldFocusReg = responseField.addDomHandler(new FocusHandler(){

			@Override
			public void onFocus(FocusEvent event) {
				//responseField.getElement().setInnerText("");
				if(expFieldFocusReg!= null){
					expFieldFocusReg.removeHandler();
					expFieldFocusReg = null;
				}
				
			}}, FocusEvent.getType());
	}
	
	private void optionButton_onClick(ClickEvent e, int buttonIndex) {
		GWT.log("Clicked option " + buttonIndex);
		DataController.getCurrentIssue().selectedClaimIndex = buttonIndex;
		
		explanationHolder.removeClassName("disabled");
		for (int i = 0; i < NUM_OPTIONS; ++i) {
			optionButtons[i].removeStyleName(BUTTON_SELECTED_CSS);
			if (i == buttonIndex) {
				optionButtons[i].addStyleName(BUTTON_SELECTED_CSS);
			}
		}
		expFieldReg = responseField.addDomHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(responseField.getElement().getInnerText().replaceAll("\\W","").length() >0){
					submitButton.removeStyleName("btn_disabled");
					ElementUtil.updateScrollbars(explanationHolder);
					if (mSubmitClickReg == null) {
						
						mSubmitClickReg = submitButton.addDomHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								submitButton_onClick(event);
							}
						}, ClickEvent.getType());
					}
				} else {
					submitButton.addStyleName("btn_disabled");
					if(mSubmitClickReg != null)
						mSubmitClickReg.removeHandler();
					mSubmitClickReg = null;
				}
					
				
			}}, KeyUpEvent.getType());
		/*
		submitButton.removeStyleName("hidden");
		if (mSubmitClickReg == null) {
			mSubmitClickReg = submitButton.addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					submitButton_onClick(event);
				}
			}, ClickEvent.getType());
		}
		*/
		//explanationField.focus();
		//explanationField.select();
	}
	
	private void submitButton_onClick(ClickEvent e) {
		GWT.log("Vote clicked");
		if (mOptionClickRegs != null) {
			for (int i = 0; i < NUM_OPTIONS; ++i)
				mOptionClickRegs[i].removeHandler();
			mOptionClickRegs = null;
		}
		if(mSubmitClickReg != null){
			mSubmitClickReg.removeHandler();
			mSubmitClickReg = null;
		}
		if(expFieldReg != null){
			expFieldReg.removeHandler();
			expFieldReg = null;
		}
		if(expFieldFocusReg!= null){
			expFieldFocusReg.removeHandler();
			expFieldFocusReg = null;
		}
		
		
		//Commit data
		IssueInstance issue = DataController.getCurrentIssue();
		issue.claim = new ClaimInstance(issue.issue.claims[issue.selectedClaimIndex]);
		issue.counterClaim = new ClaimInstance(issue.issue.claims[issue.selectedClaimIndex == 0 ? 1 : 0]);
		issue.claimSelectExplanation = responseField.getElement().getInnerText().trim();
		close();
	}
	
	public void close() {
		getParentPanel().close(this);
	}
}
