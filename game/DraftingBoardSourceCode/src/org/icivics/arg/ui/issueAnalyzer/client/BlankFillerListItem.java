package org.icivics.arg.ui.issueAnalyzer.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class BlankFillerListItem extends Composite {
	private static BlankFillerListItemUiBinder uiBinder = GWT.create(BlankFillerListItemUiBinder.class);
	interface BlankFillerListItemUiBinder extends UiBinder<Widget, BlankFillerListItem> {}
	
	@UiField
	protected ParagraphElement textHolder;

	public BlankFillerListItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public String getText() {
		return textHolder.getInnerHTML();
	}
	public void setText(String text) {
		textHolder.setInnerHTML(text);
	}
	
	public void setActive(boolean active) {
		removeStyleName("active");
		if (active) {
			addStyleName("active");
		}
	}
	
	public void setComplete(boolean complete) {
		removeStyleName("complete");
		if (complete) {
			addStyleName("complete");
		}
	}
}
