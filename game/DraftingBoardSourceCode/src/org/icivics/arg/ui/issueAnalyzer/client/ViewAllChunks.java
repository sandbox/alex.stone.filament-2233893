package org.icivics.arg.ui.issueAnalyzer.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.StoryChunk;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewAllChunks<submitButton> extends Subpanel<IssueAnalyzer> {
	private static ViewAllChunksUiBinder uiBinder = GWT.create(ViewAllChunksUiBinder.class);
	interface ViewAllChunksUiBinder extends UiBinder<Widget, ViewAllChunks> {}
	
	@UiField
	DivElement displayText;
	@UiField
	LabelButton submitButton;
	
	//@UiField
	//ParagraphElement instructionParagraph;
	@UiField
	AudioConcatenatedControl audioControl;
	//@UiField
	//AudioControl instructionAudio;
	
	public int numCorrect;
	
	public ViewAllChunks(IssueAnalyzer parent, BlankFiller bf) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		ArrayList<StoryChunk> chunks = DataController.getCurrentIssue().issue.storyChunks;
				
		numCorrect = 0;
		if(getParentPanel().numChunkFailures == -1)
			getParentPanel().numChunkFailures = 0;
		
		HTML chunk;
		String text = "";
		for (int i = 0, count = chunks.size(); i < count; ++i){
			GWT.log("Inside viewallchunks constructor" + getParentPanel().chunkChoices[i]);
			if(!chunks.get(i).correctItemID.equals(getParentPanel().chunkChoices[i])){
				text = "<p><span class=\"highlight-red\">" + chunks.get(i).getOptionText(getParentPanel().chunkChoices[i]) + "</span></p>";
				chunk = new HTML(text);
				getParentPanel().numChunkFailures++;
			} else {
				text = "<p>" + chunks.get(i).getOptionText(getParentPanel().chunkChoices[i]) + "</p>";
				chunk = new HTML(text);
				bf.setCorrect(i);
				numCorrect++;
			}
			chunk.setStyleName("chunk");
			for(int j=0;j<3;j++)
				if(getParentPanel().chunkChoices[i] == chunks.get(i).options[j].item.id){
					StoryChunk _chunk = chunks.get(i);
					audioControl.loadSound(_chunk.audioUrlA,"chunk"+i+"A");
					audioControl.loadSound(_chunk.options[j].audioUrl, "chunk"+i+"option" + j);
					audioControl.loadSound(_chunk.audioUrlB,"chunk"+i+"B");
				}
			getRoot().add(chunk, displayText);
			
		}
		ElementUtil.addScrollbars(displayText);
		if(numCorrect != getParentPanel().chunkChoices.length){
			//submitButton.setEnabled(false);
			submitButton.setText("Go Back");
			submitButton.setInnerClass("submit-btn-back");
			//instructionAudio.loadSound("./audio/viewAllChunksIncorrect.mp3", "storyincorrectaudio");
			//instructionParagraph.setInnerText("Some of the story chunks do not make sense. Take a moment to read over the story and correct them. Click 'Go Back' when you're ready.");
			//GWT.log("Not all chunks are completed correctly " + numCorrect + " " + getParentPanel().chunkChoices.length);
		} else {
			submitButton.setText("Continue");
		}
		
		if(getParentPanel().numCorrectChunks == -1)
			getParentPanel().numCorrectChunks = numCorrect;
		
		GWT.log("Num completed correctly the first time through: " + getParentPanel().numCorrectChunks + " Number of failures so far: " + getParentPanel().numChunkFailures);
		
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		ElementUtil.updateScrollbars(displayText);
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		super.onDetach();
	}
	
	private void submitButton_onClick() {
		//submitButton.clickHandler = null;
		audioControl.clear();
		getParentPanel().close(this);
	}
}
