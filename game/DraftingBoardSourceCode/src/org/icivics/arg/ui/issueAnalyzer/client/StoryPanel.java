package org.icivics.arg.ui.issueAnalyzer.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.*;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class StoryPanel extends Subpanel<IssueAnalyzer> {
	private static StoryPanelUiBinder uiBinder = GWT.create(StoryPanelUiBinder.class);
	interface StoryPanelUiBinder extends UiBinder<Widget, StoryPanel> {}

	private HandlerRegistration mSubmitClickReg;
	
	public StoryPanel(IssueAnalyzer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiField
	ParagraphElement titleText;
	@UiField
	ParagraphElement text;
	@UiField
	HTMLPanel submitButton;
	@UiField
	AudioControl audioControl;
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		titleText.setInnerText(DataController.getCurrentIssue().issue.titleText);
		text.setInnerHTML("<img src=\"" + DataController.getCurrentIssue().issue.imageSrc + "\" width=\"150px\" height=\"150px\" alt=\"\" align=\"right\" />"
							+ DataController.getCurrentIssue().issue.fullText);
		audioControl.loadSound(DataController.getCurrentIssue().issue.audioSrc);
		
		mSubmitClickReg = submitButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick(event);
			}
		}, ClickEvent.getType());
	}
	
	@Override
	protected void onDetach() {
		
		super.onDetach();
	}

	private void submitButton_onClick(ClickEvent e) {
		GWT.log("Vote clicked");
		mSubmitClickReg.removeHandler();
		mSubmitClickReg = null;
		close();
	}
	
	public void close() {
		getParentPanel().close(this);
	}
}
