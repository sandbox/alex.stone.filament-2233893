package org.icivics.arg.ui.issueAnalyzer.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.json.client.IssueAnalyzerProgress;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class IssueAnalyzer extends ModuleUI<IssueAnalyzer> {
	protected int numCorrectChunks = -1;
	protected int numChunkFailures = 0;
	protected IssueAnalyzerProgress userData;
	
	protected String[] chunkChoices;
	protected BlankFiller mBlankFiller;
	
	protected EvidenceSidebar hud;
	
	public IssueAnalyzer(EvidenceSidebar hud) {
		this.hud = hud;
	}
	
	protected void commitScore() {
		int numChunks = DataController.getCurrentIssue().issue.storyChunks.size();
		double chunkScore = (double) numChunks / (double) (numChunks + numChunkFailures);
		
		chunkChoices = new String[numChunks];
		
		DataController.getCurrentIssue().scoreStoryChunks = chunkScore;
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		userData = DataController.getModuleProgress().forIssueAnalyzer();

		openUI(getSavedUI());
	}
	
	private Subpanel<IssueAnalyzer> getSavedUI() {
		int stepIndex = userData.getStepIndex();
		
		//Bad hack to prevent crash on step index = 2
		//stepIndex = 0;
		
		DataController.moduleStepIndex = stepIndex;
		
		GWT.log("Num correct when issue analyzer is created: " + numCorrectChunks );
		numCorrectChunks = userData.getCorrectChunks();
		GWT.log("Num correct soon after is created: " + numCorrectChunks  + " after user data loaded");
		
		numChunkFailures = userData.getChunkFailures();
		
		switch (stepIndex) {
			case 0: 
				hud.init();
				hud.hide();
				return new StoryPanel(this);
			case 1: 
				hud.show();
				mBlankFiller = new BlankFiller(this);
				return mBlankFiller;
			case 2: 
				hud.show();
				hud.init();
				//TODO: Load the progress from the progress
				BlankFiller bf = new BlankFiller(this);
				
				chunkChoices = userData.getChunkSelections();
				for(int i = 0; i < chunkChoices.length; i++)
					GWT.log(chunkChoices[i]);
				return new ViewAllChunks(this, bf);
			case 3: 
				hud.init();
				hud.show();
				return new VotePanel(this);
			case 4:	return null;
			default:
				DataController.moduleStepIndex = 0;
				return new StoryPanel(this);
		}
	}
	
	void close(StoryPanel ui) {
		DataController.saveModuleProgress(1);
		if(mBlankFiller == null)
			mBlankFiller = new BlankFiller(this);
		transitionTo(mBlankFiller);
		hud.show();
	}
	
	void close(BlankFiller ui, Boolean goingBack) {
		if(goingBack){
			DataController.saveModuleProgress(0);
			hud.hide();
			transitionTo(new StoryPanel(this));
		} else {
			userData.setChunkSelections(chunkChoices);
			DataController.saveModuleProgress(2);
			hud.init();
			transitionTo(new ViewAllChunks(this, ui));
		}
	}
	
	void close(ViewAllChunks ui) {
		// Decide if need to go back
		userData.setCorrectChunks(numCorrectChunks);
		DataController.getCurrentIssue().issueAnalyzerFirstPass = numCorrectChunks;
		userData.setChunkFailures(numChunkFailures);
		DataController.getCurrentIssue().issueAnalyzerTotalFailures = numChunkFailures;
		
		
		if(ui.numCorrect == chunkChoices.length){
			hud.init();
			DataController.saveModuleProgress(3);
			transitionTo(new VotePanel(this));
		} else {
			if(mBlankFiller == null)
				mBlankFiller = new BlankFiller(this);
			DataController.saveModuleProgress(1);	
			transitionTo(mBlankFiller);
		}
	}
	
	void close(VotePanel ui) {
		userData.setSelectedClaimIndex(DataController.getCurrentIssue().selectedClaimIndex);
		userData.setSelectedClaimExplanation(DataController.getCurrentIssue().claimSelectExplanation);
		
		

		// Moved from close(Debate ui), in clam creator now that the 'debate' section is being moved {
		DataController.submitClaimVote(DataController.getCurrentIssue().selectedClaimIndex);
		// }
		
		DataController.saveModuleProgress(3);
		hud.hide();
		
		finish();
	}
}
