package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class Instructions extends Subpanel<ConclusionCrafter> {
	private static InstructionsUiBinder uiBinder = GWT.create(InstructionsUiBinder.class);
	interface InstructionsUiBinder extends UiBinder<Widget, Instructions> {}
	
	private TextPopup mPopup;
	
	public Instructions(ConclusionCrafter parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		mPopup = new TextPopup();
		mPopup.setTitle("Building a Conclusion Paragraph");
		mPopup.setBody("<p>It is time to complete your essay with a strong conclusion paragraph. This is your chance to:</p><ol><li>Restate your claim.</li> <li> Remind your audience of the reasons that support your claim.</li> <li>Drive your argument home with a \"clincher\" that leaves a strong impression.</li><ol><p>Let's finish this essay!</p>"
				);
		mPopup.setDimmerVisible(false);
		mPopup.setHandler(new TextPopup.PopupHandler() {
			@Override
			public void onClose(TextPopup popup) {
				popup_onClose();
			}
		});
		getRoot().add(mPopup);
	}
	
	@Override
	protected void onDetach() {
		if (mPopup != null) {
			mPopup.close();
			mPopup = null;
		}
		super.onDetach();
	}
	
	private void popup_onClose() {
		mPopup.clearHandler();
		mPopup = null;
		getParentPanel().close(this);
	}
}
