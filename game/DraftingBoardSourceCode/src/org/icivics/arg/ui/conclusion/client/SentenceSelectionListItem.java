package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.ui.common.client.AudioControl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceSelectionListItem extends Composite {
	private static IssueSelectListItemUiBinder uiBinder = GWT.create(IssueSelectListItemUiBinder.class);
	interface IssueSelectListItemUiBinder extends UiBinder<Widget, SentenceSelectionListItem> {}
	
	@UiField
	public HTMLPanel button;
	@UiField
	ParagraphElement text;
	@UiField
	AudioControl itemAudio;
	
	public SentenceSelectionListItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public String getText() {
		return text.getInnerText();
	}
	public void setText(String v) {
		text.setInnerText(v);
	}
	public void setAudio(String url){
		itemAudio.loadSound(url);
	}
}
