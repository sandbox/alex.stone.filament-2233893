package org.icivics.arg.ui.conclusion.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class ClozeSentenceOption extends Composite implements HasText {
	private static ClozeSentenceOptionUiBinder uiBinder = GWT.create(ClozeSentenceOptionUiBinder.class);
	interface ClozeSentenceOptionUiBinder extends UiBinder<Widget, ClozeSentenceOption> {}

	@UiField
	HTMLPanel sentence;
	
	public ClozeSentenceOption() {
		initWidget(uiBinder.createAndBindUi(this));
	}



	public ClozeSentenceOption(String text) {
		initWidget(uiBinder.createAndBindUi(this));
		
		setText(text);
	}

	public void setText(String text) {
		String[] textArray = text.split(ItemChunk.REPLACEMENT_TOKEN_REGEX, -1);
		for(int i = 0, count = textArray.length; i < count; i++){
			HTMLPanel dbContent = new HTMLPanel("span",textArray[i]);
			dbContent.addStyleName(DistinguishableTextBox.STUDENTSELECTEDCLASSNAME);
			sentence.add(dbContent);
			if(i+1 != count){
				HTMLPanel blank = new HTMLPanel("span", " ");
				blank.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
				blank.addStyleName("user-written");
				blank.getElement().setAttribute("contenteditable","true");
				blank.getElement().setAttribute("contentEditable", "true");
				blank.getElement().getStyle().setProperty("MozUserSelect", "text");
				blank.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
				blank.getElement().getStyle().setProperty("WebkitUserSelect", "text");
				blank.getElement().getStyle().setProperty("userSelect", "text");
				
				blank.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
				blank.getElement().getStyle().setProperty("minWidth","6em");
				blank.getElement().getStyle().setProperty("minHeight","1em");
				sentence.add(blank);
			}
		}
		
	}

	public String getText() {
		
		HTMLPanel sentence = new HTMLPanel(this.sentence.getElement().getInnerHTML());
		
		Element child = sentence.getElement().getFirstChildElement();
		do {
			child.removeAttribute("contenteditable");
			child.removeAttribute("contentEditable");
			child.getStyle().clearProperty("MozUserSelect");
			child.getStyle().clearProperty("KhtmlUserSelect");
			child.getStyle().clearProperty("WebkitUserSelect");
			child.getStyle().clearProperty("userSelect");
			if(child.hasClassName("user-written")){
				child.addClassName(DistinguishableTextBox.STUDENTCLASSNAME);
				child.removeClassName("user-written");
			}
			
			child.getStyle().clearDisplay();
			child.getStyle().clearProperty("minWidth");
			
			
			SafeHtmlBuilder sanitizer = new SafeHtmlBuilder();
			sanitizer.appendEscaped(child.getInnerText());
			
			child.setInnerHTML(sanitizer.toSafeHtml().asString());
			
			child = child.getNextSiblingElement();
		} while(child != null);
		
		GWT.log(sentence.getElement().getInnerHTML());
		
		return sentence.getElement().getInnerHTML();
		
	}

	public void setSelected(boolean selected){
		if(selected)
			this.addStyleName("selected");
		else
			this.removeStyleName("selected");
	}
	
	public void preventCollapse(){
		Iterator<Widget> children = sentence.iterator();
		//items[i-1].customSentenceText = "";
		while(children.hasNext()){
			Widget element = children.next();
			if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains("user-written") &&
					element.getElement().getInnerText().length() == 0){
				element.getElement().setInnerHTML("&nbsp;");
			}
		}
	}
	
	public boolean isComplete(){
		Iterator<Widget> children = sentence.iterator();
		boolean slotsFilled = true;
		//items[i-1].customSentenceText = "";
		while(children.hasNext()){
			Widget element = children.next();
			if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains("user-written") &&
					element.getElement().getInnerText().replaceAll("\\s", "").length() == 0){
				slotsFilled = false;
			}
		}
		
		return slotsFilled;
	}
}
