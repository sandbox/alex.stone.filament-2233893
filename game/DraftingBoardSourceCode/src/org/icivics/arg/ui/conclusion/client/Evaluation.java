package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.paragraphBuilder.client.ChecklistQuestion;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class Evaluation extends Subpanel<ConclusionCrafter> {
	private static EvaluationUiBinder uiBinder = GWT.create(EvaluationUiBinder.class);
	interface EvaluationUiBinder extends UiBinder<Widget, Evaluation> {}

	@UiField
	DistinguishableTextBox paragraphText;
	
	@UiField
	HTMLPanel sideColumn;
	
	@UiField
	LabelButton submitButton, backButton;
	
	ChecklistQuestion[] mQuestions;
	
	public Evaluation(ConclusionCrafter parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		String txt = "";
		txt += getParentPanel().userData.getFirstSentence();
		txt += getParentPanel().userData.getSecondSentence();
		txt += getParentPanel().userData.getThirdSentence();
		txt += getParentPanel().userData.getFourthSentence();
		
		String str = "";
		
		str = txt + "<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME + "\">" + DataController.getCurrentIssue().getSelectedOutro().text + "</span>";
		DataController.getCurrentIssue().claim.conclusionParagraphOriginal = str;
		paragraphText.setInnerText(str);
		
		mQuestions = new ChecklistQuestion[3];
		for(int i=0; i <3; i++){
			sideColumn.add(mQuestions[i] = new ChecklistQuestion(0, i));
		}
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};	
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		
		for(int i=0; i<3; i++)
			mQuestions[i].addChangeHandler(new ChecklistQuestion.ChangeHandler(){
				@Override
				public void onChange(){
					submitButton.setEnabled(checkComplete());
				}
			});
		ElementUtil.setTextScaling();
	}
	
	private boolean checkComplete() {
		for (int i = 0; i < mQuestions.length; ++i)
			if (mQuestions[i].getSelectedOption() == -1)
				return false;
		return true;
	}
	
	private void submitButton_onClick() {
		GWT.log("Submit clicked");
		
		DataController.getCurrentIssue().claim.conclusionParagraph = paragraphText.getValue();
		getParentPanel().close(this, false);
	}
	
	private void backButton_onClick(){
		getParentPanel().close(this, true);
	}

}
