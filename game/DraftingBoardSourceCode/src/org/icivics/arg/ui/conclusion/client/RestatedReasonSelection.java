package org.icivics.arg.ui.conclusion.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.data.model.client.ItemChunk;
import org.icivics.arg.data.model.client.Reason;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class RestatedReasonSelection extends Subpanel<ConclusionCrafter> implements ReasonRestatement{

	private static RestatedReasonSelectionUiBinder uiBinder = GWT.create(RestatedReasonSelectionUiBinder.class);

	interface RestatedReasonSelectionUiBinder extends UiBinder<Widget, RestatedReasonSelection> {}

	String[] DIRECTIONS_LABELS = {"RESTATE YOUR FIRST REASON","RESTATE YOUR SECOND REASON","RESTATE YOUR THIRD REASON"};
	String[] DIRECTIONS_TEXT = {"<ul><li>READ each sentence starter. They each restate your reason in a different way.</li><li>CLICK the sentence you want to use to restate your reason.</li><li>COMPLETE the sentence using information from your original reason.</li></ul>",
								"<ul><li>READ each sentence starter. They each restate your reason in a different way.</li><li>CLICK the sentence you want to use to restate your reason.</li><li>COMPLETE the sentence using information from your original reason.</li></ul>",
								"<ul><li>READ each sentence starter. They each restate your reason in a different way.</li><li>CLICK the sentence you want to use to restate your reason.</li><li>COMPLETE the sentence using information from your original reason.</li></ul>"};
	
	String[] DIRECTIONS_AUDIO_URLS = {"Directions_Conclusion1_restate1.mp3","Directions_Conclusion2_restate1.mp3", "Directions_Conclusion3_restate1.mp3",
									  "",									"Directions_Conclusion2_restate2.mp3","Directions_Conclusion3_restate2.mp3",
									  "",									"Directions_Conclusion2_restate3.mp3","Directions_Conclusion3_restate3.mp3"};
	@UiField
	SpanElement paragraphSoFar, paragraphEnd;
	
	@UiField
	HTMLPanel sentenceA, sentenceB, sentenceC, sentenceD;
	
	@UiField
	HTMLPanel optionA, optionB, optionC, optionD;
	
	@UiField
	LabelButton submitButton, backButton;
	
	@UiField
	AudioControl directionsAudio;
	
	@UiField
	AudioConcatenatedControl claimAudio;
	
	@UiField
	ParagraphElement claimText, reasonText;
	@UiField
	DivElement directionsLabel, directionsText;
	
	HTMLPanel mOptionButtons[];
	HTMLPanel[] mOptions;
	
	private int sentenceIndex;
	private int mSelectedSentenceIndex;
	
	public String finalSentence = "Sentence that gets made by the screen";
	
	public RestatedReasonSelection(ConclusionCrafter parent, int sentenceIndex){
		this(parent,sentenceIndex, 0);
	}
	
	public RestatedReasonSelection(ConclusionCrafter parent, int sentenceIndex, int scaffoldLevel) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		this.sentenceIndex = sentenceIndex;
		
		mOptions = new HTMLPanel[4];
		mOptions[0] = sentenceA;
		mOptions[1] = sentenceB;
		mOptions[2] = sentenceC;
		mOptions[3] = sentenceD;
		
		mOptionButtons = new HTMLPanel[4];
		mOptionButtons[0] = optionA;
		mOptionButtons[1] = optionB;
		mOptionButtons[2] = optionC;
		mOptionButtons[3] = optionD;
	
		setOptions(scaffoldLevel);
		
		String paragraphText = new String();
		paragraphText = getParentPanel().userData.getFirstSentence();
		if(sentenceIndex > 0)
			paragraphText += getParentPanel().userData.getSecondSentence();
		if(sentenceIndex > 1)
			paragraphText += getParentPanel().userData.getThirdSentence();
		if(sentenceIndex > 2)
			paragraphText += getParentPanel().userData.getFourthSentence();
		
		paragraphSoFar.setInnerHTML(paragraphText);
		submitButton.setEnabled(false);
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}};
			
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onBack();
			}
		};
		paragraphEnd.getStyle().setDisplay(Display.INLINE_BLOCK);
		paragraphEnd.getStyle().setProperty("minWidth","8em");
		
		setDirectionsText(sentenceIndex, scaffoldLevel);
		
		Claim claim = DataController.getCurrentClaim();
		claimText.setInnerText(claim.text);
		reasonText.setInnerText(claim.reasons[sentenceIndex].text);
		
		claimAudio.loadSound(claim.audioSrc, "claim" + claim.id);
		claimAudio.loadSound(DataController.getIssueURL() + "because.mp3", "because");
		claimAudio.loadSound(claim.reasons[sentenceIndex].audioSrc , "reason" + claim.reasons[sentenceIndex].id);
	}
	
	protected void setDirectionsText(int sentenceIndex, int scaffoldIndex){
		directionsLabel.setInnerText(DIRECTIONS_LABELS[sentenceIndex]);
		if(scaffoldIndex == 0){
			//WHY?
			directionsText.setInnerHTML("<ul><li>READ each sentence. They each restate your reason in a different way.</li><li>CLICK the sentence you think best restates your reason.</li></ul>");
		}
		if(scaffoldIndex == 1){
			directionsText.setInnerHTML(DIRECTIONS_TEXT[sentenceIndex]);
		}
		directionsAudio.setLocation(DIRECTIONS_AUDIO_URLS[sentenceIndex*3 + scaffoldIndex]);
	}
	
	protected void setOptions(int scaffoldLevel){
		Reason reason = DataController.getCurrentClaim().reasons[sentenceIndex];
		
		if(scaffoldLevel == 0){
			for(int i = 0; i < 4; i++){
				final int index = i;
				
				HTMLPanel text = new HTMLPanel("span"," " + reason.conclusionCrafterPrewritten[i]);
				text.addStyleName("player-selected");
				
				mOptions[i].add(text);
				mOptionButtons[i].addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						onOptionSelect(index);					
					}}, ClickEvent.getType());
			}
		} else if(scaffoldLevel == 1){
			for(int i = 0; i < 4; i++){
				final int index = i;
				
				String text = " " + reason.conclusionCrafterClozeStrings[i];
				HTMLPanel sentence = mOptions[i];
				
				String[] textArray = text.split(ItemChunk.REPLACEMENT_TOKEN_REGEX);
				for(int k = 0, count = textArray.length; k < count; k++){
					HTMLPanel dbContent = new HTMLPanel("span",textArray[k]);
					dbContent.addStyleName(DistinguishableTextBox.STUDENTSELECTEDCLASSNAME);
					sentence.add(dbContent);
					if(k+1 != count){
						HTMLPanel blank = new HTMLPanel("span", " ");
						blank.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
						blank.addStyleName("user-written");
						blank.getElement().setAttribute("contenteditable","true");
						blank.getElement().setAttribute("contentEditable", "true");
						blank.getElement().getStyle().setProperty("MozUserSelect", "text");
						blank.getElement().getStyle().setProperty("KhtmlUserSelect", "text");
						blank.getElement().getStyle().setProperty("WebkitUserSelect", "text");
						blank.getElement().getStyle().setProperty("userSelect", "text");
						
						blank.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
						blank.getElement().getStyle().setProperty("minWidth","6em");
						blank.getElement().getStyle().setProperty("minHeight","1em");
						
						sentence.add(blank);
					}
				}
				
				mOptionButtons[i].addDomHandler(new KeyUpHandler(){
					@Override
					public void onKeyUp(KeyUpEvent event) {
						onOptionSelect(index);		
					}}, KeyUpEvent.getType());
				mOptionButtons[i].addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						onOptionSelect(index);					
					}}, ClickEvent.getType());
			}
		}
		
	}
	
	protected boolean checkComplete(){
		HTMLPanel sentence = mOptions[mSelectedSentenceIndex];
		
		Iterator<Widget> children = sentence.iterator();
		boolean slotsFilled = true;
		//items[i-1].customSentenceText = "";
		while(children.hasNext()){
			Widget element = children.next();
			//GWT.log("TEsting this tring for having non-space characters: " + element.getElement().getInnerText());
			if(new ArrayList<String>(Arrays.asList(element.getElement().getClassName().split("\\s"))).contains("user-written") &&
					element.getElement().getInnerText().replaceAll("\\s", "").length() == 0){
				slotsFilled = false;
			}
		}
		
		return slotsFilled;
	}
	
	protected String getOptionText(int index){
		//String text = mOptions[index].getElement().getInnerHTML();
		
		HTMLPanel sentence = new HTMLPanel("");
		Iterator<Widget> children = mOptions[index].iterator();
		//items[i-1].customSentenceText = "";
		while(children.hasNext()){
			Widget element = children.next();
			HTMLPanel element2 = new HTMLPanel("span", element.getElement().getInnerHTML());
			
			if(element.getElement().hasClassName("user-written")){
				element2.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
				element2.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
			}
			if(element.getElement().hasClassName("player-selected"))
				element2.addStyleName(DistinguishableTextBox.STUDENTSELECTEDCLASSNAME);
			
			SafeHtmlBuilder sanitizer = new SafeHtmlBuilder();
			sanitizer.appendEscaped(element.getElement().getInnerText());
			
			element2.getElement().setInnerHTML(sanitizer.toSafeHtml().asString());
			element2.getElement().getStyle().setProperty("minWidth","3em");
			element2.getElement().getStyle().setProperty("minHeight","1em");
			
			sentence.add(element2);
		}
		
		return sentence.getElement().getInnerHTML();

	}
	
	protected void onOptionSelect(int index){
		for(int i = 0 ; i < 4; i++){
			mOptionButtons[i].removeStyleName("selected");
		}
		mOptionButtons[index].addStyleName("selected");
		
		mSelectedSentenceIndex = index;
		
		submitButton.setEnabled(checkComplete());
		
		paragraphEnd.removeClassName("user-written");
		paragraphEnd.getStyle().clearDisplay();
		paragraphEnd.setInnerHTML(getOptionText(index));
	}
	
	protected void onSubmit(){
		finalSentence = getOptionText(mSelectedSentenceIndex);
		submitButton.setEnabled(false);
		backButton.setEnabled(false);
		submitButton.clickHandler = null;
		getParentPanel().close(this,sentenceIndex, false);
	}
	
	private void onBack(){
		submitButton.setEnabled(false);
		backButton.setEnabled(false);
		submitButton.clickHandler = null;
		getParentPanel().close(this,sentenceIndex, true);
	}
	
	public String getFinalSentence(){
		return finalSentence;
	}

}
