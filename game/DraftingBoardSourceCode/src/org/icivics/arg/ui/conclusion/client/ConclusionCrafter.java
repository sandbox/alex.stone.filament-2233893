package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.json.client.ConclusionCrafterProgress;
import org.icivics.arg.data.json.client.ParagraphBuilderProgress;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class ConclusionCrafter extends ModuleUI<ConclusionCrafter> {
	protected int numFailures = 0;
	protected ConclusionCrafterProgress userData;
	
	public ConclusionCrafter() {
		
	}
	
	@Override
	protected void commitScore() {
		DataController.getCurrentIssue().scoreConclusion = 4.0 / (4.0 + numFailures);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		userData = DataController.getModuleProgress().forConclusionCrafter();
		openUI(getSavedUI());
	}
	
	private Subpanel<ConclusionCrafter> getSavedUI() {
		int stepIndex = userData.getStepIndex();
		DataController.moduleStepIndex = stepIndex;
		
		if (stepIndex > 0) {
			numFailures = userData.getSentenceFailures();
		}
		
		switch (stepIndex) {
		
		case 0: return new Instructions(this);
		case 1: return new SentenceSelectionFiller(this);
		case 2: 
		case 3: 
		case 4: 
		default:
			DataController.moduleStepIndex = 0;
			return new Instructions(this);
			/*
			//TODO: Fix all this
			case 0: return new Instructions(this);
			case 1:
			case 2:
			case 3: 
				//TODO: Switch between the scaffolding options
				return new SentenceWriting(this, stepIndex);
			case 4: return new Categorize(this);
			case 5: return new SentenceSelection(this);
			case 6: return new EditParagraph(this);
			case 7: return null;
			default:
				DataController.moduleStepIndex = 0;
				return new Instructions(this);
				*/
		}
	}
	
	void close(Instructions ui) {
		transitionTo(new SentenceSelectionFiller(this));
	}
	
	void close(SentenceSelectionFiller ui){	
		userData.setFirstSentence(ui.finalSentence);
		
		int nextScaffold = ParagraphBuilderProgress.SCAFFOLDING_LEVELS[DataController.getCurrentIssue().studentLevel][0];
		switch(nextScaffold){
			case 0:
				transitionTo(new RestatedReasonSelection(this, 0));
				break;
			case 1:
				transitionTo(new RestatedReasonSelection(this, 0, 1));
				break;
			case 2:
				transitionTo(new RestatedReasonFiller(this,0));
				break;
			default:
				GWT.log("An unreasonable scaffolding level has been selected");
				break;
		}
	}
	
	void close(RestatedReasonSelection ui, int sentenceIndex, boolean backwards){
		sentenceTransition(ui, sentenceIndex, backwards);
	}
	
	void close(RestatedReasonFiller ui, int sentenceIndex, boolean backwards){
		sentenceTransition(ui, sentenceIndex, backwards);
	}
	
	void sentenceTransition(ReasonRestatement ui, int sentenceIndex, boolean backwards){
		if(!backwards){
			sentenceIndex++;
			
			int nextScaffold = 0;
			if(sentenceIndex <3)
				nextScaffold = ParagraphBuilderProgress.SCAFFOLDING_LEVELS[DataController.getCurrentIssue().studentLevel][sentenceIndex];
			
			switch(sentenceIndex){
			case 0:
				//This should not happen!
				userData.setFirstSentence(ui.getFinalSentence());
				break;
			case 1:
				userData.setSecondSentence(ui.getFinalSentence());
				break;
			case 2:
				userData.setThirdSentence(ui.getFinalSentence());
				break;
			case 3:
				userData.setFourthSentence(ui.getFinalSentence());
				transitionTo(new Categorize(this));
				break;
			default:
				break;
			}
			
			if(sentenceIndex < 3)
				switch(nextScaffold){
					case 0:
						transitionTo(new RestatedReasonSelection(this, sentenceIndex));
						break;
					case 1:
						transitionTo(new RestatedReasonSelection(this, sentenceIndex, 1));
						break;
					case 2:
						transitionTo(new RestatedReasonFiller(this,sentenceIndex));
						break;
					default:
						GWT.log("An unreasonable scaffolding level has been selected");
						break;
				}
		} else {
			sentenceIndex--;
			
			if(sentenceIndex == -1){
				transitionTo(new SentenceSelectionFiller(this));
			} else {
				int nextScaffold = 0;
				if(sentenceIndex <3)
					nextScaffold = ParagraphBuilderProgress.SCAFFOLDING_LEVELS[DataController.getCurrentIssue().studentLevel][sentenceIndex];
				switch(nextScaffold){
					case 0:
						transitionTo(new RestatedReasonSelection(this, sentenceIndex));
						break;
					case 1:
						transitionTo(new RestatedReasonSelection(this, sentenceIndex, 1));
						break;
					case 2:
						transitionTo(new RestatedReasonFiller(this,sentenceIndex));
						break;
					default:
						GWT.log("An unreasonable scaffolding level has been selected");
						break;
				}
			}
		}
	}
	
	void close(Categorize ui, boolean backwards) {
		//DataController.saveModuleProgress(5);
		if(!backwards){
			transitionTo(new SentenceSelection(this));
		} else {
			sentenceTransition(null, 3, true);
		}
	}
	
	void close(SentenceSelection ui, boolean backwards) {
		userData.setSelectedOutroIndex(DataController.getCurrentIssue().selectedOutroIndex);
		
		//DataController.saveModuleProgress(6);
		if(!backwards){
			transitionTo(new EditParagraph(this));
		} else {
			transitionTo(new Categorize(this));
		}
	}
	
	void close(Evaluation ui, boolean backwards){
		
		if(!backwards){
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.conclusionParagraphOriginal);
			userData.setFinalParagraph(DataController.getCurrentIssue().claim.conclusionParagraphOriginal);
			
			DataController.saveModuleProgress(7);
			finish();
		} else {
			transitionTo(new SentenceSelection(this));
		}
	}
	
	void close(EditParagraph ui) {
		userData.setOriginalParagraph(DataController.getCurrentIssue().claim.conclusionParagraphOriginal);
		userData.setFinalParagraph(DataController.getCurrentIssue().claim.conclusionParagraphOriginal);
		
		DataController.saveModuleProgress(7);
		finish();
	}
}
