package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.OutroSentence;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class SentenceSelection extends Subpanel<ConclusionCrafter> {
	private static SentenceSelectionUiBinder uiBinder = GWT.create(SentenceSelectionUiBinder.class);
	interface SentenceSelectionUiBinder extends UiBinder<Widget, SentenceSelection> {}
	
	@UiField
	DivElement optionList;
	@UiField
	LabelButton submitButton, backButton;
	
	private int mSelectedIndex = -1;
	private SentenceSelectionListItem[] mListItems;
	private HandlerRegistration[] mToggleClickRegs;
	
	public SentenceSelection(ConclusionCrafter parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		OutroSentence[] sentences = DataController.getCurrentIssue().claim.claim.outroSentences;
		mListItems = new SentenceSelectionListItem[sentences.length];
		for (int i = 0; i < sentences.length; ++i) {
			mListItems[i] = new SentenceSelectionListItem();
			mListItems[i].setText(sentences[i].text);
			mListItems[i].setAudio(sentences[i].audioSrc);
			mListItems[i].button.addStyleName("off");
			getRoot().add(mListItems[i], optionList);
		}
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		mToggleClickRegs = new HandlerRegistration[mListItems.length];
		for (int i = 0; i < mListItems.length; ++i) {
			final int buttonIndex = i;
			mToggleClickRegs[i] = mListItems[i].button.addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toggleButton_onClick(buttonIndex);
				}
			}, ClickEvent.getType());
		}
		submitButton.setEnabled(false);
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		backButton.clickHandler = new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
			
		};
	}

	@Override
	protected void onDetach() {
		if (mToggleClickRegs != null) {
			for (int i = 0; i < mToggleClickRegs.length; ++i)
				mToggleClickRegs[i].removeHandler();
			mToggleClickRegs = null;
		}
		submitButton.destroy();
		super.onDetach();
	}
	
	private void toggleButton_onClick(int buttonIndex) {
		for (int i = 0; i < mListItems.length; ++i) {
			mListItems[i].button.removeStyleName("on");
			mListItems[i].button.removeStyleName("off");
			mListItems[i].button.addStyleName(i == buttonIndex ? "on" : "off");
		}
		mSelectedIndex = buttonIndex;
		submitButton.setEnabled(true);
	}

	private void submitButton_onClick() {
		if (mToggleClickRegs != null) {
			for (int i = 0; i < mToggleClickRegs.length; ++i)
				mToggleClickRegs[i].removeHandler();
			mToggleClickRegs = null;
		}
		submitButton.destroy();
		DataController.getCurrentIssue().selectedOutroIndex = mSelectedIndex;
		getParentPanel().close(this, false);
	}
	
	private void backButton_onClick(){
		getParentPanel().close(this, true);
	}
}
