package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SentenceSelectionFiller extends Subpanel<ConclusionCrafter> {
	private static SentenceSelectionFillerUiBinder uiBinder = GWT.create(SentenceSelectionFillerUiBinder.class);
	interface SentenceSelectionFillerUiBinder extends UiBinder<Widget, SentenceSelectionFiller> {}

	@UiField
	HTMLPanel optionHolder;
	
	@UiField
	ParagraphElement claimText;
	
	@UiField
	LabelButton submitButton;
	
	@UiField
	AudioControl claimAudio;
	
	ClozeSentenceOption[] mOptions;
	int mSelectedSentence = -1;
	
	public String finalSentence;
	
	public SentenceSelectionFiller(ConclusionCrafter parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		Claim claim = DataController.getCurrentClaim();
		claimText.setInnerText(claim.text);
		claimAudio.loadSound(claim.audioSrc);
		
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}};
		
		mOptions = new ClozeSentenceOption[4];
		
		
		
		for(int i=0; i < 4; i++){
			final int index = i;
			ClozeSentenceOption option = new ClozeSentenceOption(claim.restateClozeSentence[i]);
			
			option.addDomHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					onOptionSelected(index);
				}}, ClickEvent.getType());
			
			mOptions[i] = option;
			
			optionHolder.add(option);
		}
		
		optionHolder.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				onInput();
			}},KeyUpEvent.getType());
		
		onInput();
		
	}

	protected void onOptionSelected(int index){
		mSelectedSentence = index;
		for(int i =0; i < 4; i++)
			mOptions[i].setSelected(i==index);
		onInput();
	}
	
	
	protected boolean checkComplete(){
		if(mSelectedSentence == -1)
			return false;
		if(!mOptions[mSelectedSentence].isComplete())
			return false;
		/* In case there's a correct thing
		if(mSelectedSentence != mCorrectSentence)
			return false;
		 * 
		 */
		
		
		return true;
	}
	
	protected void onInput(){
		if(mSelectedSentence > 0 && mSelectedSentence < mOptions.length)
		mOptions[mSelectedSentence].preventCollapse();
		submitButton.setEnabled(checkComplete());
	}
	
	protected void onSubmit(){
		GWT.log(mOptions[mSelectedSentence].getText());
		finalSentence = mOptions[mSelectedSentence].getText();
		submitButton.clickHandler = null;
		getParentPanel().close(this);
	}
}
