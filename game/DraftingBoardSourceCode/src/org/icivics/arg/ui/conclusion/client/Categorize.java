package org.icivics.arg.ui.conclusion.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.OutroSentence;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Categorize extends Subpanel<ConclusionCrafter> {
	private static CategorizeUiBinder uiBinder = GWT.create(CategorizeUiBinder.class);
	interface CategorizeUiBinder extends UiBinder<Widget, Categorize> {}
	
	@UiField
	DivElement sentenceList;
	@UiField
	SimplePanel panelHolder;
	@UiField
	LabelButton submitButton, backButton;
	
	private CategorizeListItem[] mListItems = new CategorizeListItem[5];
	private TextPopup mPopup;
	
	public Categorize(ConclusionCrafter parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		OutroSentence[] sentences = DataController.getCurrentIssue().claim.claim.outroSentences;
		for (int i = 0; i < sentences.length; ++i) {
			mListItems[i] = new CategorizeListItem();
			mListItems[i].setText(sentences[i].text);
			mListItems[i].setAudio(sentences[i].audioSrc);
			getRoot().add(mListItems[i], sentenceList);
		}
		
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		for (int i = 0; i < mListItems.length; ++i) {
			final int itemIndex = i;
			mListItems[i].handler = new CategorizeListItem.Handler() {
				@Override
				public void onChangeType() {
					item_onChangeType(itemIndex);
				}
			};
		}
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		backButton.clickHandler = new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
			
		};
	}
	
	@Override
	protected void onDetach() {
		if (mPopup != null) {
			mPopup.clearHandler();
			mPopup.removeFromParent();
			mPopup = null;
		}
		for (int i = 0; i < mListItems.length; ++i)
			mListItems[i].handler = null;
		submitButton.clickHandler = null;
		super.onDetach();
	}
	
	private boolean checkCorrect() {
		
		boolean correct = true;
		
		OutroSentence[] sentences = DataController.getCurrentIssue().claim.claim.outroSentences;
		for (int i = 0; i < mListItems.length; ++i) {
			mListItems[i].listField.removeStyleName("alert");
			if (mListItems[i].getSelectedType() != sentences[i].type) {
				mListItems[i].listField.addStyleName("alert");
				correct = false;
			}
		}
		
		return correct;
	}
	
	private boolean checkComplete() {
		for (int i = 0; i < mListItems.length; ++i)
			if (mListItems[i].getSelectedType() == -1)
				return false;
		return true;
	}
	
	private void item_onChangeType(int itemIndex) {
		if (checkComplete()) {
			submitButton.setEnabled(true);
		}
	}
	
	private void submitButton_onClick() {
		if (mPopup != null)
			return;
			
		if (checkCorrect()) {
			GWT.log("Submit correct");
			submitButton.clickHandler = null;
			getParentPanel().close(this, false);
		} else {
			GWT.log("Submit incorrect");
			mPopup = new TextPopup();
			mPopup.setTitle("Incorrect");
			mPopup.setBody("You categorized one or more of these clincher sentences incorrectly. Try again.");
			mPopup.setHandler(new TextPopup.PopupHandler() {
				@Override
				public void onClose(TextPopup popup) {
					feedbackPopup_onClose(popup);
				}
			});
			panelHolder.add(mPopup);
		}
	}
	
	private void backButton_onClick(){
		getParentPanel().close(this, true);
	}
	
	private void feedbackPopup_onClose(TextPopup popup) {
		popup.clearHandler();
		if (mPopup == popup)
			mPopup = null;
	}
}
