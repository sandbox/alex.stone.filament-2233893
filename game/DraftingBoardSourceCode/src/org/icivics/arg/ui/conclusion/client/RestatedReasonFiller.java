package org.icivics.arg.ui.conclusion.client;

import java.util.Iterator;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Claim;
import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class RestatedReasonFiller extends Subpanel<ConclusionCrafter> implements ReasonRestatement{
	private static RestatedReasonFillerUiBinder uiBinder = GWT.create(RestatedReasonFillerUiBinder.class);
	interface RestatedReasonFillerUiBinder extends	UiBinder<Widget, RestatedReasonFiller> {}

	String[] DIRECTIONS_LABELS = {"RESTATE YOUR FIRST REASON","RESTATE YOUR SECOND REASON","RESTATE YOUR THIRD REASON"};
	String[] DIRECTIONS_TEXT = {"<ul><li>READ your original reason.</li><li>THINK of a new, stronger way to say the same thing.</li><li>TYPE your restated reason in the blank space in the paragraph.</li></ul>",
								"<ul><li>READ your original reason.</li><li>THINK of a new, stronger way to say the same thing.</li><li>TYPE your restated reason in the blank space in the paragraph.</li></ul>",
								"<ul><li>READ your original reason.</li><li>THINK of a new, stronger way to say the same thing.</li><li>TYPE your restated reason in the blank space in the paragraph.</li></ul>"
			};
	String[] DIRECTIONS_AUDIO_URLS = {"Directions_Conclusion1_restate1.mp3","Directions_Conclusion2_restate1.mp3", "Directions_Conclusion3_restate1.mp3",
			  "",									"Directions_Conclusion2_restate2.mp3","Directions_Conclusion3_restate2.mp3",
			  "",									"Directions_Conclusion2_restate3.mp3","Directions_Conclusion3_restate3.mp3"};
	
	@UiField
	SpanElement paragraphSoFar, paragraphEnd;
	
	@UiField
	LabelButton submitButton, backButton;
	
	@UiField
	AudioControl directionsAudio;
	
	@UiField
	HTMLPanel entireParagraph;
	
	@UiField
	DivElement directionsLabel, directionsText;
	
	@UiField
	ParagraphElement claimText, reasonText;
	
	@UiField
	AudioConcatenatedControl claimAudio;
	
	private int sentenceIndex;
	private String finalSentence;
	
	public RestatedReasonFiller(ConclusionCrafter parent, int sentenceIndex) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		this.sentenceIndex = sentenceIndex;
		
		String paragraphText;
		paragraphText = getParentPanel().userData.getFirstSentence();
		if(sentenceIndex > 0)
			paragraphText += paragraphText = getParentPanel().userData.getSecondSentence();
		if(sentenceIndex > 1)
			paragraphText += paragraphText = getParentPanel().userData.getThirdSentence();
		if(sentenceIndex > 2)
			paragraphText += paragraphText = getParentPanel().userData.getFourthSentence();
		
		paragraphSoFar.setInnerHTML(paragraphText);
		submitButton.setEnabled(false);
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}};
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onBack();
			}
		};
			
		paragraphEnd.setInnerText(" ");
		paragraphEnd.getStyle().setDisplay(Display.INLINE_BLOCK);
		paragraphEnd.getStyle().setProperty("minWidth","10em");
		paragraphEnd.getStyle().setProperty("minHeight","1em");
		
		paragraphEnd.setAttribute("contentEditable", "true");
		paragraphEnd.setAttribute("contenteditable", "true");
		paragraphEnd.getStyle().setProperty("MozUserSelect", "text");
		paragraphEnd.getStyle().setProperty("KhtmlUserSelect", "text");
		paragraphEnd.getStyle().setProperty("WebkitUserSelect", "text");
		paragraphEnd.getStyle().setProperty("userSelect", "text");
		
		entireParagraph.addDomHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				checkComplete();
			}}, KeyUpEvent.getType());
		
		setDirectionsText(sentenceIndex, 2);
		
		Claim claim = DataController.getCurrentClaim();
		claimText.setInnerText(claim.text);
		reasonText.setInnerText(claim.reasons[sentenceIndex].text);
		
		claimAudio.loadSound(claim.audioSrc, "claim" + claim.id);
		claimAudio.loadSound(DataController.getIssueURL() + "because.mp3", "because");
		claimAudio.loadSound(claim.reasons[sentenceIndex].audioSrc , "reason" + claim.reasons[sentenceIndex].id);
	}
	
	protected void setDirectionsText(int sentenceIndex, int scaffoldIndex){
		
		if(scaffoldIndex == 2){
			directionsLabel.setInnerText(DIRECTIONS_LABELS[sentenceIndex]);
			directionsText.setInnerHTML(DIRECTIONS_TEXT[sentenceIndex]);
		}
		directionsAudio.setLocation(DIRECTIONS_AUDIO_URLS[sentenceIndex*3 + scaffoldIndex]);
	}
	
	protected void checkComplete(){
		submitButton.setEnabled(paragraphEnd.getInnerText().replaceAll("\\s", "").length() > 0);
	}
	
	protected void onSubmit(){
		submitButton.setEnabled(false);
		
		SafeHtmlBuilder sanitizer = new SafeHtmlBuilder();
		sanitizer.appendEscaped(" " + paragraphEnd.getInnerText());
		
		HTMLPanel sentenceInner = new HTMLPanel("span",sanitizer.toSafeHtml().asString());
		sentenceInner.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
		HTMLPanel sentence = new HTMLPanel("");
		sentence.add(sentenceInner);
		
		finalSentence = sentence.getElement().getInnerHTML();
		
		GWT.log("Sentence typed in is presumably this: " + finalSentence);
		
		getParentPanel().close(this, sentenceIndex, false);
	}
	
	protected void onBack(){
		getParentPanel().close(this, sentenceIndex, true);
	}
	
	public String getFinalSentence(){
		return finalSentence;
	}

}
