package org.icivics.arg.ui.intro.client;

import org.icivics.arg.ui.common.client.AudioControl;
import org.icivics.arg.ui.common.client.DropdownMenu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class CategorizeListItem extends Composite {
	private static CategorizeListItemUiBinder uiBinder = GWT.create(CategorizeListItemUiBinder.class);
	interface CategorizeListItemUiBinder extends UiBinder<Widget, CategorizeListItem> {}
	
	private static final String[] OPTION_NAMES = {"Background Info", "Statistic or Quote", "Question or Scenario"};
	
	@UiField
	DropdownMenu listField;
	@UiField
	ParagraphElement text;
	@UiField
	AudioControl itemAudio;
	
	public static interface Handler { void onChangeType(); }
	public Handler handler;
	
	public CategorizeListItem() {
		initWidget(uiBinder.createAndBindUi(this));
		for (int i = 0; i < OPTION_NAMES.length; ++i)
			listField.addItem(OPTION_NAMES[i]);
		
		listField.handler = new DropdownMenu.Handler() {
			@Override
			public void onSelect() {
				listField_onSelect();
			}
		};
	}
	
	private void listField_onSelect() {
		listField.handler = null;
		if (handler != null)
			handler.onChangeType();
	}
	
	public int getSelectedType() {
		return listField.getSelectedIndex();
	}
	
	public String getText() {
		return text.getInnerText();
	}
	public void setText(String v) {
		text.setInnerText(v);
	}
	public void setAudio(String url){
		itemAudio.loadSound(url);
	}
}
