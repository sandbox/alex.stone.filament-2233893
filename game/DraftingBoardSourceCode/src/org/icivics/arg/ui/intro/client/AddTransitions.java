package org.icivics.arg.ui.intro.client;

import java.util.Iterator;
import java.util.List;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.Glossary;
import org.icivics.arg.data.model.client.TransitionWord;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.GlossariedTextPanel;
import org.icivics.arg.ui.common.client.GlossaryPopupContent;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.paragraphBuilder.client.ParagraphBuilder;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

public class AddTransitions extends Subpanel<Introducer> {
	private static AddTransitionsUiBinder uiBinder = GWT.create(AddTransitionsUiBinder.class);
	interface AddTransitionsUiBinder extends UiBinder<Widget, AddTransitions> {}

	
	@UiField
	GlossariedTextPanel titleText;
	
	@UiField
	HTMLPanel paragraphText;
	@UiField
	HTMLPanel sideColumn;
	
	@UiField
	LabelButton submitButton, backButton;
	
	HTMLPanel[] mSentences, mTransitionSlots, mSlotButtons;
	private HandlerRegistration[] mSlotButtonHandlerRegs = new HandlerRegistration[4];
	private boolean[] mSufficientText = new boolean[4]; 
	
	private Element helpBoxLastTgt;
	private Timer mouseOverTimer;
	
	public AddTransitions(Introducer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		List<TransitionWord> transitions = DataController.getTransitions();
		Iterator<String> types = TransitionWord.types.keySet().iterator();
		while(types.hasNext()){
			final String t = types.next();
			HTMLPanel typePanel = new HTMLPanel(" ");
			HTMLPanel typeLabel = new HTMLPanel(t);
			final String helpText = TransitionWord.types.get(t);
			if(helpText != null){
				//This type has help text, add a mouse-over listener
				typeLabel.addDomHandler(new MouseOverHandler(){
					@Override
					public void onMouseOver(MouseOverEvent event) {
						mouseOverHelpfulText(t, helpText, event);
					}}, MouseOverEvent.getType());
				typeLabel.addDomHandler(new MouseOutHandler(){
					@Override
					public void onMouseOut(MouseOutEvent event) {
						mouseOutOfHelpfulText();
					}}, MouseOutEvent.getType());
				typeLabel.addDomHandler(new MouseOutHandler(){
					@Override
					public void onMouseOut(MouseOutEvent event) {
						mouseOutOfHelpfulText();
					}}, MouseOutEvent.getType());
				typeLabel.addStyleName("glossaryWord");
			}
			typePanel.add(typeLabel);
			sideColumn.add(typePanel);
			for(int i=0,count = transitions.size(); i<count; i++){
				if(transitions.get(i).getType().equals(t)){
					HTMLPanel transitionWord = new HTMLPanel("p",transitions.get(i).getWord());
					//TODO: add mouse-over listener
					final String word = transitions.get(i).getWord();
					final String helpTextWord = transitions.get(i).getDescription() + "\n" + transitions.get(i).getExamples();
					transitionWord.addStyleName("glossaryWord");
					transitionWord.addDomHandler(new MouseOverHandler(){
						@Override
						public void onMouseOver(MouseOverEvent event) {
							mouseOverHelpfulText(word, helpTextWord, event);
						}}, MouseOverEvent.getType());
					typePanel.add(transitionWord);
					transitionWord.addDomHandler(new ClickHandler(){

						@Override
						public void onClick(ClickEvent event) {
							Element tgt = Element.as(event.getNativeEvent().getEventTarget());
							GWT.log("Transition word clicked");
							displayHelpfulText(word, helpTextWord, tgt);
							
						}}, ClickEvent.getType());
					transitionWord.addDomHandler(new MouseOutHandler(){
						@Override
						public void onMouseOut(MouseOutEvent event) {
							mouseOutOfHelpfulText();
						}}, MouseOutEvent.getType());
				}
			}
		}
		
		
		//Build paragraph
		mSentences = new HTMLPanel[6];
		mTransitionSlots = new HTMLPanel[4];
		mSlotButtons = new HTMLPanel[4];
		for(int i = 0; i < 6; i++){
			mSentences[i] = new HTMLPanel("span",getSentenceText(i));
			if(i > 1){
				paragraphText.add(makeTransitionSlot(i-2));
				mSufficientText[i-2] = false;
			}
			paragraphText.add(mSentences[i]);
		}
		
	
		ElementUtil.setTextScaling();
	}
	
	private String getSentenceText(int index){
		return "<span class=\"" + DistinguishableTextBox.DRAFTINGBOARDCLASSNAME+ "\">" + getSentenceTextHelper(index) + "&nbsp;</span>";
	}
	
	private String getSentenceTextHelper(int index) {
		switch (index) {
			case 0: return DataController.getCurrentIssue().getSelectedIntro().hook;
			case 1: return DataController.getCurrentIssue().getSelectedIntro().transition;
			case 2: return DataController.getCurrentIssue().claim.reasons[0].reason.text;
			case 3: return DataController.getCurrentIssue().claim.reasons[1].reason.text;
			case 4: return DataController.getCurrentIssue().claim.reasons[2].reason.text;
			case 5: return DataController.getCurrentIssue().claim.claim.text;
			default:
				GWT.log("Invalid sentence index: " + index);
				return null;
		}
	}
	
	private HTMLPanel makeTransitionSlot(int i){
		HTMLPanel container = new HTMLPanel("span","&nbsp;");
		mTransitionSlots[i] = new HTMLPanel("span","&nbsp;");
		mTransitionSlots[i].getElement().setAttribute("contentEditable", "true");
		mTransitionSlots[i].getElement().getStyle().setProperty("MozUserSelect", "text");
		mTransitionSlots[i].getElement().getStyle().setProperty("KhtmlUserSelect", "text");
		mTransitionSlots[i].getElement().getStyle().setProperty("WebkitUserSelect", "text");
		mTransitionSlots[i].getElement().getStyle().setProperty("userSelect", "text");
		container.addStyleName("transition-slot");
		container.addStyleName(DistinguishableTextBox.STUDENTCLASSNAME);
		container.add(mTransitionSlots[i]);
		
		ElementUtil.addPasteHandler(mTransitionSlots[i].getElement().getParentNode());
		
		mSlotButtons[i] = new HTMLPanel(" ");
		mSlotButtons[i] .addStyleName("remove-btn");
		
		container.add(mSlotButtons[i]);
		return container;
	}
	
	protected void mouseOverHelpfulText(final String word, final String helpText, MouseOverEvent event) {
		final Element tgt = Element.as(event.getNativeEvent().getEventTarget());

		if(tgt != helpBoxLastTgt){
			if(helpBoxLastTgt != null ){
				//String txt = lastTarget.getInnerText();
				if(mouseOverTimer != null){
					mouseOverTimer.cancel();
					mouseOverTimer = null;
					hideHelpfulText();
				}
			}
			
			helpBoxLastTgt = tgt;
			
			mouseOverTimer = new Timer(){
				@Override
				public void run() {
					displayHelpfulText(word, helpText, tgt);
				}};
			mouseOverTimer.schedule(500);
		}
		
	}
	
	protected void mouseOutOfHelpfulText(){
		if(mouseOverTimer != null){
			mouseOverTimer.cancel();
			mouseOverTimer = null;
		}
		hideHelpfulText();
	}

	protected void hideHelpfulText(){
		Glossary.popup.hide();
	}
	
	protected void displayHelpfulText(final String word, final String def, final Element tgt){
		if(def !=null){
			
			GWT.log("HELP TEXT: " + word + " : " + def);
			
			GlossaryPopupContent popupContents = new GlossaryPopupContent(word,def);
			Glossary.popup.setWidget(popupContents);

			Glossary.popup.addDomHandler(new MouseMoveHandler(){
				@Override
				public void onMouseMove(MouseMoveEvent event) {
					mouseOutOfHelpfulText();
				}}, MouseMoveEvent.getType());
			
			Glossary.popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
				
				@Override
				public void setPosition(int offsetWidth, int offsetHeight) {
					int pos_x;
					int pos_y;
					
					int x = tgt.getAbsoluteLeft();
					int fallbacky = tgt.getAbsoluteTop();
					int fallbackx = tgt.getAbsoluteRight();
					int y = tgt.getAbsoluteBottom();
					
					if(y + offsetHeight > 705 ){
						pos_y = fallbacky - offsetHeight;
					}else{
						pos_y = y;
					}
					if(x + offsetWidth > 940 ){
						pos_x = fallbackx - offsetWidth;
					}else{
						pos_x = x;
					}
					
					Glossary.popup.setPopupPosition(pos_x,pos_y);
				}
			});
			ElementUtil.setTextScaling();
		} 
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		for(int i = 0; i < 4; i++){
			final int index = i;
			mSlotButtonHandlerRegs[i] = mSlotButtons[i].addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					removeButton_onClick(index);
				}}, ClickEvent.getType());
			
			mTransitionSlots[i].addDomHandler(new KeyUpHandler(){
				@Override
				public void onKeyUp(KeyUpEvent event) {
					//mTransitionSlots[index-1].getElement().setInnerHTML(mTransitionSlots[index-1].getElement().getInnerText());
					if(mTransitionSlots[index].getElement().getInnerText().replaceAll("\\W", "").length() > 0)
						mSufficientText[index] = true;
					else
						mSufficientText[index] = false;
					if(mTransitionSlots[index].getElement().getInnerText().length() == 0){
						mTransitionSlots[index].getElement().setInnerHTML("&nbsp;");
					}
					submitButton.setEnabled(checkComplete());
					updateScrollbars();
				}},KeyUpEvent.getType());
		}
		
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
		
		backButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				backButton_onClick();
			}
		};
		
		//getParentPanel().hud.init();
		
		addScrollbars();
		submitButton.setEnabled(checkComplete());
	}
	

	private boolean checkComplete() {
		
		for( int i = 0; i < mSufficientText.length; i++){
			if(!mSufficientText[i])
				return false;
		}
		
		return true;

	}
	
	protected void removeButton_onClick(final int index) {
		mTransitionSlots[index].getElement().setInnerText("");
		mTransitionSlots[index].getParent().removeStyleName("transition-slot");
		mTransitionSlots[index].getParent().addStyleName("transition-slot-collapsed");
		mSlotButtons[index].removeStyleName("remove-btn");
		mSlotButtons[index].addStyleName("add-btn");
		mSlotButtonHandlerRegs[index].removeHandler();
		
		mSufficientText[index] = true;
		submitButton.setEnabled(checkComplete());
		
		mSlotButtonHandlerRegs[index] = mSlotButtons[index].addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addButton_onClick(index);
			}}, ClickEvent.getType());
		
	}
	
	protected void addButton_onClick(final int index) {
		mTransitionSlots[index].getParent().removeStyleName("transition-slot-collapsed");
		mTransitionSlots[index].getParent().addStyleName("transition-slot");
		mSlotButtons[index].removeStyleName("add-btn");
		mSlotButtons[index].addStyleName("remove-btn");
		mSlotButtonHandlerRegs[index].removeHandler();
		
		mSufficientText[index] = false;
		submitButton.setEnabled(checkComplete());
		
		mSlotButtonHandlerRegs[index] = mSlotButtons[index].addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				removeButton_onClick(index);
			}}, ClickEvent.getType());
	}
	
	private String getModifiedText(int index) {
		String base = getSentenceText(index);
		
		GWT.log(base);
		
		if (index < 2) {
			return base;
		} else {
			String prefix = mTransitionSlots[index-2].getElement().getInnerText();
			prefix = new SafeHtmlBuilder().appendEscaped(prefix).toSafeHtml().asString();
			
			if (prefix.replaceAll("\\W", "").length() == 0) {
				return base;
			} else {
				RegExp regex = RegExp.compile("[^a-zA-Z]+$");
				prefix = regex.replace(prefix, "");
				
				regex = RegExp.compile("^[^a-zA-Z]+");
				prefix = regex.replace(prefix, "");
				
				prefix = "<span class=\"" + DistinguishableTextBox.STUDENTCLASSNAME +"\">" + 
						prefix +
						", </span>";
				
				HTMLPanel tempPanel = new HTMLPanel(base);
				base = tempPanel.getElement().getFirstChildElement().getInnerText();

				base = Character.toLowerCase(base.charAt(0)) + base.substring(1);

				tempPanel.getElement().getFirstChildElement().setInnerText(base);
				
				base = tempPanel.getElement().getInnerHTML();

				return prefix + base;
			}
		}
	}
	
	private void submitButton_onClick() {
		submitButton.clickHandler = null;
		backButton.clickHandler = null;

		String text = "";
		for(int i = 0; i < 6; i++){
			text += getModifiedText(i);
		}
		
		DataController.getCurrentIssue().claim.introParagraphOriginal = text;
		
		getParentPanel().close(this, false);
		
	}
	
	private void backButton_onClick(){
		submitButton.clickHandler = null;
		backButton.clickHandler = null;
		
		getParentPanel().close(this, true);
	}
	
	private native void addScrollbars() /*-{
	$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar({
			wheelSpeed: 30,
			wheelPropagation: true,
			suppressScrollX: true,
			useKeyboard:false
		});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.paragraph-constructor .panel-container .paragraph-panel').perfectScrollbar('update');
	}-*/;
}
