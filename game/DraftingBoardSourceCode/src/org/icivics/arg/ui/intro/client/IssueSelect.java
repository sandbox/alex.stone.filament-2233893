package org.icivics.arg.ui.intro.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.IntroSentence;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class IssueSelect extends Subpanel<Introducer> {
	private static IssueSelectUiBinder uiBinder = GWT.create(IssueSelectUiBinder.class);
	interface IssueSelectUiBinder extends UiBinder<Widget, IssueSelect> {}
	
	@UiField
	DivElement optionList;
	@UiField
	LabelButton submitButton;
	
	private int mSelectedIndex = -1;
	private IssueSelectListItem[] mListItems;
	private HandlerRegistration[] mToggleClickRegs;
	
	public IssueSelect(Introducer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		IntroSentence[] sentences = DataController.getCurrentIssue().claim.claim.introSentences;
		mListItems = new IssueSelectListItem[sentences.length];
		for (int i = 0; i < sentences.length; ++i) {
			mListItems[i] = new IssueSelectListItem();
			mListItems[i].setHookText(sentences[i].hook);
			mListItems[i].setTransitionText(sentences[i].transition);
			mListItems[i].button.addStyleName("off");
			getRoot().add(mListItems[i], optionList);
		}
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		mToggleClickRegs = new HandlerRegistration[mListItems.length];
		for (int i = 0; i < mListItems.length; ++i) {
			final int buttonIndex = i;
			mToggleClickRegs[i] = mListItems[i].button.addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toggleButton_onClick(buttonIndex);
				}
			}, ClickEvent.getType());
		}
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}

	@Override
	protected void onDetach() {
		if (mToggleClickRegs != null) {
			for (int i = 0; i < mToggleClickRegs.length; ++i)
				mToggleClickRegs[i].removeHandler();
			mToggleClickRegs = null;
		}
		submitButton.destroy();
		super.onDetach();
	}
	
	private void toggleButton_onClick(int buttonIndex) {
		for (int i = 0; i < mListItems.length; ++i) {
			mListItems[i].button.removeStyleName("on");
			mListItems[i].button.removeStyleName("off");
			mListItems[i].button.addStyleName(i == buttonIndex ? "on" : "off");
		}
		mSelectedIndex = buttonIndex;
		submitButton.setEnabled(true);
	}

	private void submitButton_onClick() {
		if (mToggleClickRegs != null) {
			for (int i = 0; i < mToggleClickRegs.length; ++i)
				mToggleClickRegs[i].removeHandler();
			mToggleClickRegs = null;
		}
		submitButton.destroy();
		
		DataController.getCurrentIssue().selectedIntroIndex = mSelectedIndex;
		GWT.log("Issue Select: selected intro index " + mSelectedIndex);
		getParentPanel().close(this);
	}
}
