package org.icivics.arg.ui.intro.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class IssueSelectListItem extends Composite {
	private static IssueSelectListItemUiBinder uiBinder = GWT.create(IssueSelectListItemUiBinder.class);
	interface IssueSelectListItemUiBinder extends UiBinder<Widget, IssueSelectListItem> {}
	
	@UiField
	public HTMLPanel button;
	@UiField
	ParagraphElement hookText;
	@UiField
	ParagraphElement transitionText;
	
	public IssueSelectListItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public String getHookText() {
		return hookText.getInnerText();
	}
	public void setHookText(String v) {
		hookText.setInnerText(v);
	}

	public String getTransitionText() {
		return transitionText.getInnerText();
	}
	public void setTransitionText(String v) {
		transitionText.setInnerText(v);
	}
}
