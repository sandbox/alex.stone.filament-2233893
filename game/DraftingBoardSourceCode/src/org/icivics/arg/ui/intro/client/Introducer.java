package org.icivics.arg.ui.intro.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.json.client.IntroducerProgress;
import org.icivics.arg.data.model.client.Item;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class Introducer extends ModuleUI<Introducer> {
	protected int numCategorizeFailures = 0;
	protected IntroducerProgress userData;
	
	EvidenceSidebar mSidebar;
	
	public Introducer(EvidenceSidebar sidebar) {
		mSidebar = sidebar;
	}
	
	@Override
	protected void commitScore() {
		DataController.getCurrentIssue().scoreIntroducer = 1.0 / (1.0 + numCategorizeFailures);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		GWT.log("Attaching introducer, haven't crashed yet");
		userData = DataController.getModuleProgress().forIntroducer();
		GWT.log("Loaded some user data for the introducer");
		openUI(getSavedUI());
	}
	
	private Subpanel<Introducer> getSavedUI() {
		GWT.log("Introducer is loading a saved UI");
		int stepIndex = userData.getStepIndex();
		GWT.log("Introducer has found step index: " + stepIndex);
		DataController.moduleStepIndex = stepIndex;
		switch (stepIndex) {
			case 0: return new Instructions(this);
			case 1: return new IssueSelect(this);
			case 2: return new AddTransitions(this);
			case 3: return new ParagraphEdit(this);
			case 4: return null;
			default:
				DataController.moduleStepIndex = 0;
				return new Instructions(this);
		}
	}
	
	void close(Instructions ui) {
		transitionTo(new Categorize(this));
	}
	
	void close(Categorize ui) {
		userData.setCategorizeFailures(numCategorizeFailures);
		
		DataController.saveModuleProgress(1);
		transitionTo(new IssueSelect(this));
	}
	
	void close(IssueSelect ui) {
		userData.setSelectedIntroIndex(DataController.getCurrentIssue().selectedIntroIndex);
		
		DataController.saveModuleProgress(2);
		transitionTo(new AddTransitions(this));
	}
	
	
	void close(AddTransitions ui, boolean goingBack){
		
		if(goingBack){
			DataController.saveModuleProgress(1);
			transitionTo(new IssueSelect(this));
		}else{
			userData.setOriginalParagraph(DataController.getCurrentIssue().claim.introParagraphOriginal);
			Item[] items = new Item[6];
			for(int i =0; i < 3; i++){
				items[i*2] = DataController.getCurrentIssue().claim.reasons[i].reason.items[0];
				items[i*2 + 1] = DataController.getCurrentIssue().claim.reasons[i].reason.items[1];
			}
			
			sidebar.init(items);
			
			DataController.saveModuleProgress(3);
			transitionTo(new ParagraphEdit(this));
		}
	}
	
	void close(Evaluation ui){
		userData.setFinalParagraph(DataController.getCurrentIssue().claim.introParagraph);
		
		DataController.saveModuleProgress(4);
		finish();
	}
	
	void close(ParagraphEdit ui) {
		userData.setFinalParagraph(DataController.getCurrentIssue().claim.introParagraph);
		
		DataController.saveModuleProgress(4);
		finish();
		
	}
}
