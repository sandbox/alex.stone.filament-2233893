package org.icivics.arg.ui.intro.client;

import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.TextPopup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class Instructions extends Subpanel<Introducer> {
	private static InstructionsUiBinder uiBinder = GWT.create(InstructionsUiBinder.class);
	interface InstructionsUiBinder extends UiBinder<Widget, Instructions> {}
	
	private TextPopup mPopup;
	
	public Instructions(Introducer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		mPopup = new TextPopup();
		mPopup.setTitle("Building an Intro Paragraph");
		mPopup.setBody("<p>A great introductory paragraph does three things:</p> <br> <ol><li> Grabs the reader's attention with a \"hook\" to get them interested in your topic.</li><li>States the claim you are making.</li><li>Summarizes the reasons that support your claim.</li></ol> <br><p>The Introduction Introducer will help you build the three pieces of this paragraph. Ready to get started?</p>");
		mPopup.setDimmerVisible(false);
		mPopup.setHandler(new TextPopup.PopupHandler() {
			@Override
			public void onClose(TextPopup popup) {
				popup_onClose();
			}
		});
		getRoot().add(mPopup);
	}
	
	@Override
	protected void onDetach() {
		if (mPopup != null) {
			mPopup.close();
			mPopup = null;
		}
		super.onDetach();
	}
	
	private void popup_onClose() {
		mPopup.clearHandler();
		mPopup = null;
		getParentPanel().close(this);
	}
}
