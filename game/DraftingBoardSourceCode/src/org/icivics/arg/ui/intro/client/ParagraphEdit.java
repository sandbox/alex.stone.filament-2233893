package org.icivics.arg.ui.intro.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.TextAreaElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ParagraphEdit extends Subpanel<Introducer> {
	private static ParagraphEditUiBinder uiBinder = GWT.create(ParagraphEditUiBinder.class);
	interface ParagraphEditUiBinder extends UiBinder<Widget, ParagraphEdit> {}
	
	@UiField
	DistinguishableTextBox paragraphField;
	
	@UiField
	LabelButton submitButton;
	
	/*
	private HTMLPanel[] toggles = new HTMLPanel[5];
	private boolean[] toggleStates = {false, false, false, false, false};
	private HandlerRegistration[] toggleRegs;
	*/
	
	public ParagraphEdit(Introducer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		paragraphField.setInnerText(DataController.getCurrentIssue().claim.introParagraphOriginal);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		/*
		toggleRegs = new HandlerRegistration[toggles.length];
		for (int i = 0; i < toggles.length; ++i) {
			final int buttonIndex = i;
			toggleRegs[i] = toggles[i].addDomHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toggle_onClick(buttonIndex);
				}
			}, ClickEvent.getType());
		}
		*/
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		clearListeners();
		submitButton.destroy();
		super.onDetach();
	}
	
	private void clearListeners() {
		/*
		if (toggleRegs != null) {
			for (int i = 0; i < toggleRegs.length; ++i)
				toggleRegs[i].removeHandler();
			toggleRegs = null;
		}
		*/
		submitButton.clickHandler = null;
	}

	private boolean checkComplete() {
		/*
		for (int i = 0; i < toggleStates.length; ++i)
			if (!toggleStates[i])
				return false;
			*/
		return true;
	}

	/*
	private void toggle_onClick(int buttonIndex) {
		if (toggleStates[buttonIndex] = !toggleStates[buttonIndex]) {
			toggles[buttonIndex].addStyleName("checked");
			submitButton.setEnabled(checkComplete());
		} else {
			toggles[buttonIndex].removeStyleName("checked");
			submitButton.setEnabled(false);
		}
	}
	*/
	
	private void submitButton_onClick() {
		GWT.log("Submit clicked");
		clearListeners();
		
		DataController.getCurrentIssue().claim.introParagraph = paragraphField.getValue();
		getParentPanel().close(this);
	}
}
