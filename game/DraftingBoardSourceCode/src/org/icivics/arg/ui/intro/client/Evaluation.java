package org.icivics.arg.ui.intro.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.paragraphBuilder.client.ChecklistQuestion;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class Evaluation extends Subpanel<Introducer> {
	private static EvaluationUiBinder uiBinder = GWT.create(EvaluationUiBinder.class);
	interface EvaluationUiBinder extends UiBinder<Widget, Evaluation> {}

	@UiField
	DistinguishableTextBox paragraphText;
	
	@UiField
	HTMLPanel sideColumn;
	
	@UiField
	LabelButton submitButton;
	
	ChecklistQuestion[] mQuestions;
	
	public Evaluation(Introducer parent) {
		super(parent);
		initWidget(uiBinder.createAndBindUi(this));
		
		paragraphText.setInnerText(DataController.getCurrentIssue().claim.introParagraphOriginal);
		
		mQuestions = new ChecklistQuestion[3];
		for(int i=0; i <3; i++){
			sideColumn.add(mQuestions[i] = new ChecklistQuestion(0, i));
		}
	}

	
	@Override
	protected void onAttach() {
		super.onAttach();
		
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};	
		
		for(int i=0; i<3; i++)
			mQuestions[i].addChangeHandler(new ChecklistQuestion.ChangeHandler(){
				@Override
				public void onChange(){
					submitButton.setEnabled(checkComplete());
				}
			});
		ElementUtil.setTextScaling();
	}
	
	private boolean checkComplete() {
		for (int i = 0; i < mQuestions.length; ++i)
			if (mQuestions[i].getSelectedOption() == -1)
				return false;
		return true;
	}
	
	private void submitButton_onClick() {
		DataController.getCurrentIssue().claim.introParagraph = paragraphText.getValue();
		getParentPanel().close(this);
	}
}
