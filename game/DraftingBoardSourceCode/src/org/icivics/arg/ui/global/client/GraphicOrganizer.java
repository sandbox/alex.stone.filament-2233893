package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class GraphicOrganizer extends UIPanel {
	private static GraphicOrganizerUiBinder uiBinder = GWT.create(GraphicOrganizerUiBinder.class);
	interface GraphicOrganizerUiBinder extends UiBinder<Widget, GraphicOrganizer> {}

	@UiField
	HTMLPanel imagePanel;
	
	@UiField 
	LabelButton continueButton;
	
	public GraphicOrganizer() {
		initWidget(uiBinder.createAndBindUi(this));
		imagePanel.getElement().getStyle().setCursor(Cursor.POINTER);
	}

}
