package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class Loading extends UIPanel {
	private static LoadingUiBinder uiBinder = GWT.create(LoadingUiBinder.class);
	interface LoadingUiBinder extends UiBinder<Widget, Loading> {}
	
	public Loading() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
