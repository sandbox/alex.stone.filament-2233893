package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.BasicButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class ModuleObjectivesPanel extends UIPanel {
	private static ModuleObjectivesPanelUiBinder uiBinder = GWT.create(ModuleObjectivesPanelUiBinder.class);
	interface ModuleObjectivesPanelUiBinder extends UiBinder<Widget, ModuleObjectivesPanel> {}
	
	@UiField
	DivElement titleHolder;
	@UiField
	ParagraphElement titleText;
	@UiField
	ParagraphElement infoText;
	
	@UiField
	BasicButton closeButton;
	@UiField
	DivElement objectiveList;
	
	public ModuleObjectivesPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onDetach() {
		closeButton.destroy();
		super.onDetach();
	}
}
