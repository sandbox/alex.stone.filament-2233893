package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ScaffoldingSelection extends UIPanel {
	private static ScaffoldingSelectionUiBinder uiBinder = GWT.create(ScaffoldingSelectionUiBinder.class);
	interface ScaffoldingSelectionUiBinder extends UiBinder<Widget, ScaffoldingSelection> {}

	public static interface StartHandler { void onComplete(); }
	public StartHandler startHandler;
	
	@UiField 
	LabelButton submitButton;
	
	@UiField
	HTMLPanel option1, option2, option3, option4, option5, option6;
	
	HTMLPanel[] options;
	
	public int selectedLevel = 0;
	
	public ScaffoldingSelection() {
		initWidget(uiBinder.createAndBindUi(this));
		
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}
		};
		
		options = new HTMLPanel[6];
		options[0] = option1;
		options[1] = option2;
		options[2] = option3;
		options[3] = option4;
		options[4] = option5;
		options[5] = option6;
		
		for(int i =0; i < 6; i++){
			final int index = i;
			options[i].addDomHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					optionSelect(index);
				}
				
			},ClickEvent.getType());
		}
	}

	@Override
	protected void onAttach() {
		super.onAttach();
		
		ElementUtil.setTextScaling();
	}
	
	private void optionSelect(int index){
		for(int i =0; i < options.length; i++){
			options[i].removeStyleName("selected");
		}
		
		options[index].addStyleName("selected");
		
		this.selectedLevel = index;
	};
	
	private void onSubmit(){
		removeFromParent();
		if(startHandler != null){
			startHandler.onComplete();
			startHandler = null;
		}
	}
}
