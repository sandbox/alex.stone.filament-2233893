package org.icivics.arg.ui.global.client;

import java.util.List;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.BasicButton;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class TransitionsGuidePopup extends UIPanel implements NavAccessiblePanel {
	private static final int ITEMS_PER_PAGE = 10;
	
	private static TransitionsGuidePopupUiBinder uiBinder = GWT.create(TransitionsGuidePopupUiBinder.class);
	interface TransitionsGuidePopupUiBinder extends UiBinder<Widget, TransitionsGuidePopup> {}
	
	@UiField
	protected DivElement transitionList;
	
	@UiField
	protected LabelButton prevButton;
	@UiField
	protected LabelButton nextButton;
	
	@UiField
	public BasicButton closeButton;
	
	private int mCurrentPage;
	private ParagraphElement[] mTransitionItems;
	private CloseHandler mCloseHandler;
	
	public TransitionsGuidePopup() {
		initWidget(uiBinder.createAndBindUi(this));
		mTransitionItems = new ParagraphElement[DataController.getTransitions().size()];
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		prevButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				prevButton_onClick();
			}
		};
		nextButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				nextButton_onClick();
			}
		};
		closeButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}
		};
		showPage(0);
	}
	
	@Override
	protected void onDetach() {
		prevButton.destroy();
		nextButton.destroy();
		closeButton.destroy();
		super.onDetach();
	}
	
	private int getLastPage() {
		return (DataController.getTransitions().size() - 1) / ITEMS_PER_PAGE;
	}
	
	private void showPage(int index) {
		mCurrentPage = index;
		showPage();
	}
	
	private void showPage() {/*
		List<String> transitions = DataController.getTransitions();
		int offset = mCurrentPage * ITEMS_PER_PAGE;
		int count = transitions.size();
		for (int i = 0; i < count; ++i) {
			if (mTransitionItems[i] == null) {
				mTransitionItems[i] = Document.get().createPElement();
				transitionList.appendChild(mTransitionItems[i]);
			}
			mTransitionItems[i].setInnerText(transitions.get(i + offset));
		}
		for (int i = count; i < mTransitionItems.length; ++i) {
			if (mTransitionItems[i] == null)
				break;
			mTransitionItems[i].removeFromParent();
			mTransitionItems[i] = null;
		}
		
		prevButton.setEnabled(mCurrentPage > 0);
		nextButton.setEnabled(mCurrentPage < getLastPage());
		*/
	}
	
	private void prevButton_onClick() {
		if (mCurrentPage == 0)
			return;
		
		--mCurrentPage;
		showPage();
	}
	
	private void nextButton_onClick() {
		if (mCurrentPage == getLastPage())
			return;
		
		++mCurrentPage;
		showPage();
	}
	
	private void closeButton_onClick() {
		close();
	}
	
	private void close() {
		removeFromParent();
		if (mCloseHandler != null) {
			mCloseHandler.onClose(this);
			mCloseHandler = null;
		}
	}

	@Override
	public void setCloseHandler(CloseHandler handler) {
		mCloseHandler = handler;
	}
}
