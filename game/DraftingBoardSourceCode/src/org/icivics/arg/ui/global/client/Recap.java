package org.icivics.arg.ui.global.client;

import java.util.ArrayList;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.ui.common.client.ConfirmPopup;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class Recap extends UIPanel implements NavAccessiblePanel {
	private static final String[] GRAPHIC_ORGANIZER_URLS = {
		"./images/go-issue-analyzer.png",
		"./images/go-claim-creator.png",
		"./images/go-paragraph-constructor.png",
		"./images/go-paragraph-constructor.png",
		"./images/go-paragraph-constructor.png",
		"./images/go-critic-crusher.png",
		"./images/go-introduction-introducer.png",
		"./images/go-conclusion-crafter.png",
		"./images/go-issue-analyzerBROKEN.png",
		"./images/go-issue-analyzerBROKEN.png",
		"./images/go-issue-analyzerBROKEN.png",
		"./images/go-issue-analyzerBROKEN.png"
	};
	
	private static final String[][] MODULE_OBJECTIVES = {
		{"Read the article", "Complete the story chunks", "Choose a side"},//IssueAnalyzer
		{"Vote Results", "Pin your reasons", "Justify", "Support your side"},//ClaimCreator
		{"Arrange the sentences", "Complete sentences with evidence", "Place transitions", "Edit your paragraph"},//ParagraphBuilder1
		{"Arrange the sentences", "Complete sentences with evidence", "Place transitions", "Edit your paragraph"},//ParagraphBuilder2
		{"Write your own paragraph"},//ParagraphBuilder3
		{ "Pin the other side","Paragraph 1: Acknowledge the other side's argument", "Paragraph 1: Use your evidence to weaken their argument", "Paragraph 1: Support your side with more evidence", "Paragraph 1: Restate your claim to make your point",
		 "Paragraph 2: Acknowledge the other side's argument", "Paragraph 2: Use your evidence to weaken their argument", "Paragraph 2: Support your side with more evidence", "Paragraph 2: Restate your claim to make your point",
		 "Choose your best paragraph", "Place transitions", "Edit and check"},//CriticCrusher
		{"Sort the hook sentences", /*"Match hooks with bridges",*/ "Choose a hook", "Place transitions", "Edit your paragraph"},//Introducer
		{"Summarize your side of the issue", "Reword your side's reason 1", "Reword your side's reason 2", "Reword your side's reason 3", "Sort the sentences", "Choose a sentence", "Edit and check"}//Conclusion
	};
	static final String[] MODULE_TITLES = {
		"Issue Analyzer",
		"Claim Creator",
		"Paragraph Constructor 1",
		"Paragraph Constructor 2",
		"Paragraph Constructor 3",
		"Critic Crusher",
		"Introduction Introducer",
		"Conclusion Crafter"
	};
	private static final String[] MODULE_INFO_INTRO = {
		"Welcome to Drafting Board! In the Issue Analyzer, you will investigate both sides of the issue. First, you must sort through your evidence to complete the missing sections of the story. When you're done, you will understand both sides of the issue and have a chance to pick your side!",
		"Welcome to the Claim Creator! This tool helps you outline the reasons behind your claim. These reasons will help you draft the three main paragraphs of your essay, so they're really important. You will also have to back up your reasons with evidence, so read closely.",
		"Welcome to the Paragraph Constructor! This tool helps you write the three body paragraphs of your essay. For the 1st body paragraph, make sure to support your reason with evidence. Here are your goals for body paragraph #1:",
		"Now it's time to create your 2nd body paragraph. For your 2nd paragraph, make sure to support your reason with evidence. This time, use your own language to complete the evidence sentences. Here are your goals for body paragraph #2:",
		"Now it's time to create your final body paragraph. For your 3rd body paragraph, make sure to support your reason with evidence. This time, you're ready to write the whole paragraph yourself. If you get stuck, use the blue evidence buttons for help. Here are your goals for body paragraph #3:",
		"Welcome to the Critic Crusher! This tool helps you look at the other side's argument. You can see your opponent's reasoning and use your evidence to shoot it down. The first exercise will guide you through how to write a rebuttal paragraph, and the second exercise will let you try it yourself. At the end, you will pick which paragraph you think is best! Here are your goals for both Critic Crusher paragraphs:",
		"Welcome to the Introduction Introducer! This tool provides strategies for creating a strong opening paragraph that will grab your reader's attention. Remember, you only have one chance to make a first impression. Here are your goals for the Introduction Introducer:",
		"Welcome to the Conclusion Crafter! This tool gives you strategies for creating a strong closing paragraph that summarizes your argument. Here are your goals for the Conclusion Crafter:"
	};
	private static final String[] MODULE_INFO_OBJECTIVES = {
		"In the Issue Analyzer, you will investigate both sides of the issue. First, you must sort through your evidence to complete the missing sections of the story. When you're done, you will understand both sides of the issue and have a chance to pick your side!",
		"The Claim Creator helps you outline the reasons behind your claim. These reasons will help you draft the three main paragraphs of your essay, so they're really important. You will also have to back up your reasons with evidence, so read closely.",
		"The Paragraph Constructor helps you write the three body paragraphs of your essay. For the 1st body paragraph, make sure to support your reason with evidence. Here are your goals for body paragraph #1:",
		"The Paragraph Constructor helps you write the three body paragraphs of your essay. For the 2nd body paragraph, make sure to support your reason with evidence � using your own language. Here are your goals for body paragraph #2:",
		"The Paragraph Constructor helps you write the three body paragraphs of your essay. For the 3rd body paragraph, make sure to support your reason with evidence. If you get stuck, use the blue evidence buttons for help. Here are your goals for body paragraph #3:",
		"The Critic Crusher helps you look at the other side's argument. You can see your opponent's reasoning and use your evidence to shoot it down. The first exercise will guide you through how to write a rebuttal paragraph, and the second exercise will let you try it yourself. At the end, you will pick which paragraph you think is best! Here are your goals for both Critic Crusher paragraphs:",
		"The Introduction Introducer provides strategies for creating a strong opening paragraph that will grab your reader's attention. Remember, you only have one chance to make a first impression. Here are your goals for the Introduction Introducer:",
		"The Conclusion Crafter gives you strategies for creating a strong closing paragraph that summarizes your argument. Here are your goals for the Conclusion Crafter:"
	};
	private static final String[] MODULE_INFO_RECAP = {
		"Nice work! You have successfully completed the Issue Analyzer and picked your side of the argument. Here's the side you chose:",
		"Well done! You have successfully completed the Claim Creator and outlined your argument. You've confirmed your claim and selected the best supporting reasons. Here's what you decided:",
		"Good job! You completed your 1st body paragraph using the Paragraph Constructor. Here it is for your review:",
		"Great job! You completed your 2nd body paragraph using the Paragraph Constructor. Here it is for your review:",
		"Great job! You completed your last body paragraph using the Paragraph Constructor. Here it is for your review:",
		"Excellent! You have successfully completed the Critic Crusher and addressed the other side's argument. By recognizing the other side's reason and knocking it down with evidence, you've strengthened your essay. Here is your Critic Crusher paragraph:",
		"Nice work! You have successfully completed the Introduction Introducer and grabbed your reader's attention. You've also laid out the structure of your argument by stating your claim and supporting reasons. Here's your introduction paragraph:",
		"Nice work! You have successfully completed the Conclusion Crafter and finished your essay in a strong way. Here's your final paragraph:"
	};
	
	private static RecapUiBinder uiBinder = GWT.create(RecapUiBinder.class);
	interface RecapUiBinder extends UiBinder<Widget, Recap> {}
	
	@UiField
	DivElement contentHolder;
	
	@UiField
	DivElement progressDisplay;
	
	@UiField
	HTMLPanel mainPanel;
	
	public static interface ModuleRecapHandler {
		void onRedo();
		void onContinue();
	}
	public ModuleRecapHandler moduleRecapHandler;
	
	private CloseHandler mCloseHandler;
	private UIPanel mDetailPanel;
	private ConfirmPopup mConfirmPopup;
	
	protected Recap() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public Recap(GraphicOrganizer goPanel){
		this();
		
		contentHolder.setInnerHTML("");
		
		//TODO: Graphic organizer image setting thing
		//<img src="../images/go-claim-creator.png" alt=""/>
		int moduleIndex = DataController.moduleIndex;
		goPanel.imagePanel.getElement().setInnerHTML("<img src=\"" + GRAPHIC_ORGANIZER_URLS[moduleIndex] + "\" alt=\"\"/>");
		
		goPanel.imagePanel.addDomHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}}, ClickEvent.getType());
		
		goPanel.continueButton.clickHandler = new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}};
		
		mDetailPanel = goPanel;
		mainPanel.getElement().setInnerHTML("");
		mainPanel.add(mDetailPanel);
	}
	
	public Recap(ModuleIntroPanel detailPanel) {
		this();
		
		
		int moduleIndex = DataController.moduleIndex;
		String[] objectives = MODULE_OBJECTIVES[moduleIndex];
		
		detailPanel.titleHolder.addClassName("step" + moduleIndex);
		detailPanel.titleText.setInnerText(MODULE_TITLES[moduleIndex]);
		detailPanel.infoText.setInnerText(MODULE_INFO_INTRO[moduleIndex]);
		for (int i = 0; i < objectives.length; ++i) {
			ParagraphElement elem = Document.get().createPElement();
			elem.setInnerText(objectives[i]);
			detailPanel.objectiveList.appendChild(elem);
		}
		
		detailPanel.closeButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}
		};
		
		mDetailPanel = detailPanel;
		getRoot().add(mDetailPanel, contentHolder);
		
		progressDisplay.addClassName("step" + moduleIndex);
	}
	
	public Recap(ModuleObjectivesPanel detailPanel) {
		this();
		int moduleIndex = DataController.moduleIndex;
		int stepIndex = DataController.moduleStepIndex;
		String[] objectives = MODULE_OBJECTIVES[moduleIndex];
		
		detailPanel.titleHolder.addClassName("step" + moduleIndex);
		detailPanel.titleText.setInnerText(MODULE_TITLES[moduleIndex]);
		detailPanel.infoText.setInnerText(MODULE_INFO_OBJECTIVES[moduleIndex]);
		for (int i = 0; i < objectives.length; ++i) {
			ParagraphElement elem = Document.get().createPElement();
			elem.setInnerText(objectives[i]);
			if (i < stepIndex)
				elem.addClassName("complete");
			detailPanel.objectiveList.appendChild(elem);
		}
		
		detailPanel.closeButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				closeButton_onClick();
			}
		};
		
		mDetailPanel = detailPanel;
		getRoot().add(mDetailPanel, contentHolder);
		
		progressDisplay.addClassName("step" + moduleIndex);
	}
	
	public Recap(ModuleRecapPanel detailPanel) {
		this();
		int moduleIndex = DataController.moduleIndex;
		
		if(moduleIndex == 1){
			ClaimInstance claim = DataController.getCurrentIssue().claim;
			
			ArrayList<String> urls = new ArrayList<String>();
			urls.add("/audio/youroutline.wav");
			urls.add("/audio/claim"+claim.claim.id + ".wav");
			urls.add("/audio/because.wav");
			urls.add("/audio/one.wav");
			urls.add("/audio/loremipsum.wav");
			urls.add("/audio/two.wav");
			urls.add("/audio/loremipsum.wav");
			urls.add("/audio/three.wav");
			urls.add("/audio/loremipsum.wav");
			
			detailPanel.setAudio(urls,"outline");
		} else {
			detailPanel.setAudio(null,null);
		}
		
		detailPanel.titleHolder.addClassName("step" + moduleIndex);
		detailPanel.titleText.setInnerText(MODULE_TITLES[moduleIndex]);
		detailPanel.infoText.setInnerText(MODULE_INFO_RECAP[moduleIndex]);
		detailPanel.bodyText.setInnerHTML(getModuleRecapText(moduleIndex));
		
		detailPanel.redoButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				redoButton_onClick();
			}
		};
		detailPanel.continueButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				continueButton_onClick();
			}
		};
		
		mDetailPanel = detailPanel;
		getRoot().add(mDetailPanel, contentHolder);
		
		progressDisplay.addClassName("step" + (moduleIndex + 1));
	}
	
	public Recap(FinalScreen detailPanel) {
		this();
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		/*
		appendParagraphElement(detailPanel.essayHolder, claim.introParagraph);
		for (int i = 0; i < claim.paragraphs.length; ++i)
			appendParagraphElement(detailPanel.essayHolder, claim.paragraphs[i]);
		appendParagraphElement(detailPanel.essayHolder, claim.rebuttalParagraph);
		appendParagraphElement(detailPanel.essayHolder, claim.conclusionParagraph);
		*/
		detailPanel.printButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				printButton_onClick();
			}
		};
		detailPanel.restartButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				restartButton_onClick();
			}
		};
		
		mDetailPanel = detailPanel;
		
		mainPanel.getElement().setInnerHTML("");
		mainPanel.add(mDetailPanel);
		//getRoot().add(mDetailPanel, contentHolder);
		
		progressDisplay.addClassName("step8");
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
	}
	
	@Override
	protected void onDetach() {
		if (mConfirmPopup != null) {
			mConfirmPopup.close();
			mConfirmPopup = null;
		}
		if (mDetailPanel != null) {
			mDetailPanel.removeFromParent();
			mDetailPanel = null;
		}
		super.onDetach();
	}
	
	private String getModuleRecapText(int moduleIndex) {
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		switch(moduleIndex) {
			case 0:
				return claim.getClaimText();
			case 1:
				//TODO: Improve formatting
				String str = "<center><b>Your Outline</b></center><br/>" + claim.getClaimText() + "<br/>BECAUSE:";
				for (int i = 0; i < claim.reasons.length; ++i){
					str += "<br/><b>" + (i + 1) + ") " + claim.reasons[i].reason.text + "</b>";
					for(int j = 0; j < claim.reasons[i].reason.items.length; ++j)
					str += "<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b>Evidence:</b> " + claim.reasons[i].reason.items[j].summary;
				}
				return str;
			case 2:
				return claim.paragraphs[0];
			case 3:
				return claim.paragraphs[1];
			case 4:
				return claim.paragraphs[2];
			case 5:
				return claim.rebuttalParagraph;
			case 6:
				return claim.introParagraph;
			case 7:
				return claim.conclusionParagraph;
			default:
				GWT.log("Invalid index for module recap " + moduleIndex);
				return null;
		}
	}
	
	private void appendParagraphElement(DivElement holder, String text) {
		ParagraphElement elem = Document.get().createPElement();
		elem.setInnerText(text);
		holder.appendChild(elem);
	}
	
	private void redoButton_onClick() {
		mConfirmPopup = new ConfirmPopup("Redo Step");
		mConfirmPopup.setTitle("Are you Sure?");
		mConfirmPopup.setBody("If you redo this step, you will overwrite your previous work for the step.");
		mConfirmPopup.setHandler(new ConfirmPopup.Handler() {
			@Override
			public void onConfirm(ConfirmPopup popup) {
				redoConfirmPopup_onConfirm();
			}
			
			@Override
			public void onCancel(ConfirmPopup popup) {
				confirmPopup_onCancel();
			}
		});
		getRoot().add(mConfirmPopup);
	}
	
	private void redoConfirmPopup_onConfirm() {
		mConfirmPopup = null;
		
		ModuleRecapHandler handler = this.moduleRecapHandler;
		close();
		if (handler != null)
			handler.onRedo();
	}
	
	private void continueButton_onClick() {
		ModuleRecapHandler handler = this.moduleRecapHandler;
		close();
		if (handler != null)
			handler.onContinue();
	}
	
	private void printButton_onClick() {
		if (mConfirmPopup != null)
			return;
		
		GWT.log("Print clicked, navigate to: " + DataController.getService().getPrintURL());
		Window.open(DataController.getService().getPrintURL(), "_blank", "");
	}
	private void restartButton_onClick() {
		if (mConfirmPopup != null)
			return;
		
		GWT.log("Restart clicked");
		mConfirmPopup = new ConfirmPopup("Start Over");
		mConfirmPopup.setTitle("Are you Sure?");
		mConfirmPopup.setBody("If you start over, your essay progress will be lost. Make sure you have printed your essay or downloaded it from your student profile before restarting.");
		mConfirmPopup.setHandler(new ConfirmPopup.Handler() {
			@Override
			public void onConfirm(ConfirmPopup popup) {
				restartConfirmPopup_onConfirm();
			}
			
			@Override
			public void onCancel(ConfirmPopup popup) {
				confirmPopup_onCancel();
			}
		});
		getRoot().add(mConfirmPopup);
	}
	
	private void restartConfirmPopup_onConfirm() {
		mConfirmPopup = null;
		close();
	}
	
	private void confirmPopup_onCancel() {
		mConfirmPopup = null;
	}
	
	private void closeButton_onClick() {
		if (mConfirmPopup != null)
			return;
		
		close();
	}
	
	private void close() {
		removeFromParent();
		if (mCloseHandler != null) {
			mCloseHandler.onClose(this);
			mCloseHandler = null;
		}
		moduleRecapHandler = null;
	}

	@Override
	public void setCloseHandler(CloseHandler handler) {
		mCloseHandler = handler;
	}
}
