package org.icivics.arg.ui.global.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class Title extends UIPanel {
	private static TitleUiBinder uiBinder = GWT.create(TitleUiBinder.class);
	interface TitleUiBinder extends UiBinder<Widget, Title> {}
	
	@UiField
	DivElement loginHolder;
	@UiField
	AnchorElement loginLink;
	@UiField
	LabelButton submitButton;
	
	private boolean loggedIn;
	
	public static interface Handler { void onComplete(); }
	public Handler handler;
	
	public Title() {
		initWidget(uiBinder.createAndBindUi(this));
		loggedIn = DataController.getService().isLoggedIn();
		if (loggedIn) {
			loginHolder.addClassName("hidden");
		} else {
			submitButton.addStyleName("hidden");
			loginLink.setHref(DataController.getService().getLoginURL());
			loginLink.setTarget("_top");
		}
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		if (loggedIn) {
			submitButton.clickHandler = new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					proceed();
				}
			};
		}
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		super.onDetach();
	}
	
	private void proceed() {
		removeFromParent();
		if (handler != null) {
			handler.onComplete();
			handler = null;
		}
	}
}
