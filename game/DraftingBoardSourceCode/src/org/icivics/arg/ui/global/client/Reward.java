package org.icivics.arg.ui.global.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.Subpanel;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class Reward<T extends ModuleUI<T>> extends Subpanel<T> {
	private static RewardUiBinder uiBinder = GWT.create(RewardUiBinder.class);
	interface RewardUiBinder extends UiBinder<Widget, Reward> {}
	
	public static interface Handler { void onComplete(); }
	public Handler handler;
	
	@UiField
	LabelButton submitButton;
	@UiField
	ParagraphElement titleText;
	
	public Reward() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public Reward(Handler handler) {
		this();
		this.handler = handler;
		String newTitle = Recap.MODULE_TITLES[DataController.moduleIndex];
		if(newTitle != null)
			titleText.setInnerText(newTitle);
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}

	@Override
	protected void onDetach() {
		submitButton.destroy();
		handler = null;
		super.onDetach();
	}
	
	protected void submitButton_onClick() {
		submitButton.clickHandler = null;
		complete();
	}
	
	public void complete() {
		if (handler != null) {
			handler.onComplete();
			handler = null;
		}
	}
}
