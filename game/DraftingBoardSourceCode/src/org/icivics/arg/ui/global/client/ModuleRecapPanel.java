package org.icivics.arg.ui.global.client;

import java.util.ArrayList;

import org.icivics.arg.ui.common.client.AudioConcatenatedControl;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class ModuleRecapPanel extends UIPanel {
	private static ModuleRecapPanelUiBinder uiBinder = GWT.create(ModuleRecapPanelUiBinder.class);
	interface ModuleRecapPanelUiBinder extends UiBinder<Widget, ModuleRecapPanel> {}
	
	@UiField
	DivElement titleHolder;
	@UiField
	ParagraphElement titleText;
	@UiField
	ParagraphElement infoText;
	@UiField
	AudioConcatenatedControl recapAudio;
	@UiField
	ParagraphElement bodyText;
	
	@UiField
	LabelButton redoButton;
	@UiField
	LabelButton continueButton;
	
	public ModuleRecapPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setAudio(ArrayList<String> urls, String baseId){
		if(urls == null || urls.size() == 0){
			//Clear the audio stuff
			recapAudio.removeFromParent();
		}else{
			//load the audio clips
			int count=urls.size();
			for(int i=0;i<count;i++){
				recapAudio.loadSound(urls.get(i), baseId+i);
			}
		}
	}
	
}
