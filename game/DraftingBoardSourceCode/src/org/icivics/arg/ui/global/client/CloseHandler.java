package org.icivics.arg.ui.global.client;

public interface CloseHandler {
	void onClose(NavAccessiblePanel ui);
}
