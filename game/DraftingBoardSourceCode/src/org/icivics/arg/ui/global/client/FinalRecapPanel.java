package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class FinalRecapPanel extends UIPanel {
	private static FinalRecapPanelUiBinder uiBinder = GWT.create(FinalRecapPanelUiBinder.class);
	interface FinalRecapPanelUiBinder extends UiBinder<Widget, FinalRecapPanel> {}
	
	@UiField
	DivElement essayHolder;
	
	@UiField
	LabelButton printButton;
	@UiField
	LabelButton restartButton;
	
	public FinalRecapPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onDetach() {
		printButton.destroy();
		super.onDetach();
	}
}
