package org.icivics.arg.ui.global.client;

import org.icivics.arg.client.ICivicsArgumentation;
import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.ui.common.client.ConfirmPopup;
import org.icivics.arg.ui.common.client.DistinguishableTextBox;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.ui.global.client.Recap.ModuleRecapHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class Summation extends UIPanel{
	private static final String COMPLETE_CLASS_NAME = "complete";
	private static final String SELECTED_CLASS_NAME = "selected";
	
	//Borrowed from recap panel
	public static final String[] MODULE_TITLES = {
		"Issue Analyzer",
		"Claim Creator",
		"Paragraph Constructor 1",
		"Paragraph Constructor 2",
		"Paragraph Constructor 3",
		"Critic Crusher",
		"Introduction Introducer",
		"Conclusion Crafter"
	};

	private static final String[] MODULE_INFO_RECAP = {
		"Nice work! You have successfully completed the Issue Analyzer and picked your side of the argument. Here's the side you chose:",
		"Well done! You have successfully completed the Claim Creator and outlined your argument. You've confirmed your claim and selected the best supporting reasons. Here's what you decided:",
		"Good job! You completed your 1st body paragraph using the Paragraph Constructor. Here it is for your review:",
		"Great job! You completed your 2nd body paragraph using the Paragraph Constructor. Here it is for your review:",
		"Great job! You completed your last body paragraph using the Paragraph Constructor. Here it is for your review:",
		"Excellent! You have successfully completed the Critic Crusher and addressed the other side's argument. By recognizing the other side's reason and knocking it down with evidence, you've strengthened your essay. Here is your Critic Crusher paragraph:",
		"Nice work! You have successfully completed the Introduction Introducer and grabbed your reader's attention. You've also laid out the structure of your argument by stating your claim and supporting reasons. Here's your introduction paragraph:",
		"Nice work! You have successfully completed the Conclusion Crafter and finished your essay in a strong way. Here's your final paragraph:"
	};
	
	
	
	private static SummationUiBinder uiBinder = GWT.create(SummationUiBinder.class);
	interface SummationUiBinder extends UiBinder<Widget, Summation> {}

	@UiField
	HTMLPanel userText;
	@UiField
	DivElement outlineText;
	@UiField
	LabelButton submitButton/*, backButton*/;
	HandlerRegistration mSubmitBtnReg;
	
	HandlerRegistration mEditBtnReg;
	
	private ConfirmPopup mConfirmPopup;
	private EvidenceSidebar mHud;
	
	DistinguishableTextBox[] essayEdit = new DistinguishableTextBox[6];
	
	HTMLPanel[] tabs = new HTMLPanel[8];
	private boolean isPopup;
	
	@UiField
	HTMLPanel IssueAnalyzerTab,
		ClaimCreatorTab,
		ParagraphConstructorTab,
		CriticCrusherTab,
		IntroductionIntroducerTab,
		ConclusionCrafterTab,
		editButton;
	
	public static interface ModuleSummationHandler {
		void onRedo();
		void onContinue();
	}
	public ModuleSummationHandler navHandler;
	
	public Summation( EvidenceSidebar hud){
		this(false, hud);
	}
	
	public Summation(boolean popup, EvidenceSidebar hud) {
		initWidget(uiBinder.createAndBindUi(this));
		
		mHud = hud;
		
		final int moduleIndex;
		
		isPopup = popup;
		if(popup){
			submitButton.setText("CLOSE");
		}

		//backButton.setEnabled(!popup);
		
		moduleIndex = DataController.moduleIndex;

		tabs[0] = IssueAnalyzerTab;
		tabs[1] = ClaimCreatorTab;
		tabs[2] = ParagraphConstructorTab;
		tabs[3] = ParagraphConstructorTab;
		tabs[4] = ParagraphConstructorTab;
		tabs[5] = CriticCrusherTab;
		tabs[6] = IntroductionIntroducerTab;
		tabs[7] = ConclusionCrafterTab;
		
		mSubmitBtnReg = submitButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}}, ClickEvent.getType());
		
		/*
		backButton.clickHandler = new ClickHandler() {
		 
			@Override
			public void onClick(ClickEvent event) {
				redoButton_onClick();
			}
		};
		*/
		
		switch(moduleIndex){
			case 7:
				ConclusionCrafterTab.addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						viewSummation(7);
					}},ClickEvent.getType());
				// Notice the lack of a break;
			case 6:
				IntroductionIntroducerTab.addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						viewSummation(6);
					}},ClickEvent.getType());
			case 5:
				CriticCrusherTab.addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						viewSummation(5);
					}},ClickEvent.getType());
			case 4:
			case 3:
			case 2:
				ParagraphConstructorTab.addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						viewSummation(moduleIndex > 4 ? 4 : moduleIndex);
					}},ClickEvent.getType());
			case 1:
				ClaimCreatorTab.addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						viewSummation(1);
					}},ClickEvent.getType());
			case 0:
				IssueAnalyzerTab.addDomHandler(new ClickHandler(){
					@Override
					public void onClick(ClickEvent event) {
						viewSummation(0);
					}},ClickEvent.getType());
		}
		
		viewSummation(moduleIndex);
	}

	
	private void viewSummation(final int moduleIndex){
		GWT.log("Trying to display summation " + moduleIndex);
		
		// Set the text, and the edit text button
		showStatic(moduleIndex);
		editButton.getElement().setInnerHTML("<p>Edit Text</p>");
		if(mEditBtnReg != null)
			mEditBtnReg.removeHandler();
		mEditBtnReg = editButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onEdit(moduleIndex);
			}},ClickEvent.getType());
		
		outlineText.setInnerHTML(getModuleOutline(moduleIndex));
		
		// Mark the right tab
		for(int i=tabs.length-1; i > -1; i--){
			tabs[i].removeStyleName(SELECTED_CLASS_NAME);
			tabs[i].removeStyleName(COMPLETE_CLASS_NAME);
			if(DataController.moduleIndex >= i)
				tabs[i].addStyleName(COMPLETE_CLASS_NAME);
		}
		if(moduleIndex > -1)
			tabs[moduleIndex].addStyleName(SELECTED_CLASS_NAME);
		
		if(moduleIndex < 2){
			editButton.getElement().getStyle().setDisplay(Display.NONE);
		} else {
			editButton.getElement().getStyle().clearDisplay();
		}
		
		ICivicsArgumentation.scaleText();
		updateScrollbars();
	}

	private void onSubmit(){
		saveEditedText();
		if(navHandler != null){
			navHandler.onContinue();
			mSubmitBtnReg.removeHandler();
		}
		close();
	}
	
	public void onAttach(){
		super.onAttach();
		
		mHud.enableProgressButton(false);
		
		addScrollbars();
		ICivicsArgumentation.scaleText();
	}
	
	private void onEdit(final int moduleIndex){
		viewSummation(moduleIndex);
		
		editButton.getElement().setInnerHTML("<p>Save Text</p>");
		
		showEditable(moduleIndex);
		
		mEditBtnReg.removeHandler();
		mEditBtnReg = editButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSave(moduleIndex);
			}}, ClickEvent.getType());
	}
	
	private void onSave(final int moduleIndex){
		editButton.getElement().setInnerHTML("<p>Edit Text</p>");
		saveEditedText();
		
		showStatic(moduleIndex);
		
		mEditBtnReg.removeHandler();
		mEditBtnReg = editButton.addDomHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onEdit(moduleIndex);
			}}, ClickEvent.getType());
	}
	
	private void showEditable(int moduleIndex){
		userText.clear();
		userText.getElement().setInnerHTML("");
		userText.add(getModuleEditableText(moduleIndex));
	}
	
	private void showStatic(int moduleIndex){
		userText.clear();
		userText.getElement().setInnerHTML(getModuleRecapText(moduleIndex));
				
	}
	
	private String getModuleOutline(int moduleIndex){
		if(moduleIndex > 0){
			if(isPopup && DataController.moduleIndex == 1)
				return "This will hold your outline once you have completed claim creator";
				
			ClaimInstance claim = DataController.getCurrentIssue().claim;
			String str = "<center><b>Your Outline</b></center><br/>" + claim.getClaimText() + "<br/>BECAUSE:";
			for (int i = 0; i < claim.reasons.length; ++i){
				str += "<br/><b>" + (i + 1) + ") " + claim.reasons[i].reason.text + "</b>";
				for(int j = 0; j < claim.reasons[i].reason.items.length; ++j)
				str += "<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b>Evidence:</b> " + claim.reasons[i].reason.items[j].summary;
			}	
			return str;
		} else {
			return "This will hold your outline once you have completed claim creator";
		}
	}
	
	private void redoButton_onClick() {
		mConfirmPopup = new ConfirmPopup("Redo Step");
		//TODO: add audio to redo popup
		mConfirmPopup.setAudio(null, "LJWD");
		mConfirmPopup.getElement().getStyle().setZIndex(2);
		mConfirmPopup.setTitle("Are you Sure?");
		mConfirmPopup.setBody("If you redo this step, you will overwrite your previous work for the step.");
		mConfirmPopup.setHandler(new ConfirmPopup.Handler() {
			@Override
			public void onConfirm(ConfirmPopup popup) {
				redoConfirmPopup_onConfirm();
			}
			
			@Override
			public void onCancel(ConfirmPopup popup) {
				confirmPopup_onCancel();
			}
		});
		getRoot().add(mConfirmPopup);
	}
	
	private void redoConfirmPopup_onConfirm() {
		mConfirmPopup = null;
		
		ModuleSummationHandler handler = this.navHandler;
		close();
		if (handler != null)
			handler.onRedo();
	}
	
	private void confirmPopup_onCancel() {
		mConfirmPopup = null;
	}
	
	private void close() {
		mHud.enableProgressButton(true);
		removeFromParent();
		/*
		if (mCloseHandler != null) {
			mCloseHandler.onClose(this);
			mCloseHandler = null;
		}
		moduleRecapHandler = null;
		*/
	}
	
	private String getModuleRecapText(int moduleIndex) {
		if(isPopup && DataController.moduleIndex == moduleIndex){
			if(moduleIndex <= 2)
				return "";
			moduleIndex--;
			
		}
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		
		//
		//String txt = MODULE_INFO_RECAP[moduleIndex];
		
		switch(moduleIndex) {
			case 0:
				return claim.getClaimText();
			case 1:
				//TODO: Improve formatting
				String str = "<center><b>Your Outline</b></center><br/>" + claim.getClaimText() + "<br/>BECAUSE:";
				for (int i = 0; i < claim.reasons.length; ++i){
					str += "<br/><b>" + (i + 1) + ") " + claim.reasons[i].reason.text + "</b>";
					for(int j = 0; j < claim.reasons[i].reason.items.length; ++j)
					str += "<br/>&nbsp;&nbsp;&nbsp;&nbsp;<b>Evidence:</b> " + claim.reasons[i].reason.items[j].summary;
				}
				return str;
			case 2:
				return claim.paragraphs[0];
			case 3:
				return claim.paragraphs[0] + "<br/><br/><br/>" + claim.paragraphs[1];
			case 4:
				return claim.paragraphs[0] + "<br/><br/><br/>" + claim.paragraphs[1] + "<br/><br/><br/>" + claim.paragraphs[2];
			case 5:
				return claim.paragraphs[0] + "<br/><br/><br/>" + claim.paragraphs[1] + "<br/><br/><br/>" + claim.paragraphs[2] + "<br/><br/><br/>" + claim.rebuttalParagraph;
			case 6:
				return claim.introParagraph + "<br/><br/><br/>" + claim.paragraphs[0] + "<br/><br/><br/>" + claim.paragraphs[1] + "<br/><br/><br/>" + claim.paragraphs[2] + "<br/><br/><br/>" + claim.rebuttalParagraph;
			case 7:
				return claim.introParagraph + "<br/><br/><br/>" + claim.paragraphs[0] + "<br/><br/><br/>" + claim.paragraphs[1] + "<br/><br/><br/>" + claim.paragraphs[2] + "<br/><br/><br/>" + claim.rebuttalParagraph + "<br/><br/><br/>" + claim.conclusionParagraph;
			default:
				GWT.log("Invalid index for module recap " + moduleIndex);
				return null;
		}
	}
	
	private HTMLPanel getModuleEditableText(int moduleIndex){
		HTMLPanel panel = new HTMLPanel("");

		ClaimInstance claim = DataController.getCurrentIssue().claim;
		
		String[] essayText = new String[6];
		essayText[0] = claim.introParagraph;
		essayText[1] = claim.paragraphs[0];
		essayText[2] = claim.paragraphs[1];
		essayText[3] = claim.paragraphs[2];
		essayText[4] = claim.rebuttalParagraph;
		essayText[5] = claim.conclusionParagraph;
		
		for(int i=0; i<6; i++){
			if(essayText[i] != null){
				essayEdit[i] = new DistinguishableTextBox();
				essayEdit[i].setInnerText(essayText[i]);
				panel.add(essayEdit[i]);
				panel.add(new HTMLPanel("<br/><br/>"));
			} else {
				essayEdit[i] = null;
			}
		}
		
		return panel;
	}

	
	private void saveEditedText(){
		//TODO: walk through the editable textboxes and save the text in them to the appropriate paragraph
		String[] essayText = new String[6];
		for(int i = 0; i < 6; i++ ){
			if(essayEdit[i] != null){
				GWT.log("I am trying to save essay text for paragraph with index " + i);
				essayText[i] = essayEdit[i].getValue();
			}
		}
		
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		
		if(essayText[0] != null)
			claim.introParagraph = essayText[0];
		if(essayText[1] != null)
			claim.paragraphs[0] = essayText[1];
		if(essayText[2] != null)
			claim.paragraphs[1] = essayText[2];
		if(essayText[3] != null)
			claim.paragraphs[2] = essayText[3];
		if(essayText[4] != null)
			claim.rebuttalParagraph = essayText[4];
		if(essayText[5] != null)
			claim.conclusionParagraph = essayText[5];
				
	}

	private native void addScrollbars() /*-{
	$wnd.$('.summation .usertext-wrap .usertext').perfectScrollbar({
			wheelSpeed: 30,
			wheelPropagation: true,
			suppressScrollX: true,
			useKeyboard:false
		});
	$wnd.$('.summation .outline-wrap .outline').perfectScrollbar({
			wheelSpeed: 30,
			wheelPropagation: true,
			suppressScrollX: true,
			useKeyboard:false
		});
	}-*/;
	
	private native void updateScrollbars() /*-{
		$wnd.$('.summation .outline-wrap .outline').perfectScrollbar('update');
		$wnd.$('.summation .usertext-wrap .usertext').perfectScrollbar('update');
	}-*/;
}
