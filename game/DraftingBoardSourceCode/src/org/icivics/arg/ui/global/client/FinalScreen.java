package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class FinalScreen extends UIPanel {
	private static FinalScreenUiBinder uiBinder = GWT.create(FinalScreenUiBinder.class);
	interface FinalScreenUiBinder extends UiBinder<Widget, FinalScreen> {}

	@UiField
	LabelButton restartButton, printButton;
	
	public FinalScreen() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	protected void onAttach(){
		super.onAttach();
		ElementUtil.setTextScaling();
	}
	
	@Override
	protected void onDetach() {
		printButton.destroy();
		super.onDetach();
	}
	
}
