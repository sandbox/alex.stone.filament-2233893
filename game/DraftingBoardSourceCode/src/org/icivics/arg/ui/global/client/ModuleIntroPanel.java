package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.BasicButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class ModuleIntroPanel extends UIPanel {
	private static ModuleIntroPanelUiBinder uiBinder = GWT.create(ModuleIntroPanelUiBinder.class);
	interface ModuleIntroPanelUiBinder extends UiBinder<Widget, ModuleIntroPanel> {}
	
	@UiField
	DivElement titleHolder;
	@UiField
	ParagraphElement titleText;
	@UiField
	ParagraphElement infoText;
	
	@UiField
	BasicButton closeButton;
	@UiField
	DivElement objectiveList;
	
	public ModuleIntroPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onDetach() {
		closeButton.destroy();
		super.onDetach();
	}
}
