package org.icivics.arg.ui.global.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.BasicButton;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceListPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Navigation extends Composite {
	private static NavigationUiBinder uiBinder = GWT.create(NavigationUiBinder.class);
	interface NavigationUiBinder extends UiBinder<Widget, Navigation> {}
	
	//@UiField
	SimplePanel popupMount;
	
	@UiField
	BasicButton progressButton;
	@UiField
	BasicButton evidenceButton;
	@UiField
	BasicButton transitionsButton;
	
	private Widget mPopup;
	
	private EvidenceListPanel.Handler evidenceHandler;
	
	public Navigation(SimplePanel popupMount) {
		this.popupMount = popupMount;
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		progressButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				progressButton_onClick();
			}
		};
		evidenceButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				evidenceButton_onClick();
			}
		};
		transitionsButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				transitionsButton_onClick();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		progressButton.destroy();
		evidenceButton.destroy();
		transitionsButton.destroy();
		super.onDetach();
	}
	
	public void updateProgress() {
		progressButton.setStyleName("progress step" + DataController.moduleIndex);
	}
	
	private void progressButton_onClick() {
		Recap popup = new Recap(new ModuleObjectivesPanel());
		popup.setCloseHandler(new CloseHandler() {
			@Override
			public void onClose(NavAccessiblePanel ui) {
				recap_onClose();
			}
		});
		popupMount.add(popup);
		mPopup = popup;
	}
	
	private void evidenceButton_onClick() {
		EvidenceListPanel popup = new EvidenceListPanel();
		
		if(evidenceHandler != null){
			popup.setHandler(evidenceHandler);
		}
		
		popup.setCloseHandler(new CloseHandler() {
			@Override
			public void onClose(NavAccessiblePanel ui) {
				evidence_onClose();
			}
		});
		popupMount.add(popup);
		mPopup = popup;
	}
	
	private void transitionsButton_onClick() {
		TransitionsGuidePopup popup = new TransitionsGuidePopup();
		popup.setCloseHandler(new CloseHandler() {
			@Override
			public void onClose(NavAccessiblePanel ui) {
				transitions_onClose();
			}
		});
		popupMount.add(popup);
		mPopup = popup;
	}
	
	private void recap_onClose() {
		//Recap popup = (Recap) mPopup;
		mPopup = null;
	}
	
	private void evidence_onClose() {
		//EvidenceListPanel popup = (EvidenceListPanel) mPopup;
		mPopup = null;
	}

	private void transitions_onClose() {
		//TransitionsGuidePopup popup = (TransitionsGuidePopup) mPopup;
		mPopup = null;
	}

	public void setEvidenceHandler(EvidenceListPanel.Handler handler){
		evidenceHandler = handler;
	}
	
	public void clearEvidenceHandler(){
		evidenceHandler = null;
	}
}
