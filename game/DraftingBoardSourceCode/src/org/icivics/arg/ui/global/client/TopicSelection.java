package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.ui.global.client.ScaffoldingSelection.StartHandler;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class TopicSelection extends UIPanel {
	private static TopicSelectionUiBinder uiBinder = GWT.create(TopicSelectionUiBinder.class);
	interface TopicSelectionUiBinder extends UiBinder<Widget, TopicSelection> {}

	
	// bigphil
	
	private static String[] TOPIC_IDS =   {"8977","8973","8975","8959","8957","9369","9649"};
	private static String[] TOPIC_NAMES = {"Community Service","Electoral College","Credit Cards","Military Intervention", "Interest Groups", "Voting Age", "Student Expression"};
	private static String[] TOPIC_UNITS = {"Citizenship & Participation","Politics and Public Policy","Government & the Market","International Affairs", "Media and Influence", "The Constitution", "The Constitution"};
	/*
	private static String[] TOPIC_IDS =   {"284139","284141","284142","284143","284140","155248"};
	private static String[] TOPIC_NAMES = {"Community Service","Electoral College","Credit Cards","Military Intervention", "Interest Groups", "Student Expression"};
	private static String[] TOPIC_UNITS = {"Citizenship & Participation","Politics and Public Policy","Government & the Market","International Affairs", "Media and Influence", "The Constitution"};
	*/
	public static interface StartHandler { void onComplete(); }
	public StartHandler startHandler;
	
	public String selectedTopic;
	
	@UiField 
	LabelButton submitButton;
	
	@UiField
	HTMLPanel optionHolder;
	
	HTMLPanel[] options;
	
	public TopicSelection() {
		initWidget(uiBinder.createAndBindUi(this));
		
		submitButton.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				onSubmit();
			}
		};
		
		options = new HTMLPanel[TOPIC_IDS.length];
		for(int i=0; i < options.length; i++){
			options[i] = new HTMLPanel("<div class=\"check\"></div>"+
									"<div class=\"unit-name\">" +
										"<p>" + TOPIC_UNITS[i] + "</p>" +
									"</div>" +
									"<div class=\"topic-title\">" +
										"<p>" + TOPIC_NAMES[i] + "</p>" +
									"</div>");
			options[i].addStyleName("option");
			
			final int index = i;
			options[i].addDomHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					optionSelect(index);
				}
				
			},ClickEvent.getType());
			
			optionHolder.add(options[i]);
		}
		
		
		
		submitButton.setEnabled(false);
	}

	@Override
	protected void onAttach() {
		super.onAttach();
		
		ElementUtil.setTextScaling();
	}
	
	private void optionSelect(int index){
		for(int i =0; i < options.length; i++){
			options[i].removeStyleName("selected");
		}
		
		options[index].addStyleName("selected");
		
		submitButton.setEnabled(true);
		
		this.selectedTopic = TOPIC_IDS[index];
	};
	
	private void onSubmit(){
		removeFromParent();		
		if(startHandler != null){
			startHandler.onComplete();
			startHandler = null;
		}
	}
	
}
