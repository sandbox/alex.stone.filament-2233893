package org.icivics.arg.ui.global.client;

import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class CompletionCertificate extends UIPanel {
	private static CompletionCertificateUiBinder uiBinder = GWT.create(CompletionCertificateUiBinder.class);
	interface CompletionCertificateUiBinder extends UiBinder<Widget, CompletionCertificate> {}
	
	@UiField
	LabelButton submitButton;
	
	public static interface Handler { void onClose(); }
	public Handler handler;
	
	public CompletionCertificate() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		submitButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		submitButton.destroy();
		super.onDetach();
	}

	protected void submitButton_onClick() {
		submitButton.clickHandler = null;
		removeFromParent();
		
		if (handler != null) {
			handler.onClose();
			handler = null;
		}
	}
}
