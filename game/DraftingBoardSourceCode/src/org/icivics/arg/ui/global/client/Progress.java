package org.icivics.arg.ui.global.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.ui.common.client.ConfirmPopup;
import org.icivics.arg.ui.common.client.LabelButton;
import org.icivics.arg.ui.common.client.UIPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.ui.global.client.Summation.ModuleSummationHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Progress extends  UIPanel {
	private static ProgressUiBinder uiBinder = GWT.create(ProgressUiBinder.class);
	interface ProgressUiBinder extends UiBinder<Widget, Progress> {}

	private static String[] PROGRESS_BAR_CLASSES = {
		"issue",
		"claim",
		"paracon",
		"paracon",
		"paracon",
		"critic",
		"intro",
		"conclusion"
	};
	
	public static interface Handler {
		void onRedo();
		void onContinue();
	}
	
	@UiField
	LabelButton viewEssay, backButton, submitButton;
	
	@UiField
	DivElement progressBar,
	issue, claim, intro, paracon1, paracon2, paracon3, critic, conclusion;
	
	public Handler navHandler;
	
	private int moduleIndex;
	private DivElement[] moduleMarkers = new DivElement[8];
	private EvidenceSidebar mHud;
	private ConfirmPopup mConfirmPopup;
	private Summation mSummation;
	private SimplePanel mSummationHolder;
	
	public Progress(EvidenceSidebar hud, SimplePanel summationHolder) {
		initWidget(uiBinder.createAndBindUi(this));
		mSummationHolder = summationHolder;
		mHud = hud;
		mHud.enableProgressButton(false);
		
		moduleIndex = DataController.moduleIndex;
		
		moduleMarkers[0] = issue;
		moduleMarkers[1] = claim;
		moduleMarkers[2] = paracon1;
		moduleMarkers[3] = paracon2;
		moduleMarkers[4] = paracon3;
		moduleMarkers[5] = critic;
		moduleMarkers[6] = intro;
		moduleMarkers[7] = conclusion;
		
		progressBar.addClassName(PROGRESS_BAR_CLASSES[moduleIndex]);
		for(int i = 0; i < moduleIndex+1; i++)
			moduleMarkers[i].addClassName("complete");
		
		submitButton.clickHandler = new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				submitButton_onClick();
			}
			
		};
		backButton.clickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				redoButton_onClick();
			}
		};
		viewEssay.clickHandler = new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				essayButton_onClick();
			}
		};
		
	}

	private void essayButton_onClick(){		
		mSummationHolder.addStyleName("progress-popup");
		mSummationHolder.clear();
		
		final Summation summation = new Summation(mHud);
		summation.navHandler = new Summation.ModuleSummationHandler() {
			@Override
			public void onRedo() {
				summation.removeFromParent();
				mSummationHolder.removeStyleName("progress-popup");
			}
			@Override
			public void onContinue() {
				summation.removeFromParent();
				mSummationHolder.removeStyleName("progress-popup");
			}
		};
		
		
		mSummationHolder.add(summation);
	}
	
	private void redoButton_onClick() {
		mConfirmPopup = new ConfirmPopup("Redo Step");
		//TODO: add audio to redo popup
		mConfirmPopup.setAudio(null, "LJWD");
		mConfirmPopup.getElement().getStyle().setZIndex(2);
		mConfirmPopup.setTitle("Are you Sure?");
		mConfirmPopup.setBody("If you redo this step, you will overwrite your previous work for the step.");
		mConfirmPopup.setHandler(new ConfirmPopup.Handler() {
			@Override
			public void onConfirm(ConfirmPopup popup) {
				redoConfirmPopup_onConfirm();
			}
			
			@Override
			public void onCancel(ConfirmPopup popup) {
				confirmPopup_onCancel();
			}
		});
		getRoot().add(mConfirmPopup);
	}
	
	private void redoConfirmPopup_onConfirm() {
		mConfirmPopup = null;
		
		Handler handler = this.navHandler;
		close();
		if (handler != null)
			handler.onRedo();
	}
	
	private void confirmPopup_onCancel() {
		mConfirmPopup = null;
	}
	
	private void submitButton_onClick(){
		if(navHandler != null){
			navHandler.onContinue();
		}
		close();
	}
	
	private void close() {
		mHud.enableProgressButton(true);
		removeFromParent();
	}
}
