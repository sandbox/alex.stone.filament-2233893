package org.icivics.arg.client;


import java.util.Map;

import org.icivics.arg.data.client.*;
import org.icivics.arg.data.client.DataController.ResultHandler;
import org.icivics.arg.data.model.client.*;
import org.icivics.arg.ui.claimCreator.client.ClaimCreator;
import org.icivics.arg.ui.common.client.ModuleUI;
import org.icivics.arg.ui.common.client.TextPopup;
import org.icivics.arg.ui.conclusion.client.ConclusionCrafter;
import org.icivics.arg.ui.criticCrusher.client.CriticCrusher;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceListPanel;
import org.icivics.arg.ui.evidenceEvaluator.client.EvidenceSidebar;
import org.icivics.arg.ui.global.client.*;
import org.icivics.arg.ui.intro.client.Introducer;
import org.icivics.arg.ui.issueAnalyzer.client.IssueAnalyzer;
import org.icivics.arg.ui.paragraphBuilder.client.ParagraphBuilder;
import org.icivics.arg.util.client.ElementUtil;
import org.icivics.arg.util.client.JSONUtil;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ICivicsArgumentation implements EntryPoint {	
	private EvidenceSidebar mSidebar;
	
	private SimplePanel mModuleHolder = new SimplePanel();
	private SimplePanel mHudHolder = new SimplePanel();
	public SimplePanel mOverlayHolder = new SimplePanel();
	
	private HTMLPanel mLogWindow = new HTMLPanel("Drafting Board has encountered a problem");
	
	private Loading mLoadScreen = new Loading();
	
	private ModuleUI<?> mCurrentModule;
	private Navigation mHud;
	
	private static ICivicsArgumentation sInstance;
	public static final ICivicsArgumentation getInstance() {
		return sInstance;
	}
	
	private interface RetryHandler {void onRetry();}

	
	public static native void scaleText()/*-{
		$wnd.scaleText();
	}-*/;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		sInstance = this;
		injectDependencies();
		
		mSidebar = new EvidenceSidebar();
		
		// Although this exception handler is set here, it will not catch anything until onModuleLoad returns
		GWT.setUncaughtExceptionHandler(new ExceptionHandler(mLogWindow));
		
		mSidebar.setProgressHandler(new EvidenceSidebar.ViewProgressHandler(){
			@Override
			public void onViewProgress() {
				GWT.log("trying to view progress");
				mOverlayHolder.addStyleName("progress-popup");
				mOverlayHolder.clear();
				
				final Summation summation = new Summation(true, mSidebar);
				summation.navHandler = new Summation.ModuleSummationHandler() {
					@Override
					public void onRedo() {
						summation.removeFromParent();
						mOverlayHolder.removeStyleName("progress-popup");
					}
					@Override
					public void onContinue() {
						summation.removeFromParent();
						mOverlayHolder.removeStyleName("progress-popup");
					}
				};
				
				mOverlayHolder.add(summation);
			}});
		
		
		RootPanel.get("screen").add(mLogWindow);
		RootPanel.get("screen").add(mSidebar);
		mModuleHolder = mSidebar.getContentContainer();
		
		mSidebar.contentStyleContainer.add(mOverlayHolder);
		//RootPanel.get("screen").add(mOverlayHolder);
		
		showLoadingScreen();
		
		DataController.initService();
		loadStaticData();
	}
	
	private void injectDependencies() {
		JSONUtil.initialize();
	}
	
	private void loadStaticData() {
		
		GWT.log("Building static data . . . ");
		loadConfig();
	}
	
	private void loadConfig() {
		DataController.initConfig(new DataController.ResultHandler() {
			@Override
			public void onResult(boolean success) {
				loadImageList();
			}
		});
	}
	
	private void loadImageList() {
		DataController.preloadImages(new DataController.ResultHandler() {
			@Override
			public void onResult(boolean success) {
				loadTransitions();
			}
		});
	}
	
	private void loadTransitions() {
		DataController.initTransitions(new DataController.ResultHandler() {
			@Override
			public void onResult(boolean success) {
				connect();
			}
		});
	}
	
	private void loadGlossary(){
		DataController.initGlossary(new DataController.ResultHandler() {
			@Override
			public void onResult(boolean success) {
				
			}
		});
	}
	
	private void connect() {
		try {
			DataController.getService().connect(new AsyncCallback<Map<String, ?>>() {
				@Override
				public void onSuccess(Map<String, ?> result) {
					GWT.log("CONNECTION SUCCESS: " + result);
					
					onConnected();
				}
				
				@Override
				public void onFailure(Throwable caught) {
					caught.printStackTrace();
					LogUtil.log("Could not connect.  App terminated.");
					assert false:caught;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void onConnected() {
		hideLoadingScreen();
		showTitle();
	}
	
	private void showTitle() {
		
		Title screen = new Title();
		screen.handler = new Title.Handler() {
			@Override
			public void onComplete() {
				onTitleComplete();
			}
		};
		mModuleHolder.add(screen);
		
	}
	
	private void onTitleComplete() {		
		mSidebar.hide();
		GWT.log("Topic is set: " + DataController.getService().topicSet());
		if(DataController.getService().topicSet()){
			showScaffoldSelector();
		} else {
			showTopicSelector();
		}
	}
	
	private void showTopicSelector(){
		final TopicSelection selector = new TopicSelection();
		
		selector.startHandler = new TopicSelection.StartHandler(){
			@Override
			public void onComplete(){
				DataController.getService().setTopic(selector.selectedTopic);
				showScaffoldSelector();
			};
		};
		mModuleHolder.add(selector);
	}
	
	private void showScaffoldSelector(){
		final ScaffoldingSelection selector = new ScaffoldingSelection();
		
		selector.startHandler = new ScaffoldingSelection.StartHandler(){
			@Override
			public void onComplete(){
				DataController.scaffoldLevel = selector.selectedLevel;
				loadGameData();
			};
		};
		mModuleHolder.add(selector);
	}
	
	private void loadGameData() {
		LogUtil.log("Loading game data . . .");
		showLoadingScreen();
		DataController.initIssue(new ResultHandler() {
			@Override
			public void onResult(boolean success) {
				loadGlossary();
				hideLoadingScreen();
				if (success) {
					if (DataController.SAVE_ENABLED) {
						loadUserData();
					} else {
						startGame();
					}
				} else {
					showNetworkErrorPopup(new RetryHandler(){
						public void onRetry(){
							loadGameData();
						}
					});
					LogUtil.log("Failed to load game data.  App terminated.");
				}
			}
		});
	}
	
	private void loadUserData() {
		LogUtil.log("Loading user data . . .");
		DataController.initUserData(new ResultHandler() {
			@Override
			public void onResult(boolean success) {
				if (success) {
					loadModuleProgress();
				} else {
					showNetworkErrorPopup(new RetryHandler(){
						public void onRetry(){
							loadUserData();
						}
					});
					LogUtil.log("Failed to load user data.  App terminated.");
				}
			}
		});
	}
	
	private void loadModuleProgress() {
		LogUtil.log("Loading module progress . . .");
		DataController.initModuleProgress(new ResultHandler() {
			@Override
			public void onResult(boolean success) {
				if (success) {
					startGame();
				} else {
					showNetworkErrorPopup(new RetryHandler(){
						public void onRetry(){
							loadModuleProgress();
						}
					});
					LogUtil.log("Failed to load module progress.  App terminated.");
				}
			}
		});
	}
	
	private void loadCriticCrusher() {
		loadCriticCrusher(false);
	}
	
	private void loadCriticCrusher(final boolean resume) {
		showLoadingScreen();
		DataController.initCriticCrusher(new ResultHandler() {
			@Override
			public void onResult(boolean success) {
				if (success) {
					hideLoadingScreen();
					if (resume) {
						DataController.getModuleProgress().forCriticCrusher().applyProgress();
						showCriticCrusher();
					} else {
						beginCriticCrusher();
					}
				} else {
					LogUtil.log("Failed to load Critic Crusher data.  App terminated.");
				}
			}
		});
	}
	
	private void startGame() {
		LogUtil.log("Starting game.");
		
		mSidebar.init();
		
		hideLoadingScreen();
		hideHud();
		mHud = new Navigation(mOverlayHolder);
		mHudHolder.add(mHud);
		
		
		GWT.log("URL hash: " + Window.Location.getHash());
		GWT.log(Window.Location.getHash().equals(DataController.MENU_URL_HASH)? "That's the magic word!" : "Not the magic word");
		
		setupDebugMenuFunction(this);
		
		String hash = "";
		if(Window.Location.getHash().length() >= DataController.MENU_URL_HASH.length() )
			hash = Window.Location.getHash().substring(0, DataController.MENU_URL_HASH.length());
		
		if (DataController.MENU_ENABLED || hash.equals(DataController.MENU_URL_HASH)) {
			
			if(Window.Location.getHash().length() > DataController.MENU_URL_HASH.length()){
				GWT.log("Extra characters after the hash: " + Window.Location.getHash().substring(DataController.MENU_URL_HASH.length()));
				int scaffoldLevel = -1;
				try{
					scaffoldLevel = Integer.parseInt(Window.Location.getHash().substring(DataController.MENU_URL_HASH.length()));
				} catch(Exception e) {
					
				}
				
				if(scaffoldLevel >= 0 && scaffoldLevel < 7)
					DataController.getCurrentIssue().studentLevel = scaffoldLevel;
				
			} else {
				DataController.getCurrentIssue().studentLevel = DataController.scaffoldLevel;
			}
			showDebugMenu();
			
		} else {
			DataController.getCurrentIssue().studentLevel = DataController.scaffoldLevel;
			beginCurrentModule();
		}
	}
	
	public void showNetworkErrorPopup(){
		showNetworkErrorPopup(null);
	}
	
	public void showNetworkErrorPopup(final RetryHandler handler){
		hideLoadingScreen();
		
		TextPopup errorPopup =  new TextPopup();
		errorPopup.setTitle("A network error has occured");
		errorPopup.setBody("A network error has prevented Drafting Board from continuing.");

		if(handler ==  null){
			// No handler, so no retry button
			
		} else {
			errorPopup.setHandler(new TextPopup.PopupHandler() {
				
				@Override
				public void onClose(TextPopup popup) {
					handler.onRetry();
				}
			});
		}
		
		mOverlayHolder.add(errorPopup);
	}
	
	public void showLoadingScreen() {
		if (!mLoadScreen.isAttached())
			RootPanel.get("screen").add(mLoadScreen);
	}
	
	public void hideLoadingScreen() {
		mLoadScreen.removeFromParent();
	}
	
	private void showHud() {
		mHudHolder.removeStyleName("hidden");
	}
	
	private void hideHud() {
		mHudHolder.removeStyleName("hidden");
		mHudHolder.addStyleName("hidden");
	}
	
	private void showUI(Widget ui) {
		mModuleHolder.add(ui);
	}
	
	
	private void beginIssueAnalyzer() {
		GWT.log("Begin IssueAnalyzer");
		DataController.moduleIndex = 0;
		DataController.flushModuleProgress();
		DataController.startTimer();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new GraphicOrganizer());
		recap.setCloseHandler(new CloseHandler() {
			public void onClose(NavAccessiblePanel ui) {
				showIssueAnalyzer();
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void showIssueAnalyzer() {
		mCurrentModule = new IssueAnalyzer(mSidebar);
		mCurrentModule.handler = new ModuleUI.Handler() {
			public void onComplete() {
				endIssueAnalyzer();
			}
		};
		showUI(mCurrentModule);
		showHud();
	}
	
	private void endIssueAnalyzer() {
		GWT.log("End IssueAnalyzer");
		//final Summation summation = new Summation(mSidebar);
		final Progress progress = new Progress(mSidebar, mOverlayHolder);
		progress.navHandler = new Progress.Handler() {
			@Override
			public void onRedo() {
				beginIssueAnalyzer();
			}
			@Override
			public void onContinue() {
				DataController.endTimer();
				DataController.saveModule(1, new ResultHandler(){
					@Override
					public void onResult(boolean success) {
						if(success){
							progress.removeFromParent();
							beginClaimCreator();
						} else {
							showNetworkErrorPopup(new RetryHandler(){
								public void onRetry(){
									endIssueAnalyzer();
								}
							});
						}
					}});
			}
		};
		mModuleHolder.add(progress);
	}
	
	
	private void beginClaimCreator() {
		GWT.log("Begin ClaimCreator");
		DataController.moduleIndex = 1;
		DataController.flushModuleProgress();
		DataController.startTimer();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new GraphicOrganizer());
		recap.setCloseHandler(new CloseHandler() {
			public void onClose(NavAccessiblePanel ui) {
				showClaimCreator();
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void showClaimCreator() {
		//TODO: Make sure the sidebar is shown/hidden when it needs to be
		mSidebar.show();
		mCurrentModule = new ClaimCreator(mSidebar, mOverlayHolder);
		mCurrentModule.sidebar = mSidebar;
		mCurrentModule.handler = new ModuleUI.Handler() {
			public void onComplete() {
				endClaimCreator();
			}
		};
		showUI(mCurrentModule);
		showHud();
	}
	
	private void endClaimCreator() {
		GWT.log("End ClaimCreator");
		
		mSidebar.clearEvidenceHandler();
		mSidebar.clearMarked();
		
		final Progress progress = new Progress(mSidebar, mOverlayHolder);
		progress.navHandler = new Progress.Handler() {
			@Override
			public void onRedo() {
				DataController.getCurrentIssue().reasonSelectExplanation = null;
				beginClaimCreator();
			}
			@Override
			public void onContinue() {
				DataController.endTimer();
				DataController.saveModule(2, new ResultHandler(){
					@Override
					public void onResult(boolean success) {
						if(success){
							progress.removeFromParent();
							beginParagraphBuilder(0);
						} else {
							showNetworkErrorPopup(new RetryHandler(){
								public void onRetry(){
									endClaimCreator();
								}
							});
						}
					}});
			}
		};
		mModuleHolder.add(progress);
	}
	
	
	private void beginParagraphBuilder(final int paragraphIndex) {
		GWT.log("Begin ParagraphBuilder " + paragraphIndex);
		DataController.moduleIndex = 2 + paragraphIndex;
		DataController.flushModuleProgress();
		DataController.startTimer();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new GraphicOrganizer());
		recap.setCloseHandler(new CloseHandler() {
			public void onClose(NavAccessiblePanel ui) {
				showParagraphBuilder(paragraphIndex);
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void showParagraphBuilder(final int paragraphIndex) {
		//TODO: Make sure the sidebar is shown/hidden when it needs to be
		mSidebar.show();
		mCurrentModule = new ParagraphBuilder(paragraphIndex, mSidebar);
		mCurrentModule.sidebar = mSidebar;
		mCurrentModule.handler = new ModuleUI.Handler() {
			public void onComplete() {
				endParagraphBuilder(paragraphIndex);
			}
		};
		showUI(mCurrentModule);
		showHud();
	}
	
	private void endParagraphBuilder(final int paragraphIndex) {
		GWT.log("End ParagraphBuilder");
		
		final Progress progress = new Progress(mSidebar, mOverlayHolder);
		progress.navHandler = new Progress.Handler() {
			@Override
			public void onRedo() {
				// Clear the paragraph
				DataController.getCurrentIssue().claim.paragraphsOriginal[paragraphIndex] = null;
				DataController.getCurrentIssue().claim.paragraphs[paragraphIndex] = null;
						
				beginParagraphBuilder(paragraphIndex);
			}
			
			@Override
			public void onContinue() {
				DataController.endTimer();
				DataController.saveModule(3 + paragraphIndex, new ResultHandler(){
					@Override
					public void onResult(boolean success) {
						if(success){
							progress.removeFromParent();
							if (paragraphIndex < 2) {
								beginParagraphBuilder(paragraphIndex + 1);
							} else {
								loadCriticCrusher();
							}
						} else {
							showNetworkErrorPopup(new RetryHandler(){
								public void onRetry(){
									endParagraphBuilder(paragraphIndex);
								}
							});
						}
					}});
			}
		};

		mModuleHolder.add(progress);
	}
	
	
	private void beginCriticCrusher() {
		GWT.log("Begin CriticCrusher");
		
		DataController.moduleIndex = 5;
		DataController.flushModuleProgress();
		DataController.startTimer();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new GraphicOrganizer());
		recap.setCloseHandler(new CloseHandler() {
			public void onClose(NavAccessiblePanel ui) {
				showCriticCrusher();
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void showCriticCrusher() {
		//TODO: Make sure the sidebar is shown/hidden when it needs to be
		mSidebar.show();
		mCurrentModule = new CriticCrusher(mSidebar);
		mCurrentModule.sidebar = mSidebar;
		mCurrentModule.handler = new ModuleUI.Handler() {
			public void onComplete() {
				endCriticCrusher();
			}
		};
		showUI(mCurrentModule);
		showHud();
	}

	private void endCriticCrusher() {
		GWT.log("End CriticCrusher");
		
		final Progress progress = new Progress(mSidebar, mOverlayHolder);
		progress.navHandler = new Progress.Handler() {
			@Override
			public void onRedo() {
				beginCriticCrusher();
			}
			
			@Override
			public void onContinue() {
				DataController.endTimer();
				
				DataController.saveModule(6, new ResultHandler(){
					@Override
					public void onResult(boolean success) {
						if(success){
							progress.removeFromParent();
							beginIntroducer();
						} else {
							showNetworkErrorPopup(new RetryHandler(){
								public void onRetry(){
									endCriticCrusher();
								}
							});
						}
					}});
			}
		};

		mModuleHolder.add(progress);
	}
	
	
	private void beginIntroducer() {
		GWT.log("Begin Introducer");
		DataController.moduleIndex = 6;
		DataController.flushModuleProgress();
		DataController.startTimer();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new GraphicOrganizer());
		recap.setCloseHandler(new CloseHandler() {
			public void onClose(NavAccessiblePanel ui) {
				showIntroducer();
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void showIntroducer() {
		//TODO: Make sure the sidebar is shown/hidden when it needs to be
				mSidebar.show();
		mCurrentModule = new Introducer(mSidebar);
		mCurrentModule.sidebar = mSidebar;
		mCurrentModule.handler = new ModuleUI.Handler() {
			public void onComplete() {
				endIntroducer();
			}
		};
		GWT.log("Game has not crashed yet");
		showUI(mCurrentModule);
		showHud();
	}
	
	private void endIntroducer() {
		GWT.log("End Introducer");
		
		final Progress progress = new Progress(mSidebar, mOverlayHolder);
		progress.navHandler = new Progress.Handler() {
			@Override
			public void onRedo() {
				beginIntroducer();
			}
			
			@Override
			public void onContinue() {
				DataController.endTimer();
				
				DataController.saveModule(7, new ResultHandler(){
					@Override
					public void onResult(boolean success) {
						if(success){
							progress.removeFromParent();
							beginConclusionCrafter();
						} else {
							showNetworkErrorPopup(new RetryHandler(){
								public void onRetry(){
									endIntroducer();
								}
							});
						}
					}});
			}
		};

		mModuleHolder.add(progress);
	}
	
	
	private void beginConclusionCrafter() {
		GWT.log("Begin ConclusionCrafter");
		DataController.moduleIndex = 7;
		DataController.flushModuleProgress();
		DataController.startTimer();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new GraphicOrganizer());
		recap.setCloseHandler(new CloseHandler() {
			public void onClose(NavAccessiblePanel ui) {
				showConclusionCrafter();
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void showConclusionCrafter() {
		//TODO: Make sure the sidebar is shown/hidden when it needs to be
				mSidebar.show();
		mCurrentModule = new ConclusionCrafter();
		mCurrentModule.sidebar = mSidebar;
		mCurrentModule.handler = new ModuleUI.Handler() {
			public void onComplete() {
				endConclusionCrafter();
			}
		};
		showUI(mCurrentModule);
		showHud();
	}
	
	private void endConclusionCrafter() {
		GWT.log("End ConclusionCrafter");
		
		final Progress progress = new Progress(mSidebar, mOverlayHolder);
		progress.navHandler = new Progress.Handler() {
			@Override
			public void onRedo() {
				beginConclusionCrafter();
			}
			
			@Override
			public void onContinue() {
				DataController.endTimer();
				
				DataController.saveModule(8, new ResultHandler(){
					@Override
					public void onResult(boolean success) {
						if(success){
							progress.removeFromParent();
							showEndNotification();
						} else {
							showNetworkErrorPopup(new RetryHandler(){
								public void onRetry(){
									endConclusionCrafter();
								}
							});
						}
					}});
			}
		};

		mModuleHolder.add(progress);
	}
	
	private void showEndNotification() {
		/*
		CompletionCertificate certificate = new CompletionCertificate();
		certificate.handler = new CompletionCertificate.Handler() {
			@Override
			public void onClose() {
				showFinalRecap();
			}
		};
		mModuleHolder.add(certificate);
		*/
		TextPopup popup = new TextPopup();
		popup.setTitle("Congrats!");
		popup.setBody("<p>You have completed Drafting Board! You will now have one last chance to edit your essay before the final submission</p>");
		popup.setHandler(new TextPopup.PopupHandler() {
			@Override
			public void onClose(TextPopup popup) {
				showFinalEdit();
			}
		});
		
		mOverlayHolder.add(popup);
		ElementUtil.setTextScaling();
	}
	
	private void showFinalEdit(){
		//mOverlayHolder.addStyleName("progress-popup");
		//mOverlayHolder.clear();
		
		Item[] items = new Item[6];
		for(int i =0; i < 3; i++){
			items[i*2] = DataController.getCurrentIssue().claim.reasons[i].reason.items[0];
			items[i*2 + 1] = DataController.getCurrentIssue().claim.reasons[i].reason.items[1];
		}
		
		mSidebar.init(items);
		mSidebar.show();
		
		final Summation summation = new Summation(false, mSidebar);
		summation.navHandler = new Summation.ModuleSummationHandler() {
			@Override
			public void onRedo() {
				summation.removeFromParent();
				//mOverlayHolder.removeStyleName("progress-popup");
			}
			@Override
			public void onContinue() {
				summation.removeFromParent();
				mSidebar.hide();
				showFinalRecap();
				//mOverlayHolder.removeStyleName("progress-popup");
			}
		};
		
		mModuleHolder.add(summation);
	}
	
	private void showFinalRecap() {
		DataController.moduleIndex = 8;
		DataController.flushModuleProgress();
		mHud.updateProgress();
		
		mSidebar.hide();
		
		Recap recap = new Recap(new FinalScreen());
		recap.setCloseHandler(new CloseHandler() {
			@Override
			public void onClose(NavAccessiblePanel ui) {
				restartGame();
			}
		});
		mModuleHolder.add(recap);
	}
	
	private void restartGame() {
		beginIssueAnalyzer();
	}
	
	
	private void showDebugMenu() {
		final ModuleSelector selector = new ModuleSelector();
		selector.handler = new ModuleSelector.Handler() {
			public void onIssueAnalyzer() {
				selector.removeFromParent();
				beginIssueAnalyzer();
			}
			
			public void onClaimCreator() {
				selector.removeFromParent();
				selectClaim();
				beginClaimCreator();
			}
			
			public void onParagraphBuilder() {
				selector.removeFromParent();
				selectClaim();
				selectReasons();
				beginParagraphBuilder(0);
			}
			
			public void onCriticCrusher() {
				selector.removeFromParent();
				selectClaim();
				selectReasons();
				loadCriticCrusher();
			}
			
			public void onIntro() {
				selector.removeFromParent();
				selectClaim();
				selectReasons();
				beginIntroducer();
			}
			
			public void onConclusion() {
				selector.removeFromParent();
				selectClaim();
				selectReasons();
				beginConclusionCrafter();
			}
			
			public void onEvidenceEvaluator() {
				selector.removeFromParent();
				
				GWT.log("Selected claim index being reset in icivicsthing1");
				DataController.getCurrentIssue().selectedClaimIndex = 0;
				EvidenceListPanel test = new EvidenceListPanel();
				test.setHandler(new EvidenceListPanel.Handler() {
					@Override
					public void onSelectItem(Item item) {
						GWT.log(item.name + " selected");
					}
				});
				mModuleHolder.add(test);
			}
			
			private void selectClaim() {
				if (DataController.getCurrentIssue().moduleIndex >= 1)
					return;
				
				GWT.log("Selected claim index being reset in icivicsthing2");
				DataController.getCurrentIssue().selectedClaimIndex = 0;
				DataController.getCurrentIssue().claim = new ClaimInstance(DataController.getCurrentIssue().issue.claims[0]);
				DataController.getCurrentIssue().counterClaim = new ClaimInstance(DataController.getCurrentIssue().issue.claims[1]);
			}
			
			private void selectReasons() {
				if (DataController.getCurrentIssue().moduleIndex >= 2)
					return;
				
				Reason[] input = DataController.getCurrentIssue().claim.claim.reasons;
				ReasonInstance[] output = DataController.getCurrentIssue().claim.reasons;
				for (int inputIndex = 0, outputIndex = 0; inputIndex < input.length && outputIndex < output.length; ++inputIndex)
					if (input[inputIndex].isCorrect)
						output[outputIndex++] = new ReasonInstance(input[inputIndex]);
				if (output[output.length - 1] == null)
					GWT.log("WARNING: Content error. Less than 3 valid Reasons were found in the claim.");
				input = DataController.getCurrentIssue().counterClaim.claim.reasons;
				output = DataController.getCurrentIssue().counterClaim.reasons;
				for (int inputIndex = 0, outputIndex = 0; inputIndex < input.length && outputIndex < output.length; ++inputIndex)
					if (input[inputIndex].isCorrect)
						output[outputIndex++] = new ReasonInstance(input[inputIndex]);
				if (output[output.length - 1] == null)
					GWT.log("WARNING: Content error. Less than 3 valid Reasons were found in the counterclaim.");
			}
		};
		mModuleHolder.add(selector);
	}
	
	private void beginCurrentModule() {
		GWT.log("Resuming Current Module");
		int moduleIndex = DataController.getCurrentIssue().moduleIndex;
		int stepIndex = DataController.getModuleProgress().getStepIndex();
		int timerSec = DataController.getModuleProgress().getTimerSeconds();
		
		DataController.moduleIndex = moduleIndex;
		DataController.moduleStepIndex = stepIndex;
		DataController.startTimer();
		DataController.setTimerSeconds(timerSec);
		mHud.updateProgress();
		
		if (stepIndex == 0) {
			beginModuleByIndex(moduleIndex);
		} else {
			resumeModuleByIndex(moduleIndex);
		}
	}
	
	private void beginModuleByIndex(int index) {
		mSidebar.show();
		switch (index) {
			case 0: beginIssueAnalyzer(); break;
			case 1: beginClaimCreator(); break;
			case 2: beginParagraphBuilder(0); break;
			case 3: beginParagraphBuilder(1); break;
			case 4: beginParagraphBuilder(2); break;
			case 5: loadCriticCrusher(); break;
			case 6: beginIntroducer(); break;
			case 7: beginConclusionCrafter(); break;
			case 8: showFinalRecap(); break;
		}
	}
	
	private void resumeModuleByIndex(int index) {
		mSidebar.show();
		switch (index) {
			case 0: showIssueAnalyzer(); break;
			case 1: showClaimCreator(); break;
			case 2: showParagraphBuilder(0); break;
			case 3: showParagraphBuilder(1); break;
			case 4: showParagraphBuilder(2); break;
			case 5: loadCriticCrusher(true); break;
			case 6: showIntroducer(); break;
			case 7: showConclusionCrafter(); break;
			case 8: 
				if (DataController.getModuleProgress().getStepIndex() > 0) {
					DataController.moduleIndex = 0;
					DataController.moduleStepIndex = DataController.getModuleProgress().getStepIndex();
					mHud.updateProgress();
					showIssueAnalyzer();
				} else {
					showFinalRecap();
				}
				break;
		}
	}
	
	public void reloadWithMenuHash(){
		GWT.log("This is actually getting called");
		
		UrlBuilder ub = Window.Location.createUrlBuilder();
		ub.setHash(DataController.MENU_URL_HASH);
		
		GWT.log(ub.buildString());
		
		Window.Location.replace(ub.buildString());
	}
	
	private native void setupDebugMenuFunction(ICivicsArgumentation control)/*-{
		//var control = this;
		$wnd.treesteps = function(){
			control.@org.icivics.arg.client.ICivicsArgumentation::reloadWithMenuHash()();
		};
	}-*/;
}
