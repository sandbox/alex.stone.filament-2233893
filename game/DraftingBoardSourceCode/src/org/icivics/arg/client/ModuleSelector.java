package org.icivics.arg.client;

import org.icivics.arg.ui.common.client.LabelButton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class ModuleSelector extends Composite {
	private static ModuleSelectorUiBinder uiBinder = GWT.create(ModuleSelectorUiBinder.class);
	interface ModuleSelectorUiBinder extends UiBinder<Widget, ModuleSelector> {}
	
	public static interface Handler {
		void onIssueAnalyzer();
		void onClaimCreator();
		void onParagraphBuilder();
		void onCriticCrusher();
		void onIntro();
		void onConclusion();
		void onEvidenceEvaluator();
	}
	public Handler handler;
	
	@UiField
	LabelButton issueAnalyzer;
	@UiField
	LabelButton claimCreator;
	@UiField
	LabelButton intro;
	@UiField
	LabelButton paragraphBuilder;
	@UiField
	LabelButton criticCrusher;
	@UiField
	LabelButton conclusion;
	
	public ModuleSelector() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@Override
	protected void onAttach() {
		super.onAttach();
		issueAnalyzer.clickHandler = new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (handler != null)
					handler.onIssueAnalyzer();
				removeFromParent();
			}
		};
		claimCreator.clickHandler = new ClickHandler() {
			public void onClick(ClickEvent event) {
				GWT.log("Hello");
				
				if (handler != null)
					handler.onClaimCreator();
				GWT.log("World");
				removeFromParent();
			}
		};
		intro.clickHandler = new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (handler != null)
					handler.onIntro();
				removeFromParent();
			}
		};
		paragraphBuilder.clickHandler = new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (handler != null)
					handler.onParagraphBuilder();
				removeFromParent();
			}
		};
		criticCrusher.clickHandler = new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (handler != null)
					handler.onCriticCrusher();
				removeFromParent();
			}
		};
		conclusion.clickHandler = new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (handler != null)
					handler.onConclusion();
				removeFromParent();
			}
		};
	}
	
	@Override
	protected void onDetach() {
		issueAnalyzer.destroy();
		claimCreator.destroy();
		intro.destroy();
		paragraphBuilder.destroy();
		criticCrusher.destroy();
		conclusion.destroy();
		handler = null;
		super.onDetach();
	}
}
