package org.icivics.arg.client;

import java.util.Date;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.Issue;
import org.icivics.arg.data.model.client.IssueInstance;
import org.icivics.arg.ui.global.client.Summation;
import org.icivics.arg.util.client.ElementUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.event.shared.UmbrellaException;
import com.google.gwt.user.client.ui.HTMLPanel;

public class ExceptionHandler implements GWT.UncaughtExceptionHandler {

	private HTMLPanel mDisplay;
	
	public ExceptionHandler(HTMLPanel display) {
		mDisplay = display;
		display.getElement().getStyle().setZIndex(5);
		display.addStyleName("error-log-window");
		
	}

	@Override
	public void onUncaughtException(Throwable e) {
		animateToTopPosition(mDisplay.getElement(),0);
		ElementUtil.addScrollbars(mDisplay.getElement());
		
		e.printStackTrace();
		
		Throwable unwrappedException = unwrap(e);
		textOutput("An exception has prevented the program from continuing.");
		textOutput("Here is the exception: " + unwrappedException.toString());
		unwrappedException.fillInStackTrace();
		Throwable lastCause;
		lastCause = unwrappedException.getCause();
		while (lastCause != null) {
			textOutput("Caused by: " + lastCause.toString());
			lastCause = unwrappedException.getCause();
		}
		textOutput("On module: " + DataController.moduleIndex + " (" + Summation.MODULE_TITLES[DataController.moduleIndex] + ")");
		textOutput("Step: " + DataController.moduleStepIndex);
		
		
		
		IssueInstance theIssue = DataController.getCurrentIssue();
		ClaimInstance theClaim = theIssue.claim;
		
		textOutput("The story is \"" + theIssue.issue.titleText + "\"");
		if(theIssue.selectedClaimIndex != -1)
			textOutput("The chosen claim is #" + theIssue.selectedClaimIndex + ": " + theClaim.getClaimText());
		
		
		textOutput("The chosen reasons:");
		for(int i = 0; i <theClaim.reasons.length; i++)
			textOutput("Reason " + (i+1) + ": " + theClaim.reasons[i].reason.text);
		
		Date date = new Date();
		textOutput("The date and time is: " + date.toString());
		
		
	}
	
	private void textOutput(String s){
		GWT.log(s);
		mDisplay.getElement().setInnerHTML(mDisplay.getElement().getInnerHTML() + "<p>" + s + "</p>");
	}
	
	private Throwable unwrap(Throwable e) {   
	    if(e instanceof UmbrellaException) {   
	      UmbrellaException ue = (UmbrellaException) e;  
	      if(ue.getCauses().size() == 1) {   
	        return unwrap(ue.getCauses().iterator().next());  
	      }  
	    }  
	    return e;  
	  }  
	
	private static native void animateToTopPosition(Element e, int newTop)/*-{
		$wnd.$(e).animate({top:newTop+"%"},500);
	}-*/;
	
	private static native void addScrollbars(Element e)/*-{
		$wnd.$(e).perfectScrollBars({
			wheelSpeed: 30,
			wheelPropagation: true,
			suppressScrollX: true,
			useKeyboard: false
		});
	}-*/;
	

}
