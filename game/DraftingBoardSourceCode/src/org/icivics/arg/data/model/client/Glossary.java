package org.icivics.arg.data.model.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.PopupPanel;

public class Glossary {
	private static Map<String,String> mDefinitions = new HashMap<String,String>();
	private static Map<String,String> mCapitalizedTerms = new HashMap<String,String>();
	
	public static PopupPanel popup = new PopupPanel(true);
	
	public static void addDefinition(String key, String term, String definition){
		String prevDef = mDefinitions.get(key.toLowerCase());
		if(prevDef == null){
			mCapitalizedTerms.put(key.toLowerCase(), term);
			mDefinitions.put(key.toLowerCase(), definition);
		} else {
			GWT.log("Trying to add a second definition to a word, " + key + ". the latest one, " + definition + " is ignored");
		}
	}
	
	public static String getDefinition(String term){
		return mDefinitions.get(term.toLowerCase());
	}
	
	public static String getCapitalizedTerm(String term){
		return mCapitalizedTerms.get(term.toLowerCase());
	}
	
	public static ArrayList<String> getTerms(){
		return new ArrayList<String>(mDefinitions.keySet());
	}
	
	public static String getGlossary(){
		String gls = "[\n";
		for(Map.Entry<String,String> e : mDefinitions.entrySet()){
			gls += "\t" + e.getKey() + " -- " + e.getValue() +"\n";
		}
		return gls + "]";
	}
}
