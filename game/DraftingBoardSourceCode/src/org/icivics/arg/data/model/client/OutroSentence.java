package org.icivics.arg.data.model.client;

public class OutroSentence {
	public static final int TYPE_STAT_QUOTE = 0;
	public static final int TYPE_NEXT_STEPS = 1;
	public static final int TYPE_CONSEQUENCES = 2;
	
	
	public String text;
	public String audioSrc;
	public int type = -1;

	public OutroSentence() {
		
	}
	
	public OutroSentence(int type, String text) {
		this.type = type;
		this.text = text;
	}
}
