package org.icivics.arg.data.model.client;

public class CriticCrusherData {
	public Item item;
	public Item rebuttalItem;
	
	public String[] openingStartOptions = new String[3];
	public String[] openingStartAudioURLs = new String[3];
	public String[] openingEndOptions = new String[3];
	public String[] openingEndAudioURLs = new String[3];
	
	public String[] strengthenerStartOptions = new String[3];
	public String[] strengthenerStartAudioURLs = new String[3];
	public String[] strengthenerEndOptions = new String[3];
	public String[] strengthenerEndAudioURLs = new String[3];
	
	public String[] rebuttalSentenceOptions = new String[3];
	public String[] rebuttalSentenceAudioURLs = new String[3];
	
	public String[] closingSentenceOptions = new String[3];
	public String[] closingSentenceAudioURLs = new String[3];
	
	public String clozeOpening;
	public String clozeStrengthener;
	public String clozeEvidenceSentence;
	public String clozeClosing;
	
	public CriticCrusherData() {
		
	}
}
