package org.icivics.arg.data.model.client;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;

public class Issue {
	public String titleText, fullText, shortText, imageSrc, audioSrc;
	public Claim[] claims = new Claim[2];
	public ArrayList<StoryChunk> storyChunks = new ArrayList<StoryChunk>();
	public Item[] items = new Item[40];
	
	public Issue() {
		
	}
	
	public int getClaimIndex(String id) {
		for (int i = 0; i < claims.length; ++i)
			if (claims[i].id.equals(id))
				return i;
		return -1;
	}

	public Claim getClaim(String id) {
		GWT.log("Geting claim " + id);
		for (int i = 0; i < claims.length; ++i)
			if (claims[i].id.equals(id))
				return claims[i];
		
		GWT.log("Claim " + id + " not found");
		return null;
	}

	public Item getItem(String id) {
		for (int i = 0; i < items.length && items[i] != null; ++i)
			if (items[i] != null && items[i].id.equals(id))
				return items[i];
		return null;
	}
}
