package org.icivics.arg.data.model.client;

public class Reason {
	public String id, text, mismatchText, audioSrc;
	public Item[] items = new Item[2];
	
	public boolean isCorrect = false;
	public boolean hasCounter = false;

	public String exampleConclusionSentence;
	
	public String[] conclusionCrafterPrewritten = new String[4]; 
	public String[] conclusionCrafterClozeStrings = new String[4]; 
	
	public Reason() {
		
	}

	public Reason(String text) {
		this.text = text;
	}
	
	public Reason(String text, Item item1, Item item2) {
		this.text = text;
		items[0] = item1;
		items[1] = item2;
	}
}
