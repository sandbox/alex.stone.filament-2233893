package org.icivics.arg.data.model.client;

import java.util.ArrayList;

public class Claim {
	public String[] restateClozeSentence = new String[4];
	public String id;
	public String text;
	public String imageSrc;
	public String audioSrc;
	public Reason[] reasons = new Reason[4];
	
	public IntroSentence[] introSentences = new IntroSentence[5];
	public OutroSentence[] outroSentences = new OutroSentence[5];

	public ArrayList<String> debateJumbles = new ArrayList<String>(4);
	public ArrayList<String> debateJumbleAudio = new ArrayList<String>(4);
	public String exampleConclusionSentence;
	
	public Claim() {
		
	}
	
	public Reason getReason(String id) {
		for (int i = 0; i < reasons.length; ++i)
			if (reasons[i].id.equals(id))
				return reasons[i];
		return null;
	}
}
