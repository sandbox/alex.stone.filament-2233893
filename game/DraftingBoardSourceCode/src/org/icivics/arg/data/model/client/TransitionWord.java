package org.icivics.arg.data.model.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TransitionWord {

	public static HashMap<String, String> types;
	private String mWord; // Does not have to be one word
	private String mTypeId;
	private String mDescription;
	private ArrayList<String> mExamples;
	//private static Map<>
	
	public TransitionWord(String word, String typeId, String description) {
		// TODO Auto-generated constructor stub
		mWord = word;
		mTypeId = typeId;
		mDescription = description;
		mExamples = new ArrayList<String>();
	}

	public void addExample(String example){
		mExamples.add(example);
	}
	
	public String getWord(){return mWord;}
	public String getDescription(){return mDescription;}
	public String getType(){return mTypeId;}
	public String getExamples(){
		String str = "";
		for(int i = 0; i< mExamples.size(); i++)
			str += "<p>" + mExamples.get(i) ;
		return str;
	}
}
