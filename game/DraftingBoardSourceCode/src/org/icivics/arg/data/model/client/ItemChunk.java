package org.icivics.arg.data.model.client;

public class ItemChunk {
	public static final String CLASS_TOKEN_STR = "**";
	public static final String CLASS_TOKEN_REGEX = "\\*\\*";
	public static final String DELIMITER_TOKEN_START_STR = "{*}";
	public static final String DELIMITER_TOKEN_START_REGEX = "\\{\\*}";
	public static final String DELIMITER_TOKEN_END_STR = "{/*}";
	public static final String DELIMITER_TOKEN_END_REGEX = "\\{\\/\\*}";
	public static final String REPLACEMENT_TOKEN_REGEX = "\\{token\\}";
	public static final String REPLACEMENT_TOKEN_STR = "{token}";
	public static final String REPLACEMENT_UNKNOWN_TEXT = " ________ ";
	
	public int index;
	public String itemText;
	public String sentenceText;
	public String audioSrc;
	
	
	public ItemChunk() {
		this(0, "", "");
	}
	
	public ItemChunk(int index, String text) {
		this(index, text, text);
	}
	
	public ItemChunk(int index, String itemText, String sentenceText) {
		this.index = index;
		this.itemText = itemText;
		this.sentenceText = sentenceText;
	}
}
