package org.icivics.arg.data.model.client;

public class ClaimInstance {
	public Claim claim;
	public String initialExplanation = "";
	public ReasonInstance[] reasons = new ReasonInstance[3];
	
	public boolean[] paragraphsSwapped = {false, false, false};
	
	public String introParagraph;
	public String introParagraphOriginal;
	
	public String[] paragraphs = new String[3];
	public String[] paragraphsOriginal = new String[3];
	
	public String rebuttalParagraph;
	public String rebuttalParagraphOriginal;
	
	public String conclusionParagraph;
	public String conclusionParagraphOriginal;

	public ClaimInstance(Claim claim) {
		this.claim = claim;
	}
	
	public String getClaimText() {
		return claim.text;
	}
}
