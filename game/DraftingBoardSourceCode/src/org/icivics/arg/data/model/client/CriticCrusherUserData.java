package org.icivics.arg.data.model.client;

import com.google.gwt.core.client.GWT;

public class CriticCrusherUserData {
	public CriticCrusherData data;
	public int openingSelection = -1;
	public int strengthenerSelection = -1;
	public int rebuttalSelection = -1;
	public int closingSelection = -1;
	
	public String openingCustomEnd;
	public String strengthenerCustomEnd;
	
	public String customParagraph;
	
	public CriticCrusherUserData(CriticCrusherData data) {
		this.data = data;
	}
	
	public String getSentenceText(int sentenceIndex) {
		switch (sentenceIndex) {
			case 0: return data.openingStartOptions[openingSelection].trim()+ " "
						+ (openingCustomEnd == null ? data.openingEndOptions[openingSelection] : openingCustomEnd).trim();
			case 1: return data.strengthenerStartOptions[strengthenerSelection].trim() + " "
						+ (strengthenerCustomEnd == null ? data.strengthenerEndOptions[strengthenerSelection] : strengthenerCustomEnd).trim();
			case 2: return data.rebuttalSentenceOptions[rebuttalSelection].trim();
			case 3: return data.closingSentenceOptions[closingSelection].trim();
			default:
				GWT.log("Invalid sentence index " + sentenceIndex);
				return null;
		}
	}
	
	public String buildParagraphText() {
		if(customParagraph != null){
			return customParagraph;
		} else {
			return getSentenceText(0) + " " + getSentenceText(1) + " " + getSentenceText(2) + " " + getSentenceText(3);
		}
	}
	
	public String[] getParagraphAudio(){
		String[] out = new String[6];
		
		out[0] = data.openingStartAudioURLs[openingSelection];
		out[1] = data.openingEndAudioURLs[openingSelection];
		out[2] = data.strengthenerStartAudioURLs[strengthenerSelection];
		out[3] = data.strengthenerEndAudioURLs[strengthenerSelection];
		out[4] = data.rebuttalSentenceAudioURLs[rebuttalSelection];
		out[5] = data.closingSentenceAudioURLs[closingSelection];
		
		return out;
		
	}
}
