package org.icivics.arg.data.model.client;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.regexp.shared.RegExp;

public class Item {
	public String id;
	public String name, text;
	public int type = 1;
	public ArrayList<ItemChunk> chunks = new ArrayList<ItemChunk>(3);
	public int correctChunkIndex;
	public String[] sentenceText = new String[2];
	
	public String initialTextStartAudioURL;
	public String initialTextEndAudioURL;
	//public String sentenceTextLvl1;
	//public String sentenceTextLvl2;
	public String imageSrc = null;
	public String audioSrc = null;
	public String summary = null;
	public String description = null;
	
	//FIXME: This is user data and should probably be stored in a user class
	public String customSentenceText;
	
	public String counterID = null;
	public String summaryAudioSrc;
	
	public Item() {
		this("", "");
	}
	
	public Item(int type) {
		this(type, "", "", "");
	}
	
	public Item(String text) {
		this(text, text);
	}
	
	public Item(int type, String text) {
		this(type, text, text, text);
	}
	
	public Item(String name, String text) {
		this(1, name, text, text);
	}
	
	public Item(String name, String text, String sentenceText) {
		this(1, name, text, sentenceText);
	}
	
	public Item(int type, String name, String text, String sentenceText) {
		this.type = type;
		this.name = name;
		this.text = text;
		this.sentenceText[0] = sentenceText;
		
		GWT.log(text);
	}
	
	public ItemChunk getCorrectChunk() {
		return chunks.get(correctChunkIndex);
	}
	
	public String getFullText() {
		String str = new String(text);
		for (int i = 0, count = chunks.size(); i < count; ++i) {
			RegExp nrgex = RegExp.compile(ItemChunk.REPLACEMENT_TOKEN_REGEX,"");
			str = nrgex.replace(str, chunks.get(i).itemText);
		}
		
		RegExp delimregex = RegExp.compile(ItemChunk.DELIMITER_TOKEN_END_REGEX + "|" + ItemChunk.DELIMITER_TOKEN_START_REGEX + "|" + ItemChunk.CLASS_TOKEN_REGEX,"g");
		str = delimregex.replace(str,"");
		
		return str;
		//return "<ol><li>One</li><li>Two</li><li>Three</li></ol>";
	}
	
	public String getTextForPC(){
		RegExp delimregex = RegExp.compile(ItemChunk.DELIMITER_TOKEN_END_REGEX + "|" + ItemChunk.DELIMITER_TOKEN_START_REGEX + "|" + ItemChunk.CLASS_TOKEN_REGEX,"g");
		return delimregex.replace(this.text,"");
	}
	
	public String getTextForSegmentation(){
		GWT.log("Getting text for segmentation: " + this.text);
		return this.text;
		
	}
	
	public String getFullSentenceText() {
		
		if (customSentenceText != null){
			return customSentenceText;
		}
		RegExp regex = RegExp.compile(ItemChunk.REPLACEMENT_TOKEN_REGEX,"g");
		return regex.replace(sentenceText[0], getCorrectChunk().sentenceText);
		//return sentenceText.replaceAll(ItemChunk.REPLACEMENT_TOKEN_REGEX, getCorrectChunk().sentenceText);
	}
	
	public String[] getTextPieces() {
		RegExp delimregex = RegExp.compile(ItemChunk.DELIMITER_TOKEN_END_REGEX + "|" + ItemChunk.DELIMITER_TOKEN_START_REGEX + "|" + ItemChunk.CLASS_TOKEN_REGEX,"g");
		
		String[] result = new String[chunks.size() + 1];
		int lastCharIndex = 0;
		for (int i = 0, count = chunks.size(); i < count; ++i) {
			int nextCharIndex = text.indexOf(ItemChunk.REPLACEMENT_TOKEN_STR, lastCharIndex);
			
			//assert nextCharIndex != -1 : "No insertion point for chunk " + i + " in '" + text + "'";
			if (nextCharIndex == -1) {
				result[i] = "";
				break;
			}
			
			result[i] = text.substring(lastCharIndex, nextCharIndex);
			result[i] = delimregex.replace(result[i],"");
			lastCharIndex = nextCharIndex + 7;
		}
		result[result.length - 1] = text.substring(lastCharIndex);
		return result;
	}
}
