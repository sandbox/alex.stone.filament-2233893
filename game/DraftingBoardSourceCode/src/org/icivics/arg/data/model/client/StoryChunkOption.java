package org.icivics.arg.data.model.client;

import com.google.gwt.regexp.shared.RegExp;

public class StoryChunkOption {
	public Item item;
	public String text;
	public String audioUrl;
	
	public StoryChunkOption() {
		
	}
	
	public StoryChunkOption(Item item, String text) {
		this.item = item;
		RegExp rgex = RegExp.compile("\\$","g");
		
		this.text = rgex.replace(text, "$$$$");
	}
}
