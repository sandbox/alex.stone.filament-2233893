package org.icivics.arg.data.model.client;

public class IntroSentence {
	public static final int TYPE_BACKGROUND_INFO = 0;
	public static final int TYPE_STAT_QUOTE = 1;
	public static final int TYPE_QUESTION = 2;

	public String hookAudioSrc = "";
	public String hook = "";
	public String transitionAudioSrc = "";
	public String transition = "";
	public int type = -1;
	
	public IntroSentence() {
		
	}
	
	public IntroSentence(int type, String hook, String transition) {
		assert type == 0 || type == 1 || type == 2 : "Invalid type.";
		this.type = type;
		this.hook = hook;
		this.transition = transition;
	}
}
