package org.icivics.arg.data.model.client;

public class IssueInstance {
	public Issue issue;
	public ClaimInstance claim;
	public ClaimInstance counterClaim;
	public int selectedClaimIndex = -1;
	public int selectedIntroIndex = -1;
	public int selectedOutroIndex = -1;
	public String writtenOutroSentences[] = new String[4];
	public Reason counterReason;
	
	public String claimSelectExplanation = "";
	public String reasonSelectExplanation = "";
	
	public int studentLevel = 0;
	
	public double scoreStoryChunks = 1.0,
			scoreEvidenceLinker = 1.0,
			scoreRebuttal = 1.0,
			scoreIntroducer = 1.0,
			scoreConclusion = 1.0,
			scoreParagraphs[] = {1.0, 1.0, 1.0};
	public int issueAnalyzerFirstPass,
			issueAnalyzerTotalFailures,
			claimCreatorFirstPass,
			claimCreatorTotalFailures;
	
	public int[] voteCounts = {0, 0};
	
	public int moduleIndex = 0;

	public IssueInstance(Issue issue) {
		this.issue = issue;
	}
	
	public Claim getSelectedClaim() {
		return claim.claim;//selectedClaimIndex == -1 ? null : issue.claims[selectedClaimIndex];
	}
	
	public Claim getOpposingClaim() {
		return counterClaim.claim;//selectedClaimIndex == -1 ? null : issue.claims[1 - selectedClaimIndex];
	}
	
	public IntroSentence getSelectedIntro() {
		if (selectedIntroIndex == -1 || claim == null)
			return null;
		
		return claim.claim.introSentences[selectedIntroIndex];
	}
	
	public OutroSentence getSelectedOutro() {
		if (selectedOutroIndex == -1 || claim == null)
			return null;
		
		return claim.claim.outroSentences[selectedOutroIndex];
	}
	
	public int getVotesForClaim(int claimIndex) {
		return voteCounts[claimIndex];
	}
	
	public float getVotePercentageForClaim(int claimIndex) {
		return (float) getVotesForClaim(claimIndex) / (float) getTotalVotes();
	}
	
	public int getTotalVotes() {
		int result = 0;
		for (int i = 0; i < voteCounts.length; ++i)
			result += voteCounts[i];
		return result;
	}
}
