package org.icivics.arg.data.model.client;

import com.google.gwt.regexp.shared.RegExp;

public class StoryChunk {
	public static final String REPLACEMENT_TOKEN = "\\{answer\\}";
	public static final String REPLACEMENT_UNKNOWN_TEXT = " ________ ";
	
	public String textBase;
	public StoryChunkOption[] options = new StoryChunkOption[3];
	public String correctItemID;
	public String audioUrlA;
	public String audioUrlB;
	
	public StoryChunk() {
		
	}
	
	public String getCorrectText() {
		if (correctItemID == null)
			return "ERROR: No correctItemID";
		for (int i = 0; i < options.length; ++i)
			if (options[i].item.id.equals(correctItemID)){
				RegExp rgex = RegExp.compile(REPLACEMENT_TOKEN,"g");
				return rgex.replace(textBase, options[i].text);
			}
		return "ERROR: No correct text available";
	}
	
	public String getOptionText(String id){
		for(int index = 0; index < options.length; index++){
			if(options[index].item.id.equals(id)){
				/*
				 * RegExp rgex = RegExp.compile("\\$","g");
		
					this.text = rgex.replace(text, "$$$$");
				 */
				
				//return textBase.replaceAll(REPLACEMENT_TOKEN, options[index].text );
				RegExp rgex = RegExp.compile(REPLACEMENT_TOKEN,"g");
				return rgex.replace(textBase, options[index].text);
			}
		}
		
		return "Option id " + id + " not found";
	}
}
