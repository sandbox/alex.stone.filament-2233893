package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.util.client.JSONUtil;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsArrayNumber;
import com.google.gwt.core.client.JsArrayString;

public class ModuleProgress {
	private static final String PROPERTY_STEP_INDEX = "stepIndex";
	private static final String PROPERTY_TIME_TAKEN = "timerSec";
	
	private JavaScriptObject data;
	
	/**
	 * Returns the current step index within the active module.
	 * If this value is zero, attempting to retrieve saved data may trigger an error.
	 * @return the current moduleStepIndex
	 */
	public int getStepIndex() { return getIntValue(PROPERTY_STEP_INDEX); }
	public void setStepIndex(int value) { setValue(PROPERTY_STEP_INDEX, value); }
	
	public int getTimerSeconds() { return getIntValue(PROPERTY_TIME_TAKEN); }
	public void setTimerSeconds(int value) { setValue(PROPERTY_TIME_TAKEN, value); }
	
	/**
	 * Returns the JSON representation of the progress data.
	 * @return the JSON representation of the progress data
	 */
	public String getJSON() {
		return JSONUtil.stringify(data);
	}
	
	public IssueAnalyzerProgress forIssueAnalyzer() {
		return new IssueAnalyzerProgress(data);
	}
	
	public ClaimCreatorProgress forClaimCreator() {
		return new ClaimCreatorProgress(data);
	}
	
	public ParagraphBuilderProgress forParagraphBuilder(int paragraphIndex) {
		return new ParagraphBuilderProgress(paragraphIndex, data);
	}
	
	public CriticCrusherProgress forCriticCrusher() {
		return new CriticCrusherProgress(data);
	}
	
	public IntroducerProgress forIntroducer() {
		return new IntroducerProgress(data);
	}
	
	public ConclusionCrafterProgress forConclusionCrafter() {
		return new ConclusionCrafterProgress(data);
	}
	
	public ModuleProgress() {
		this(JavaScriptObject.createObject());
	}
	
	public ModuleProgress(String dataJSON) {
		this(JSONUtil.parse(dataJSON));
	}
	
	public ModuleProgress(JavaScriptObject data) {
		this.data = data == null ? JavaScriptObject.createObject() : data;
	}
	
	public void applyProgress() {
		DataController.setTimerSeconds(getTimerSeconds());
	}
	
	public void clear() {
		data = JavaScriptObject.createObject();
	}
	
	protected final int getIntValue(String propName) {
		return getIntValueJS(data, propName);
	}
	protected native int getIntValueJS(JavaScriptObject obj, String propName) /*-{
		var parsed = parseInt(obj[propName]);
		if (isNaN(parsed))
			return 0;
		return parsed;
	}-*/;
	
	protected final int[] getIntValues(String propName) {
		JsArrayInteger srcArray = getIntValuesJS(data, propName);
		int[] result = new int[srcArray.length()];
		for (int i = 0; i < result.length; ++i)
			result[i] = srcArray.get(i);
		return result;
	}
	private native JsArrayInteger getIntValuesJS(JavaScriptObject obj, String propName) /*-{
		return obj[propName] || [];
	}-*/;
	
	protected final float getFloatValue(String propName) {
		return getFloatValueJS(data, propName);
	}
	private native float getFloatValueJS(JavaScriptObject obj, String propName) /*-{
		var parsed = parseFloat(obj[propName]);
		if (isNaN(parsed))
			return 0;
		return parsed;
	}-*/;
	
	protected final float[] getFloatValues(String propName) {
		JsArrayNumber srcArray = getFloatValuesJS(data, propName);
		float[] result = new float[srcArray.length()];
		for (int i = 0; i < result.length; ++i)
			result[i] = (float) srcArray.get(i);
		return result;
	}
	private native JsArrayNumber getFloatValuesJS(JavaScriptObject obj, String propName) /*-{
		return obj[propName] || [];
	}-*/;
	
	protected final String getStringValue(String propName) {
		return getStringValueJS(data, propName);
	}
	private native String getStringValueJS(JavaScriptObject obj, String propName) /*-{
		return String(obj[propName]);
	}-*/;
	
	protected final String[] getStringValues(String propName) {
		JsArrayString srcArray = getStringValuesJS(data, propName);
		String[] result = new String[srcArray.length()];
		for (int i = 0; i < result.length; ++i)
			result[i] = srcArray.get(i);
		return result;
	}
	private native JsArrayString getStringValuesJS(JavaScriptObject obj, String propName) /*-{
		return obj[propName] || [];
	}-*/;
	
	protected final void setValue(String propName, int value) {
		setValueJS(data, propName, value);
	}
	private native void setValueJS(JavaScriptObject obj, String propName, int value) /*-{
		obj[propName] = value;
	}-*/;
	
	protected final void setValue(String propName, float value) {
		setValueJS(data, propName, value);
	}
	private native void setValueJS(JavaScriptObject obj, String propName, float value) /*-{
		obj[propName] = value;
	}-*/;
	
	protected final void setValue(String propName, String value) {
		setValueJS(data, propName, value);
	}
	private native void setValueJS(JavaScriptObject obj, String propName, String value) /*-{
		obj[propName] = value;
	}-*/;
	
	protected final void setValue(String propName, int[] value) {
		JsArrayInteger jsList = (JsArrayInteger) JavaScriptObject.createArray();
		for (int i = 0; i < value.length; ++i)
			jsList.set(i, value[i]);
		setValueJS(data, propName, jsList);
	}
	private native void setValueJS(JavaScriptObject obj, String propName, JsArrayInteger value) /*-{
		obj[propName] = value;
	}-*/;
	
	protected final void setValue(String propName, float[] value) {
		JsArrayNumber jsList = (JsArrayNumber) JavaScriptObject.createArray();
		for (int i = 0; i < value.length; ++i)
			jsList.set(i, value[i]);
		setValueJS(data, propName, jsList);
	}
	private native void setValueJS(JavaScriptObject obj, String propName, JsArrayNumber value) /*-{
		obj[propName] = value;
	}-*/;
	
	protected final void setValue(String propName, String[] value) {
		JsArrayString jsList = (JsArrayString) JavaScriptObject.createArray();
		for (int i = 0; i < value.length; ++i)
			jsList.set(i, value[i]);
		setValueJS(data, propName, jsList);
	}
	private native void setValueJS(JavaScriptObject obj, String propName, JsArrayString value) /*-{
		obj[propName] = value;
	}-*/;
}
