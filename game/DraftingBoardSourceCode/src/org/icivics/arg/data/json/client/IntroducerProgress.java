package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;

import com.google.gwt.core.client.JavaScriptObject;

public class IntroducerProgress extends ModuleProgress {
	//Categorize
	private static final String PROPERTY_CATEGORIZE_FAILURES = "ii_catFails";
	
	public int getCategorizeFailures() { return getIntValue(PROPERTY_CATEGORIZE_FAILURES); }
	public void setCategorizeFailures(int value) { setValue(PROPERTY_CATEGORIZE_FAILURES, value); }
	
	//IssueSelect
	private static final String PROPERTY_SELECTED_INTRO_INDEX = "ii_introIndex";
	
	public int getSelectedIntroIndex() { return getIntValue(PROPERTY_SELECTED_INTRO_INDEX); }
	public void setSelectedIntroIndex(int value) { setValue(PROPERTY_SELECTED_INTRO_INDEX, value); }
	
	//ParagraphTransitions
	private static final String PROPERTY_PARAGRAPH_ORIGINAL = "ii_original";
	
	public String getOriginalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_ORIGINAL); }
	public void setOriginalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_ORIGINAL, value); }
	
	//ParagraphEdit
	private static final String PROPERTY_PARAGRAPH_FINAL = "ii_final";
	
	public String getFinalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_FINAL); }
	public void setFinalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_FINAL, value); }
	
	
	public IntroducerProgress() {
		
	}

	public IntroducerProgress(String dataJSON) {
		super(dataJSON);
	}

	public IntroducerProgress(JavaScriptObject data) {
		super(data);
	}
	
	@Override
	public void applyProgress() {
		super.applyProgress();
		int stepIndex = getStepIndex();
		
		if (stepIndex >= 2) {
			DataController.getCurrentIssue().selectedIntroIndex = getSelectedIntroIndex();
		}
		if (stepIndex >= 3) {
			DataController.getCurrentIssue().claim.introParagraphOriginal = getOriginalParagraph();
		}
		if (stepIndex >= 4) {
			DataController.getCurrentIssue().claim.introParagraph = getFinalParagraph();
		}
	}
}
