package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.IssueInstance;

import com.google.gwt.core.client.JavaScriptObject;

public class IssueAnalyzerProgress extends ModuleProgress {
	private static final String PROPERTY_CHUNK_FAILURES = "ia_chunkFails";
	private static final String PROPERTY_CHUNKS_CORRECT = "ia_correctChunks";
	
	private static final String PROPERTY_VOTE_CLAIM_INDEX = "ia_votedClaimIndex";
	private static final String PROPERTY_VOTE_EXPLANATION = "ia_votedClaimExpl";
	
	public int getChunkFailures() { return getIntValue(PROPERTY_CHUNK_FAILURES); }
	public void setChunkFailures(int value) { setValue(PROPERTY_CHUNK_FAILURES, value); }
	
	public int getCorrectChunks() { return getIntValue(PROPERTY_CHUNKS_CORRECT); }
	public void setCorrectChunks(int value) { setValue(PROPERTY_CHUNKS_CORRECT, value); }
	
	public int getSelectedClaimIndex() { return getIntValue(PROPERTY_VOTE_CLAIM_INDEX); }
	public void setSelectedClaimIndex(int value) { setValue(PROPERTY_VOTE_CLAIM_INDEX, value); }
	
	public String getSelectedClaimExplanation() { return getStringValue(PROPERTY_VOTE_EXPLANATION); }
	public void setSelectedClaimExplanation(String value) { setValue(PROPERTY_VOTE_EXPLANATION, value); }
	
	

	
	private static final String PROPERTY_CHUNK_SELECTIONS = "ia_chunkSelections";
	public String[] getChunkSelections(){ return getStringValues(PROPERTY_CHUNK_SELECTIONS);}
	public void setChunkSelections(String[] values){setValue(PROPERTY_CHUNK_SELECTIONS, values);}
	
	public IssueAnalyzerProgress() {
		
	}

	public IssueAnalyzerProgress(String dataJSON) {
		super(dataJSON);
	}

	public IssueAnalyzerProgress(JavaScriptObject data) {
		super(data);
	}
	
	@Override
	public void applyProgress() {
		super.applyProgress();
		int stepIndex = getStepIndex();
		stepIndex = 0;
		
		if (stepIndex >= 3) {
			IssueInstance issue = DataController.getCurrentIssue();
			issue.selectedClaimIndex = getSelectedClaimIndex();
			
			issue.claim = new ClaimInstance(issue.issue.claims[issue.selectedClaimIndex]);
			issue.counterClaim = new ClaimInstance(issue.issue.claims[issue.selectedClaimIndex == 0 ? 1 : 0]);
			issue.claimSelectExplanation = getSelectedClaimExplanation();
		}
	}
	
	@Override
	protected native int getIntValueJS(JavaScriptObject obj, String propName) /*-{
		var parsed = parseInt(obj[propName]);
		if (isNaN(parsed))
			return -1;
		return parsed;
	}-*/;
}
