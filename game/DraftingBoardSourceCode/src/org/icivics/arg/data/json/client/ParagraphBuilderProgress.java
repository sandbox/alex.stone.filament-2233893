package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.ReasonInstance;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;

public class ParagraphBuilderProgress extends ModuleProgress {
	public static int[][] SCAFFOLDING_LEVELS = {{0,1,2},
												 {0,2,2},
												 {1,1,1},
												 {1,1,2},
												 {1,2,2},
												 {2,2,2},
												 {0,0,0}};
	
	protected int paragraphIndex = 0;
	
	
	
	//SentenceOrdering
	private static final String PROPERTY_SWAPPED = "pb_swapped";
	private static final String PROPERTY_ORDER_FAILURES = "pb_orderFails";
	
	public boolean getParagraphSwapped() { return getIntValue(PROPERTY_SWAPPED) != 0; }
	public void setParagraphSwapped(boolean value) { setValue(PROPERTY_SWAPPED, value ? 1 : 0); }
	
	public int getOrderFailures() { return getIntValue(PROPERTY_ORDER_FAILURES); }
	public void setOrderFailures(int value) { setValue(PROPERTY_ORDER_FAILURES, value); }
	
	//AddEvidence?
	private static final String PROPERTY_CUSTOM_ITEM_TEXT = "pb_custItemText%";
	
	public String getCustomItemText(int index) { return getStringValue(PROPERTY_CUSTOM_ITEM_TEXT.replaceFirst("%", Integer.toString(index))); }
	public void setCustomItemText(int index, String value) { setValue(PROPERTY_CUSTOM_ITEM_TEXT.replaceFirst("%", Integer.toString(index)), value); }
	
	//AddTransitions
	private static final String PROPERTY_PARAGRAPH_ORIGINAL = "pb_original";
	
	public String getOriginalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_ORIGINAL); }
	public void setOriginalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_ORIGINAL, value); }
	
	//ParagraphEdit
	private static final String PROPERTY_PARAGRAPH_FINAL = "pb_final";
	
	public String getFinalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_FINAL); }
	public void setFinalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_FINAL, value); }
	
	//Scaffold Level
	private static final String PROPERTY_PARAGRAPH_SCAFFOLD = "pb_level";
	
	public int getParagraphLevel() {return getIntValue(PROPERTY_PARAGRAPH_SCAFFOLD);}
	public void setParagraphLevel(int value){setValue(PROPERTY_PARAGRAPH_SCAFFOLD, value);}
	
	
	public ParagraphBuilderProgress(int paragraphIndex) {
		this.paragraphIndex = paragraphIndex;
	}

	public ParagraphBuilderProgress(int paragraphIndex, String dataJSON) {
		super(dataJSON);
		this.paragraphIndex = paragraphIndex;
	}

	public ParagraphBuilderProgress(int paragraphIndex, JavaScriptObject data) {
		super(data);
		this.paragraphIndex = paragraphIndex;
	}
	
	@Override
	public void applyProgress() {
		super.applyProgress();
		int stepIndex = getStepIndex();
		ClaimInstance claim = DataController.getCurrentIssue().claim;
		
		int paragraphLevel = SCAFFOLDING_LEVELS[DataController.getCurrentIssue().studentLevel][paragraphIndex];
		
		GWT.log("Loading PC progress, for paragraph index " + paragraphIndex + " at scaffolding level " + paragraphLevel);
		
		switch(paragraphLevel){
		case 0:
			GWT.log("Loading a paragraph at scaffolding level 0");
			if (stepIndex >= 1) {
				claim.paragraphsSwapped[paragraphIndex] = getParagraphSwapped();
				GWT.log("Loaded paragraph sentence swap status");
			}
			if (stepIndex >= 2) {
			
				ReasonInstance reason = claim.reasons[paragraphIndex];
				for (int i = 0; i < reason.reason.items.length; ++i) {
					claim.reasons[paragraphIndex].reason.items[i].customSentenceText = getCustomItemText(i);
				}
				
				GWT.log("Loaded paragraph custom sentence text");
			}
			if (stepIndex >= 3) {
				claim.paragraphsOriginal[paragraphIndex] = getOriginalParagraph();
			}
			if (stepIndex >= 4) {
				claim.paragraphs[paragraphIndex] = getFinalParagraph();
			}
			break;
			
		case 1:
			GWT.log("Loading a paragraph at scaffolding level 1");
			if (stepIndex >= 1) {
				claim.paragraphsSwapped[paragraphIndex] = getParagraphSwapped();
				GWT.log("Loaded paragraph sentence swap status");
			}
			if (stepIndex >= 3) {
				// Cloze sentences take up two steps
				ReasonInstance reason = claim.reasons[paragraphIndex];
				for (int i = 0; i < reason.reason.items.length; ++i) {
					claim.reasons[paragraphIndex].reason.items[i].customSentenceText = getCustomItemText(i);
				}
				
				GWT.log("Loaded paragraph custom sentence text");
			}
			if (stepIndex >= 4) {
				claim.paragraphsOriginal[paragraphIndex] = getOriginalParagraph();
			}
			if (stepIndex >= 5) {
				claim.paragraphs[paragraphIndex] = getFinalParagraph();
			}
			break;
		case 2:
			GWT.log("Loading a paragraph at scaffolding level 2");
			if(stepIndex >= 1){
				claim.paragraphsOriginal[paragraphIndex] = getOriginalParagraph();
				claim.paragraphs[paragraphIndex] = getFinalParagraph();
			}
			break;
		default:
			GWT.log("Somehow, the scaffolding level is invalid: " + paragraphLevel);
		
		}
		/*
		if (paragraphIndex < 2) {
			if (stepIndex >= 1) {
				claim.paragraphsSwapped[paragraphIndex] = getParagraphSwapped();
				GWT.log("Loaded paragraph sentence swap status");
			}
			if (stepIndex >= 2) {
			
				ReasonInstance reason = claim.reasons[paragraphIndex];
				for (int i = 0; i < reason.reason.items.length; ++i) {
					claim.reasons[paragraphIndex].reason.items[i].customSentenceText = getCustomItemText(i);
				}
				
				GWT.log("Loaded paragraph custom sentence text");
			}
			if (stepIndex >= 3) {
				claim.paragraphsOriginal[paragraphIndex] = getOriginalParagraph();
			}
			if (stepIndex >= 4) {
				claim.paragraphs[paragraphIndex] = getFinalParagraph();
			}
		} else {
			if (stepIndex >= 1) {
				claim.paragraphsOriginal[paragraphIndex] = getOriginalParagraph();
				claim.paragraphs[paragraphIndex] = getFinalParagraph();
			}
		}
		*/
	}
}
