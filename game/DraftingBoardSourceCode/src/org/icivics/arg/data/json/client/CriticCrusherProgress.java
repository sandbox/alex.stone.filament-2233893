package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.CriticCrusherUserData;

import com.google.gwt.core.client.JavaScriptObject;

public class CriticCrusherProgress extends ModuleProgress {
	//FragmentSelectionUI
	private static final String PROPERTY_CORRECT_SELECTIONS = "cr_correctFrags";
	private static final String PROPERTY_SELECTION_FAILURES = "cr_fragFails";
	
	public int getCorrectFragmentSelections() { return getIntValue(PROPERTY_CORRECT_SELECTIONS); }
	public void setCorrectFragmentSelections(int value) { setValue(PROPERTY_CORRECT_SELECTIONS, value); }
	
	public int getFragmentSelectionFailures() { return getIntValue(PROPERTY_SELECTION_FAILURES); }
	public void setFragmentSelectionFailures(int value) { setValue(PROPERTY_SELECTION_FAILURES, value); }
	
	//PinningReasons
	private static final String PROPERTY_CLAIM_REASON_IDS = "cl_claimReasons";
	private static final String PROPERTY_COUNTERCLAIM_REASON_IDS = "cl_counterClaimReasons";
	
	public String[] getClaimReasonIDs() { return getStringValues(PROPERTY_CLAIM_REASON_IDS); }
	public void setClaimReasonIDs(String[] value) { setValue(PROPERTY_CLAIM_REASON_IDS, value); }
	
	public String[] getCounterClaimReasonIDs() { return getStringValues(PROPERTY_COUNTERCLAIM_REASON_IDS); }
	public void setCounterClaimReasonIDs(String[] value) { setValue(PROPERTY_COUNTERCLAIM_REASON_IDS, value); }
	
	//ReasonSelection
	public static final String PROPERTY_REASON_TO_CRUSH = "cr_crushReason";
	
	public String getCriticCrushReasonID(){return getStringValue(PROPERTY_REASON_TO_CRUSH);}
	public void setCriticCrushReasonID(String value){setValue(PROPERTY_REASON_TO_CRUSH, value);}
	
	//OpeningSentence + OpeningSentenceCustom
	private static final String PROPERTY_OPENING_INDEX = "cr_openingIndex%";
	private static final String PROPERTY_OPENING_END = "cr_openingEnd%";
	
	public int getOpeningIndex(int paragraphIndex) { return getIntValue(PROPERTY_OPENING_INDEX.replaceFirst("%", Integer.toString(paragraphIndex))); }
	public void setOpeningIndex(int paragraphIndex, int value) { setValue(PROPERTY_OPENING_INDEX.replaceFirst("%", Integer.toString(paragraphIndex)), value); }
	
	public String getOpeningEnd(int paragraphIndex) { return getStringValue(PROPERTY_OPENING_END.replaceFirst("%", Integer.toString(paragraphIndex))); }
	public void setOpeningEnd(int paragraphIndex, String value) { setValue(PROPERTY_OPENING_END.replaceFirst("%", Integer.toString(paragraphIndex)), value); }
	
	//RebuttalStrengthener + RebuttalStrengthenerCustom
	private static final String PROPERTY_STRENGTHENER_INDEX = "cr_strengthenerIndex%";
	private static final String PROPERTY_STRENGTHENER_END = "cr_strengthenerEnd%";
	
	public int getStrengthenerIndex(int paragraphIndex) { return getIntValue(PROPERTY_STRENGTHENER_INDEX.replaceFirst("%", Integer.toString(paragraphIndex))); }
	public void setStrengthenerIndex(int paragraphIndex, int value) { setValue(PROPERTY_STRENGTHENER_INDEX.replaceFirst("%", Integer.toString(paragraphIndex)), value); }
	
	public String getStrengthenerEnd(int paragraphIndex) { return getStringValue(PROPERTY_STRENGTHENER_END.replaceFirst("%", Integer.toString(paragraphIndex))); }
	public void setStrengthenerEnd(int paragraphIndex, String value) { setValue(PROPERTY_STRENGTHENER_END.replaceFirst("%", Integer.toString(paragraphIndex)), value); }
	
	//RebuttalEvidence
	private static final String PROPERTY_REBUTTAL_INDEX = "cr_rebuttalIndex%";
	
	public int getRebuttalIndex(int paragraphIndex) { return getIntValue(PROPERTY_REBUTTAL_INDEX.replaceFirst("%", Integer.toString(paragraphIndex))); }
	public void setRebuttalIndex(int paragraphIndex, int value) { setValue(PROPERTY_REBUTTAL_INDEX.replaceFirst("%", Integer.toString(paragraphIndex)), value); }
	
	//ClosingAssertion
	private static final String PROPERTY_CLOSING_INDEX = "cr_closingIndex%";
	
	public int getClosingIndex(int paragraphIndex) { return getIntValue(PROPERTY_CLOSING_INDEX.replaceFirst("%", Integer.toString(paragraphIndex))); }
	public void setClosingIndex(int paragraphIndex, int value) { setValue(PROPERTY_CLOSING_INDEX.replaceFirst("%", Integer.toString(paragraphIndex)), value); }
	
	//ParagraphSelection
	private static final String PROPERTY_SELECTED_PARAGRAPH_INDEX = "cr_pIndex";
	
	public int getSelectedParagraphIndex() { return getIntValue(PROPERTY_SELECTED_PARAGRAPH_INDEX); }
	public void setSelectedParagraphIndex(int value) { setValue(PROPERTY_SELECTED_PARAGRAPH_INDEX, value); }
	
	//ParagraphTransitions
	private static final String PROPERTY_PARAGRAPH_ORIGINAL = "cr_original";
	
	public String getOriginalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_ORIGINAL); }
	public void setOriginalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_ORIGINAL, value); }
	
	//ParagraphChecklist
	private static final String PROPERTY_PARAGRAPH_FINAL = "cr_final";
	
	public String getFinalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_FINAL); }
	public void setFinalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_FINAL, value); }
	
	
	public CriticCrusherProgress() {
		
	}

	public CriticCrusherProgress(String dataJSON) {
		super(dataJSON);
	}

	public CriticCrusherProgress(JavaScriptObject data) {
		super(data);
	}
	
	@Override
	public void applyProgress() {
		super.applyProgress();
		int stepIndex = getStepIndex();
		if (stepIndex == 0 || !DataController.isCriticCrusherAvailable())
			return;
		
		int paragraphIndex = 0;
		CriticCrusherUserData paragraphData = DataController.getCriticCrusherUserData(paragraphIndex);
		
		if (stepIndex >= 1) {
			paragraphData.openingSelection = getOpeningIndex(paragraphIndex);
		}
		if (stepIndex >= 2) {
			paragraphData.strengthenerSelection = getStrengthenerIndex(paragraphIndex);
		}
		if (stepIndex >= 3) {
			paragraphData.rebuttalSelection = getRebuttalIndex(paragraphIndex);
		}
		if (stepIndex >= 4) {
			paragraphData.closingSelection = getClosingIndex(paragraphIndex);
		}
		
		paragraphIndex = 1;
		paragraphData = DataController.getCriticCrusherUserData(paragraphIndex);
		
		if (stepIndex >= 5) {
			paragraphData.openingSelection = getOpeningIndex(paragraphIndex);
			paragraphData.openingCustomEnd = getOpeningEnd(paragraphIndex);
		}
		if (stepIndex >= 6) {
			paragraphData.strengthenerSelection = getStrengthenerIndex(paragraphIndex);
			paragraphData.strengthenerCustomEnd = getStrengthenerEnd(paragraphIndex);
		}
		if (stepIndex >= 7) {
			paragraphData.rebuttalSelection = getRebuttalIndex(paragraphIndex);
		}
		if (stepIndex >= 8) {
			paragraphData.closingSelection = getClosingIndex(paragraphIndex);
		}
		
		if (stepIndex >= 10) {
			DataController.getCurrentIssue().claim.rebuttalParagraphOriginal = getOriginalParagraph();
		}
		if (stepIndex >= 11) {
			DataController.getCurrentIssue().claim.rebuttalParagraph = getFinalParagraph();
		}
	}
}
