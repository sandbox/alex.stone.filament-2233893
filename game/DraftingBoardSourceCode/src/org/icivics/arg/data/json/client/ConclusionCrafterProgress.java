package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.IssueInstance;

import com.google.gwt.core.client.JavaScriptObject;

public class ConclusionCrafterProgress extends ModuleProgress {
	//SentenceWriting
	private static final String PROPERTY_WRITTEN_SENTENCE = "co_sentence%";
	private static final String PROPERTY_SENTENCE_FAILURES = "co_sentenceFails";
	
	public String getWrittenSentence(int index) { return getStringValue(PROPERTY_WRITTEN_SENTENCE.replaceFirst("%", Integer.toString(index))); }
	public void setWrittenSentence(int index, String value) { setValue(PROPERTY_WRITTEN_SENTENCE.replaceFirst("%", Integer.toString(index)), value); }
	
	public int getSentenceFailures() { return getIntValue(PROPERTY_SENTENCE_FAILURES); }
	public void setSentenceFailures(int value) { setValue(PROPERTY_SENTENCE_FAILURES, value); }
	
	//SentenceSelection
	private static final String PROPERTY_SELECTED_OUTRO_INDEX = "ii_outroIndex";
	
	public int getSelectedOutroIndex() { return getIntValue(PROPERTY_SELECTED_OUTRO_INDEX); }
	public void setSelectedOutroIndex(int value) { setValue(PROPERTY_SELECTED_OUTRO_INDEX, value); }
	
	//Claim restatement
	private static final String PROPERTY_FIRST_SENTENCE = "co_firstSentence";
	public String getFirstSentence(){return getStringValue(PROPERTY_FIRST_SENTENCE);}
	public void setFirstSentence(String value){setValue(PROPERTY_FIRST_SENTENCE, value);}
	
	//First reason
	private static final String PROPERTY_SECOND_SENTENCE = "co_secondSentence";
	public String getSecondSentence(){return getStringValue(PROPERTY_SECOND_SENTENCE);}
	public void setSecondSentence(String value){setValue(PROPERTY_SECOND_SENTENCE, value);}
	
	//Second reason
	private static final String PROPERTY_THIRD_SENTENCE = "co_thirdSentence";
	public String getThirdSentence(){return getStringValue(PROPERTY_THIRD_SENTENCE);}
	public void setThirdSentence(String value){setValue(PROPERTY_THIRD_SENTENCE, value);}
	
	//Third reason
	private static final String PROPERTY_FOURTH_SENTENCE = "co_fourthSentence";
	public String getFourthSentence(){return getStringValue(PROPERTY_FOURTH_SENTENCE);}
	public void setFourthSentence(String value){setValue(PROPERTY_FOURTH_SENTENCE, value);}
	
	//EditParagraph
	private static final String PROPERTY_PARAGRAPH_ORIGINAL = "co_original";
	private static final String PROPERTY_PARAGRAPH_FINAL = "co_final";
	
	public String getOriginalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_ORIGINAL); }
	public void setOriginalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_ORIGINAL, value); }
	
	public String getFinalParagraph() { return getStringValue(PROPERTY_PARAGRAPH_FINAL); }
	public void setFinalParagraph(String value) { setValue(PROPERTY_PARAGRAPH_FINAL, value); }
	
	
	public ConclusionCrafterProgress() {
		
	}

	public ConclusionCrafterProgress(String dataJSON) {
		super(dataJSON);
	}

	public ConclusionCrafterProgress(JavaScriptObject data) {
		super(data);
	}
	
	@Override
	public void applyProgress() {
		super.applyProgress();
		int stepIndex = getStepIndex();
		IssueInstance issue = DataController.getCurrentIssue();
		
		for (int i = 0; i < stepIndex && i < issue.writtenOutroSentences.length; ++i) {
			issue.writtenOutroSentences[i] = getWrittenSentence(i);
		}
		if (stepIndex >= 6) {
			DataController.getCurrentIssue().selectedOutroIndex = getSelectedOutroIndex();
		}
		if (stepIndex >= 7) {
			DataController.getCurrentIssue().claim.conclusionParagraphOriginal = getOriginalParagraph();
			DataController.getCurrentIssue().claim.conclusionParagraph = getFinalParagraph();
		}
	}
}
