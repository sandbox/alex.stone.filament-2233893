package org.icivics.arg.data.json.client;

import org.icivics.arg.data.client.DataController;
import org.icivics.arg.data.model.client.ClaimInstance;
import org.icivics.arg.data.model.client.IssueInstance;
import org.icivics.arg.data.model.client.ReasonInstance;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;

public class ClaimCreatorProgress extends ModuleProgress {
	//Debate
	private static final String PROPERTY_CLAIM_INDEX = "cl_claimIndex";
	private static final String PROPERTY_CLAIM_EXPLANATION = "cl_claimExpl";
	private static final String PROPERTY_COUNTERCLAIM_EXPLANATION = "cl_counterClaimExpl";
	
	public int getSelectedClaimIndex() { return getIntValue(PROPERTY_CLAIM_INDEX); }
	public void setSelectedClaimIndex(int value) { setValue(PROPERTY_CLAIM_INDEX, value); }
	
	public String getClaimExplanation() { return getStringValue(PROPERTY_CLAIM_EXPLANATION); }
	public void setClaimExplanation(String value) { setValue(PROPERTY_CLAIM_EXPLANATION, value); }
	
	public String getCounterClaimExplanation() { return getStringValue(PROPERTY_COUNTERCLAIM_EXPLANATION); }
	public void setCounterClaimExplanation(String value) { setValue(PROPERTY_COUNTERCLAIM_EXPLANATION, value); }
	
	//PinningReasons
	private static final String PROPERTY_CLAIM_REASON_IDS = "cl_claimReasons";
	private static final String PROPERTY_COUNTERCLAIM_REASON_IDS = "cl_counterClaimReasons";
	
	public String[] getClaimReasonIDs() { return getStringValues(PROPERTY_CLAIM_REASON_IDS); }
	public void setClaimReasonIDs(String[] value) { setValue(PROPERTY_CLAIM_REASON_IDS, value); }
	
	public String[] getCounterClaimReasonIDs() { return getStringValues(PROPERTY_COUNTERCLAIM_REASON_IDS); }
	public void setCounterClaimReasonIDs(String[] value) { setValue(PROPERTY_COUNTERCLAIM_REASON_IDS, value); }
	
	//Justification
	private static final String PROPERTY_REASON_EXPLANATION = "cl_reasonExpl";
	
	public String getReasonExplanation() { return getStringValue(PROPERTY_REASON_EXPLANATION); }
	public void setReasonExplanation(String value) { setValue(PROPERTY_REASON_EXPLANATION, value); }
	
	//EvidenceLinker
	private static final String PROPERTY_LINKS_CORRECT = "cl_linksCorrect";
	private static final String PROPERTY_LINK_FAILURES = "cl_linkFails";
	
	public int getCorrectLinks() { return getIntValue(PROPERTY_LINKS_CORRECT); }
	public void setCorrectLinks(int value) { setValue(PROPERTY_LINKS_CORRECT, value); }
	
	public int getLinkFailures() { return getIntValue(PROPERTY_LINK_FAILURES); }
	public void setLinkFailures(int value) { setValue(PROPERTY_LINK_FAILURES, value); }
	
	
	public ClaimCreatorProgress() {
		
	}

	public ClaimCreatorProgress(String dataJSON) {
		super(dataJSON);
	}

	public ClaimCreatorProgress(JavaScriptObject data) {
		super(data);
	}
	
	@Override
	protected native int getIntValueJS(JavaScriptObject obj, String propName) /*-{
		var parsed = parseInt(obj[propName]);
		if (isNaN(parsed))
			return -1;
		return parsed;
	}-*/;
	
	@Override
	public void applyProgress() {
		super.applyProgress();
		int stepIndex = getStepIndex();
		
		IssueInstance issue = DataController.getCurrentIssue();
		
		GWT.log("Claim creator applying progress for step " + stepIndex + "The selected claim index is " + getSelectedClaimIndex() + ": " +
				issue.issue.claims[issue.selectedClaimIndex].text);
		
		if(getSelectedClaimIndex() < 0)
			return;
		
		if (stepIndex >= 1) {
			// I don't think this does anything that org.icivics.arg.data.client.DataController.buildUserData(Map<String, ?>) didn't already do{
			/*
			issue.selectedClaimIndex = getSelectedClaimIndex();
			issue.claim = new ClaimInstance(issue.issue.claims[issue.selectedClaimIndex]);
			issue.counterClaim = new ClaimInstance(issue.issue.claims[issue.selectedClaimIndex == 0 ? 1 : 0]);
			
			issue.claim.initialExplanation = getClaimExplanation();
			issue.counterClaim.initialExplanation = getCounterClaimExplanation();
			*/
			//}
			
		}
		
		
		if (stepIndex >= 2) {
			String debugMsg = "Reasons ";
			
			String[] reasonIDs = getClaimReasonIDs();
			for (int i = 0; i < issue.claim.reasons.length; ++i){
				issue.claim.reasons[i] = new ReasonInstance(issue.claim.claim.getReason(reasonIDs[i]));
				debugMsg += reasonIDs[i] + " ";
			}
			
			GWT.log(debugMsg + "found for claim " + issue.selectedClaimIndex);
		}
		if(stepIndex >= 3){
			DataController.getCurrentIssue().reasonSelectExplanation = getReasonExplanation();
		}
		// Below here never happens
		if (stepIndex >= 6) {
			String[] reasonIDs = getCounterClaimReasonIDs();
			for (int i = 0; i < issue.counterClaim.reasons.length; ++i)
				issue.counterClaim.reasons[i] = new ReasonInstance(issue.counterClaim.claim.getReason(reasonIDs[i]));
		}
	}
}
