package org.icivics.arg.data.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.icivics.arg.data.model.client.IssueInstance;
import org.icivics.arg.data.model.client.ReasonInstance;
import org.icivics.arg.util.client.LogUtil;

import com.fredhat.gwt.xmlrpc.client.XmlRpcClient;
import com.fredhat.gwt.xmlrpc.client.XmlRpcRequest;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


public class WebService {
	private static final int NUM_GLOBAL_PARAMS = 5;
	static String WEB_ROOT = "https://staging.icivics.org";
						//jenkins: "https://vpn.filamentgames.com/projects/icivics_website"; //Does not work, too much redirecting
						//bigphil: "https://bigphil.filament/projects/icivics_website";
						//staging: "https://staging.icivics.org";
	
	private String sessionID;
	private String userID;
	private String issueSessionID;
	
	private String issueID = "0"; //electoral college bigphil "8973"; //vote age staging 2232 // vote age bigphil 9369    8977
	private String loginURL = WEB_ROOT + "/user/login?destination=node/" + issueID;
	private String printURL = WEB_ROOT + "/print/arg-mod/" + issueID + "/30";
	private String serviceURL = WEB_ROOT + "/services/xmlrpc";
	private String domain = "am";
	private String apiKey = "2bffe9064d2a540e4ab25d896b218607";
						//live:    "3566bbd7c0ab6dad983cbfad272f0605";
						//bigphil: "205d1bc15436cd056c97372a175b936e";
						//staging: "2bffe9064d2a540e4ab25d896b218607";
	
	private void setSessionID(String v) { sessionID = v; }
	private void setUserID(String v) { userID = v; }

	public boolean topicSet(){return !issueID.equals("0");}
	public void setTopic(String value){issueID = value;}
	
	private XmlRpcClient client;
	
	public WebService() {}
	
	public void initialize() {
		initializeJS();
		
		WEB_ROOT = serviceURL.substring(0, serviceURL.length() - "/services/xmlrpc".length());
		GWT.log("URL: " + WEB_ROOT);
		
		client = new XmlRpcClient(serviceURL);
	}
	
	private native void initializeJS() /*-{
		if (window.parent && window.parent.parent && window.parent.parent.Drupal && window.parent.parent.Drupal.settings && window.parent.parent.Drupal.settings.argumentations) {
			this.@org.icivics.arg.data.client.WebService::serviceURL = window.parent.parent.Drupal.settings.argumentations.url;
			this.@org.icivics.arg.data.client.WebService::loginURL = window.parent.parent.Drupal.settings.argumentations.login_url;
			this.@org.icivics.arg.data.client.WebService::printURL = window.parent.parent.Drupal.settings.argumentations.print_url;
			this.@org.icivics.arg.data.client.WebService::apiKey = window.parent.parent.Drupal.settings.argumentations.apikey;
			this.@org.icivics.arg.data.client.WebService::domain = window.parent.parent.Drupal.settings.argumentations.domain;
			this.@org.icivics.arg.data.client.WebService::issueID = window.parent.parent.Drupal.settings.argumentations.nid;
		} else {
			@org.icivics.arg.util.client.LogUtil::warn(Ljava/lang/String;)("XMLRPC service data not found.  The app will attempt to use defaults.");
		}
	}-*/;
	
	public String getLoginURL() {
		return loginURL;
	}
	
	public String getPrintURL() {
		return WEB_ROOT + "/print/arg-mod/" + issueID + "/" + userID;
	}
	
	public String getIssueURL(){
		return WEB_ROOT + "/sites/default/files/arg_mods/" + issueID + "/";
	}
	
	public boolean isLoggedIn() {
		return userID != null && !userID.equals("0");
	}
	
	public <T> void callMethod(final String methodName) throws Exception {
		callMethod(methodName, new ArrayList<Object>(NUM_GLOBAL_PARAMS));
	}
	
	public <T> void callMethod(final String methodName, final AsyncCallback<T> callback) throws Exception {
		callMethod(methodName, new ArrayList<Object>(NUM_GLOBAL_PARAMS), callback);
	}
	
	public <T> void callMethod(final String methodName, List<Object> params) throws Exception {
		callMethod(methodName, params, new AsyncCallback<T>() {
			@Override
			public void onSuccess(T result) {
				GWT.log(methodName + " SUCCESS: " + result.toString());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				LogUtil.log(methodName + " FAILURE: " + caught.getMessage());
			}
		});
	}
	
	public <T> void callMethod(final String methodName, List<Object> params, final AsyncCallback<T> callback) throws Exception {
		String nonce = generateNonce();
		String timestamp = generateTimestamp();
		String hash = generateHash(timestamp, domain, nonce, methodName);
		params.add(0, sessionID);
		params.add(0, nonce);
		params.add(0, timestamp);
		params.add(0, domain);
		params.add(0, hash);
		XmlRpcRequest<T> request = new XmlRpcRequest<T>(client, methodName, params.toArray(), callback);
		GWT.log("CALL '" + methodName + "' with params: " + params.toString());
		request.execute();
	}
	
	public void connect(final AsyncCallback<Map<String, ?>> response) throws Exception {
		AsyncCallback<Map<String, ?>> immediateResponse = new AsyncCallback<Map<String, ?>>() {
			@Override
			public void onFailure(Throwable caught) {
				response.onFailure(caught);
			}
			
			@Override
			public void onSuccess(Map<String, ?> result) {
				setSessionID(result.get("sessid").toString());
				Object userObj = result.get("user");
				if (userObj instanceof Map<?, ?>) {
					setUserID(((Map<?, ?>) userObj).get("uid").toString());
				} else {
					LogUtil.log("WARNING: User ID interpretation failed. Subsequent errors may occur.");
					setUserID(null);
				}
				response.onSuccess(result);
			}
		};
		GWT.log("Attempting to connect to service . . .");
		new XmlRpcRequest<Map<String, ?>>(client, "system.connect", new Object[0], immediateResponse).execute();
	}
	
	public void login(String userID, String password, final AsyncCallback<Map<String, ?>> response) throws Exception {
		AsyncCallback<Map<String, ?>> immediateResponse = new AsyncCallback<Map<String, ?>>() {
			@Override
			public void onFailure(Throwable caught) {
				response.onFailure(caught);
			}
			
			@Override
			public void onSuccess(Map<String, ?> result) {
				sessionID = result.get("sessid").toString();
				Object userObj = result.get("user");
				if (userObj instanceof Map<?, ?>) {
					setUserID(((Map<?, ?>) userObj).get("uid").toString());
				} else {
					LogUtil.log("WARNING: User ID interpretation failed. Subsequent errors may occur.");
					setUserID(null);
				}
				response.onSuccess(result);
			}
		};
		List<Object> params = new ArrayList<Object>(NUM_GLOBAL_PARAMS + 2);
		params.add(userID);
		params.add(password);
		callMethod("user.login", params, immediateResponse);
	}
	
	public void getIssue(final AsyncCallback<Map<String, ?>> response) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(issueID);
		callMethod("icam.get_issue_data", params, response);
	}
	
	public void getRebuttal(String reasonID, final AsyncCallback<List<?>> response) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(reasonID);
		params.add(issueID);
		callMethod("icam.rebuttal_data", params, response);
	}
	
	public void getUserData(final AsyncCallback<Map<String, ?>> response, int level) throws Exception {
		AsyncCallback<Map<String, ?>> immediateResponse = new AsyncCallback<Map<String, ?>>() {
			@Override
			public void onFailure(Throwable caught) {
				response.onFailure(caught);
			}
			
			@Override
			public void onSuccess(Map<String, ?> result) {
				issueSessionID = result.get("issue_session_id").toString();
				response.onSuccess(result);
			}
		};
		List<Object> params = new ArrayList<Object>(2 + NUM_GLOBAL_PARAMS);
		params.add(issueID);
		params.add(Integer.toString(level));
		params.add(userID);
		callMethod("icam.init", params, immediateResponse);
	}
	
	public void getModuleProgress(final AsyncCallback<String> response) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		callMethod("icam.get_quick_save", params, response);
	}
	
	public void setScaffoldingLevel(final AsyncCallback<String> response, int level) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(issueID);
		params.add(level);
		callMethod("icam.set_student_scaffolding", params, response);
	}
	
	//{ Claim Votes
	public void submitClaimVote(final AsyncCallback<Boolean> response, String votedClaimID) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(issueID);
		params.add(votedClaimID);
		callMethod("icam.set_student_vote", params, response);
	}
	
	
	public void getVoteCounts(final AsyncCallback<List<String>> response) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(issueID);
		callMethod("icam.get_class_votes", params, response);
	}
	
	
	public void getVoteCountsHash(final AsyncCallback<HashMap<String,String>> response) throws Exception {
		List<Object> params = new ArrayList<Object>(1 + NUM_GLOBAL_PARAMS);
		params.add(issueID);
		callMethod("icam.get_class_votes", params, response);
	}
	
	//}
	
	//{ Saves
	public void saveIssueAnalyzer(final AsyncCallback<Boolean> response) throws Exception {
		IssueInstance issue = DataController.getCurrentIssue();
		List<Object> params = new ArrayList<Object>(2 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(issue.getSelectedClaim().id);
		params.add(issue.scoreStoryChunks);
		params.add(DataController.getTimerSeconds());
		params.add(issue.claimSelectExplanation);
		
		params.add(issue.issueAnalyzerFirstPass);
		params.add(8);
		params.add(issue.issueAnalyzerTotalFailures);
		callMethod("icam.issue_analyzer_save", params, response);
	}
	
	public void saveClaimCreator(final AsyncCallback<Boolean> response) throws Exception {
		IssueInstance issue = DataController.getCurrentIssue();
		List<Object> params = new ArrayList<Object>(9 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(issue.claim.claim.id);
		for (int i = 0; i < issue.claim.reasons.length; ++i)
			params.add(issue.claim.reasons[i].reason.id);
		params.add(issue.counterClaim.claim.id);
		
		 // Counterclaim reasons are no longer set in the claim creator
		for (int i = 0; i < issue.counterClaim.reasons.length; ++i){
			if(issue.counterClaim.reasons[i] == null){
				issue.counterClaim.reasons[i] = new ReasonInstance(issue.counterClaim.claim.reasons[i]);
				
			}
			
			params.add(issue.counterClaim.reasons[i].reason.id);
		}
		
		// Set the reasons to dummies, to keep the number of parameters the same
		
		params.add(issue.scoreEvidenceLinker);
		params.add(DataController.getTimerSeconds());
		params.add(issue.claim.initialExplanation);
		params.add(issue.counterClaim.initialExplanation);
		params.add(issue.reasonSelectExplanation);
		
		params.add(issue.claimCreatorFirstPass);
		params.add(6);
		params.add(issue.claimCreatorTotalFailures);
		callMethod("icam.claim_creator_save", params, response);
	}
	
	public void saveParagraphBuilder(int paragraphIndex, final AsyncCallback<Boolean> response) throws Exception {
		IssueInstance issue = DataController.getCurrentIssue();
		List<Object> params = new ArrayList<Object>(3 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(paragraphIndex + 1);
		params.add(issue.claim.paragraphs[paragraphIndex]);
		params.add(issue.scoreParagraphs[paragraphIndex]);
		params.add(!issue.claim.paragraphs[paragraphIndex].equals(issue.claim.paragraphsOriginal[paragraphIndex]));
		params.add(DataController.getTimerSeconds());
		callMethod("icam.paragraph_constructor_save", params, response);
	}
	
	public void saveCriticCrusher(final AsyncCallback<Boolean> response) throws Exception {
		IssueInstance issue = DataController.getCurrentIssue();
		List<Object> params = new ArrayList<Object>(2 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(issue.claim.rebuttalParagraph);
		params.add(issue.scoreRebuttal);
		params.add(!issue.claim.rebuttalParagraph.equals(issue.claim.rebuttalParagraphOriginal));
		params.add(DataController.getTimerSeconds());
		callMethod("icam.critic_crusher_save", params, response);
	}
	
	public void saveIntroducer(final AsyncCallback<Boolean> response) throws Exception {
		IssueInstance issue = DataController.getCurrentIssue();
		List<Object> params = new ArrayList<Object>(2 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(issue.claim.introParagraph);
		params.add(issue.scoreIntroducer);
		params.add(!issue.claim.introParagraph.equals(issue.claim.introParagraphOriginal));
		params.add(DataController.getTimerSeconds());
		callMethod("icam.introduction_introducer_save", params, response);
	}
	
	public void saveConclusion(final AsyncCallback<Boolean> response) throws Exception {
		IssueInstance issue = DataController.getCurrentIssue();
		List<Object> params = new ArrayList<Object>(2 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(issue.claim.conclusionParagraph);
		params.add(issue.scoreConclusion);
		params.add(!issue.claim.conclusionParagraph.equals(issue.claim.conclusionParagraphOriginal));
		params.add(DataController.getTimerSeconds());
		callMethod("icam.conclusion_crafter_save", params, response);
	}
	
	public void saveModuleProgress(final AsyncCallback<Boolean> response) throws Exception {
		List<Object> params = new ArrayList<Object>(2 + NUM_GLOBAL_PARAMS);
		params.add(issueSessionID);
		params.add(DataController.getCurrentModuleProgress().getJSON());
		callMethod("icam.set_quick_save", params, response);
	}
	//}
	
	private String generateNonce() {
		String a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		List<String> alphabet = Arrays.asList(a.split(""));
		String nonce = "";
		int length = 5;
		Random rand = new Random();
		
		while(length-- > 0)
			nonce += alphabet.get(rand.nextInt(alphabet.size()));
		return nonce;
	}
	
	private String generateTimestamp() {
		return ((Long)(new Date().getTime() / 1000)).toString();
	}
	
	private String generateHash(String timestamp, String domain, String nonce, String method) throws Exception {
		
		//MessageDigest digest = MessageDigest.getInstance("SHA-256");
		String secret = timestamp + ";" + domain + ";" + nonce + ";" + method;
		
		SecretKey keySpec = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(keySpec);
		
		byte[] hash = mac.doFinal(secret.getBytes());
		
		String result = "";
		for (int i = 0; i < hash.length; i++) {
			result += Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1);
		}
		
		return result;
	}
}
