package org.icivics.arg.data.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PlaceholderContent {

	// Evidence





public static String[] itemSummaries = {
	"Results of past elections where the candidate with the least popular votes won the election.",
	"Evidence showing citizens' votes have less impact in large population states ",
	"Statistics showing almost all modern voters have access to good information about candidates",
	"Evidence showing that modern citizens are more educated because they are required to attend school.",
	"Evidence that a Republican voter in a majority-Democrat state feels his vote doesn't count",
	"Statistics showing that more people vote when a race is close and they think their vote will make a difference",
	"Statistics showing candidates spend their money in a few key states with important electoral votes.",
	"Statistics showing candidates spend most of their time in a few key states with important electoral votes.",
	"Article discussing the long history of the Electoral College and the many failed attempts to abolish it.",
	"Interview with an expert who says the Electoral College has worked peacefully for a long time.",
	"Evidence that without additional changes, a popular vote could lead to electing a candidate who received few votes",
	"Evidence showing a country that uses a popular vote system with 2 rounds of voting.",
	"Statement from a senator who says citizens are represented in the Electoral College in the same way they are represented in Congress.",
	"Evidence showing the Electoral College is made up of many smaller popular elections.",
	"Interview with a rural voter who appreciates candidates stopping in the country on the campaign trail.",
	"Interview with an expert who says without the Electoral College, many smaller states would be ignored by candidates."
};

public static String[] itemDescriptions = {
	"Historic elections where the \"loser\" won",
	"Less voting power in high-population states",
	"Plenty of information for voters",
	"All kids today are educated",
	"Voter feels his vote doesn't count",
	"Turnout increases in competitive elections",
	"Campaign spending from 2004",
	"Where candidates spend their time",
	"The Electoral College is here to stay",
	"Abolish the Electoral College?",
	"These election results could happen...",
	"France has 2-round elections",
	"Electoral college is as fair as Congress",
	"Popular election exists now",
	"Rural voter likes attention",
	"Electoral College helps small states"
};

public static String[] initialTextTwo = {
	"The Electoral College makes it possible for a candidate to become president without {token}. In the 2000 election, {token}.",
	"A democracy should have a fair voting system, but the Electoral College system is unfair because the {token} is different in each state. People in some states end up with more voting power than people in other states.",
	"In modern times, it's easy for voters to {token}. Census data in 2010 showed {token}.",
	"Modern citizens are more educated than most citizens who lived when America was born.  Every single state now has {token}, which would have been unimaginable to our Founding Fathers.",
	"John Smith is a Republican in a state where most people are Democrats, so the electoral votes usually {token}. This situation makes Mr. Smith and others in his situation feel like {token}. ",
	"More people vote in close elections where every vote could make a difference. During the 2004 presidential election, {token}.",
	"Candidates don't {token} in most states because they don't have a chance to win there. Just a few \"battleground\" states received {token} during the 2004 election. ",
	"During the 2008 election, candidates did not waste time in states where they did not expect to win. While two states received almost 30 candidate visits, {token}.",
	"The Constitution was written over {token} years ago. Since then, {token} have failed in Congress. ",
	"The American people have {token} chosen their president 56 times using the Electoral College method.",
	"With a straight popular vote, each candidate could receive {token}. We would need a system to make sure a person could not {token}. ",
	"In countries like France where they use a popular vote to choose the president, they have a first round vote and {token}.",
	"Each state has the same number of {token} as {token}. This makes the Electoral College another form of {token}.",
	"The Electoral College depends on citizens' votes to choose the president. In order to choose the electors from each state, there are {token}.",
	"With the Electoral College system, candidates {token}. Even though 73% of Pennsylvanians live in cities, rural voter Mary Schneider still gets to {token}.",
	"The Electoral College protects the interests of Americans {token}. With a popular vote, candidates would have little incentive to visit low-population states when they could influence more people by visiting one large city."
};

public static String[][] claimClozeRestatements = {
	{
		"Restated Claim (List of 4 options; student chooses 1 to complete; none are wrong)",
		"It's time for America to start electing {token}.",
		"Popular vote is the best way to {token}.",
		"American citizens should be able to {token}.",
	},
	{
		"Electing the president should happen {token}.",
		"The Electoral College is the best way to {token}.",
		"There's no reason to stop {token}.",
		"{token} should always be how we elect our president.",
	}
};

public static String[][] reasonMismatch = {
	{
		"Look for evidence that the person with less votes can win or that citizens in some states have less voting power.",
		"Look for evidence that modern citizens have easy access to information or must learn about government in school.",
		"Look for evidence that more people vote when the election is competitive or that voters outnumbered by the opposing political party feel their candidate never wins.",
		"Look for evidence that candidates spend most of their money or time in only a few states."
	},
	{
		"Look for evidence that the Electoral College has worked peacefully for many years or that attempts to abolish it have always failed.",
		"Look for evidence of what the results might be if we did have a popular election or what systems another country uses.",
		"Look for evidence comparing the Electoral College to Congress or claiming that popular elections already exist.",
		"Look for evidence that the Electoral College helps rural voters or small states that might otherwise be ignored."
	}
};

public static String[][][] reasonRestatement = {
	{
		{
			"Level 1 Restated Reasons (List of 4 options; student chooses one; none are wrong)",
			"There's nothing democratic about the Electoral College.",
			"The Electoral College goes against the principles of democracy.",
			"Democracy cannot support a system like the Electoral College.",
		},
		{
			"The Electoral College does not belong in a democracy.",
			"In modern America, it doesn't make sense to be afraid of letting people vote directly for president.",
			"Letting people vote directly for president is nothing to be afraid of in modern times.",
			"Citizens today are perfecly capable of voting directly for president.",
		},
		{
			"The fear of letting people vote directly for president is outdated in our Information Age.",
			"In states with a big political majority, voters in the state's political minority may not bother to vote.",
			"The Electoral College system can make people in a state's political minority feel like their votes don't count.",
			"All citizens should feel motivated to vote, not just those in a state's political majority.",
		},
		{
			"We don't need a system where people in a state's political majority know their candidate will always win.",
			"Presidential candidates ignore all but a few states in the Electoral College system.",
			"Most states get ignored while candidates focus on just a few.",
			"It isn't fair for a few states to get all of the candidates' attention while the rest get little or nothing.",
		},
	},
	{
		{
			"With the Electoral College, most states can be sure they won't get much attention from candidates.",
			"The Electoral College has worked well for over 200 years.",
			"Time has shown that the Electoral College is a system that works.",
			"If the Electoral College didn't work, it would have been changed by now.",
		},
		{
			"The true test of the Electoral College is how long it has worked for America.",
			"We would need a completely new way to choose the president if we abolished the Electoral College.",
			"Replacing the Electoral College with a popular vote would mean figuring out a whole new way to choose the president.",
			"To change to a popular vote, we would need an entirely new method of choosing the president.",
		},
		{
			"If the Electoral College were abolished, America would need a completely new method of choosing the president.",
			"The Electoral College is entirely consistent with the principles of democracy.",
			"The Electoral College is as democratic as the Constitution that describes it.",
			"The Electoral College is a key part of America's democracy.",
		},
		{
			"Fairness and democracy are at the root of the Electoral College.",
			"Candidates would focus on large population centers without the Electoral College.",
			"The Electoral College ensures candidates don't just focus on large population centers.",
			"Large population centers would get all the attention from candidates without the Electoral College.",
		},
	}
};

public static String[][][] clozeReasonRestatement = {
	{
		{
			"Level 2 Restated Reasons -- Fill In the Blank (List of 4 options; student chooses one to complete; none are wrong)",
			"There's nothing democratic about {token}.",
			"The {token} goes against the principles of {token}.",
			"Democracy cannot support a system like {token}.",
		},
		{
			"{token} does not belong in a democracy.",
			"In modern America, it doesn't make sense to be afraid of {token}.",
			"{token} is nothing to be afraid of in modern times.",
			"Citizens today are perfecly capable of {token}.",
		},
		{
			"The fear of {token} is outdated in our Information Age.",
			"In states with a big political majority, voters in the state's {token} may not bother to {token}.",
			"The Electoral College system can make people in a state's {token} feel like their {token} doesn't count.",
			"All citizens should feel motivated to vote, not just those in a state's {token}.",
		},
		{
			"We don't need a system where people in a state's {token} know their candidate will always win.",
			"Presidential candidates {token} in the Electoral College system.",
			"Most states get ignored while candidates {token}.",
			"It isn't fair for a few states to {token} while the rest get little or nothing.",
		},
	},
	{
		{
			"With the Electoral College, most states can be sure they won't get {token}.",
			"{token} has worked well for over 200 years.",
			"Time has shown {token} is a system that works.",
			"If {token} didn't work, it would have been changed by now.",
		},
		{
			"The true test of {token} is how long it has worked for America.",
			"We would need {token} if we abolished the Electoral College.",
			"Replacing the Electoral College with a popular vote would mean figuring out {token}.",
			"To change to a popular vote, we would need {token}.",
		},
		{
			"If the Electoral College were abolished, America would need {token}.",
			"{token} is entirely consistent with the principles of democracy.",
			"{token} is as democratic as the Constitution that describes it.",
			"{token} is a key part of America's democracy.",
		},
		{
			"Fairness and democracy are at the root of {token}.",
			"Candidates would focus on {token} without the Electoral College.",
			"The Electoral College ensures candidates don't just {token}.",
			"{token} would get all the attention from candidates without the Electoral College.",
		},
	}
};

//Critic crusher datas
public static String[][] clozeCrusherOpeners = {
	{
		"Some would argue that {token}, but this system is actually a form of {token}.",
		"undefined",
		"undefined",
		"Some argue that the Electoral College system {token}, but a national popular vote would only cause candidates to {token}.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"Some argue that {token}, but a system that weighs people's votes differently is not {token}.",
		"Some would argue the Electoral College prevents candidates from {token}, but the Electoral College system actually causes candidates to {token}."
	},
	{
		"Some would argue that {token}, but any system where the people decide {token} is democratic.",
		"undefined",
		"undefined",
		"Some argue that the Electoral College system {token}, but a national popular vote would only cause candidates to {token}.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"Some might say the Electoral College {token}, but a system that goes against \"majority rule\" cannot be {token}.",
		"Some would argue the Electoral College prevents candidates from {token}, but the Electoral College system actually causes candidates to {token}."
	}
};

public static String[][] clozeCrusherStrengtheners = {
	{
		"While there may have been a few times when {token}, the Electoral College is just as democratic as {token}.",
		"undefined",
		"undefined",
		"While it may be true that presidential candidates spend most of their money {token}, the Electoral College system means they still need {token} in order to win the election.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"Although technically {token} to decide which candidate will get a state's electoral votes, the number of people represented by one electoral vote {token}.",
		"While it may be true that some rural voters {token} with the Electoral College system, this same system is set up so that many entire states never {token} at all."
	},
	{
		"While it may be true that in the presidential election people actually vote for {token}, when they do this voters are participating in {token}.",
		"undefined",
		"undefined",
		"Although candidates may {token} in states where the race is closest, when they're in those states they pay attention to {token}.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"While the Electoral College could be seen as a form of {token}, the citizens' choice in a presidential election doesn't always {token}.",
		"While candidates may need support even from {token} in order to win, candidates actually don't {token} in most states."
	}
};

public static String[][] clozeCrusherEvidence = {
	{
		"The number of electors each state has is the same as {token}.",
		"undefined",
		"undefined",
		"With a popular vote, candidates would ignore entire states such as {token} because {token}.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"In Wyoming, {token}, but in California {token}.",
		"During the 2008 presidential campaign, {token}."
	},
	{
		"In reality, when America chooses a new president there are {token} on Election Day.",
		"undefined",
		"undefined",
		"Mary Schneider of Pennsylvania is an example of a voter who {token}.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"As recently as the 2000 presidential election, the candidate who became president did not {token}.",
		"In the 2004 presidential campaign, {token}."
	}
};

public static String[][] clozeCrusherClosing = {
	{
		"Nobody would suggest we abolish {token}, so there's no reason to {token} either.",
		"undefined",
		"undefined",
		"{token} ensures that every state counts.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"In a democracy, every vote should {token}.",
		"Visits to a few rural voters in a few states can't make up for {token}."
	},
	{
		"When people {token}, that's a democracy.",
		"undefined",
		"undefined",
		"The Electoral College ensures that both {token} are heard.",
		"undefined",
		"undefined",
		"undefined",
		"undefined",
		"Letting {token} overturns majority rule--and democracy itself.",
		"Presidential candidates should not be able to {token} and still win an election."
	}
};



	public static String[] evidenceAudioUrl = {
		"./audio/evidence1.mp3",
		"./audio/evidence2.mp3",
		"./audio/evidence3.mp3",
		"./audio/evidence4.mp3",
		"./audio/evidence5.mp3",
		"./audio/evidence6.mp3",
		"./audio/evidence7.mp3",
		"./audio/evidence8.mp3",
		"./audio/evidence9.mp3",
		"./audio/evidence10.mp3",
		"./audio/evidence11.mp3",
		"./audio/evidence12.mp3",
		"./audio/evidence13.mp3",
		"./audio/evidence14.mp3",
		"./audio/evidence15.mp3",
		"./audio/evidence16.mp3"
	};
	
}
