package org.icivics.arg.data.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ExternalTextResource;
import com.google.gwt.resources.client.TextResource;

public interface ClientData extends ClientBundle {
	ClientData INSTANCE = GWT.create(ClientData.class);
	
	@Source("Transitions.txt")
	ExternalTextResource transitions();
	
	@Source("PreloadImageRefs.txt")
	ExternalTextResource preloadImageRefs();
	
	@Source("Config.txt")
	ExternalTextResource config();
	
	@Source("Glossary.txt")
	ExternalTextResource glossary();
}
