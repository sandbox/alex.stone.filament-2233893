package org.icivics.arg.data.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.icivics.arg.data.json.client.ModuleProgress;
import org.icivics.arg.data.model.client.*;
import org.icivics.arg.util.client.FileUtil;
import org.icivics.arg.util.client.ImageLoader;
import org.icivics.arg.util.client.LogUtil;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.regexp.shared.SplitResult;
import com.google.gwt.resources.client.ResourceCallback;
import com.google.gwt.resources.client.ResourceException;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

public final class DataController {
	public static boolean SAVE_ENABLED = false;
	public static boolean MENU_ENABLED = false;
	
	public static int scaffoldLevel = 0;
	
	public static String MENU_URL_HASH = "#treesteps";
	//public static String SCAFFOLD_SET_HASH="#treesteps";
	
	private static List<String> mPreloadImageRefs;
	private static ImageLoader mImagePreloader;
	
	private static final int TIMER_INTERVAL_SEC = 60;
	private static int mTimeSec = 0;
	private static Timer mTimer = new Timer() {
		@Override
		public void run() {
			mTimeSec += TIMER_INTERVAL_SEC;
		}
	};
	
	private static WebService mService;
	public static WebService getService() {
		return mService;
	}
	
	private static IssueInstance mIssue;
	public static IssueInstance getCurrentIssue() {
		return mIssue;
	}
	
	private static List<TransitionWord> mTransitions = new ArrayList<TransitionWord>();
	public static List<TransitionWord> getTransitions() {
		return mTransitions;
	}
	
	private static String glossaryUrl;
	private static Glossary mGlossary;
	public static Glossary getGlossary(){
		return mGlossary;
	}
	
	private static CriticCrusherUserData[] mCriticCrusherData;
	public static CriticCrusherUserData getCriticCrusherUserData(int index) {
		return mCriticCrusherData[index];
	}
	public static CriticCrusherData getCriticCrusherData(int index) {
		return mCriticCrusherData[index].data;
	}
	public static boolean isCriticCrusherAvailable() {
		return mCriticCrusherData != null;
	}
	
	public static Claim getCurrentClaim() {
		return mIssue == null ? null : mIssue.getSelectedClaim();
	}
	
	public static ClaimInstance getClaimInstance() {
		return mIssue == null ? null : mIssue.claim;
	}
	
	public static interface ResultHandler { void onResult(boolean success); }
	
	private static ModuleProgress mModuleProgress = new ModuleProgress();
	public static ModuleProgress getModuleProgress() {
		return mModuleProgress;
	}
	public static ModuleProgress getCurrentModuleProgress() {
		switch (moduleIndex) {
			case 0: return mModuleProgress.forIssueAnalyzer();
			case 1: return mModuleProgress.forClaimCreator();
			case 2: return mModuleProgress.forParagraphBuilder(0);
			case 3: return mModuleProgress.forParagraphBuilder(1);
			case 4: return mModuleProgress.forParagraphBuilder(2);
			case 5: return mModuleProgress.forCriticCrusher();
			case 6: return mModuleProgress.forIntroducer();
			case 7: return mModuleProgress.forConclusionCrafter();
			default: return mModuleProgress;
		}
	}
	
	public static int moduleIndex = 0;
	public static int moduleStepIndex = 0;
	
	public static void initService() {
		mService = new WebService();
		mService.initialize();
	}
	
	public static void initIssue(final ResultHandler response) {
		try {
			mService.getIssue(new AsyncCallback<Map<String, ?>>() {
				public void onSuccess(Map<String, ?> result) {
					GWT.log("GET ISSUE SUCCESS: " + mapToString(result, 0));
					buildIssue(result);
					response.onResult(true);
				}
				
				public void onFailure(Throwable caught) {
					LogUtil.log("GET ISSUE FAILURE: " + caught.getMessage());
					caught.printStackTrace();
					response.onResult(false);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void initCriticCrusher(final ResultHandler response) {
		mIssue.counterReason = findCounterReason();
		try {
			mService.getRebuttal(mIssue.counterReason.id, new AsyncCallback<List<?>>() {
				public void onSuccess(List<?> result) {
					GWT.log("GET CC DATA SUCCESS: " + listToString(result, 0));
					buildCriticCrusherData(result);
					response.onResult(true);
				}
				
				public void onFailure(Throwable caught) {
					LogUtil.log("GET CC DATA FAILURE: " + caught.getMessage());
					caught.printStackTrace();
					response.onResult(false);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void initUserData(final ResultHandler response) {
		try {
			
			mService.getUserData(new AsyncCallback<Map<String,?>>() {
				
				@Override
				public void onSuccess(Map<String, ?> result) {
					GWT.log("GET USER DATA SUCCESS: " + mapToString(result, 0));
					buildUserData(result);
					response.onResult(true);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					LogUtil.log("GET USER DATA FAILURE: " + caught.getMessage());
					caught.printStackTrace();
					response.onResult(false);
				}
			}, scaffoldLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void initModuleProgress(final ResultHandler response) {
		try {
			mService.getModuleProgress(new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					GWT.log("GET MODULE PROGRESS SUCCESS:\n\t" + result);
					buildModuleProgress(result);
					response.onResult(true);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					LogUtil.log("GET MODULE PROGRESS FAILURE: " + caught.getMessage());
					caught.printStackTrace();
					response.onResult(false);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void initTransitions(final ResultHandler response) {
		try {
			ClientData.INSTANCE.transitions().getText(new ResourceCallback<TextResource>() {
				
				@Override
				public void onSuccess(TextResource resource) {
					ArrayList<String> raw = FileUtil.interpretStringList(resource.getText());
					String EOIToken = raw.get(0);
					int i=1;
					TransitionWord.types = new HashMap<String,String>();
					while(!raw.get(i).equals(EOIToken)){
						if(raw.get(i+1).equals(EOIToken)){
							GWT.log(raw.get(i) + ": No description");
							TransitionWord.types.put(raw.get(i),null);
							i+=2;
						}else{
							GWT.log(raw.get(i) + ": " + raw.get(i+1));
							TransitionWord.types.put(raw.get(i),raw.get(i+1));
							i+=3;
						}
					}
					i++;
					boolean processingExamples = false;
					for(int count=raw.size(); i < count; i++){
						if(processingExamples){
							// check if the end-of-input token is used
							if(raw.get(i).equals(EOIToken)){
								// Stop processing examples, and go on with adding transitions
								processingExamples = false;
							} else {
								// Add an example to the latest transition
								if(mTransitions.size() > 0){
									mTransitions.get(mTransitions.size() -1).addExample(raw.get(i));
								}
							}
						} else {
							// Add a transition to the list of available transitions
							mTransitions.add(new TransitionWord(raw.get(i), raw.get(++i), raw.get(++i)));
							processingExamples = true;
						}
					}
					//mTransitions = FileUtil.interpretStringList(resource.getText());
					//GWT.log("Processed transitions: " + listToString(mTransitions, 0));
					String str = "";
					i=0;
					for( int count=mTransitions.size(); i < count; i++){
						str += mTransitions.get(i).getWord() + " " + mTransitions.get(i).getDescription()+"\n";
					}
					GWT.log("Processed transitions: " + str);
					
					response.onResult(true);
					
					
				}
				
				@Override
				public void onError(ResourceException e) {
					e.printStackTrace();
					LogUtil.log("Failed to load transitions. Proceeding with empty list.");
					response.onResult(false);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.log("Failed to load transitions. Proceeding with empty list.");
			response.onResult(false);
		}
	}
	
	public static void initGlossary(final ResultHandler response) {
	GWT.log("Loading Glossary");
		RequestBuilder glossaryGet = new RequestBuilder(RequestBuilder.GET, URL.encode(glossaryUrl));
		glossaryGet.setCallback(
			new RequestCallback(){

				@Override
				public void onResponseReceived(Request request,
						Response response) {
					RegExp lineEndSplitter = RegExp.compile("\r?\n|\r");
					
					SplitResult raw = lineEndSplitter.split(response.getText());
					for(int i=0; i<raw.length(); i+=3){
						String[] keys = raw.get(i).split(", ");
						int numKeys = keys.length;
						for(int j=0; j<numKeys; j++){
							Glossary.addDefinition(keys[j], raw.get(i+1),raw.get(i+2));
						}
					}
					mGlossary = new Glossary();
					
				}

				@Override
				public void onError(Request request, Throwable exception) {
					// TODO Auto-generated method stub
					GWT.log("could not load glossary");
				}}
			);
		
		try {
				glossaryGet.send();
			/*
			ClientData.INSTANCE.glossary().getText(new ResourceCallback<TextResource>() {
				@Override
				public void onSuccess(TextResource resource) {
					ArrayList<String> raw = FileUtil.interpretStringList(resource.getText());
					
					int count = raw.size();
					for(int i=0; i<count;i+=3){
						String[] keys = raw.get(i).split(", ");
						int numKeys = keys.length;
						for(int j=0; j<numKeys; j++){
							Glossary.addDefinition(keys[j], raw.get(i+1),raw.get(i+2));
						}
					}
					GWT.log("Processed glossary terms: " + Glossary.getGlossary());
					//GWT.log("Processed glossary terms: " + listToString(Glossary.getTerms(), 0));
					
					//TODO: Do I even need a member variable, since all glossary stuff is static?
					mGlossary = new Glossary();
					response.onResult(true);
				}
				
				@Override
				public void onError(ResourceException e) {
					e.printStackTrace();
					LogUtil.log("Failed to load glossary. Proceeding with empty list.");
					response.onResult(false);
				}
			});
			*/
			
			
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.log("Failed to load glossary. Proceeding with empty list.");
			response.onResult(false);
		}
	}
	
	public static void initConfig(final ResultHandler response) {
		try {
			ClientData.INSTANCE.config().getText(new ResourceCallback<TextResource>() {
				
				@Override
				public void onSuccess(TextResource resource) {
					Map<String, String> config = FileUtil.interpretStringMap(resource.getText());
					SAVE_ENABLED = config.containsKey("SaveEnabled") && !config.get("SaveEnabled").equals("0");
					MENU_ENABLED = config.containsKey("MenuEnabled") && !config.get("MenuEnabled").equals("0");
					
					/*//{ HACKHACKHACK
					SAVE_ENABLED = true;
					MENU_ENABLED = false;
					//}*/
					
					LogUtil.log("CONFIG loaded successfully:"
								+ "\nsave: " + (SAVE_ENABLED ? "enabled" : "disabled")
								+ "\nmenu: " + (MENU_ENABLED ? "enabled" : "disabled")
					);
					response.onResult(true);
				}
				
				@Override
				public void onError(ResourceException e) {
					e.printStackTrace();
					LogUtil.log("Failed to load Config. Proceeding with defaults.");
					response.onResult(false);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.log("Failed to load Config. Proceeding with defaults.");
			response.onResult(false);
		}
	}
	
	public static void preloadImages(final ResultHandler response) {
		mImagePreloader = new ImageLoader();
		try {
			ClientData.INSTANCE.preloadImageRefs().getText(new ResourceCallback<TextResource>() {

				@Override
				public void onSuccess(TextResource resource) {
					mPreloadImageRefs = FileUtil.interpretStringList(resource.getText());
					mImagePreloader.add(mPreloadImageRefs);
					mImagePreloader.load();
					LogUtil.log("Image preload list retrieved successfully.");
					response.onResult(true);
				}

				@Override
				public void onError(ResourceException e) {
					e.printStackTrace();
					LogUtil.log("Failed to load image list. Images may not be preloaded.");
					response.onResult(false);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			LogUtil.log("Failed to load image list. Images may not be preloaded.");
			response.onResult(false);
		}
	}
	
	public static void saveModule(int index) {
		saveModule(index, null);
	}
	public static void saveModule(int index, final ResultHandler response) {
		if (!SAVE_ENABLED) {
			mIssue.moduleIndex = index;
			if (response != null)
				response.onResult(true);
			return;
		}
		
		AsyncCallback<Boolean> immediateResponse = new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				LogUtil.log("SAVE DATA COMPLETED " + (result.booleanValue() ? "(SUCCESS)" : "(FAILURE)"));
				if (response != null)
					response.onResult(result.booleanValue());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				LogUtil.log("SAVE DATA FAILURE: " + caught.getMessage());
				caught.printStackTrace();
				if (response != null)
					response.onResult(false);
			}
		};
		
		try {
			switch (index) {
				case 0:
					break;
				case 1:
					mService.saveIssueAnalyzer(immediateResponse);
					break;
				case 2:
					mService.saveClaimCreator(immediateResponse);
					break;
				case 3:
					mService.saveParagraphBuilder(0, immediateResponse);
					break;
				case 4:
					mService.saveParagraphBuilder(1, immediateResponse);
					break;
				case 5:
					mService.saveParagraphBuilder(2, immediateResponse);
					break;
				case 6:
					mService.saveCriticCrusher(immediateResponse);
					break;
				case 7:
					mService.saveIntroducer(immediateResponse);
					break;
				case 8:
					mService.saveConclusion(immediateResponse);
					break;
				default:
					LogUtil.log("SAVE ABORTED: Unrecognized module index " + index);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mIssue.moduleIndex = index;
		flushModuleProgress();
	}
	
	public static void saveModuleProgress(int stepIndex) {
		saveModuleProgress(stepIndex, null);
	}
	
	public static void saveModuleProgress(int stepIndex, final ResultHandler response) {
		moduleStepIndex = stepIndex;
		mModuleProgress.setStepIndex(stepIndex);
		mModuleProgress.setTimerSeconds(getTimerSeconds());
		
		if (!SAVE_ENABLED) {
			if (response != null)
				response.onResult(true);
			return;
		}
		
		AsyncCallback<Boolean> immediateResponse = new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				LogUtil.log("QUICK SAVE COMPLETED " + (result.booleanValue() ? "(SUCCESS)" : "(FAILURE)"));
				if (response != null)
					response.onResult(result.booleanValue());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				LogUtil.log("QUICK SAVE FAILURE: " + caught.getMessage());
				caught.printStackTrace();
				if (response != null)
					response.onResult(false);
			}
		};
		
		try {
			mService.saveModuleProgress(immediateResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void flushModuleProgress() {
		moduleStepIndex = 0;
		mModuleProgress.clear();
	}
	
	public static void submitClaimVote(int claimIndex) {
		submitClaimVote(claimIndex, null);
	}
	public static void submitClaimVote(int claimIndex, final ResultHandler response) {
		AsyncCallback<Boolean> immediateResponse = new AsyncCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean result) {
				LogUtil.log("SUBMIT VOTE COMPLETED " + (result.booleanValue() ? "(SUCCESS)" : "(FAILURE)"));
				if (response != null)
					response.onResult(result.booleanValue());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				LogUtil.log("SUBMIT VOTE FAILURE: " + caught.getMessage());
				caught.printStackTrace();
				if (response != null)
					response.onResult(false);
			}
		};
		
		try {
			mService.submitClaimVote(immediateResponse, mIssue.issue.claims[claimIndex].id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getClaimVotes(final ResultHandler response) {
		AsyncCallback<List<String>> immediateResponse = new AsyncCallback<List<String>>() {
			@Override
			public void onSuccess(List<String> result) {
				GWT.log("GET VOTES SUCCESS: " + listToString(result, 0));
				boolean success = applyVoteCountResult(result);
				if (response != null)
					response.onResult(success);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// The server has failed to return a List, but not all is lost:
				// it may be trying to return a hashmap is some cases
				LogUtil.log("GET VOTES FAILURE: " + caught.getMessage());
				
				AsyncCallback<HashMap<String, String>> immediateResponseB = new AsyncCallback<HashMap<String,String>>(){
					@Override
					public void onSuccess(HashMap<String, String> result) {
						// The server has returned a hashmap. Most likely because claim 0 had no votes for it.
						// Convert the hashmap into a parsable list and go on with the usual flow.
						List<String> convertedResult = new ArrayList<String>(2);
						convertedResult.add(0, result.get("0") == null ? "0" : result.get("0"));
						convertedResult.add(1, result.get("1") == null ? "0" : result.get("1"));
						
						boolean success = applyVoteCountResult(convertedResult);
						if (response != null)
							response.onResult(success);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						//fail altogether
						GWT.log("GET VOTES FAILURE: " + caught.getMessage());
						
						caught.printStackTrace();
						
						boolean success = applyVoteCountResult(null);
						if (response != null)
							response.onResult(success);
					}
				};
				
				try {
					mService.getVoteCountsHash(immediateResponseB);
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		};
		try {
			mService.getVoteCounts(immediateResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static boolean applyVoteCountResult(List<String> source) {
		boolean valid = false;
		
		final int[] claimKeys = {1, 0};
		for (int i = 0; i < mIssue.voteCounts.length; ++i) {
			if(source != null && source.size()>claimKeys[i]){										//this can be OOB, think this was causing the crash
				mIssue.voteCounts[i] = source != null && source.size() > 0 ? Integer.parseInt(source.get(claimKeys[i])) : 0;
				if (mIssue.voteCounts[i] != 0)
					valid = true;
			}
		}
		
		if (valid) {
			return true;
		} else {
			mIssue.voteCounts[mIssue.selectedClaimIndex] = 1;
			return false;
		}
	}
	
	private static String mapToString(Map<?, ?> map, int depth) {
		String result = getTabString(depth) + "{\n";
		Set<?> keys = map.keySet();
		Iterator<?> iter = keys.iterator();
		while (iter.hasNext()) {
			Object key = iter.next();
			result += getTabString(depth + 1) + key + ":" + valueToString(map.get(key), depth + 1, false);
			if (iter.hasNext())
				result += ",\n";
		}
		result += "\n" + getTabString(depth) + "}";
		return result;
	}
	
	private static String listToString(List<?> list, int depth) {
		String result = getTabString(depth) + "[\n";
		for (int i = 0, count = list.size(); i < count; ++i) {
			result += valueToString(list.get(i), depth + 1, true);
			if (i < count - 1)
				result += ",\n";
		}
		result += "\n" + getTabString(depth) + "]";
		return result;
	}
	
	private static String valueToString(Object value, int depth, boolean tabPrimitives) {
		if (value == null) {
			return (tabPrimitives ? getTabString(depth) : " ") + "<NULL>";
		} else if (value instanceof Map) {
			return "\n" + mapToString((Map<?, ?>) value, depth);
		} else if (value instanceof List) {
			return "\n" + listToString((List<?>) value, depth);
		} else if (value instanceof String) {
			return (tabPrimitives ? getTabString(depth) + "\"" : " \"") + value.toString() + "\"";
		} else {
			return (tabPrimitives ? getTabString(depth) : " ") + value.toString();
		}
	}
	
	private static String getTabString(int depth) {
		String result = "";
		while (depth-- > 0)
			result += "\t";
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private static void buildIssue(Map<String, ?> issueData) {
		GWT.log("Interpreting issue data . . .");
		Issue issue = new Issue();
		
		if((String) issueData.get("glossary") != null)
			glossaryUrl = (String) issueData.get("glossary");
		
		issue.titleText = (String) issueData.get("title_text");
		issue.fullText = (String) issueData.get("full_text");
		issue.shortText = (String) issueData.get("short_text");
		issue.imageSrc = (String) issueData.get("image_url");
		issue.audioSrc = (String) issueData.get("audio_url");
		
		List<?> items = (List<?>) issueData.get("evidence");
		for (int i = 0, count = items.size(); i < count; ++i) {
			Map<String, ?> itemData = (Map<String, ?>) items.get(i);
			Item item = new Item();
			item.id = (String) itemData.get("item_id");
			item.name = (String) itemData.get("name");
			item.text = (String) itemData.get("description");
			
			item.sentenceText[0] = (String) itemData.get("initial_text");
			item.type = Integer.parseInt((String) itemData.get("item_type"));
			
			item.initialTextStartAudioURL = WebService.WEB_ROOT + "/" + (String) itemData.get("initial_text_start_audio_url");
			item.initialTextEndAudioURL = WebService.WEB_ROOT + "/" + (String) itemData.get("initial_text_end_audio_url");
			
			// PLACEHOLDER {
			int idval = Integer.parseInt(item.id);
			if(!itemData.containsKey("summary")){
				item.summary = PlaceholderContent.itemSummaries[i];
				GWT.log("PLACEHOLDER USED FOR ITEM SUMMARY");
			} else {
				item.summary = (String) itemData.get("summary");
			}
			if(!itemData.containsKey("subheader")){
				item.description = PlaceholderContent.itemDescriptions[i];
				GWT.log("PLACEHOLDER USED FOR ITEM SHORT SUBHEADER THING");
			} else {
				item.description = (String) itemData.get("subheader");
			}
			if(!itemData.containsKey("initial_text_two")){
				item.sentenceText[1] = PlaceholderContent.initialTextTwo[i];
				GWT.log("PLACEHOLDER USED FOR ITEM INITAL TEXT TWO");
			}else {
				item.sentenceText[1] = (String) itemData.get("initial_text_two");
			}
			// }
			
			if (itemData.containsKey("image_url")) {
				item.imageSrc = (String) itemData.get("image_url");
			}
			if (itemData.containsKey("audio_url")) {
				item.audioSrc = (String) itemData.get("audio_url");
			} else {
				//item.audioSrc = PlaceholderContent.evidenceAudioUrl[i];
			}
			if (itemData.containsKey("summary_audio_url")) {
				item.summaryAudioSrc = (String) itemData.get("summary_audio_url");
			} else {
				//PLACEHOLDER
				int audioindex = i+1; 
				item.summaryAudioSrc = "./audio/evidence"+ audioindex +"summary.mp3";
			}
			//TODO: interpret counter
			if (itemData.containsKey("counter")) {
				item.counterID = (String) itemData.get("counter");
				GWT.log("Item " + item.id + " is countered by item " + item.counterID);
			} else {
				
			}
			List<?> chunks = (List<?>) itemData.get("tokens");
			for (int j = 0, jCount = chunks.size(); j < jCount; ++j) {
				Map<String, ?> chunkData = (Map<String, ?>) chunks.get(j);
				ItemChunk chunk = new ItemChunk();
				chunk.index = j;
				chunk.itemText = (String) chunkData.get("item_text");
				RegExp rgex = RegExp.compile("\\$","g");
				chunk.itemText = rgex.replace(chunk.itemText, "$$$$");
				chunk.sentenceText = (String) chunkData.get("sentence_text");
				chunk.sentenceText = rgex.replace(chunk.sentenceText, "$$$$");
				chunk.audioSrc = (String) chunkData.get("audio_url");
				item.chunks.add(chunk);
			}
			item.correctChunkIndex = ((Integer) itemData.get("correct")).intValue();
			issue.items[i] = item;
		}
		
		List<?> storyChunks = (List<?>) issueData.get("story_chunks");
		for (int i = 0, count = storyChunks.size(); i < count; ++i) {
			Map<String, ?> chunkData = (Map<String, ?>) storyChunks.get(i);
			StoryChunk chunk = new StoryChunk();
			chunk.textBase = (String) chunkData.get("text");
			chunk.correctItemID = (String) chunkData.get("correct");
			Map<String, String> chunkOptionData = (Map<String, String>) chunkData.get("evidence");
			Iterator<String> iter = chunkOptionData.keySet().iterator();
			Map<String,String> evidenceAudio = (Map<String, String>) chunkData.get("evidence_audio");
			
			for (int j = 0; iter.hasNext(); ++j) {
				String itemID = iter.next();
				chunk.options[j] = new StoryChunkOption(issue.getItem(itemID), chunkOptionData.get(itemID));
				if(evidenceAudio != null)
					chunk.options[j].audioUrl = evidenceAudio.get(itemID);
				else
					GWT.log("No evidence audio URLs found for chunk " + i);
			}
			
			chunk.audioUrlA = (String) chunkData.get("starting_audio_url");
			chunk.audioUrlB = (String) chunkData.get("closing_audio_url");
			
			issue.storyChunks.add(chunk);
		}
		
		List<?> claims = (List<?>) issueData.get("claims");
		final int[] claimKeys = {1, 0};
		for (int i = 0; i < claimKeys.length; ++i) {
			Map<String, ?> claimData = (Map<String, ?>) claims.get(claimKeys[i]);
			issue.claims[i] = new Claim();
			issue.claims[i].id = (String) claimData.get("claim_id");
			issue.claims[i].text = (String) claimData.get("text");
			issue.claims[i].imageSrc = (String) claimData.get("image_url");
			if(claimData.containsKey("audio_url")){
				issue.claims[i].audioSrc = (String) claimData.get("audio_url");
			} else {
				//Placeholder audio url
				issue.claims[i].audioSrc = "audio/claim" + (i+1) + ".mp3";
			}
			
			if(claimData.containsKey("cloze_restatements")){
				List<String> restatements = (List<String>) claimData.get("cloze_restatements");
				if(restatements != null)
				for(int j = 0; j < 4 && j<restatements.size(); j++){
					issue.claims[i].restateClozeSentence[j] = restatements.get(j);
				}
			}else{
				//PLACEHOLDER
				for(int j = 0; j< 4; j++)
					issue.claims[i].restateClozeSentence[j] = PlaceholderContent.claimClozeRestatements[i][j];
			}
			
			
			List<?> reasons = (List<?>) claimData.get("reasons");
			for (int j = 0, jCount = issue.claims[i].reasons.length; j < jCount; ++j) {
				Map<String, ?> reasonData = (Map<String, ?>) reasons.get(j);
				issue.claims[i].reasons[j] = new Reason();
				issue.claims[i].reasons[j].id = (String) reasonData.get("reason_id");
				issue.claims[i].reasons[j].text = (String) reasonData.get("reason_text");
				if(reasonData.containsKey("audio_url")){
					issue.claims[i].reasons[j].audioSrc = WebService.WEB_ROOT + "/" + (String) reasonData.get("audio_url");
				} else {
					//PLACEHOLDER
					issue.claims[i].reasons[j].audioSrc = "audio/claim" + (i+1) + "reason" + (j+1) + ".mp3";
				}
				
				if(reasonData.containsKey("cloze_restatement")){
					List<Map<String,String>> restatementData =  (List<Map<String, String>>) reasonData.get("cloze_restatement");
					for(int k=0; k<4; k++){
						issue.claims[i].reasons[j].conclusionCrafterPrewritten[k] = restatementData.get(k).get("level_one_text");
						issue.claims[i].reasons[j].conclusionCrafterClozeStrings[k] = restatementData.get(k).get("level_two_text");
					}
				} else {
					//PLACEHOLDER
					for(int k = 0; k < 4; k++){
						issue.claims[i].reasons[j].conclusionCrafterPrewritten[k] = PlaceholderContent.reasonRestatement[i][j][k];
						issue.claims[i].reasons[j].conclusionCrafterClozeStrings[k] = PlaceholderContent.clozeReasonRestatement[i][j][k];
					}
				}
				
				/*
				if(reasonData.containsKey("cloze_restatement")){
					
				} else {
					//PLACEHOLDER
					for(int k = 0; k < 4; k++){
						issue.claims[i].reasons[j].conclusionCrafterClozeStrings[k] = PlaceholderContent.clozeReasonRestatement[i][j][k];
					}
				}
				*/
				
				if(reasonData.containsKey("mismatch_text")){
					issue.claims[i].reasons[j].mismatchText = (String) reasonData.get("mismatch_text");
				}else {
					issue.claims[i].reasons[j].mismatchText = PlaceholderContent.reasonMismatch[i][j];
				}
				
				
				if (reasonData.containsKey("evidence")) {
					List<String> reasonItemList = (List<String>) reasonData.get("evidence");
					for (int k = 0, kCount = reasonItemList.size(); k < kCount; ++k) {
						Item item = issue.getItem(reasonItemList.get(k));
						issue.claims[i].reasons[j].items[k] = item;
						if (item.counterID != null){
							issue.claims[i].reasons[j].hasCounter = true;
							GWT.log("Counter found for reason " + issue.claims[i].reasons[j].id);
						}
					}
					issue.claims[i].reasons[j].isCorrect = true;
				} else {
					//TODO: Ensure safety
					issue.claims[i].reasons[j].items = null;
					issue.claims[i].reasons[j].isCorrect = false;
				}
				if (reasonData.containsKey("example_closing_sentence")) {
					issue.claims[i].reasons[j].exampleConclusionSentence = (String) reasonData.get("example_closing_sentence");
				}
			}
			
			
			List<Map<String,?>> jumbles = (List<Map<String,?>>) claimData.get("options");
			for (int j = 0, jCount = jumbles.size(); j < jCount; ++j) {
				issue.claims[i].debateJumbles.add((String) jumbles.get(j).get("text"));
				if(jumbles.get(j).get("audio_url") != null)
					issue.claims[i].debateJumbleAudio.add((String) jumbles.get(j).get("audio_url"));
				else
					issue.claims[i].debateJumbleAudio.add("loremipsum.mp3");
			}
			
			/*
			//{ OLD
			List<String> jumbles = (List<String>) claimData.get("options");
			for (int j = 0, jCount = jumbles.size(); j < jCount; ++j) {
				issue.claims[i].debateJumbles.add((String) jumbles.get(j));
			}
			//}
			*/
			
			if (claimData.containsKey("example_closing_sentence")) {
				issue.claims[i].exampleConclusionSentence = (String) claimData.get("example_closing_sentence");
			}
			
			List<Map<String, ?>> introSentences = (List<Map<String, ?>>) claimData.get("intro_sentence");
			for (int j = 0, jCount = issue.claims[i].introSentences.length; j < jCount; ++j) {
				Map<String, String> introData = (Map<String, String>) introSentences.get(j);
				issue.claims[i].introSentences[j] = new IntroSentence();
				issue.claims[i].introSentences[j].hook = introData.get("hook");
				issue.claims[i].introSentences[j].hookAudioSrc = WebService.WEB_ROOT + "/" + introData.get("hook_audio_url");
				issue.claims[i].introSentences[j].transition = introData.get("bridge");
				issue.claims[i].introSentences[j].transitionAudioSrc = WebService.WEB_ROOT + "/" + introData.get("bridge_audio_url");
				issue.claims[i].introSentences[j].type = Integer.parseInt(introData.get("type"));
			}
			
			List<Map<String, String>> outroSentences = (List<Map<String, String>>) claimData.get("closing_sentence");
			for (int j = 0, jCount = issue.claims[i].outroSentences.length; j < jCount; ++j) {
				Map<String, String> outroData = outroSentences.get(j);
				issue.claims[i].outroSentences[j] = new OutroSentence();
				issue.claims[i].outroSentences[j].text = outroData.get("sentence");
				issue.claims[i].outroSentences[j].audioSrc = WebService.WEB_ROOT + "/" + outroData.get("audio_url");
				issue.claims[i].outroSentences[j].type = Integer.parseInt(outroData.get("type"));
			}
		}
		
		mIssue = new IssueInstance(issue);
		GWT.log("Issue data interpreted successfully.");
	}
	
	@SuppressWarnings("unchecked")
	private static void buildCriticCrusherData(List<?> data) {
		int count = data.size();
		mCriticCrusherData = new CriticCrusherUserData[count];
		for (int index = 0; index < count; ++index) {
			Map<String, ?> instanceData = (Map<String, ?>) data.get(index);
			List<Map<String,?>> openingData = (List<Map<String,?>>) instanceData.get("opening");
			List<Map<String,?>> strengthenerData = (List<Map<String,?>>) instanceData.get("strengthener");
			/*{ OLD
				List<String> openingStarts = (List<String>) openingData.get("start");
				List<String> openingEnds = (List<String>) openingData.get("end");
			
				List<String> strengthenerStarts = (List<String>) strengthenerData.get("start");
				List<String> strengthenerEnds = (List<String>) strengthenerData.get("end");
			}*/
			List<Map<String,?>> rebuttalSentences = (List<Map<String,?>>) instanceData.get("evidence");
				
			List<Map<String,?>> closingSentences = (List<Map<String,?>>) instanceData.get("closing");
			
			CriticCrusherData result = new CriticCrusherData();
			
			//assert openingEnds.size() > 2 : "Not enough critic crusher data recieved from server";
			for (int i = 0; i < 3; ++i) {
				result.openingStartOptions[i] = (String) openingData.get(i).get("start");
				result.openingStartAudioURLs[i] = WebService.WEB_ROOT + "/" +(String) openingData.get(i).get("start_audio_url");
				result.openingEndOptions[i] = (String) openingData.get(i).get("end");
				result.openingEndAudioURLs[i] = WebService.WEB_ROOT + "/" +(String) openingData.get(i).get("end_audio_url");
				
				result.strengthenerStartOptions[i] = (String) strengthenerData.get(i).get("start");
				result.strengthenerStartAudioURLs[i] = WebService.WEB_ROOT + "/" +(String) strengthenerData.get(i).get("start_audio_url");
				result.strengthenerEndOptions[i] = (String) strengthenerData.get(i).get("end");
				result.strengthenerEndAudioURLs[i] = WebService.WEB_ROOT + "/" +(String) strengthenerData.get(i).get("end_audio_url");
				
				result.rebuttalSentenceOptions[i] = (String) rebuttalSentences.get(i).get("text");
				result.rebuttalSentenceAudioURLs[i] = WebService.WEB_ROOT + "/" +(String) rebuttalSentences.get(i).get("audio_url");
				
				result.closingSentenceOptions[i] = (String) closingSentences.get(i).get("text");
				result.closingSentenceAudioURLs[i] = WebService.WEB_ROOT + "/" +(String) closingSentences.get(i).get("audio_url");
			}
			
			Map<String,?> clozeSentences = (Map<String,?>) instanceData.get("cloze");
			List<String> clozeOpeners = (List<String>) clozeSentences.get("opening");
			List<String> clozeStrenghteners = (List<String>) clozeSentences.get("strengthener");
			List<String> clozeEvidences = (List<String>) clozeSentences.get("evidence_sentence");
			List<String> clozeClosings = (List<String>) clozeSentences.get("closing");
			
			
			//GWT.log("The cloze opening sentence should be: " + clozeOpeners.get(k));
			
			result.clozeOpening = clozeOpeners.get(0);
			result.clozeStrengthener = clozeStrenghteners.get(0);
			result.clozeEvidenceSentence = clozeEvidences.get(0);
			result.clozeClosing = clozeClosings.get(0);
			
			result.item = mIssue.issue.getItem((String) instanceData.get("item_id"));
			result.rebuttalItem = mIssue.issue.getItem(result.item.counterID);
			mCriticCrusherData[index] = new CriticCrusherUserData(result);
		}
	}
	
	@SuppressWarnings("unchecked")
	private static void buildUserData(Map<String, ?> data) {
		mIssue.moduleIndex = Integer.parseInt((String) data.get("last_section"));
		
		GWT.log("This user's last section was: " + mIssue.moduleIndex);
		
		// Hack to fix student/student
		// mIssue.moduleIndex = 0;
		
		if (mIssue.moduleIndex < 1) return;
		Map<String, ?> userData = (Map<String, ?>) data.get("user_data");
		Map<String, ?> claimData = (Map<String, ?>) userData.get("claim_data");
		mIssue.claim = new ClaimInstance(mIssue.issue.getClaim((String) claimData.get("claim_id")));
		//GWT.log("Got claim " + (String) claimData.get("claim_id") + mIssue.claim.getClaimText());
		mIssue.counterClaim = new ClaimInstance(mIssue.issue.getClaim((String) claimData.get("counter_claim_id")));
		//GWT.log("Got counter-claim " + (String) claimData.get("counter_claim_id") + mIssue.counterClaim.getClaimText());
		for (int i = 0; i < mIssue.issue.claims.length; ++i)
			if (mIssue.issue.claims[i] != null && 
					mIssue.claim.claim != null &&
					mIssue.issue.claims[i].id.equals(mIssue.claim.claim.id))
				mIssue.selectedClaimIndex = i;
		//GWT.log("The selected claim index was found to be " + mIssue.selectedClaimIndex);
		GWT.log((String) claimData.get("claim_id"));
		//Hack to help with debugging 
		if(((String) claimData.get("claim_id")).equals("")){
			mIssue.moduleIndex = 0;
			return;
		}
		assert mIssue.claim.claim != null : "Claim '" + claimData.get("claim_id") + "' not found in the current Issue.";
		assert mIssue.counterClaim.claim != null : "CounterClaim '" + claimData.get("counter_claim_id") + "' not found in the current Issue.";
		assert mIssue.selectedClaimIndex != -1 : "Claim '" + mIssue.claim.claim.id + "' not found in the current Issue.";
		
		List<String> reasonIDs = (List<String>) claimData.get("claim_reasons");
		for (int i = 0, count = reasonIDs.size(); i < count; ++i) {
			mIssue.claim.reasons[i] = new ReasonInstance(mIssue.claim.claim.getReason(reasonIDs.get(i)));
			//Hack to help with debugging 
			if(mIssue.claim.reasons[i].reason == null){
				mIssue.moduleIndex = 0;
				return;
			}
			assert mIssue.claim.reasons[i].reason != null : "Reason '" + reasonIDs.get(i) + "' not found in claim " + mIssue.claim.claim.id;
		}
		if (mIssue.moduleIndex < 2) return;
		
		if (mIssue.moduleIndex < 3) return;
		mIssue.claim.paragraphs[0] = (String) userData.get("first_body");
		
		if (mIssue.moduleIndex < 4) return;
		mIssue.claim.paragraphs[1] = (String) userData.get("second_body");
		
		if (mIssue.moduleIndex < 5) return;
		mIssue.claim.paragraphs[2] = (String) userData.get("third_body");
		
		if (mIssue.moduleIndex < 6) return;
		reasonIDs = (List<String>) claimData.get("counter_claim_reasons");
		for (int i = 0, count = reasonIDs.size(); i < count; ++i) {
			mIssue.counterClaim.reasons[i] = new ReasonInstance(mIssue.counterClaim.claim.getReason(reasonIDs.get(i)));
			assert mIssue.counterClaim.reasons[i].reason != null : "Reason '" + reasonIDs.get(i) + "' not found in counterclaim " + mIssue.counterClaim.claim.id;
		}
		mIssue.claim.rebuttalParagraph = (String) userData.get("crusher");
		
		if (mIssue.moduleIndex < 7) return;
		mIssue.claim.introParagraph = (String) userData.get("intro");
		
		if (mIssue.moduleIndex < 8) return;
		mIssue.claim.conclusionParagraph = (String) userData.get("closing");
	}
	
	private static void buildModuleProgress(String json) {
		mModuleProgress = new ModuleProgress(json);
		switch (mIssue.moduleIndex) {
			case 0: mModuleProgress.forIssueAnalyzer().applyProgress(); break;
			case 1: mModuleProgress.forClaimCreator().applyProgress(); break;
			case 2: mModuleProgress.forParagraphBuilder(0).applyProgress(); break;
			case 3: mModuleProgress.forParagraphBuilder(1).applyProgress(); break;
			case 4: mModuleProgress.forParagraphBuilder(2).applyProgress(); break;
			case 5: mModuleProgress.forCriticCrusher().applyProgress(); break;
			case 6: mModuleProgress.forIntroducer().applyProgress(); break;
			case 7: mModuleProgress.forConclusionCrafter().applyProgress(); break;
		}
	}
	
	private static Reason findCounterReason() {
		
		
		ReasonInstance[] reasons = mIssue.counterClaim.reasons;
		
		if(mIssue.counterReason != null)
			return mIssue.counterReason;
		
		// Hack hack hack {
		for(int i = 0; i < 3; i++)
			reasons[i] = new ReasonInstance(mIssue.counterClaim.claim.reasons[i]);
		GWT.log("finding counter reasons, reasons length: " + reasons.length);
		
		//}
		
		
		for (int i = 0; i < reasons.length; ++i){
			if (reasons[i].reason.hasCounter)
				return reasons[i].reason;
		}
		
		GWT.log("No counter reason found, oh no");
		return null;
	}
	
	public static Reason[] findCounterReasons(){
		Reason[] reasonsOut = new Reason[2];
		Reason[] reasons = mIssue.getOpposingClaim().reasons;
		
		int k = 0;
		for (int i = 0; i < reasons.length; ++i){
			GWT.log("Reason " + i + ", " + reasons[i].text + " has counter:" + reasons[i].hasCounter);
			if (reasons[i].hasCounter)
				reasonsOut[k++] = reasons[i];
		}
		
		return reasonsOut;
	}
	
	public static void startTimer() {
		mTimeSec = mModuleProgress.getTimerSeconds();
		mTimer.scheduleRepeating(TIMER_INTERVAL_SEC * 1000);
	}
	
	public static void endTimer() {
		mTimer.cancel();
	}
	
	public static int getTimerSeconds() {
		return mTimeSec;
	}
	
	public static void setTimerSeconds(int seconds) {
		mTimeSec = seconds;
	}
	
	public static String getIconPath(int type){
		switch (type){
		case 1: return "images/evidence_document.png";
		case 2: return "images/evidence_envelope.png";
		case 3: return "images/evidence_conversation.png";
		case 4: return "images/evidence_gavel.png";
		case 5: return "images/evidence_quotation.png";
		case 6: return "images/evidence_email.png";
		case 7: return "images/evidence_article.png";
		case 8: return "images/evidence_graph.png";
		default: return "images/evidence_document.png";
		}
	}
	public static String getIssueURL() {
		return mService.getIssueURL();
	}
}
