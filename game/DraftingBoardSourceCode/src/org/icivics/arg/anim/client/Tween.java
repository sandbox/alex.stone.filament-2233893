package org.icivics.arg.anim.client;

import com.google.gwt.animation.client.Animation;

public abstract class Tween extends Animation {
	protected Ease mEase;
	
	public Tween() {
		this(null);
	}
	
	public Tween(Ease ease) {
		super();
		setEase(ease);
	}
	
	public void setEase(Ease v) {
		mEase = v == null ? Ease.NONE : v;
	}
	
	protected double interpolate(double progress) {
		return mEase.interpolate(progress);
	};
}
