package org.icivics.arg.anim.client;

public class ValueTween extends Tween {
	protected double mValueBegin;
	protected double mValueChange;
	protected double mValueCurrent;

	public ValueTween() {
		this(0.0, 1.0, null);
	}

	public ValueTween(Ease ease) {
		this(0.0, 1.0, ease);
	}

	public ValueTween(double endValue) {
		this(0.0, endValue, null);
	}

	public ValueTween(double endValue, Ease ease) {
		this(0.0, endValue, ease);
	}

	public ValueTween(double beginValue, double endValue) {
		this(beginValue, endValue, null);
	}

	public ValueTween(double beginValue, double endValue, Ease ease) {
		super(ease);
		mValueCurrent = beginValue;
		mValueBegin = beginValue;
		mValueChange = endValue - beginValue;
	}
	
	public double getValue() {
		return mValueCurrent;
	}

	@Override
	protected void onUpdate(double progress) {
		mValueCurrent = mValueBegin + mValueChange * progress;
	}
}
