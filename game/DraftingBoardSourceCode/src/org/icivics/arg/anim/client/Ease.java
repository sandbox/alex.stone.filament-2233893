package org.icivics.arg.anim.client;

public abstract class Ease {
	public static final Ease NONE = new Ease() {
		public double interpolate(double t) {
			return t;
		}
	};
	
	public static final Ease ACCELERATE = new Ease() {
		public double interpolate(double t) {
			return Math.cos(t * Math.PI / 4);
		}
	};
	
	public static final Ease DECELERATE = new Ease() {
		public double interpolate(double t) {
			return Math.cos((1 - t) * Math.PI / 4);
		}
	};
	
	public static final Ease ACCEL_DECEL = new Ease() {
		public double interpolate(double t) {
			return (1 + Math.cos(Math.PI + t * Math.PI)) / 2;
		}
	};
	
	public static final Ease RETURN = new Ease() {
		public double interpolate(double t) {
			return Math.sin(t * Math.PI / 2);
		}
	};
	
	/**
	 * Returns the interpolated value that applies to animation using this Ease, given a time position within that animation.
	 * @param t the position within the interpolation, generally expected to be between 0.0 and 1.0, inclusive.
	 * @return
	 */
	public abstract double interpolate(double t);
}
