package org.icivics.arg.util.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public class JSONUtil {
	
	protected static interface Resources extends ClientBundle {
		Resources INSTANCE = GWT.create(Resources.class);
		
		@Source("json2.js")
		TextResource jsonScript();
	}
	private static boolean initialized = false;
	
	public static void initialize() {
		if (!initialized) {
			FileUtil.loadScript(Resources.INSTANCE.jsonScript().getText());
			initialized = true;
		}
	}
	
	public static native JavaScriptObject parse(String json) /*-{
		return JSON.parse(json);
	}-*/;
	
	public static native String stringify(JavaScriptObject obj) /*-{
		//Clone the object, except without the GWT ID that breaks everything
		var outputObj = {};
		for (var prop in obj) {
			if (prop != "__gwt_ObjectId") {
				outputObj[prop] = obj[prop];
			}
		}
		return JSON.stringify(outputObj);
	}-*/;
}
