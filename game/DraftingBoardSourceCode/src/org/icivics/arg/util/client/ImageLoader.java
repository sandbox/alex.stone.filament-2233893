package org.icivics.arg.util.client;


import java.util.ArrayList;
import java.util.List;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

public class ImageLoader {

	public interface LoadHandler extends EventHandler {
		public void onLoaded();
	}
	
	public interface ErrorHandler extends EventHandler {
		public void onError();
	}
	
	public ImageLoader.LoadHandler loadHandler;
	public ImageLoader.ErrorHandler errorHandler;
	
	public boolean isLoaded() { return mIsLoaded; }
	
	private ArrayList<String> mUrls = new ArrayList<String>();
	private ArrayList<Image> mLoadQueue = new ArrayList<Image>();
	private ArrayList<Image> mLoadedQueue = new ArrayList<Image>();
	private ArrayList<HandlerRegistration> mLoadHandlerRegistrations = new ArrayList<HandlerRegistration>();
	private com.google.gwt.event.dom.client.LoadHandler mDomLoadHandler;
	private com.google.gwt.event.dom.client.ErrorHandler mDomErrorHandler;
	private boolean mIsLoaded = false;
	
	public ImageLoader() {
		mDomLoadHandler = new com.google.gwt.event.dom.client.LoadHandler() {
			@Override
			public void onLoad(LoadEvent event) {
				Image img = (Image) event.getSource();
				mLoadQueue.remove(img);
				mLoadedQueue.add(img);
				LogUtil.log("ImageLoader successfully loaded \"" + img.getUrl() + "\"");
				if (mLoadQueue.isEmpty()) {
					onLoaded();
				}
			}
		};
		
		mDomErrorHandler = new com.google.gwt.event.dom.client.ErrorHandler() {
			
			@Override
			public void onError(ErrorEvent event) {
				Image img = (Image) event.getSource();
				LogUtil.log("ImageLoader failed to load \"" + img.getUrl() + "\"");
				
				if (errorHandler != null)
					errorHandler.onError();
			}
		};
	}
	
	protected void onLoaded() {
		mIsLoaded = true;
		removeHandlers();
		
		for (Image img : mLoadedQueue) {
			img.removeFromParent();
		}
		
		if (loadHandler != null)
			loadHandler.onLoaded();
	}
	
	private void removeHandlers() {
		for (HandlerRegistration reg : mLoadHandlerRegistrations) {
			reg.removeHandler();
		}
		
		mLoadHandlerRegistrations = new ArrayList<HandlerRegistration>();
	}

	public void unload() {
		mIsLoaded = false;
		removeHandlers();
		mLoadQueue = new ArrayList<Image>();
		mLoadedQueue = new ArrayList<Image>();
	}
	
	public void clear() {
		unload();
		mUrls = new ArrayList<String>();
	}

	public void add(String uri) {
		mUrls.add(uri);
	}
	
	public void add(String[] uris) {
		for (String uri : uris) {
			add(uri);
		}
	}
	
	public void add(List<String> uris) {
		for (String uri : uris) {
			add(uri);
		}
	}
	
	public void load() {
		for (String uri : mUrls) {
			if (uri.startsWith("data:")) //Already loaded
				continue;
			
			Image img = new Image();
			mLoadQueue.add(img);
			mLoadHandlerRegistrations.add(img.addLoadHandler(mDomLoadHandler));
			mLoadHandlerRegistrations.add(img.addErrorHandler(mDomErrorHandler));
			img.setUrl(uri);
			Style s = img.getElement().getStyle();
			s.setVisibility(Visibility.HIDDEN);
			s.setPosition(Position.ABSOLUTE);
			s.setOverflow(Overflow.HIDDEN);
			s.setWidth(1, Unit.PX);
			s.setHeight(1, Unit.PX);
			RootPanel.get().add(img);
		}
		
		if (mLoadQueue.isEmpty())
			onLoaded();		
	}
}
