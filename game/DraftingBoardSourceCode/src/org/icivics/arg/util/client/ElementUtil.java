package org.icivics.arg.util.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;

public class ElementUtil {

	public static native void addScrollbars(Element e)/*-{
		$wnd.$(e).perfectScrollbar({
				wheelSpeed: 30,
				wheelPropagation: true,
				suppressScrollX: true,
				useKeyboard: false
			});
	}-*/;
	
	public static native void updateScrollbars(Element e)/*-{
		$wnd.$(e).perfectScrollbar('update');
	}-*/;
	
	public static native void setTextScaling()/*-{
		$wnd.scaleText();
	}-*/;
	
	
	public static native void addPasteHandler(Node n) /*-{
		$wnd.$(n).bind('paste',function(e){
		    e.preventDefault();
		});
	}-*/;
	
	public ElementUtil() {}

}
