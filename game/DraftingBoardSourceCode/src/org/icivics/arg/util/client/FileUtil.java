package org.icivics.arg.util.client;

import java.util.ArrayList;
import java.util.HashMap;

public class FileUtil {
	public static ArrayList<String> interpretStringList(String fileText, String delimiter) {
		String[] input = fileText.split(delimiter);
		
		ArrayList<String> output = new ArrayList<String>(input.length);
		for (int i = 0; i < input.length; ++i) {
			String str = input[i].trim();
			if (str.length() > 0)
				output.add(str);
		}
		return output;
	}
	
	public static ArrayList<String> interpretStringList(String fileText) {
		return interpretStringList(fileText, "[\r\n]+");
	}
	
	public static HashMap<String, String> interpretStringMap(String fileText) {
		ArrayList<String> lines = interpretStringList(fileText);
		HashMap<String, String> result = new HashMap<String, String>();
		for (int i = 0, count = lines.size(); i < count; ++i) {
			String[] parts = lines.get(i).split("=", 2);
			assert parts.length == 2 : "Data map line formatted incorrectly: " + lines.get(i);
			result.put(parts[0], parts[1]);
		}
		return result;
	}
	
	public static native void loadScript(String js) /*-{
		eval(js);
	}-*/;
}
