package org.icivics.arg.util.client;

import com.google.gwt.core.client.GWT;

public class LogUtil {
	public static void log(String msg) {
		GWT.log(msg);
		try {
			logJS(msg);
		} catch (Exception e) {}
	}
	
	private static native void logJS(String msg) /*-{
		if (console)
			console.log(msg);
	}-*/;
	
	public static void warn(String msg) {
		GWT.log(msg);
		try {
			warnJS(msg);
		} catch (Exception e) {}
	}
	
	private static native void warnJS(String msg) /*-{
		if (console)
			console.log(msg);
	}-*/;
}
