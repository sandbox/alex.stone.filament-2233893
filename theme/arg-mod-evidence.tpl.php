<table class="evidence">
  <thead>
    <tr>
      <td>&nbsp;</td>
      <td>Evidence Matched on First Try</td>
      <td>Incorrect Evidence Matches</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $row) { ?>
    <tr>
      <td><?php echo $row['section']; ?></td>
      <td><?php echo $row['correct']; ?>/<?php echo $row['total']; ?></td>
      <td><?php echo $row['wrong']; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>