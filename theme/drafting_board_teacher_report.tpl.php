<div class="drafting-board-report">

  <div class="db-report-controls">
    <h4>Your Student Reports</h4>

    <?php if (!empty($classes)): ?>
    <div class="controls">
      <div class="class-filter">
        <form action="" id="draftingboard-report-class-filter">
          <label>Filter by class</label>
          <select name="class-selector-menu" id="class-selector">
            <?php 
              foreach($classes as $key => $value) {
                print '<option value="'. $key .'" '. $value['attr'] .'>'. $value['title'] .'</option>';
              }
            ?>
          </select>
        </form>
      </div>
      <div class="data-download">
        <?php print $all_essays; ?>
        <?php print $all_evidence; ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if (empty($classes)): ?>
    <div class="no-data">
      <h2>You need to have a group with users in it.</h2>
    </div>
    <?php endif; ?>
  </div>

  <?php if (!empty($student_data['data'])): ?>
  <div class="data-wrapper">
      <?php if (!$student_data['data']['has_students']): ?>
      <div class="no-data">
          <h2>There are no users in this group.</h2>
      </div>
      <?php endif; ?>

      <?php if ($student_data['data']['has_students']): ?>
      <div class="db-report-grid">

          <div class="grid-row header">
              <div class="col col-1">Student</div>
              <div class="col col-2">Progress</div>
              <div class="col col-3">Time</div>
              <div class="col col-4">Level</div>
              <div class="col col-5">Evidence Matching</div>
              <div class="col col-6">View/Print/Download Individual Essay</div>
          </div>


          <?php foreach($student_data['data']['students'] as $student): ?>
          <div class="content grid-row">
              <div class="col col-1"><?php print $student['name']; ?></div>
              <div class="col col-2">
                  <div class="progress-bar-wrapper">
                      <h6 class="module-title"><?php print $student['current_section_title']; ?></h6>
                      <div class="<?php print $student['bar_class']; ?>">
                          <div class="<?php print $student['item_1']; ?>"></div>
                          <div class="<?php print $student['item_2']; ?>"></div>
                          <div class="<?php print $student['item_3']; ?>"></div>
                          <div class="<?php print $student['item_4']; ?>"></div>
                          <div class="<?php print $student['item_5']; ?>"></div>
                          <div class="<?php print $student['item_6']; ?>"></div>
                      </div>
                  </div>
              </div>
              <div class="col col-3">
                  <span class="not-started"><?php print $student['total_time']; ?></span>
              </div>
              <div class="col col-4">
                  <span class="not-started"><?php print $student['level']; ?></span>
              </div>
              <div class="col col-5"><?php print $student['evidence_button']; ?></div>
              <div class="col col-6"><?php print $student['essay_button']; ?></div>
          </div>
          <?php endforeach; ?>

      </div>
      <?php endif; ?>
  </div>
  <?php endif; ?>

</div>