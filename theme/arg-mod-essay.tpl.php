<div class="student-essay">
  <div class="header">Student written text is <strong class="blue">highlighted in bolded blue</strong>.</div>
  <div class="content">
    <?php foreach ($paragraphs as $paragraph) { ?>
    <p><?php echo $paragraph['text']; ?></p>
    <?php } ?>
  </div>
  <div class="controls">
    <?php  foreach ($links as $link) { echo $link; } ?>
  </div>
</div>