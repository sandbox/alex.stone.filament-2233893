<?php

/***************
 * VERSION 1.0 *
 ***************/

function _argumentations_zip_unpack($form, $node) {
  $nid = $node->nid;
  if (empty($nid))
    return FALSE;

  $form_field_name = 'zip_upload';
  $file = file_save_upload($form_field_name, array('file_validate_extensions' => array('zip')));

  $needed_files = array(
      'Issue Analyzer',
      'Evidence',
      'Story Chunks',
      'Claims',
      'Opening Sentences',
      'Closing Sentences',
      'Reasons',
      'Critic Crusher'
  );
  
  $found_files = array();

  $zip = new ZipArchive();
  if (isset($file->destination) && $zip->open($file->destination) === TRUE) {
    //Find the destination of the zip file
    $path_parts = explode('/', $file->destination);
    $path_parts_count = count($path_parts);
    array_pop($path_parts);
    $destination = implode('/', $path_parts);

    //Extract the files to the same destination of the zip file
    $extracted = $zip->extractTo($destination);

    //Find the names/path of the csv files that were passed in the zip file
    for ($i = 0; $i < $zip->numFiles; $i++) {
      $zip_file_name = $zip->getNameIndex($i);
      if (strpos($zip_file_name, '.csv')) {
        $csv_files[] = $zip_file_name;
      }
      elseif (strpos($zip_file_name, '.png')) {
        //handle image file
        //$file_parts = explode('/', $zip_file_name);
        //$count = count($file_parts);
        $file_names[] = array(
            'base' => basename($zip_file_name),
            'name' => $destination .'/'. $zip_file_name,
        );
      }
    }

    //Close the zip since we are done using it
    $zip->close();

    //If the extracted succeeded check all the files
    if ($extracted === TRUE) {
      $missing_file = FALSE;

      foreach ($csv_files as $csv_file) {
        $csv_file_parts = explode('/', $csv_file);
        $csv_file_name = array_pop($csv_file_parts);
        
        if (!file_exists($destination .'/'. $csv_file)) {
          form_set_error($form_field_name, $csv_file_name .' is not present after zip extraction.');
          $missing_file = TRUE;
        }

        if (!$missing_file) {
          foreach ($needed_files as $key => $needed_file) {
            if (strpos($csv_file_name, $needed_file) !== FALSE)
              $found_files[$key] = $csv_file;
          }
        }
        else {
          return;
        }
      }

      if (count($found_files) === count($needed_files)) {
        //Calls the handler function for each item in the array with destination
        //passed as a parameter.
        
        ksort($found_files);
        static $issues;

        foreach ($found_files as $key => $found_file) {
          _argumentations_file_upload_handler($found_file, $key, $destination, $issues, $nid);
        }

        //Flush and recreate directory
        if(!is_dir(ARG_MOD_IMAGE_DIR)) {
          mkdir(ARG_MOD_IMAGE_DIR);
        }
        $image_dir = ARG_MOD_IMAGE_DIR .'/'. $node->nid;

        //Create dir to hold images
        $removed = rrmdir(ARG_MOD_IMAGE_DIR .'/'. $node->nid);
        $created = mkdir(ARG_MOD_IMAGE_DIR .'/'. $node->nid);
        am_custom_move_files($destination .'/arg_mod_upload/images', $image_dir);

        _am_create_data($issues, $nid);
      }
      else {
        $files_needed = implode(', ', $needed_files);
        $files_found = implode(', ', $found_files);
        $error_text = t('Files found: %files_found Files needed: %files_needed', array('%files_found' => $files_found, '%files_needed' => $files_needed));
        form_set_error($form_field_name, $error_text);
      }
    }
    else {
      form_set_error($form_field_name, 'Zip file failed to be extracted in code.');
    }
  }

  return TRUE;
}

function _argumentations_file_upload_handler($found_file, $key, $desination, &$issues, $nid) {
  //Base off of the keys for $needed_files from _argumentations_zip_unpack()

  $rows = _argumentations_file_read($desination .'/'. $found_file);
  $keys = array_shift($rows);
  $descriptions = array_shift($rows);

  static $claims;
  static $reasons;
  static $evidence;

  //0 = Issue Ananlyzer
  if ($key === 0)
    $issues = _am_import_issue_analyzer_data($keys, $rows, $nid);

  //1 = Evidence
  elseif ($key === 1)
    $evidence = _am_import_evidence_data($keys, $rows, $issues, $nid);

  //2 = Story Chunk
  elseif ($key === 2)
    _am_import_story_chunk_data($keys, $rows, $issues, $nid);

  //3 = Claims
  elseif ($key === 3)
    $claims = _am_import_claims_data($keys, $rows, $issues, $nid);

  //4 = Opening Sentences
  elseif ($key === 4)
    _am_import_opening_sentences_data($keys, $rows, $claims, $issues);

  //5 = Closing Sentences
  elseif ($key === 5)
    _am_import_closing_sentences_data($keys, $rows, $claims, $issues);

  //6 = Reasons
  elseif ($key === 6)
    $reasons = _am_import_reasons_data($keys, $rows, $claims, $issues, $evidence);

  //7 = Critic Crusher
  elseif ($key === 7)
    _am_import_critic_crusher_data($keys, $rows, $reasons, $issues, $nid, $evidence);
}

function _am_import_issue_analyzer_data($keys, $rows, $nid) {
  $issues = array();
  foreach ($rows as $key => $row) {
    $issues = array(
        $nid => array(
            'headline' => $row[0],
            'question' => $row[1],
            'image' => $row[2],
            'title' => $row[3],
        ),
    );
  }
  return $issues;
}

function _am_import_evidence_data($keys, $rows, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $issues[$nid]['evidence'][$row[0]] = array(
        'name' => $row[1],
        'text' => $row[2],
        'tokens' => array(
            $row[3],
            $row[5],
            $row[7],
        ),
        'answers' => array(
            $row[4],
            $row[6],
            $row[8],
        ),
        'type' => argumentations_convert_evidence_type($row[10]),
        'image' => $row[11],
    );

    $evidence[$row[0]]['counter'] = $row[9];
  }

  return $evidence;
}

function _am_import_story_chunk_data($keys, $rows, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $issues[$nid]['chunks'][] = array(
        'text' => $row[0],
        'evidence' => array(
            $row[1] => $row[2],
            $row[3] => $row[4],
            $row[5] => $row[6],
        ),
        'correct' => $row[7],
    );
  }
}

function _am_import_claims_data($keys, $rows, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $claims[$row[0]] = $nid;
    $issues[$nid]['claims'][$row[0]] = array(
        'text' => $row[1],
        'example' => $row[2],
        'side' => $row[3],
        'issue_claim_id' => $row[0],
        'image' => $row[4],
        'big_question_options' => array($row[5], $row[6], $row[7]),
    );
  }
  return $claims;
}

function _am_import_opening_sentences_data($keys, $rows, $claims, &$issues) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['opening_sentences'][$key] = array(
        'text' => $row[1],
        'bridge' => $row[2],
        'type' => $row[3],
    );
  }
}

function _am_import_closing_sentences_data($keys, $rows, $claims, &$issues) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['closing_sentences'][$key] = array(
        'text' => $row[1],
        'type' => $row[2],
    );
  }
}

function _am_import_reasons_data($keys, $rows, $claims, &$issues, $evidence) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['reasons'][$row[1]] = array(
        'text' => $row[2],
        'evidence' => array(
            $row[3] => array(
                'correct_index' => $row[4],
                'text' => $row[5],
                'counter_item' => $evidence[$row[3]]['counter'],
            ),
            $row[6] => array(
                'correct_index' => $row[7],
                'text' => $row[8],
                'counter_item' => $evidence[$row[6]]['counter'],
            ),
        ),
        'example' => $row[9],
        'issue_reason_id' => $row[1],
    );
    $reasons[$row[1]] = $row[0];
  }
  
  return $reasons;
}

function _am_import_critic_crusher_data($keys, $rows, $reasons, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    if (!empty($row[2]) && !empty($row[3])) {
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_opening'][] = array(
          'start' => $row[2],
          'end' => $row[3],
      );
    }
    
    if (!empty($row[4]) && !empty($row[5])) {
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_strengthener'][] = array(
          'start' => $row[4],
          'end' => $row[5],
      );
    }
    
    if (!empty($row[6]))
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_evidence'][] = $row[6];
    
    if (!empty($row[7]))
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_closing'][] = $row[7];
  }
}

function _am_create_data($issues, $nid) {
  module_load_include('inc', 'argumentations', 'includes/crud');
  
  $old_issues = _argumentations_read_issue($nid);
  foreach ($old_issues as $old_issue) {
    argumentations_delete_issue($nid);
  }

  foreach ($issues as $key => $issue) {
    $new_issue = argumentations_add_issue($key, $issue['title'], $issue['question'], $issue['headline'], $issue['image']);
    $new_nid = $new_issue['nid'];

    foreach ($issue['evidence'] as $key => $evidence) {
      $new_evidence = argumentations_add_evidence($new_nid, $evidence['name'], $evidence['text'], $evidence['type'], $evidence['image']);
      $evidence_mapping[$key] = $new_evidence['item_id'];

      foreach ($evidence['tokens'] as $token_key => $token) {
        argumentations_add_evidence_chunk($new_evidence['item_id'], $token, $evidence['answers'][$token_key], $token_key);
      }
    }

    foreach ($issue['chunks'] as $chunk) {
      $new_chunk = argumentations_add_story_chunk($new_nid, $chunk['text']);

      foreach ($chunk['evidence'] as $chunk_evidence_id => $chunk_evidence_text) {
        if ($chunk['correct'] === $chunk_evidence_id)
          argumentations_link_story_chunk_evidence($new_chunk['chunk_id'], $evidence_mapping[$chunk_evidence_id], $chunk_evidence_text, TRUE);
        else
          argumentations_link_story_chunk_evidence($new_chunk['chunk_id'], $evidence_mapping[$chunk_evidence_id], $chunk_evidence_text);
      }
    }

    foreach ($issue['claims'] as $claim) {
      $new_claim = argumentations_add_claim($new_nid, $claim['text'], $claim['side'], $claim['example'], $claim['image'], $claim['issue_claim_id']);
      $new_claim_id = $new_claim['claim_id'];

      argumentations_add_big_question_options($claim['big_question_options'], $new_claim_id);

      if (is_array($claim['opening_sentences'])) {
        foreach ($claim['opening_sentences'] as $opening) {
          $type = _am_sentence_opening_type_converter($opening['type']);
          argumentations_add_intro_sentence($new_claim_id, $opening['text'], $opening['bridge'], $type);
        }
      }

      if (is_array($claim['closing_sentences'])) {
        foreach ($claim['closing_sentences'] as $closing) {
          $type = _am_sentence_closing_type_converter($closing['type']);
          argumentations_add_closing_sentence($new_claim_id, $closing['text'], $type);
        }
      }

      foreach ($claim['reasons'] as $reason) {
        $new_reason = argumentations_add_reason($new_claim_id, $reason['text'], $reason['example'], $reason['issue_reason_id']);

        foreach ($reason['evidence'] as $reason_evidence_id => $reason_evidence_data) {
          if (isset($evidence_mapping[$reason_evidence_id])) {
            $index = $reason_evidence_data['correct_index'];
            $text = $reason_evidence_data['text'];
            $counter = $evidence_mapping[$reason_evidence_data['counter_item']];

            $reason_item_data = argumentations_link_reason_evidence($new_reason['reason_id'], $evidence_mapping[$reason_evidence_id], $text, $index, $counter);
            if (is_array($reason_evidence_data['rebuttal_opening'])) {
              foreach ($reason_evidence_data['rebuttal_opening'] as $key => $data) {
                argumentations_add_rebuttal_opening($data['start'], $data['end'], $reason_item_data['reason_item_id'], $key);
              }
            }

            if (is_array($reason_evidence_data['rebuttal_strengthener'])) {
              foreach ($reason_evidence_data['rebuttal_strengthener'] as $key => $data) {
                argumentations_add_rebuttal_strengthener($data['start'], $data['end'], $reason_item_data['reason_item_id'], $key);
              }
            }

            if (is_array($reason_evidence_data['rebuttal_evidence'])) {
              foreach ($reason_evidence_data['rebuttal_evidence'] as $key => $data) {
                argumentations_add_rebuttal_evidence($data, $reason_item_data['reason_item_id'], $key);
              }
            }

            if (is_array($reason_evidence_data['rebuttal_closing'])) {
              foreach ($reason_evidence_data['rebuttal_closing'] as $key => $data) {
                argumentations_add_rebuttal_closing($data, $reason_item_data['reason_item_id'], $key);
              }
            }
          }
        }
      }
    }
  }
}

function _am_sentence_closing_type_converter($string) {
  switch ($string) {
    case 'Quote/Stat/Data':
      $int = CLOSING_SENTENCE_QSD;
      break;
    case 'Next Steps':
      $int = CLOSING_SENTENCE_NEXT_STEPS;
      break;
    case 'Consequences':
      $int = CLOSING_SENTENCE_CONSEQUENCES;
      break;
  }
  return $int;
}

function _am_sentence_opening_type_converter($string) {
  switch ($string) {
    case 'Background Info':
      $int = OPENING_SENTENCE_BACKGROUND;
      break;
    case 'Statistic/Quote':
      $int = OPENING_SENTENCE_STAT;
      break;
    case 'Question/Scenario':
      $int = OPENING_SENTENCE_QUESTION;
      break;
  }
  return $int;
}

/***************
 * VERSION 2.0 *
 ***************/

function _argumentations_zip_new_unpack($form, $node, $version = 2) {
  $nid = $node->nid;
  if (empty($nid))
    return FALSE;

  $form_field_name = 'zip_upload';
  $file = file_save_upload($form_field_name, array('file_validate_extensions' => array('zip')));

  $needed_files = array(
      'Issue Analyzer',
      'Evidence',
      'Story Chunks',
      'Claims',
      'Hooks & Bridges', //Opening Sentences
      'Clinchers', //Closing Sentences
      'Paragraph Constructor', //Reasons
      'Critic Crusher 1', //Critic Crusher
      'Conclusion Starters', //new file
      'Conclusion Reasons', //new file
      'Critic Crusher 2' //new file
  );
  
  $found_files = array();

  $zip = new ZipArchive();
  if (isset($file->destination) && $zip->open($file->destination) === TRUE) {
    //Find the destination of the zip file
    $path_parts = explode('/', $file->destination);
    $path_parts_count = count($path_parts);
    array_pop($path_parts);
    $destination = implode('/', $path_parts);

    //Extract the files to the same destination of the zip file
    $extracted = $zip->extractTo($destination);

    //Find the names/path of the csv files that were passed in the zip file
    for ($i = 0; $i < $zip->numFiles; $i++) {
      $zip_file_name = $zip->getNameIndex($i);
      if (strpos($zip_file_name, '.csv')) {
        $csv_files[] = $zip_file_name;
      }
      elseif (strpos($zip_file_name, '.png')) {
        //handle image file
        //$file_parts = explode('/', $zip_file_name);
        //$count = count($file_parts);
        $file_names[] = array(
            'base' => basename($zip_file_name),
            'name' => $destination .'/'. $zip_file_name,
        );
      }
      elseif (strpos($zip_file_name, '.txt')) {
        $file_names[] = array(
            'base' => basename($zip_file_name),
            'name' => $destination .'/'. $zip_file_name,
        );
      }
    }

    //Close the zip since we are done using it
    $zip->close();

    //If the extracted succeeded check all the files
    if ($extracted === TRUE) {
      $missing_file = FALSE;

      foreach ($csv_files as $csv_file) {
        $csv_file_parts = explode('/', $csv_file);
        $csv_file_name = array_pop($csv_file_parts);
        
        if (!file_exists($destination .'/'. $csv_file)) {
          form_set_error($form_field_name, $csv_file_name .' is not present after zip extraction.');
          $missing_file = TRUE;
        }

        if (!$missing_file) {
          foreach ($needed_files as $key => $needed_file) {
            if (strpos($csv_file_name, $needed_file) !== FALSE)
              $found_files[$key] = $csv_file;
          }
        }
        else {
          return;
        }
      }

      if (count($found_files) === count($needed_files)) {
        //Calls the handler function for each item in the array with destination
        //passed as a parameter.
        
        ksort($found_files);
        static $issues;

        foreach ($found_files as $key => $found_file) {
          _argumentations_file_new_upload_handler($found_file, $key, $destination, $issues, $nid);
        }

        //Flush and recreate directory
        if(!is_dir(ARG_MOD_MEDIA_DIR)) {
          mkdir(ARG_MOD_MEDIA_DIR);
        }
        $media_dir = ARG_MOD_MEDIA_DIR .'/'. $node->nid;

        //Create dir to hold images
        $removed = rrmdir(ARG_MOD_MEDIA_DIR .'/'. $node->nid);
        $created = mkdir(ARG_MOD_MEDIA_DIR .'/'. $node->nid);
        am_custom_move_files($destination .'/arg_mod_upload/images', $media_dir); //handles image files getting moved
        am_custom_move_files($destination .'/arg_mod_upload/audio', $media_dir); //handles audio files getting moved
        am_custom_move_files($destination .'/arg_mod_upload/extras', $media_dir); //handles audio files getting moved

        _am_create_new_data($issues, $nid, $version);
      }
      else {
        $files_needed = implode('</li><li>', $needed_files);
        $files_found = implode('</li><li>', $found_files);
        $error_text = t('Files found: <ul><li>!files_found</li></ul> Files needed: <ul><li>!files_needed</li></ul>', array('!files_found' => $files_found, '!files_needed' => $files_needed));
        form_set_error($form_field_name, $error_text);
      }
    }
    else {
      form_set_error($form_field_name, 'Zip file failed to be extracted in code.');
    }
  }

  return TRUE;
}

function _argumentations_file_new_upload_handler($found_file, $key, $desination, &$issues, $nid) {
  //Base off of the keys for $needed_files from _argumentations_zip_unpack()

  $rows = _argumentations_file_read($desination .'/'. $found_file);
  $keys = array_shift($rows);
  $descriptions = array_shift($rows);

  static $claims;
  static $reasons;
  static $evidence;

  //0 = Issue Ananlyzer
  if ($key === 0) {
    $issues = _am_import_new_issue_analyzer_data($keys, $rows, $nid);
  }
  //1 = Evidence
  elseif ($key === 1) {
    $evidence = _am_import_new_evidence_data($keys, $rows, $issues, $nid);
  }
  //2 = Story Chunk
  elseif ($key === 2) {
    _am_import_new_story_chunk_data($keys, $rows, $issues, $nid);
  }
  //3 = Claims
  elseif ($key === 3) {
    $claims = _am_import_new_claims_data($keys, $rows, $issues, $nid);
  }
  //4 = Opening Sentences
  elseif ($key === 4) {
    _am_import_new_opening_sentences_data($keys, $rows, $claims, $issues);
  }
  //5 = Closing Sentences
  elseif ($key === 5) {
    _am_import_new_closing_sentences_data($keys, $rows, $claims, $issues);
  }
  //6 = Reasons
  elseif ($key === 6) {
    $reasons = _am_import_new_reasons_data($keys, $rows, $claims, $issues, $evidence);
  }
  //7 = Critic Crusher
  elseif ($key === 7) {
    _am_import_new_critic_crusher_data($keys, $rows, $reasons, $issues, $nid, $evidence);
  }
  //8 = Conclusion Starters
  elseif ($key === 8) {
    _am_import_new_conclusion_starter_data($keys, $rows, $claims, $issues);
  }
  //9 = Conclusion Reasons
  elseif ($key === 9) {
    _am_import_new_conclusion_reason_data($keys, $rows, $reasons, $issues, $nid);
  }
  //10 = Cloze
  elseif ($key === 10) {
    _am_import_new_cloze_data($keys, $rows, $reasons, $issues, $nid, $evidence);
  }
}

function _am_import_new_issue_analyzer_data($keys, $rows, $nid) {
  $issues = array();
  foreach ($rows as $key => $row) {
    $issues = array(
        $nid => array(
            'headline' => $row[0],
            'headline_audio' => $row[1],
            'question' => $row[2],
            'question_audio' => $row[3],
            'image' => $row[4],
            'title' => $row[5],
        ),
    );
  }
  return $issues;
}

function _am_import_new_evidence_data($keys, $rows, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $issues[$nid]['evidence'][$row[0]] = array(
        'name' => $row[1],
        'description' => $row[2],
        'popup' => $row[3],
        'popup_audio' => $row[4],
        'text' => $row[5],
        'text_audio' => $row[6],
        'tokens' => array(
            $row[7],
            $row[10],
            $row[13],
        ),
        'token_audio' => array(
            $row[9],
            $row[12],
            $row[15],
        ),
        'answers' => array(
            $row[8],
            $row[11],
            $row[14],
        ),
        'type' => argumentations_convert_evidence_type($row[17]),
        'image' => $row[18],
    );

    $evidence[$row[0]]['counter'] = $row[16];
  }

  return $evidence;
}

function _am_import_new_story_chunk_data($keys, $rows, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $issues[$nid]['chunks'][] = array(
        'text' => $row[0],
        'audio_start' => $row[1],
        'audio_end' => $row[2],
        'evidence' => array(
            $row[3] => array(
                'option' => $row[4],
                'audio' => $row[5]
            ),
            $row[6] => array(
                'option' => $row[7],
                'audio' => $row[8]
            ),
            $row[9] => array(
                'option' => $row[10],
                'audio' => $row[11]
            ),
        ),
        'correct' => $row[12],
    );
  }
}

function _am_import_new_claims_data($keys, $rows, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $claims[$row[0]] = $nid;
    $issues[$nid]['claims'][$row[0]] = array(
        'text' => $row[1],
        'audio' => $row[2],
        'example' => $row[3],
        'side' => $row[4],
        'issue_claim_id' => $row[0],
        'image' => $row[5],
        'big_question_options' => array(
            array(
                'text' => $row[6],
                'audio' => $row[7],
            ),
            array(
                'text' => $row[8],
                'audio' => $row[9],
            ),
            array(
                'text' => $row[10],
                'audio' => $row[11],
            )
        ),
    );
  }
  return $claims;
}

function _am_import_new_opening_sentences_data($keys, $rows, $claims, &$issues) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['opening_sentences'][$key] = array(
        'text' => $row[1],
        'text_audio' => $row[2],
        'bridge' => $row[3],
        'bridge_audio' => $row[4],
        'type' => $row[5],
    );
  }
}

function _am_import_new_closing_sentences_data($keys, $rows, $claims, &$issues) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['closing_sentences'][$key] = array(
        'text' => $row[1],
        'type' => $row[2],
        'audio' => $row[3]
    );
  }
}

function _am_import_new_reasons_data($keys, $rows, $claims, &$issues, $evidence) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['reasons'][$row[1]] = array(
        'text' => $row[3],
        'incorrect_text'  => $row[2],
        'audio' => $row[4],
        'evidence' => array(
            $row[5] => array(
                'correct_index' => $row[6],
                'level_one_text' => $row[7],
                'level_one_start_audio' => $row[9],
                'level_one_end_audio' => $row[10],
                'level_two_text' => $row[8],
                'counter_item' => $evidence[$row[5]]['counter'],
            ),
            $row[11] => array(
                'correct_index' => $row[12],
                'level_one_text' => $row[13],
                'level_one_start_audio' => $row[15],
                'level_one_end_audio' => $row[16],
                'level_two_text' => $row[14],
                'counter_item' => $evidence[$row[11]]['counter'],
            ),
        ),
        'example' => $row[17],
        'issue_reason_id' => $row[1],
    );
    $reasons[$row[1]] = $row[0];
  }
  
  return $reasons;
}

function _am_import_new_critic_crusher_data($keys, $rows, $reasons, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    if (!empty($row[2]) && !empty($row[4])) {
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_opening'][] = array(
          'start' => $row[2],
          'start_audio' => $row[3],
          'end' => $row[4],
          'end_audio' => $row[5],
      );
    }
    if (!empty($row[6]) && !empty($row[8])) {
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_strengthener'][] = array(
          'start' => $row[6],
          'start_audio' => $row[7],
          'end' => $row[8],
          'end_audio' => $row[9],
      );
    }
    if (!empty($row[10])) {
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_evidence'][] = array(
          'text' => $row[10],
          'audio' => $row[11],
      );
    }
    if (!empty($row[12])) {
      $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['rebuttal_closing'][] = array(
          'text' => $row[12],
          'audio' => $row[13],
      );
    }
  }
}

function _am_import_new_conclusion_starter_data($keys, $rows, $claims, &$issues) {
  foreach ($rows as $key => $row) {
    $nid = $claims[$row[0]];
    $issues[$nid]['claims'][$row[0]]['conclusion_starter'][$key] = array(
        'restated_claim' => $row[1],
    );
  }
}

function _am_import_new_conclusion_reason_data($keys, $rows, $reasons, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $issues[$nid]['claims'][$reasons[$row[1]]]['reasons'][$row[1]]['conclusion_reasons'][] = array(
        'level_one_reason' => $row[2],
        'level_one_audio_url' => $row[3],
        'level_two_reason' => $row[4],
    );
  }
}

function _am_import_new_cloze_data($keys, $rows, $reasons, &$issues, $nid) {
  foreach ($rows as $key => $row) {
    $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['cloze_opening'] = $row[2];
    $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['cloze_strengthener'] = $row[3];
    $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['cloze_evidence'] = $row[4];
    $issues[$nid]['claims'][$reasons[$row[0]]]['reasons'][$row[0]]['evidence'][$row[1]]['cloze_closing'] = $row[5];
  }
}

function _am_create_new_data($issues, $nid, $version) {
  module_load_include('inc', 'argumentations', 'includes/crud');
  
  $old_issues = _argumentations_read_issue($nid);
  foreach ($old_issues as $old_issue) {
    argumentations_delete_issue($nid);
  }

  foreach ($issues as $key => $issue) {
    $new_issue = argumentations_add_issue($key, $issue['title'], $issue['question'], $issue['headline'], $issue['image'], $issue['question_audio'], $issue['headline_audio'], $version);
    $new_nid = $new_issue['nid'];

    foreach ($issue['evidence'] as $key => $evidence) {
      $new_evidence = argumentations_add_evidence($new_nid, $evidence['name'], $evidence['text'], $evidence['type'], $evidence['image'], $evidence['description'], $evidence['popup'], $evidence['popup_audio'], $evidence['text_audio']);
      $evidence_mapping[$key] = $new_evidence['item_id'];

      foreach ($evidence['tokens'] as $token_key => $token) {
        argumentations_add_evidence_chunk($new_evidence['item_id'], $token, $evidence['answers'][$token_key], $token_key, $evidence['token_audio'][$token_key], $nid);
      }
    }

    foreach ($issue['chunks'] as $chunk) {
      $new_chunk = argumentations_add_story_chunk($new_nid, $chunk['text'], $chunk['audio_start'], $chunk['audio_end']);

      foreach ($chunk['evidence'] as $chunk_evidence_id => $chunk_evidence_text) {
        if ($chunk['correct'] === $chunk_evidence_id)
          argumentations_link_story_chunk_evidence($new_chunk['chunk_id'], $evidence_mapping[$chunk_evidence_id], $chunk_evidence_text['option'], TRUE, $chunk_evidence_text['audio'], $nid);
        else
          argumentations_link_story_chunk_evidence($new_chunk['chunk_id'], $evidence_mapping[$chunk_evidence_id], $chunk_evidence_text['option'], FALSE, $chunk_evidence_text['audio'], $nid);
      }
    }

    foreach ($issue['claims'] as $claim) {
      $new_claim = argumentations_add_claim($new_nid, $claim['text'], $claim['side'], $claim['example'], $claim['image'], $claim['issue_claim_id'], $claim['audio']);
      $new_claim_id = $new_claim['claim_id'];

      argumentations_add_big_question_options($claim['big_question_options'], $new_claim_id, $nid);

      if (is_array($claim['opening_sentences'])) {
        foreach ($claim['opening_sentences'] as $opening) {
          $type = _am_sentence_opening_type_converter($opening['type']);
          argumentations_add_intro_sentence($new_claim_id, $opening['text'], $opening['bridge'], $type, $opening['text_audio'], $opening['bridge_audio'], $nid);
        }
      }

      if (is_array($claim['closing_sentences'])) {
        foreach ($claim['closing_sentences'] as $closing) {
          $type = _am_sentence_closing_type_converter($closing['type']);
          argumentations_add_closing_sentence($new_claim_id, $closing['text'], $type, $closing['audio'], $nid);
        }
      }

      foreach ($claim['reasons'] as $reason) {
        $new_reason = argumentations_add_reason($new_claim_id, $reason['text'], $reason['example'], $reason['issue_reason_id'], $reason['incorrect_text'], $reason['audio'], $nid);

        foreach ($reason['evidence'] as $reason_evidence_id => $reason_evidence_data) {
          if (isset($evidence_mapping[$reason_evidence_id])) {
            $index = $reason_evidence_data['correct_index'];
            $text = $reason_evidence_data['level_one_text'];
            $start_audio_url = $reason_evidence_data['level_one_start_audio'];
            $end_audio_url = $reason_evidence_data['level_one_end_audio'];
            $level_two_text = $reason_evidence_data['level_two_text'];
            $counter = $evidence_mapping[$reason_evidence_data['counter_item']];

            $reason_item_data = argumentations_link_reason_evidence($new_reason['reason_id'], $evidence_mapping[$reason_evidence_id], $text, $index, $counter, $level_two_text, $start_audio_url, $end_audio_url, $nid);
            if (is_array($reason_evidence_data['rebuttal_opening'])) {
              foreach ($reason_evidence_data['rebuttal_opening'] as $key => $data) {
                argumentations_add_rebuttal_opening($data['start'], $data['end'], $reason_item_data['reason_item_id'], $key, $data['start_audio'], $data['end_audio'], $nid);
              }
            }

            if (is_array($reason_evidence_data['rebuttal_strengthener'])) {
              foreach ($reason_evidence_data['rebuttal_strengthener'] as $key => $data) {
                argumentations_add_rebuttal_strengthener($data['start'], $data['end'], $reason_item_data['reason_item_id'], $key, $data['start_audio'], $data['end_audio'], $nid);
              }
            }

            if (is_array($reason_evidence_data['rebuttal_evidence'])) {
              foreach ($reason_evidence_data['rebuttal_evidence'] as $key => $data) {
                argumentations_add_rebuttal_evidence($data['text'], $reason_item_data['reason_item_id'], $key, $data['audio'], $nid);
              }
            }

            if (is_array($reason_evidence_data['rebuttal_closing'])) {
              foreach ($reason_evidence_data['rebuttal_closing'] as $key => $data) {
                argumentations_add_rebuttal_closing($data['text'], $reason_item_data['reason_item_id'], $key, $data['audio'], $nid);
              }
            }
            
            if (isset($reason_evidence_data['cloze_opening']) && 
                isset($reason_evidence_data['cloze_strengthener']) && 
                isset($reason_evidence_data['cloze_evidence']) && 
                isset($reason_evidence_data['cloze_closing'])) {
              argumentations_add_cloze($reason_item_data['reason_item_id'], $evidence_mapping[$reason_evidence_id], $reason_evidence_data['cloze_opening'], $reason_evidence_data['cloze_strengthener'], $reason_evidence_data['cloze_evidence'], $reason_evidence_data['cloze_closing']);
            }
          }
        }
        
        if (is_array($reason['conclusion_reasons'])) {
          argumentations_add_conclusion_reasons($new_reason['reason_id'], $reason['conclusion_reasons'], $nid);
        }
      }
      
      if (is_array($claim['conclusion_starter'])) {
        argumentations_add_conclusion_starter($new_claim_id, $claim['conclusion_starter']);
      }
    }
  }
}

/********************
 * HELPER FUNCTIONS *
 ********************/

function rrmdir($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (filetype($dir."/".$object) == "dir")
          rrmdir($dir."/".$object); else unlink($dir."/".$object);
      }
    }
    reset($objects);
    rmdir($dir);
  }
}

function am_custom_move_files($pass_source, $pass_destination) {
  // Get array of all source files
  $files = scandir($pass_source);
  
  if (!$files) {
    return FALSE;
  }

  // Identify directories
  $source = "$pass_source/";
  $destination = "$pass_destination/";

  // Cycle through all source files
  foreach ($files as $file) {
    if (in_array($file, array(".",".."))) continue;

    // If we copied this successfully, mark it for deletion
    if (copy($source.$file, $destination.$file)) {
      $delete[] = $source.$file;
    }
  }
  
  // Delete all successfully-copied files
  if (is_array($delete)) {
    foreach ($delete as $file) {
      unlink($file);
    }
  }
}