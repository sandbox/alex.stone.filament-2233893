<?php

/*********************
 * DATA PULL HELPERS *
 *********************/

function _am_services_old_issue_pull($issue, $nid) {
  $passed_issue = array(
      'short_text' => $issue[0]['short_text'],
      'full_text' => $issue[0]['full_text'],
      'title_text' => $issue[0]['title_text'],
  );
  if (!empty($issue[0]['image_url']))
    $passed_issue['image_url'] = url($issue[0]['image_url'], array('absolute' => TRUE));

  $story_chunks = _argumentations_read_story_chunk($nid);

  foreach ($story_chunks as $story_chunk) {
    $correct_answer = NULL;
    $chunk_evidence = _argumentations_read_story_chunk_evidence(NULL, $story_chunk['chunk_id']);

    $passed_chunk_evidence = array();
    foreach ($chunk_evidence as $chunk_item) {
      $passed_chunk_evidence[$chunk_item['item_id']] = $chunk_item['display_text'];
      if ($chunk_item['correct'])
        $correct_answer = $chunk_item['item_id'];
    }

    $passed_issue['story_chunks'][] = array(
      'story_chunk_id' => $story_chunk['chunk_id'],
      'text' => $story_chunk['full_text'],
      'evidence' => $passed_chunk_evidence,
      'correct' => $correct_answer
    );
  }

  $evidence = _argumentations_read_item($nid);
  $count = 0;
  foreach ($evidence as $item_id => $item) {
    $reason_evidence = _argumentations_read_reason_evidence($item_id);
    $passed_evidence_reasons[$reason_evidence[0]['reason_id']][] = (string) $item_id;

    $passed_issue['evidence'][$count] = array(
        'name' => $item['name'],
        'description' => $item['description'],
        'item_id' => (string) $item_id,
        'initial_text' => $reason_evidence[0]['initial_text'],
        'correct' => $reason_evidence[0]['correct_index'] - 1,
        'item_type' => $item['item_type'],
    );
    if (!empty($item['image_url']))
      $passed_issue['evidence'][$count]['image_url'] = url($item['image_url'], array('absolute' => TRUE));

    if (isset($reason_evidence[0]['counter_item_id']))
      $passed_issue['evidence'][$count]['counter'] = $reason_evidence[0]['counter_item_id'];

    $item_chunks[$item_id] = _argumentations_read_item_chunk($item_id);
    foreach ($item_chunks[$item_id] as $item_chunk) {
      $passed_issue['evidence'][$count]['tokens'][] = array(
          'item_text' => $item_chunk['replacement'],
          'sentence_text' => $item_chunk['ending'],
      );
    }
    $count++;
  }

  $claims = _argumentations_read_claim($issue[0]['nid']);
  foreach ($claims as $claim) {
    $reasons = _argumentations_read_reason($claim['claim_id']);
    $passed_reasons = array();

    $reason_count = 0;
    foreach ($reasons as $reason) {
      $passed_reasons[] = array(
          'reason_id' => $reason['issue_reason_id'],
          'reason_text' => $reason['reason_text'],
          'example_closing_sentence' => $reason['example'],
      );

      if (isset($passed_evidence_reasons[$reason['reason_id']]))
        $passed_reasons[$reason_count]['evidence'] = $passed_evidence_reasons[$reason['reason_id']];

      $reason_count++;
    }

    $intro_sentence = _argumentations_read_intro_sentence(NULL, $claim['claim_id']);
    shuffle($intro_sentence);

    $intro_sentences = array();
    $count = 0;
    foreach ($intro_sentence as $sentence) {
      if ($count < 5) {
        $intro_sentences[] = array(
            'hook' => $sentence['display'],
            'bridge' => $sentence['bridge'],
            'type' => $sentence['type'],
        );
      }
      else {
        continue;
      }
      $count++;
    }


    $closing_sentence = _argumentations_read_closing_sentence(NULL, $claim['claim_id']);
    shuffle($closing_sentence);
    
    $closing_sentences = array();
    $count = 0;
    foreach ($closing_sentence as $sentence) {
      if ($count < 5) {
        $closing_sentences[] = array(
            'sentence' => $sentence['display'],
            'type' => $sentence['type'],
        );
      }
      else {
        continue;
      }
      $count++;
    }

    $bq_options = _argumentations_read_big_question_options(NULL, $claim['claim_id']);
    $bq_final_options = array();
    foreach ($bq_options as $bq_option) {
      $bq_final_options[] = $bq_option['option_text'];
    }

    $passed_issue['claims'][(string)$claim['side']] = array(
        'text' => $claim['claim_text'],
        'claim_id' => $claim['issue_claim_id'],
        'reasons' => $passed_reasons,
        'intro_sentence' => $intro_sentences,
        'closing_sentence' => $closing_sentences,
        'example_closing_sentence' => $claim['example'],
        'options' => $bq_final_options,
    );

    if (!empty($claim['image_url']))
      $passed_issue['claims'][(string)$claim['side']]['image_url'] = url($claim['image_url'], array('absolute' => TRUE));
  }
  return $passed_issue;
}

function _am_services_new_issue_pull($issue, $nid) {
  $passed_issue = array(
    'short_text' => $issue[0]['short_text'],
    'full_text' => $issue[0]['full_text'],
    'title_text' => $issue[0]['title_text'],
    'glossary' => url(ARG_MOD_MEDIA_DIR .'/'. $nid .'/Glossary.txt', array('absolute' => TRUE))
  );
  if (!empty($issue[0]['image_url'])) {
    $passed_issue['image_url'] = url($issue[0]['image_url'], array('absolute' => TRUE));
  }
  if (!empty($issue[0]['short_text_audio_url'])) {
    $passed_issue['question_audio_url'] = url($issue[0]['short_text_audio_url'], array('absolute' => TRUE));
  }
  if (!empty($issue[0]['full_text_audio_url'])) {
    $passed_issue['audio_url'] = url($issue[0]['full_text_audio_url'], array('absolute' => TRUE));
  }

  _am_services_data_pull_story_chunk_constructor($nid, $passed_issue);
  
  $passed_evidence_reasons = array();
  _am_services_data_pull_evidence_constructor($nid, $passed_issue, $passed_evidence_reasons);

  _am_services_data_pull_claim_constructor($nid, $passed_issue, $passed_evidence_reasons);
  
  return $passed_issue;
}

function _am_services_data_pull_story_chunk_constructor($nid, &$passed_issue) {
  $story_chunks = _argumentations_read_story_chunk($nid);
  foreach ($story_chunks as $story_chunk) {
    $correct_answer = NULL;
    $chunk_evidence = _argumentations_read_story_chunk_evidence(NULL, $story_chunk['chunk_id']);

    $passed_chunk_evidence = array();
    $passed_chunk_evidence_audio = array();
    foreach ($chunk_evidence as $chunk_item) {
      $passed_chunk_evidence[$chunk_item['item_id']] = $chunk_item['display_text'];
      if ($chunk_item['correct']) {
        $correct_answer = $chunk_item['item_id'];
      }
      
      if (!empty($chunk_item['audio_url'])) {
        $passed_chunk_evidence_audio[$chunk_item['item_id']] = url($chunk_item['audio_url'], array('absolute' => TRUE));
      }
    }

    $passed_story_chunk = array(
      'story_chunk_id' => $story_chunk['chunk_id'],
      'text' => $story_chunk['full_text'],
      'evidence' => $passed_chunk_evidence,
      'correct' => $correct_answer
    );
    
    if (!empty($passed_chunk_evidence_audio)) {
      $passed_story_chunk['evidence_audio'] = $passed_chunk_evidence_audio;
    }
    if (!empty($story_chunk['starting_audio_url'])) {
      $passed_story_chunk['starting_audio_url'] = url($story_chunk['starting_audio_url'], array('absolute' => TRUE));
    }
    if (!empty($story_chunk['closing_audio_url'])) {
      $passed_story_chunk['closing_audio_url'] = url($story_chunk['closing_audio_url'], array('absolute' => TRUE));
    }
    
    $passed_issue['story_chunks'][] = $passed_story_chunk;
  }
}

function _am_services_data_pull_evidence_constructor($nid, &$passed_issue, &$passed_evidence_reasons) {
  $evidence = _argumentations_read_item($nid);
  $count = 0;
  foreach ($evidence as $item_id => $item) {
    $reason_evidence = _argumentations_read_reason_evidence($item_id);
    $passed_evidence_reasons[$reason_evidence[0]['reason_id']][] = (string) $item_id;

    $passed_issue['evidence'][$count] = array(
      'name' => $item['name'],
      'description' => $item['description'],
      'item_id' => (string) $item_id,
      'initial_text' => $reason_evidence[0]['initial_text'],
      'initial_text_start_audio_url' => $reason_evidence[0]['start_audio_url'],
      'initial_text_end_audio_url' => $reason_evidence[0]['end_audio_url'],
      'initial_text_two' => $reason_evidence[0]['initial_text_level_2'],
      'correct' => $reason_evidence[0]['correct_index'] - 1,
      'item_type' => $item['item_type'],
      'subheader' => $item['subheader'],
      'summary' => $item['popup'],
    );
    if (!empty($item['image_url']))
      $passed_issue['evidence'][$count]['image_url'] = url($item['image_url'], array('absolute' => TRUE));
    if (!empty($item['item_audio_url']))
      $passed_issue['evidence'][$count]['audio_url'] = url($item['item_audio_url'], array('absolute' => TRUE));
    if (!empty($item['popup_audio_url']))
      $passed_issue['evidence'][$count]['summary_audio_url'] = url($item['popup_audio_url'], array('absolute' => TRUE));

    if (isset($reason_evidence[0]['counter_item_id']))
      $passed_issue['evidence'][$count]['counter'] = $reason_evidence[0]['counter_item_id'];

    $item_chunks[$item_id] = _argumentations_read_item_chunk($item_id);
    foreach ($item_chunks[$item_id] as $item_chunk) {
      $evidence_token = array(
        'item_text' => $item_chunk['replacement'],
        'sentence_text' => $item_chunk['ending'],
      );
      
      if (!empty($item_chunk['audio_url'])) {
        $evidence_token['audio_url'] = url($item_chunk['audio_url'], array('absolute' => TRUE));
      }
      
      $passed_issue['evidence'][$count]['tokens'][] = $evidence_token;
    }
    $count++;
  }
}

function _am_services_data_pull_claim_constructor($nid, &$passed_issue, $passed_evidence_reasons) {
  $claims = _argumentations_read_claim($nid);
  foreach ($claims as $claim) {
    $passed_reasons = _am_services_data_pull_claim_reason_constructor($claim['claim_id'], $passed_evidence_reasons);

    $intro_sentences = _am_services_data_pull_claim_intro_sentence_constructor($claim['claim_id']);

    $closing_sentences = _am_services_data_pull_claim_closing_sentence_constructor($claim['claim_id']);

    $bq_final_options = _am_services_data_pull_claim_big_question_constructor($claim['claim_id']);
    
    $claim_cloze_restatements = _am_services_data_pull_claim_conclusiont_starters_constructor($claim['claim_id']);

    $passed_issue['claims'][(string)$claim['side']] = array(
        'text' => $claim['claim_text'],
        'claim_id' => $claim['issue_claim_id'],
        'reasons' => $passed_reasons,
        'intro_sentence' => $intro_sentences,
        'closing_sentence' => $closing_sentences,
        'example_closing_sentence' => $claim['example'],
        'options' => $bq_final_options,
        'cloze_restatements' => $claim_cloze_restatements,
    );
    
    if (!empty($claim['audio_url'])) {
      $passed_issue['claims'][(string)$claim['side']]['audio_url'] = url($claim['audio_url'], array('absolute' => TRUE));
    }

    if (!empty($claim['image_url'])) {
      $passed_issue['claims'][(string)$claim['side']]['image_url'] = url($claim['image_url'], array('absolute' => TRUE));
    }
  }
}

function _am_services_data_pull_claim_reason_constructor($claim_id, $passed_evidence_reasons) {
  $reasons = _argumentations_read_reason($claim_id);
  $passed_reasons = array();
  foreach ($reasons as $reason) {
    $reason_cloze_restatements = _am_services_data_pull_claim_reason_conclusion_reason_constructor($reason['reason_id']);

    $passed_reason = array(
      'reason_id' => $reason['issue_reason_id'],
      'reason_text' => $reason['reason_text'],
      'example_closing_sentence' => $reason['example'],
      'mismatch_text' => $reason['incorrect_text'],
      'cloze_restatement' => $reason_cloze_restatements,
      'audio_url' => $reason['audio_url']
    );

    if (isset($passed_evidence_reasons[$reason['reason_id']])) {
      $passed_reason['evidence'] = $passed_evidence_reasons[$reason['reason_id']];
    }
    
    $passed_reasons[] = $passed_reason;
  }
  
  return $passed_reasons;
}

function _am_services_data_pull_claim_reason_conclusion_reason_constructor($reason_id) {
  $conclusion_reasons = _argumentations_read_conclusion_reason(NULL, $reason_id);
  $reason_cloze_restatements = array();
  foreach ($conclusion_reasons as $conclusion_reason) {
    $reason_cloze_item = array(
      'level_one_text' => $conclusion_reason['level_one_reason'],
      'level_two_text' => $conclusion_reason['level_two_reason'],
    );
    if (!empty($conclusion_reason['level_one_audio_url'])) {
      $reason_cloze_item['level_one_audio_url'] = url($conclusion_reason['level_one_audio_url']);
    }
    $reason_cloze_restatements[] = $reason_cloze_item;
  }
  
  return $reason_cloze_restatements;
}

function _am_services_data_pull_claim_intro_sentence_constructor($claim_id) {
  $intro_sentence = _argumentations_read_intro_sentence(NULL, $claim_id);
  shuffle($intro_sentence);
  
  $intro_sentences = array();
  $intro_count = 0;
  foreach ($intro_sentence as $sentence) {
    if ($intro_count < 5) {
      $intro_sentences[] = array(
        'hook' => $sentence['display'],
        'hook_audio_url' => $sentence['display_audio_url'],
        'bridge' => $sentence['bridge'],
        'bridge_audio_url' => $sentence['bridge_audio_url'],
        'type' => $sentence['type']
      );
    }
    else {
      continue;
    }
    $intro_count++;
  }
  
  return $intro_sentences;
}

function _am_services_data_pull_claim_closing_sentence_constructor($claim_id) {
  $closing_sentence = _argumentations_read_closing_sentence(NULL, $claim_id);
  shuffle($closing_sentence);

  $closing_sentences = array();
  $closing_count = 0;
  foreach ($closing_sentence as $sentence) {
    if ($closing_count < 5) {
      $closing_sentences[] = array(
          'sentence' => $sentence['display'],
          'type' => $sentence['type'],
          'audio_url' => $sentence['audio_url'],
      );
    }
    else {
      continue;
    }
    $closing_count++;
  }
  
  return $closing_sentences;
}

function _am_services_data_pull_claim_big_question_constructor($claim_id) {
  $bq_options = _argumentations_read_big_question_options(NULL, $claim_id);
  $bq_final_options = array();
  foreach ($bq_options as $bq_option) {
    $bq_final_option = array(
      'text' => $bq_option['option_text'],
      'audio_url' => $bq_option['audio_url']
    );
    
    $bq_final_options[] = $bq_final_option;
  }
  
  return $bq_final_options;
}

function _am_services_data_pull_claim_conclusiont_starters_constructor($claim_id) {
  $conclusion_starters = _argumentations_read_conclusion_starter(NULL, $claim_id);
  $claim_cloze_restatements = array();
  foreach ($conclusion_starters as $conclusion_starter) {
    $claim_cloze_restatements[] = $conclusion_starter['restated_claim_option'];
  }
  return $claim_cloze_restatements;
}


function _am_services_rebuttal_pull($reason_id) {
  $rebuttal_data = array();
  $reason_items = _argumentations_read_reason_evidence(NULL, $reason_id);
  foreach ($reason_items as $reason_item) {
    $opening = _am_services_data_pull_rebuttal_opening_constructor($reason_item['reason_item_id']);
    $strengthener = _am_services_data_pull_rebuttal_strengthener_constructor($reason_item['reason_item_id']);
    $rebuttal_evidence = _am_services_data_pull_rebuttal_evidence_constructor($reason_item['reason_item_id']);
    $rebuttal_closing = _am_services_data_pull_rebuttal_closing_constructor($reason_item['reason_item_id']);
    $cloze = _am_services_data_pull_cloze_constructor($reason_item['reason_item_id'], $reason_item['item_id']);
    
    $rebuttal_data[] = array(
      'item_id' => $reason_item['item_id'],
      'opening' => $opening,
      'strengthener' => $strengthener,
      'evidence' => $rebuttal_evidence,
      'closing' => $rebuttal_closing,
      'cloze' => $cloze
    );
  }
  
  return $rebuttal_data;
}

function _am_services_data_pull_rebuttal_opening_constructor($reason_item_id) {
  $rebuttal_opening_holder = _argumentations_read_rebuttal_opening($reason_item_id);
  $opening = array();
  foreach ($rebuttal_opening_holder as $opening_data) {
    $opening[] = array(
      'start' => $opening_data['start'], 'start_audio_url' => $opening_data['start_audio_url'], 
      'end' => $opening_data['end'], 'end_audio_url' => $opening_data['end_audio_url']
    );
  }
  
  return $opening;
}

function _am_services_data_pull_rebuttal_strengthener_constructor($reason_item_id) {
  $strengthener = array();
  $rebuttal_strengthener_holder = _argumentations_read_rebuttal_strengthener($reason_item_id);
  foreach ($rebuttal_strengthener_holder as $strengthener_data) {
    $strengthener[] = array(
      'start' => $strengthener_data['start'], 'start_audio_url' => $strengthener_data['start_audio_url'], 
      'end' => $strengthener_data['end'], 'end_audio_url' => $strengthener_data['end_audio_url']
    );
  }
  
  return $strengthener;
}

function _am_services_data_pull_rebuttal_evidence_constructor($reason_item_id) {
  $rebuttal_evidence = array();
  $rebuttal_evidence_holder = _argumentations_read_rebuttal_evidence($reason_item_id);
  foreach ($rebuttal_evidence_holder as $evidence_data) {
    $rebuttal_evidence[] = array(
      'text' => $evidence_data['display'],
      'audio_url' => $evidence_data['audio_url']
    );
  }
  
  return $rebuttal_evidence;
}

function _am_services_data_pull_rebuttal_closing_constructor($reason_item_id) {
  $rebuttal_closing = array();
  $rebuttal_closing_holder = _argumentations_read_rebuttal_closing($reason_item_id);
  foreach ($rebuttal_closing_holder as $closing_data) {
    $rebuttal_closing[] = array(
      'text' => $closing_data['display'],
      'audio_url' => $closing_data['audio_url']
    );
  }
  
  return $rebuttal_closing;
}

function _am_services_data_pull_cloze_constructor($reason_item_id, $item_id) {
  $cloze_sentences = _argumentations_read_cloze(NULL, $reason_item_id, $item_id);
  $cloze_passed = array();
  foreach ($cloze_sentences as $cloze_sentence) {
    $cloze_passed['opening'][] = $cloze_sentence['opening'];
    $cloze_passed['strengthener'][] = $cloze_sentence['strengthener'];
    $cloze_passed['evidence_sentence'][] = $cloze_sentence['evidence_sentence'];
    $cloze_passed['closing'][] = $cloze_sentence['closing'];
  }
  
  return $cloze_passed;
}

/*********************
 * SERVICE CALLBACKS *
 *********************/

function _am_services_issue_data_pull($nid) {
  try {
    module_load_include('inc', 'argumentations', 'includes/crud');

    $issue = _argumentations_read_issue($nid);
    if ((int)$issue[0]['version'] === 1) {
      $passed_issue = _am_services_old_issue_pull($issue, $nid);
    }
    else if ((int)$issue[0]['version'] === 2) {
      $passed_issue = _am_services_new_issue_pull($issue, $nid);
    }

    return $passed_issue;
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_rebuttal_data_pull($issue_reason_id, $issue_id) {
  try {
    module_load_include('inc', 'argumentations', 'includes/crud');
    $reason_id = argumentations_get_reason_id_from_issue_reason_id($issue_id, $issue_reason_id);
    return _am_services_rebuttal_pull($reason_id);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_init($issue_id, $level = NULL, $uid = NULL) {
  try {
    if (TEST_MODE) {
      watchdog('am_watching', 'issue id:!iid uid:!uid', array('!iid' => $issue_id, '!uid' => $uid));
    }

    return argumentations_module_user_init($issue_id, $level, $uid);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_issue_analyzer_save($isid, $claim_id, $percent, $duration, $response = NULL, $correct = NULL, $total = NULL, $wrong = NULL) {
  try {
    if (TEST_MODE) {
      watchdog('am_watching', 'issue session:!isid claim_id:!claim_id percent:!percent duration:!duration response:!response correct:!correct total:!total wrong:!wrong', array('!isid' => $isid, '!claim_id' => $claim_id, '!percent' => $percent, '!duration' => $duration, '!response' => $response, '!correct' => $correct, '!total' => $total, '!wrong' => $wrong));
    }

    return (bool) argumentations_user_save_issue_analyzer($isid, $claim_id, $percent, $duration, $response, $correct, $total, $wrong);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_claim_creator_save($isid, $claim_id, $r_a, $r_b, $r_c, $counter_claim_id, $c_r_a, $c_r_b, $c_r_c, $percent_correct, $duration, $response_claim = NULL, $response_counter_claim = NULL, $response_best_reason = NULL, $correct = NULL, $total = NULL, $wrong = NULL) {
  try {
    if (TEST_MODE) {
      $params = array(
          '!isid' => $isid,
          '!cid' => $claim_id,
          '!r_a' => $r_a,
          '!r_b' => $r_b,
          '!r_c' => $r_c,
          '!ccid' => $counter_claim_id,
          '!c_r_a' => $c_r_a,
          '!c_r_b' => $c_r_b,
          '!c_r_c' => $c_r_c,
          '!duration' => $duration,
          '!percent_correct' => $percent_correct,
          '!response_1' => $response_claim,
          '!response_2' => $response_counter_claim,
          '!response_3' => $response_best_reason,
          '!correct' => $correct,
          '!total' => $total,
          '!wrong' => $wrong
      );
      watchdog('am_watching', 'issue session:!isid | claim_id:!cid (reasons !r_a, !r_b, and !r_c) | counter_claim_id:!ccid (reasons !c_r_a, !c_r_b, !c_r_c) | other: !duration, !percent_correct, !response_1, !response_2, !response_3 | correct:!correct total:!total wrong:!wrong', $params);
    }

    return (bool) argumentations_user_save_claim_creator($isid, $claim_id, $r_a, $r_b, $r_c, $counter_claim_id, $c_r_a, $c_r_b, $c_r_c, $percent_correct, $duration, $response_claim, $response_counter_claim, $response_best_reason, $correct, $total, $wrong);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_introduction_introducer_save($isid, $paragraph, $percent, $edited, $duration, $correct = NULL, $total = NULL, $wrong = NULL) {
  try {
    if (TEST_MODE) {
      watchdog('am_watching', 'edited:!e | correct:!correct total:!total wrong:!wrong', array('!e' => $edited, '!correct' => $correct, '!total' => $total, '!wrong' => $wrong));
//      watchdog('am_watching', 'issue session:!isid paragraph:!p percent:!percent edited:!e duration:!d', array('!isid' => $isid, '!p' => $paragraph, '!percent' => $percent, '!e' => $edited, '!d' => $duration));
    }

    return (bool) argumentations_user_save_introduction_introducer($isid, $paragraph, $percent, $edited, $duration, $correct, $total, $wrong);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_paragraph_constructor_save($isid, $index, $paragraph, $percent, $edited, $duration, $correct = NULL, $total = NULL, $wrong = NULL) {
  try {
    if (TEST_MODE) {
      watchdog('am_watching', 'edited:!e | correct:!correct total:!total wrong:!wrong', array('!e' => $edited, '!correct' => $correct, '!total' => $total, '!wrong' => $wrong));
//      watchdog('am_watching', 'issue session:!isid index:!index paragraph:!p duration:!d', array('!isid' => $isid, '!index' => $index, '!p' => $paragraph, '!d' => $duration));
    }

    return (bool) argumentations_user_save_paragraph_constructor($isid, $paragraph, $index, $percent, $edited, $correct, $total, $wrong);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_critic_crusher_save($isid, $paragraph, $percent, $edited, $duration, $correct = NULL, $total = NULL, $wrong = NULL) {
  try {
    if (TEST_MODE) {
      watchdog('am_watching', 'edited:!e | correct:!correct total:!total wrong:!wrong', array('!e' => $edited, '!correct' => $correct, '!total' => $total, '!wrong' => $wrong));
//      watchdog('am_watching', 'issue session:!isid paragraph:!p duration:!d', array('!isid' => $isid, '!p' => $paragraph, '!d' => $duration));
    }

    return (bool) argumentations_user_save_critic_crusher($isid, $paragraph, $percent, $edited, $duration, $correct, $total, $wrong);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_conclusion_crafter_save($isid, $paragraph, $percent, $edited, $duration, $correct = NULL, $total = NULL, $wrong = NULL) {
  try {
    if (TEST_MODE) {
      watchdog('am_watching', 'edited:!e | correct:!correct total:!total wrong:!wrong', array('!e' => $edited, '!correct' => $correct, '!total' => $total, '!wrong' => $wrong));
//      watchdog('am_watching', 'issue session:!isid paragraph:!p duration:!d', array('!isid' => $isid, '!p' => $paragraph, '!d' => $duration));
    }

    return (bool) argumentations_user_save_conclusion_crafter($isid, $paragraph, $percent, $edited, $duration, $correct, $total, $wrong);
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_set_quick_save($isid, $data, $uid = NULL) {
  try {
    argumentations_set_quick_save($isid, $data, $uid);
    return TRUE;
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_get_quick_save($isid, $uid = NULL) {
  try {
    $ret = argumentations_get_quick_save($isid, $uid);
    if(is_null($ret))
      $ret = 'null';
    
    return $ret;
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_set_student_vote($issue_id, $claim_id, $uid = NULL) {
  try {
    $ret = argumentations_set_student_vote($issue_id, $claim_id, $uid);
    return $ret;
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}

function _am_services_get_class_votes($issue_id, $uid = NULL) {
  try {
    $ret = argumentations_get_class_votes($issue_id, $uid);
    return $ret;
  } catch (Exception $e) {
    return services_error($e->getMessage(), 500);
  }
}