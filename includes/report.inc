<?php

function _drafting_board_user_print($nid, $account_uid, $class_nid = null) {
  $account = user_load($account_uid);
  $name = $account->real_name;
  if (empty($name)) $name = $account->name;
  
  if (!empty($class_nid)) {
    $class = node_load($class_nid);
    $name .= ' ('. $class->title .')';
  }
  
  drupal_set_title($name .'\'s Essay');
  
  drupal_add_css(drupal_get_path('module', 'argumentations') .'/css/argumentations.css', 'module', 'all', TRUE);

  $paragraphs = argumentations_get_user_paragraphs($nid, $account_uid);
  $paragraphs = _argumentations_convert_paragraphs($paragraphs);

  $output = '';

  foreach ($paragraphs as $paragraph) {
    $output .= '<p class="'. $paragraph['class'] .'">'. $paragraph['text'] .'</p>';
  }
	$output = '<div class="essay-header"><h2 class="essay-title">Drafting Board Essay</h2><h3 class="essay-user-name">'. $name .'</h3></div>'. $output;
  return '<div class="tool-recap">'. $output .'</div>';
}

function _drafting_board_user_download($nid, $account_uid, $class_nid) {
  global $user;
  
  $class = node_load($class_nid);
  
  $account = user_load($account_uid);
  drupal_set_title($account->real_name .'\'s Essay');

  $node = node_load($nid);
  
  $name = isset($account->real_name) ? $account->real_name : $account->name;
  $date = date('j F Y');
  $user_info = array($name .' ('. $class->title .')', $node->title);
  
  $paragraphs = argumentations_get_user_paragraphs($nid, $account_uid);
  $paragraphs = _argumentations_convert_paragraphs($paragraphs);
  
  argumentations_create_file($node->title, $user_info, $paragraphs);
}

function _drafting_board_class_evidence_download($nid, $class_nid) {
  global $user;
  
  drupal_set_title($user->real_name .'\'s Class Evidence Data');

  argumentations_load_csv($nid, $class_nid);
}

function _drafting_board_class_essay_download($nid, $class_nid) {
  global $user;
  
  $class = node_load($class_nid);
  $topic = node_load($nid);
  
  drupal_set_title('Class\'s Essay');
  
  $q = 'SELECT base.uid '
    . 'FROM {og_uid} admin '
    . 'LEFT JOIN {og_uid} base ON base.nid = admin.nid AND base.uid != %d '
    . 'LEFT JOIN {am_issue_session} am ON am.uid = base.uid AND am.nid = %d '
    . 'WHERE admin.is_admin = 1 AND admin.uid = %d AND admin.nid = %d '
    . 'ORDER BY base.nid, base.uid';
  $result = db_query($q, $user->uid, $nid, $user->uid, $class_nid);
  $class_paragraphs = array();
  while ($row = db_fetch_array($result)) {
    //loop over this data with all the class students
    $account = user_load($row['uid']);
    $name = isset($account->real_name) ? $account->real_name : $account->name;
    $paragraphs = argumentations_get_user_paragraphs($nid, $row['uid']);
    $user_paragraphs = _argumentations_convert_paragraphs($paragraphs);
    if (!empty($user_paragraphs['crafter']['text'])) {
      $class_paragraphs[] = array(
        'paragraphs' => $user_paragraphs,
        'user_info' => array($name .' ('. $class->title .')', $topic->title),
      );
    }
  }
  
  argumentations_create_class_file($class_paragraphs, $class->title, $topic->title);
}

function _drafting_board_essay_display($arg_nid,  $session_id, $account_uid, $class_nid) { 
  drupal_add_css(drupal_get_path('module', 'argumentations') .'/css/argumentations.css', 'module', 'all', TRUE);
  
  $links = array();
  $links[] = l('Download', 'download/arg-mod/'. $arg_nid .'/'. $account_uid .'/'. $class_nid, array('attributes' => array('class' => 'download button blue')));
  drupal_alter('drafting_board_essay_links', $links, $arg_nid, $account_uid, $class_nid);
  
  $paragraphs = argumentations_get_user_paragraphs($arg_nid, $account_uid);
  $paragraphs = _argumentations_convert_paragraphs($paragraphs);
  
  $output = theme('arg_mod_essay', $paragraphs, $links);
  
  return $output;
}

function _drafting_board_evidence_display($arg_nid, $session_id, $account_uid) {
  $user_evidence = argumentations_get_user_stats($session_id);
  $rows = array();
  foreach ($user_evidence as $id => $data) {
    $rows[] = array(
      'section' => argumentations_get_section_name($id),
      'correct' => $data['correct'],
      'wrong' => $data['wrong'],
      'total' => $data['total'],
    );
  }
  $output = theme('arg_mod_evidence', $rows);
  return $output;
}

function _drafting_board_report($drafting_board_nid, $class_nid = NULL) {
  $class_has_evidence = FALSE;
  $class_has_essay = FALSE;
  
  drupal_add_css(drupal_get_path('module', 'argumentations') .'/css/argumentations.css', 'module', 'all', TRUE);
  drupal_add_js(drupal_get_path('module', 'argumentations') .'/js/draftingboard_report.js', 'module', 'header', FALSE, TRUE, TRUE);
  
  $class_data = _drafting_board_report_get_class_list($drafting_board_nid, $class_nid);
  $classes = $class_data['list'];
  $class_nid = $class_data['nid'];
  
  $class_mgmt_link = 'None';
  $parent_resource_link = 'None';
  
  $student_data = _drafting_board_report_get_class_students($class_nid, $drafting_board_nid, $class_has_essay, $class_has_evidence);
  
  $all_evidence_text = 'Download All Evidence Data';
  $all_evidence = '<span class="button-disabled">'. $all_evidence_text .'</span>';
  if ($class_has_evidence) {
    $all_evidence = l($all_evidence_text, 'download-all-evidence/arg-mod/'. $drafting_board_nid .'/'. $class_nid, array('attributes' => array('class' => 'button blue')));
  }
  
  $all_essays_text = 'Download All Student Essays';
  $all_essays = '<span class="button-disabled">'. $all_essays_text .'</span>';
  if ($class_has_essay) {
    $all_essays = l($all_essays_text, 'download-all-essay/arg-mod/'. $drafting_board_nid .'/'. $class_nid, array('attributes' => array('class' => 'button blue')));
  }
  
  return theme('draftingboard_report', $classes, $class_mgmt_link, $parent_resource_link, $all_evidence, $all_essays, $student_data);
}

function _drafting_board_report_get_class_list($drafting_board_nid, $passed_class_nid = NULL) {
  global $user;
  
  $class_nid = '';
  if (!empty($passed_class_nid)) {
    $class_nid = $passed_class_nid;
    $_SESSION['arg_mod_class_picked'][$drafting_board_nid] = $class_nid;
  }
  elseif (!empty($_SESSION['arg_mod_class_picked'][$drafting_board_nid])) {
    $class_nid = $_SESSION['arg_mod_class_picked'][$drafting_board_nid];
  }
  
  $classes = array();
  foreach ($user->og_groups as $gid => $group) {
    $classes[$gid] = array(
      'title' => $group['title'],
      'attr' => '',
    );
    
    if (empty($class_nid) || (int) $class_nid === (int) $gid) {
      $class_nid = $gid;
      $classes[$gid]['attr'] = 'selected';
    }
  }
  
  return array(
    'nid' => $class_nid,
    'list' => $classes,
  );
}

function _drafting_board_report_get_class_students($class_nid, $arg_mod_nid, &$class_has_essay, &$class_has_evidence) {
  global $user;
  $loaded_users_real_name = array();
  $student_data = array();
  
  $q = 'SELECT admin.nid, base.uid, u.name, am.am_issue_session_id, am.start_date, am.end_date, am.current_section, am.level, session_data.total as evidence_data, am.total_time AS total_time '
    . 'FROM {og_uid} admin '
    . 'LEFT JOIN {og_uid} base ON base.nid = admin.nid AND base.uid != %d '
    . 'LEFT JOIN {users} u ON u.uid = base.uid '
    . 'LEFT JOIN {am_issue_session} am ON am.uid = base.uid AND am.nid = %d '
    . 'LEFT JOIN {am_issue_session_data} session_data ON session_data.am_issue_session_id = am.am_issue_session_id '
    . 'WHERE admin.is_admin = 1 AND admin.uid = %d AND admin.nid = %d '
    . 'GROUP BY base.nid, u.uid '
    . 'ORDER BY base.nid, base.uid';
  
  $result = db_query($q, $user->uid, $arg_mod_nid, $user->uid, $class_nid);
  while ($row = db_fetch_array($result)) {
    //load defaults for the base class items
    if (!isset($student_data['data'])) {
      $student_data['data']['has_students'] = FALSE;
    }
    
    //store user variables just incase we have one user in two different classes
    if (!isset($loaded_users_real_name[$row['uid']])) {
      $loaded_users_real_name[$row['uid']] = module_invoke_all('get_real_name', $row['uid']);
      if (empty($loaded_users_real_name[$row['uid']])) {
        $loaded_users_real_name[$row['uid']] = $row['name'];
      }
    }
    
    //default user vars
    $session_id = empty($row['am_issue_session_id']) ? 0 : $row['am_issue_session_id'];
    $current_module = NULL;
    $item_1 = NULL;
    $item_2 = NULL;
    $item_3 = NULL;
    $item_4 = NULL;
    $item_5 = NULL;
    $item_6 = NULL;
    $bar_class = NULL;
    
    //if there is a student present handle setting up the data to pass to the page
    if ($row['uid']) {
      //update class items to reflect that there are students in the class
      $student_data['data']['has_students'] = TRUE;
      
      //check which section the student is in
      switch ($row['current_section']) {
        case SECTION_CLOSING:
          $current_module = 'Essay Complete';
          $item_6 = 'progress-item item-6 complete';
          $bar_class = 'progress-bar completed';
        case SECTION_INTRO:
          if (is_null($current_module))
            $current_module = 'Closing';
          if (is_null($item_6))
            $item_6 = 'progress-item item-6 active';
        case SECTION_CRITIC_CRUSHER:
          if (is_null($current_module))
            $current_module = 'Intro';
          $item_5 = 'progress-item item-5 active';
          if (!empty($item_6))
            $item_5 = 'progress-item item-5 complete';
        case SECTION_PARAGRAPH_CON_3:
          if (is_null($current_module))
            $current_module = 'Critic Crusher';
          if (is_null($item_4))
            $item_4 = 'progress-item item-4 active';
          if (!empty($item_5))
            $item_4 = 'progress-item item-4 complete';
        case SECTION_PARAGRAPH_CON_2:
        case SECTION_PARAGRAPH_CON_1:
        case SECTION_CLAIM_CREATOR:
          if (is_null($current_module))
            $current_module = 'Paragraph Constructor';
          $item_3 = 'progress-item item-3 active';
          if (!empty($item_4))
            $item_3 = 'progress-item item-3 complete';

        case SECTION_ISSUE_ANALYZER:
          if (is_null($current_module))
            $current_module = 'Claim Creator';
          $item_2 = 'progress-item item-2 active';
          if (!empty($item_3))
            $item_2 = 'progress-item item-2 complete';

          $item_1 = 'progress-item item-1 complete';
          if (empty($bar_class))
            $bar_class = 'progress-bar in-progress';
      }

      if (!is_null($row['current_section']) && (int) $row['current_section'] === (int) 0) {
        if (is_null($current_module))
          $current_module = 'Issue Analyzer';
        $item_1 = 'progress-item item-1 active';
        if (!empty($item_2))
          $item_1 = 'progress-item item-1 complete';

        if (empty($bar_class))
          $bar_class = 'progress-bar in-progress';
      }
      
      //get whether there are paragrahs to display or not to set the button either be disabled or active
      $paragraphs = argumentations_get_user_paragraphs($arg_mod_nid, $row['uid']);
      if (empty($paragraphs['crafter'])) {
        $essay_button = '<span class="button-disabled" title="Not Started">View Essay</span>';
      }
      else {
        $class_has_essay = TRUE;
        $essay_button = l('View Essay', 'arg-mod/essay-display/'. $arg_mod_nid .'/'. $session_id .'/'. $row['uid'] .'/'. $class_nid, array(
        	'attributes' => array(
        		'class' => 'button blue popups',
        		'on-popups-options' => '{additionalCss: "db-report-modal.css"}',
            'popupClass' => 'essayoptions',
            'target' => '_blank',
        		)
        	)
        );
      }
      
      if (empty($row['evidence_data'])) {
        $evidence_button = '<span class="button-disabled" title="Not Started">View Data</span>';
      }
      else {
        $class_has_evidence = TRUE;
        $evidence_button = l('View Data', 'arg-mod/evidence-display/'. $arg_mod_nid .'/'. $session_id .'/'. $row['uid'], array(
          'attributes' => array(
            'class' => 'button green popups',
            'on-popups-options' => '{additionalCss: "db-report-modal"}',
            'popupClass' => 'evidencematching',
            'target' => '_blank',
            )
          )
        );
      }
      
      //passed data structured
      $student_data['data']['students'][$row['uid']] = array(
        'name' => $loaded_users_real_name[$row['uid']],
        'session_id' => $row['am_issue_session_id'],
        'start_date' => $row['start_date'],
        'end_date' => $row['end_date'],
        'total_time' => _drafting_board_report_format_time($row['total_time']),
        'current_section' => $row['current_section'],
        'current_section_title' => $current_module,
        'level' => empty($row['level']) ? NULL : $row['level'],
        'bar_class' => $bar_class,
        'evidence_button' => $evidence_button,
        'essay_button' => $essay_button,
        'item_1' => $item_1,
        'item_2' => $item_2,
        'item_3' => $item_3,
        'item_4' => $item_4,
        'item_5' => $item_5,
        'item_6' => $item_6,
      );
    }
  }
  $student_data['data']['scaffold_link'] = l('?', 'draftingboard/scaffolding', array('attributes' => array('class' => 'scaffolding-link button green popups', 'title' => 'Scaffolding Level Description')));
  
  return $student_data;
}

function _drafting_board_report_format_time($total_time) {
  $formatted_time = null;
  
  $hours = intval(intval($total_time) / 3600);
  if ($hours > 0) {
    $formatted_time .= $hours .'h';
  }
  
  $minutes = bcmod((intval($total_time) / 60), 60);
  if ($minutes > 0) {
    $formatted_time .= $minutes .'m';
  }
  
  $seconds = sprintf("%02d", bcmod(intval($seconds), 60));
  
  return $formatted_time;
}